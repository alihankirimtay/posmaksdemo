// Keyboard Language
// please update this section to match this language and email me with corrections!
// tr = ISO 639-1 code for Turkish
// ***********************
jQuery.keyboard.language.tr = {
	language: 'T\u00fcrk\u00e7e (Turkish)',
	display : {
		'a'      : '\u2714:Tamam (Shift+Enter)', // check mark - same action as accept
		'accept' : 'Tamam:Tamam (Shift+Enter)',
		'alt'    : 'AltGr:Alternate Graphemes',
		'b'      : '\u2190:Geri Al',    // Left arrow (same as &larr;)
		'bksp'   : 'Geri Al:Geri Al',
		'c'      : '\u2716:İptal (Esc)', // big X, close - same action as cancel
		'cancel' : 'İptal:İptal (Esc)',
		'clear'  : 'C:Temizle',             // clear num pad
		'combo'  : '\u00f6:Toggle Combo Keys',
		'dec'    : '.:Decimal',           // decimal point for num pad (optional), change '.' to ',' for European format
		'e'      : '\u21b5:Enter',        // down, then left arrow - enter symbol
		'enter'  : 'Enter:Enter',
		'lock'   : '\u21ea Lock:Caps Lock', // caps lock
		's'      : '\u21e7:Shift',        // thick hollow up arrow
		'shift'  : 'Shift:Shift',
		'sign'   : '\u00b1:Change Sign',  // +/- sign for num pad
		'space'  : '&nbsp;:Space',
		't'      : '\u21e5:Tab',          // right arrow to bar (used since this virtual keyboard works with one directional tabs)
		'tab'    : '\u21e5 Tab:Tab'       // \u21b9 is the true tab symbol (left & right arrows)
	},
	// Message added to the key title while hovering, if the mousewheel plugin exists
	wheelMessage : 'Diğer tuşları görmek için fare tekerleğini kullanın',
};
