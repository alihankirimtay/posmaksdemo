var MyScript = {

	init: function () {
		MyScript.setBrowserLanguage();
		MyScript.momentJs();
	},

	setBrowserLanguage: function () {
		MyScript.browser_language = window.navigator.userLanguage || window.navigator.language;
	},

	getBrowserLanguage: function () {
		MyScript.browser_language;
	},

	momentJs: function (container) {

		if (typeof container !== "undefined") {
			container = $("[momentjs]", container);
		} else {
			container = $("[momentjs]");
		}

		$.each(container, function(index, val) {

			let
			$this = $(this),
			datetime = $this.attr("datetime"),
			action = $this.attr("momentjs");
			format = $this.attr("format") || "L LT";

			if (action === "utc2local") {
				$this.html(MyScript.Helpers.utc2local(datetime, format));
				$this.removeAttr("momentjs");
			} else if (action === "utc2TimeAgo") {
				$this.html(MyScript.Helpers.utc2TimeAgo(datetime));
				$this.removeAttr("momentjs");
			}

		});
	},

	Helpers: {
		utc2local: function (datetime, format) {
			return moment
			.utc(datetime, "YYYY-MM-DD HH:mm:ss")
			.local()
			.locale(MyScript.browser_language)
			.format(format);
		},

		utc2TimeAgo: function (datetime) {
			let momentObj = moment
			.utc(datetime, "YYYY-MM-DD HH:mm:ss")
			.local();

			return moment(momentObj).fromNow();
		},
	}
};

MyScript.init();