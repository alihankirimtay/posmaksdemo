var Zones = {
    floors: function() {
        $(".location").on("change", function() {



            $.post(base_url + 'floors/ajax/getFloorsByLocationId', {
                location_id: $(this).val()
            }, function(json) {
                data = $.parseJSON(json);
                floor_list = $(".floor");
                floor_list.html("");
                if (data.result) {
                    $.each(data.data.floors, function(index, val) {
                        floor_list.append($("<option></option>").val(val.id).html(val.title));
                    });
                }
                floor_list.trigger("chosen:updated");
            });

            $.post(base_url + 'zones/ajax/getUsersByLocation', {
                location_id: $(this).val()
            },  function(json) {
                data = $.parseJSON(json);
                user_list = $(".user-zone");
                user_list.html("");
                if (data.result) {
                    $.each(data.data.users, function(index, val) {
                        user_list.append($("<option></option>").val(val.id).html(val.username));
                    });
                }
                user_list.trigger("chosen:updated");

            });
        });
    },

    init: function() {
        Zones.floors();
    }
}