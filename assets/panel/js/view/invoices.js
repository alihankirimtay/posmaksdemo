var Invoices = {

    components: {
        // Müşteri Bilgileri
        "comp-customerName" : {
            type: "static-text",
            title: "Müşteri Ünvanı",
            template: "Müşteri Ünvanı/Adı Tasarım Reklam LTD. ŞTİ.",
            icon: "building-o",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "330px",
            height: "30px",
            configs: {}
        },
        "comp-customerAddress" : {
            type: "static-text",
            title: "Müşteri Adresi",
            template: "Müşteri Adresi Herhangi Mah. Ornek Sok. Güzel Apt. No:5 Daire:7 34435", 
            icon: "envelope",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "300px",
            height: "50px",
            configs: {}
        },
        "comp-customerPhone" : {
            type: "static-text",
            title: "Müşteri Telefon Numarası",
            template: "0555 555 55 55", 
            icon: "phone",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "140px",
            height: "30px",
            configs: {}
        },
        "comp-customerTaxOfficeNo" : {
            type: "static-text",
            title: "Vergi Bilgileri ( Daire ve Numara )",
            template: "Beyoğlu V.D. 1234567890", 
            icon: "legal",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-customerTaxOfficeName" : {
            type: "static-text",
            title: "Vergi Dairesi",
            template: "Beyoğlu V.D.", 
            icon: "legal",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-customerTaxNo" : {
            type: "static-text",
            title: "Vergi Numarası",
            template: "1234567890", 
            icon: "legal",
            multiple: false,
            onClick: false,
            container: "#list-customer",
            width: "200px",
            height: "20px",
            configs: {}
        },
        // "comp-customerBalance" : {
        //     type: "static-text",
        //     title: "Müşteri Bakiyesi",
        //     template: "10.000,00₺ Borç<br>3.000,00$ Alacak", 
        //     icon: "building",
        //     multiple: false,
        //     onClick: false,
        //     container: "#list-customer",
        //     width: "200px",
        //     height: "50px",
        //     configs: {}
        // },
        // Fatura Bilgileri
        "comp-invoiceDateOfIssue" : {
            type: "static-text",
            title: "Düzenlenme Tarihi",
            template: "10.12.2014", 
            icon: "calendar",
            multiple: false,
            onClick: false,
            container: "#list-invoice",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-invoiceId" : {
            type: "static-text",
            title: "Fatura No",
            template: "1234567890", 
            icon: "file-text-o",
            multiple: false,
            onClick: false,
            container: "#list-invoice",
            width: "200px",
            height: "20px",
            configs: {}
        },
        // "comp-invoiceDispatchNoteNo" : {
        //     type: "static-text",
        //     title: "İrsaliye No",
        //     template: "1234567890", 
        //     icon: "truck",
        //     multiple: false,
        //     onClick: false,
        //     container: "#list-invoice",
        //     width: "200px",
        //     height: "20px",
        //     configs: {}
        // },
        "comp-invoicePaymentDate" : {
            type: "static-text",
            title: "Ödeme Tarihi",
            template: "10.12.2014",
            icon: "calendar",
            multiple: false,
            onClick: false,
            container: "#list-invoice",
            width: "200px",
            height: "20px",
            configs: {}
        },
        // Hizmet/Ürün Bilgileri
        "comp-productList" : {
            type: "data-table",
            title: "Hizmet/Ürün Kalemleri",
            template: "",
            icon: "list-alt",
            multiple: false,
            onClick: "Invoices.configsProductsTable",
            container: "#list-product",
            width: "700px",
            height: "378px",
            configs: {
                showFieldTitles: true,
                fields: {
                    id: {
                        title: "Ürün Kodu",
                        value: "#000",
                        active: true
                    },
                    title: {
                        title: "Hizmet/Ürün Bilgileri",
                        value: "Hizmet veya Ürün",
                        active: true
                    },
                    quantity: {
                        title: "Adet",
                        value: "1",
                        active: true
                    },
                    price: {
                        title: "Fiyat",
                        value: "1.000,00",
                        active: true
                    },
                    tax: {
                        title: "Vergi",
                        value: "%18",
                        active: true
                    },
                    total: {
                        title: "Toplam",
                        value: "1.000,00",
                        active: true
                    },
                }
            }
        },
        // Toplam Miktar Bilgileri
        "comp-amountTotal" : {
            type: "static-text",
            title: "Genel Toplam",
            template: "123.456.789,00P",
            icon: "plus",
            multiple: false,
            onClick: false,
            container: "#list-amount",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-amountSubTotal" : {
            type: "static-text",
            title: "Ara Toplam",
            template: "123.456.789,00P",
            icon: "plus",
            multiple: false,
            onClick: false,
            container: "#list-amount",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-amountDiscount" : {
            type: "static-text",
            title: "Toplam İndirim",
            template: "123.456.789,00P",
            icon: "minus",
            multiple: false,
            onClick: false,
            container: "#list-amount",
            width: "200px",
            height: "20px",
            configs: {}
        },
        "comp-amountTotalTax" : {
            type: "static-text",
            title: "Toplam Vergi",
            template: "123.456.789,00P",
            icon: "institution",
            multiple: false,
            onClick: false,
            container: "#list-amount",
            width: "200px",
            height: "20px",
            configs: {}
        },
        // Özel Notlar
        "comp-noteText" : {
            type: "editable-text",
            title: "Yazdırmaya Özel Not",
            template: "Yazdırmaya Özel Not",
            icon: "pencil-square-o",
            multiple: true,
            onClick: "Invoices.configsTextNote",
            container: "#list-note",
            width: "200px",
            height: "20px",
            configs: {
                text: ""
            }
        },
        "comp-noteTextArea" : {
            type: "editable-text",
            title: "Metin Alanı",
            template: "Metin Alanı",
            icon: "pencil",
            multiple: true,
            onClick: "Invoices.configsTextNote",
            container: "#list-note",
            width: "200px",
            height: "20px",
            configs: {
                text: ""
            }
        },
    },
    pageComponents: {},
    activePageComponent: false,
    pageW,
    pageH,

    initEditor: function(pageComponents , width ,height) {

        $('#pageSize').on('change' , Invoices.setPageSize);
        $('#customSizeApply').on('click' , Invoices.setCustomPageSize);

        if (typeof pageComponents !== "undefined") {
            Invoices.pageComponents = JSON.parse(pageComponents);
        }

        if(width != 0 && height != 0){

            Invoices.pageW = width + "cm";
            Invoices.pageH = height + "cm"

            if(width == 14.8 && height == 21){
                $('#pageSize').val(1).trigger("change");
                console.log("A5");
            } else if (width == 21 && height == 29.7)
            {
                $('#pageSize').val(0).trigger("change");
            } else {
                $('#pageSize').val(2).trigger("change");
            }
        } else {
            Invoices.pageW = "21cm";
            Invoices.pageH = "29.7cm";
        }

        $('#invoice-page').css({"width" : Invoices.pageW , "height" : Invoices.pageH});

        Invoices.handleActivePageComponent();
        Invoices.handleDeletePageComponent();
        Invoices.loadDraggableComponents();
        Invoices.loadDraggablePageComponents();
        Invoices.handleDroppableZone();
        Invoices.handleSaveInvoice();


        
    },

    

    setPageSize : function()
    {
        var element = $('#pageSize').find('option:selected');
        var target = $('#invoice-page');
        var type = element.val();


        $('#special').hide();


        if(type == 2) {
            $('#pageW').val(Invoices.pageW.replace("cm" , ""));
            $('#pageH').val(Invoices.pageH.replace("cm" , ""));
            $('#special').show();

        } else {

            Invoices.pageW = element.data('width');
            Invoices.pageH = element.data('height');

            target.css({"width" : Invoices.pageW , "height":Invoices.pageH});

        }


    },

    setCustomPageSize : function()
    {
        var target = $('#invoice-page');

        Invoices.pageW = $('#pageW').val() + "cm";
        Invoices.pageH = $('#pageH').val() + "cm";

        target.css({"width" : Invoices.pageW , "height":Invoices.pageH});
    },



    getComponentsAsJson: function () {
        return JSON.stringify(Invoices.pageComponents);  
    },

    handleSaveInvoice: function () {
        $("body").on("click", ".submit-invoice", function() {

            // Fill the json input
            $("input[name=json]").val(Invoices.getComponentsAsJson());

            let 
            buton = $(this),
            url = buton.data("url"),
            form = buton.closest("form"),
            formData = form.serializeArray();

            formData.push({name: "width" , value : Invoices.pageW.replace("cm" , "")});
            formData.push({name: "height" , value : Invoices.pageH.replace("cm" , "")});
           

            $.post(url, formData, function(json) {

                if (json.result) {
                    Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
                } else {
                    Pleasure.handleToastrSettings('Hata!', json.message, 'error');
                }
                
            }, "json");

        });
    },

    handleActivePageComponent: function () {
        $("body").on("click", "#invoice-page .component", function(){
            Invoices.setActivePageComponent($(this));  
        });  
    },

    handleDeletePageComponent: function () {

        $("body").on("click", "#invoice-page .component-delete", function(){

            let
            element = $(this).closest(".component"),
            id = $("a", element).attr("id"),
            index = element.data("index");

            element.remove();
            delete Invoices.pageComponents[index];

            $(`#invoice-components a[id="${id}"]`).closest(".component").removeClass("hidden");
        });  
    },

    loadDraggableComponents: function () {
        
        let container = $("#invoice-components");

        $.each(Invoices.components, function (id, component) {

            Invoices.createDraggableComponent(id, component);

        });

    },

    createDraggableComponent: function (id, component) {

        let item = $(`
            <li class="list-group-item p-a-0 p-l-1 component">
                <i class="fa fa-${component.icon}"></i>
                <i class="fa fa-times component-delete"></i>
                <a id="${id}">${component.title}</a>
            </li>
        `);

        if (component.multiple === false) {
            $.each(Invoices.pageComponents, function(index, pageComponent) {
                if (pageComponent.id === id) {
                    item.addClass("hidden");
                }
            });
        }

        item
        .draggable({
            containment: '#invoice-wrapper',
            stack: '#invoice-components .component',
            cursor: 'move',
            revert: 'invalid',
            opacity: 0.4,
        })
        .appendTo(`${component.container} .list-group`);    
    },

    loadDraggablePageComponents: function () {

        $.each(Invoices.pageComponents, function(index, pageComponent) {


            let
            component = Invoices.components[pageComponent.id],
            item = $(`
                <li class="list-group-item p-a-0 p-l-1 component dropped-component" data-index="${index}">
                    <i class="fa fa-times component-delete"></i>
                    <a id="${pageComponent.id}">${component.template}</a>
                </li>
            `);

            // if it has own settings then handle it.
            if (component.onClick !== false) {
                let element = `#invoice-page .component[data-index="${index}"]`;
                $("body").on("click", element, eval(component.onClick));
            }

            pageComponent.x = pageComponent.x - pageComponent.x%5;
            pageComponent.y = pageComponent.y - pageComponent.y%5;

            item
            .css({
                left: pageComponent.x,
                top: pageComponent.y,
                width: pageComponent.w,
                height: pageComponent.h,
            })
            .draggable({
                containment: '#invoice-page',
                stack: '#invoice-page .component',
                cursor: 'move',
                revert: 'invalid',
                opacity: 0.4,
                grid: [5, 5],
                stop: function (event, ui) {
                    let
                    index = $(event.target).data("index"),
                    x = ui.position.left,
                    y = ui.position.top;

                    Invoices.pageComponents[index].x = x;
                    Invoices.pageComponents[index].y = y;
                }
            })
            .resizable({
                containment: '#invoice-page',
                grid: 5,
                stop: function(event, ui) {
                    let
                    index = $(event.target).data("index"),
                    w = ui.size.width,
                    h = ui.size.height;

                    Invoices.pageComponents[index].w = w;
                    Invoices.pageComponents[index].h = h;
                }
            })
            .appendTo($("#invoice-page"))
            .trigger("click");
        });

    },

    handleDroppableZone: function () {

        $("#invoice-page").droppable({
            accept: '#invoice-components .component, #invoice-page .component',
            drop: function(event, ui) {

                let
                index = + new Date(),
                droppedItem = ui.helper.clone(),
                id = $("a", droppedItem).attr("id"),
                w = parseFloat(ui.helper.width()),
                h = parseFloat(ui.helper.height()),
                component = Invoices.components[id];

                if (droppedItem.hasClass("dropped-component")) {

                    index = droppedItem.data("index");
                    let
                    x = parseInt($(this).offset().left),
                    y = parseInt($(this).offset().top);

                    Invoices.pageComponents[index].x = x;
                    Invoices.pageComponents[index].y = y;

                } else {

                    let
                    x = parseInt(ui.offset.left - $(this).offset().left),
                    y = parseInt(ui.offset.top - $(this).offset().top);
                    x = x - x%5;
                    y = y - y%5;

                    Invoices.pageComponents[index] = {
                        id: id,
                        x: x,
                        y: y,
                        w: component.width,
                        h: component.height,
                        configs: component.configs,
                    };

                    $("a", droppedItem).html(component.template);
                    $(".fa:first", droppedItem).remove();

                    droppedItem
                    .addClass("dropped-component")
                    .attr("data-index", index)
                    .css({
                        left: x,
                        top: y,
                        opacity: 1,
                        width: component.width,
                        height: component.height,
                    })
                    .appendTo($(this));


                    // Send back the dragged item.
                    ui.helper.draggable({revert:true});

                    // if it is not multiple then hide.
                    if (component.multiple === false) {
                        ui.helper.addClass("hidden");
                    }

                    // if it has own settings then handle it.
                    if (component.onClick !== false) {
                        let element = `#invoice-page .component[data-index="${index}"]`;
                        $("body").on("click", element, eval(component.onClick));
                    }

                    
                    droppedItem
                    .draggable({
                        containment: '#invoice-page',
                        stack: '#invoice-page .component',
                        cursor: 'move',
                        revert: 'invalid',
                        opacity: 0.4,
                        grid: [5, 5],
                        stop: function (event, ui) {
                            let
                            index = $(event.target).data("index"),
                            x = ui.position.left,
                            y = ui.position.top;

                            Invoices.pageComponents[index].x = x;
                            Invoices.pageComponents[index].y = y;
                        }
                    })
                    .resizable({
                        containment: '#invoice-page',
                        grid: 5,
                        stop: function(event, ui) {
                            let
                            index = $(event.target).data("index"),
                            w = ui.size.width,
                            h = ui.size.height;

                            Invoices.pageComponents[index].w = w;
                            Invoices.pageComponents[index].h = h;
                        }
                    })
                    .trigger("click");
                }
            }
        });
    },

    setActivePageComponent: function (element) {

        Invoices.activePageComponent = element;

        $.each($("#invoice-page .component"), function(index, val) {
            $(this).removeClass("component-active");
        });

        $("#invoice-component-configs").html("");

        Invoices.activePageComponent.addClass("component-active");
    },

    // Editable text function
    configsTextNote: function (e) {

        let
        element = $(this),
        index = element.data("index"),
        id = $("a", element).attr("id"),
        configs,
        text;
    
        if (typeof Invoices.pageComponents[index] === "undefined")
            return;

        configs = Invoices.pageComponents[index].configs;
        component = Invoices.components[id];

        if (Invoices.activePageComponent === false)
            return;
        if (Invoices.activePageComponent.attr("id") !== element.attr("id"))
            return;

        text = configs.text.length > 0 ? configs.text : component.template;
            
        $("#invoice-component-configs").html("").append(`
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Yazdırmaya Özel Not</h3>
                </div>
                <div class="panel-body">
                    <form role="form" data-index="${index}">                        
                        <div class="form-group">
                            <textarea class="form-control _noteText" cols="20" rows="10" placeholder="Notunuz...">${text}</textarea>
                        </div>
                    </form>
                </div>
            </div>
        `);


        $("body").on("input change", `form[data-index="${index}"] ._noteText`, function(){
            let
            form = $(this).closest("form"),
            index = form.data('index'),
            text = $("textarea", form).val();

            Invoices.pageComponents[index].configs.text = text;
            $(`#invoice-page .component[data-index="${index}"] > a`).html(text);
        });

        $(`form[data-index="${index}"] ._noteText`).trigger("change");
    },

    // Editable table function
    configsProductsTable: function (e) {
        
        let
        element = $(this),
        index = element.data("index"),
        id = $("a", element).attr("id"),
        configs,
        component,
        configHtml = "";
    
        if (typeof Invoices.pageComponents[index] === "undefined")
            return;

        configs = Invoices.pageComponents[index].configs;
        component = Invoices.components[id];

        if (Invoices.activePageComponent === false)
            return;
        if (Invoices.activePageComponent.attr("id") !== element.attr("id"))
            return;

        configHtml += `
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="_productFieldSettings" data-setting="showFieldTitles" checked> Başlıkları göster
                </label>
            </div>
        `;

        configHtml += "<div class='well'>";
        $.each(component.configs.fields, function(field_id, field) {

            if (typeof configs.fields[field_id] === "undefined")
                return;

            let checked = (configs.fields[field_id].active ? "checked" : ""); 

            configHtml += `
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="_productFields" data-field="${field_id}" ${checked}> ${configs.fields[field_id].title}
                    </label>
                </div>
            `;
        });
        configHtml += "</div>";

        $("#invoice-component-configs").html("").append(`
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Hizmet / Ürün Bilgileri</h3>
                </div>
                <div class="panel-body">
                    <form role="form" data-index="${index}">                        
                        ${configHtml}
                    </form>
                </div>
            </div>
        `);

        // Listen to the field checkboxes
        $("body").on("change", `form[data-index="${index}"] ._productFields`, function(e) {

            let
            element = $(this),
            id = $("a", element).attr("id"),
            field_id = element.data("field"),
            isChecked = element.is(":checked"),
            form = $(this).closest("form"),
            index = form.data('index'),
            configs,
            component,
            table = "", 
            row = "",
            head = "";

            if (typeof Invoices.pageComponents[index].configs.fields[field_id] === "undefined")
                return;

            configs = Invoices.pageComponents[index].configs;
            component = Invoices.components[id];

            configs.fields[field_id].active = isChecked;

            $.each(configs.fields, function(index, field) {
                if (field.active) {
                    head += `<th>${field.title}</th>`;
                    row += `<td>${field.value}</td>`;
                }
            });

            if (configs.showFieldTitles !== true) {
                head = "";
            }
            row = `<tr>${row}</tr>`;

            for (var i = 0; i <= 4; i++)
                row += row;
            table = `<table class="table table-bordered"><thead><tr>${head}</tr></thead><tbody>${row}</tbody></table>`;

            $(`#invoice-page .component[data-index="${index}"] > a`).html(table);
        });

        // Listen to the table settings
        $("body").on("change", `form[data-index="${index}"] ._productFieldSettings`, function(e){
            let
            element = $(this),
            setting = element.data("setting"),
            isChecked = element.is(":checked"),
            form = $(this).closest("form"),
            index = form.data('index');

            if (typeof Invoices.pageComponents[index].configs[setting] === "undefined")
                return;

            Invoices.pageComponents[index].configs[setting] = isChecked;
            $(`form[data-index="${index}"] ._productFields:first`).trigger("change");
        });

        $(`form[data-index="${index}"] ._productFields:first`).trigger("change");
    },

};
