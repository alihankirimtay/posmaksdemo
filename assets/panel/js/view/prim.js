var Prim = {

  init: function() {
    
  	this.show_users();

  },

  show_users: function(){
  

  	$("body").on('change', 'select[name=l]', function() {

  		var form = $(this).closest("form").serializeArray();
        form.push({ name: 'event', value: 'users' });


  		$.post(base_url+'reports/ajax/users', form, function(json) {

  			$('select[name=garsonFilter]').html("").trigger("chosen:updated");

  			$.each(json.data, function(index, val) {
  				$('select[name=garsonFilter]').append('<option value="'+val.id+'">\
                                                 '+val.username+'\
                                           </option>');
  			});

  			$('select[name=garsonFilter]').trigger("chosen:updated");
                 

        }, "json");
  	});

  	$('select[name=l]').trigger('change');
  	

  }

}