var Roles = {

};



Roles.Permissions = {
	init: function () {
		Roles.Permissions.Events.init();
	},

	Events: {
		init: function () {
			$("body").on("click", ".check-all", Roles.Permissions.Events.onClickCheckAll);
		},

		onClickCheckAll: function () {

			let
            role_id = $(this).data("role_id"),
            isChecked = $(this).is(":checked");
            
            $(`input[name^="roles[${role_id}]"]`).prop("checked", isChecked).iCheck("update");
		},
	},
};