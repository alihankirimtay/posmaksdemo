on_click_order: function(){
 $('#hiddenInputs').html("");  

 var data = '';
 var i = 0;
 var amount= 0;
 var tax = 0;
 var sumTax = 0;


 // $('#tblProduct tbody tr').each(function(){   
 //   //ürün adetleri
 //   data = $(this).find('td:nth-child(1)').html().trim();    
 //   $('#hiddenInputs').append('<input type="hidden" name="product_quantity_'+i+'" value="'+data+'">');
 //   //ürün IDleri
 //   data = $(this).find('td:nth-child(4) > i').attr('id').substring(12);
 //   $('#hiddenInputs').append('<input type="hidden" name="product_id_'+i+'" value="'+data+'">');
 //   //vergi hesabı
 //   tax = (parseFloat($(this).find('td:nth-child(3)').html().trim().slice(0, -1))  * parseFloat($(this).find('input[type=hidden]').val()))/100;
 //   sumTax+=tax;



 //   amount += parseFloat($(this).find('td:nth-child(3)').html().trim().slice(0, -1)) + tax;

 //   i++;
 // });

 var tableId = $('#spanTableName').attr('data-table-id');
 $('#hiddenInputs').append('<input type="hidden" name="table_id" value="'+tableId+'">');
 $('#hiddenInputs').append('<input type="hidden" name="subtotal" value="'+$('#spanPrice').html().slice(0, -1)+'">');
 $('#hiddenInputs').append('<input type="hidden" name="count" value="'+i+'">');
 $('#hiddenInputs').append('<input type="hidden" name="amount" value="'+amount+'">');
 $('#hiddenInputs').append('<input type="hidden" name="sumTax" value="'+sumTax+'">');
 $('#hiddenInputs').append('<input type="hidden" name="is_order" value="1">');

   console.log( $('#pos').serialize());
     

 $.post(base_url+'mobilpos/ajax/setOrder', $('#pos').serialize(),
   function(json) {
     alert(json.data[1]);
     // $('#tblProduct tbody tr').each(function(){ 
     //   $(this).find('td:nth-child(1)').prepend('<i class="fa fa-check"></i>');
     // });
     $('#tablePrice-'+tableId).html(Math.round(json.data[0]).toFixed(2)+"₺");
     $('#tablePrice-'+tableId).parents('.table-image').css('background', '#e74c3c');
     // $('#subtotalCheck').text(parseFloat(json.data[2]).toFixed(2)+'₺');
     // $('#sumCheck').text(parseFloat(json.data[0]).toFixed(2)+'₺');
     // $('#kdv').text(sumTax.toFixed(2)+'₺');

     
   },"json");

},







on_click_order_complete : function() {

    var tableId = $('#spanTableName').attr('data-table-id');
    $('#hiddenInputs').append('<input type="hidden" name="tableId" value="'+tableId+'">');


    $.post(base_url+'mobilpos/ajax/getComplete', $('#pos').serialize(),
   
    function(json) {

      $('#subtotalCheck').text(json.data['subtotal'] +'₺');
      $('#kdv').text(json.data['tax'].toFixed(2)+'₺');
      $('#sumCheck').text(json.data['amount']+'₺');

     
   },"json");

    function payment (pay,inputName) {
      $('input[name='+inputName+']').val((parseFloat($('input[name='+inputName+']').val()) + parseFloat(pay)).toFixed(2));
      $('#sumAmount').html((parseFloat($('#sumAmount').html()) + parseFloat(pay)).toFixed(2));
    }

    function paymentRemove (pay,inputName) {
      $('input[name='+inputName+']').val((parseFloat($('input[name='+inputName+']').val()) - parseFloat(pay)).toFixed(2));
      $('#sumAmount').html((parseFloat($('#sumAmount').html()) - parseFloat(pay)).toFixed(2));
      $('input[name='+inputName+']').val("0.00");
    }

    function posChangeCalc(inputName) {
      posChange = $('#posChange').text().slice(0,-1);
      //console.log(posChange);
      inputVal =  $('input[name='+inputName+']').val();
      //console.log(inputVal);
      change = parseFloat(inputVal)+parseFloat(posChange);
      // console.log(change.toFixed(2));
      $('#posChange').text(change.toFixed(2));
    }

    function paymentCalc () {
     var sumCheck = $('#sumCheck').text().slice(0,-1)
     var sumAmount = $('#sumAmount').text();
     var rest = 0.00;
     var change = 0.00;

     if(sumCheck >= sumAmount) {
      rest = sumCheck - sumAmount;
      rest = roll_number(rest);

      $('#posRest').text(rest+'₺');
     } else {
      $('#posRest').text("0.00₺");
      change = -1 * (sumAmount - sumCheck);
      change = roll_number(change);
      $('#posChange').text(change+'₺');

     }
    }

    var voucher = 0;
    var addVoucher = 0;
    var inputName = '';

    $('#pos input[name=temp-voucher]').val("0.00");
    $('#pos input[name=temp-ticket]').val("0.00");
    $('#pos input[name=temp-sodexo]').val("0.00");
    $('#pos input[name=temp-credit]').val("0.00");
    $('#pos input[name=temp-cash]').val("0.00");
    $('#sumAmount').html("0.00");


    $('#checkScreen').click();

    $('#backOrderCheckTab').click(function() {

      $('#orderCheckTab').click();

    });

    $('#backHomePage1').click(function() {
      $('#home').click();
    });



    $('#add-voucher').click(function() {
      
      voucher = parseFloat($('input[name=add-voucher]').val());
      addVoucher = parseFloat($('input[name=temp-voucher]').val());
      if ($('input[name=add-voucher]').val().length < 1) {return;}
      $('#pos input[name=add-voucher]').val("");
      payment(voucher,'temp-voucher');
      
      paymentCalc();
     


    });

    $('#add-ticket').click(function() {
      
      voucher = parseFloat($('input[name=add-ticket]').val());
      addVoucher = parseFloat($('#hiddenInputs input[name=ticket]').val());
      if ($('input[name=add-ticket]').val().length < 1) {return;}
      $('#pos input[name=add-ticket]').val("");
      payment(voucher,'temp-ticket');

      paymentCalc();
      

    });

    $('#add-sodexo').click(function() {
      
      voucher = parseFloat($('input[name=add-sodexo]').val());
      addVoucher = parseFloat($('#hiddenInputs input[name=sodexo]').val());
      if ($('input[name=add-sodexo]').val().length < 1) {return;}
      $('#pos input[name=add-sodexo]').val("");
      payment(voucher,'temp-sodexo');
      paymentCalc();
      

    });

   $('#add-credit').click(function() {
      
      voucher = parseFloat($('input[name=add-credit]').val());
      addVoucher = parseFloat($('#hiddenInputs input[name=credit]').val());
      if ($('input[name=add-credit]').val().length < 1) {return;}
      $('#pos input[name=add-credit]').val("");
      payment(voucher,'temp-credit');
      paymentCalc();
      

    });

    $('#add-cash').click(function() {
      
      voucher = parseFloat($('input[name=add-cash]').val());
      addVoucher = parseFloat($('#hiddenInputs input[name=cash]').val());
      if ($('input[name=add-cash]').val().length < 1) {return;}
      $('#pos input[name=add-cash]').val("");
      payment(voucher,'temp-cash');
      paymentCalc();
      

    });

    $('#btnVoucher').click(function() {
      posChangeCalc('temp-voucher');
      paymentRemove($('input[name=temp-voucher]').val(), 'temp-voucher');
    });

    $('#btnTicket').click(function() {
      paymentRemove($('input[name=temp-ticket]').val(), 'temp-ticket');
    });

    $('#btnSodexo').click(function() {
      paymentRemove($('input[name=temp-sodexo]').val(), 'temp-sodexo');
    });

    $('#btnCredit').click(function() {
      paymentRemove($('input[name=temp-credit]').val(), 'temp-credit');
    });

    $('#btnCash').click(function() {
      paymentRemove($('input[name=temp-cash]').val(), 'temp-cash');
    });

  },





  on_click_interim_payment: function() {

      var sumTotal = 0.0;

      $('#interimPaymentScreen').click();

      //Ödemesi yapılmayan siparişlerin ödemesi yapılacaklara aktarılması

      $('#interimPayment tr').click(function(event) {
        
      var payable_basket = $("#interimPayable tbody");
      unpaid_row = $(this);
      payable_clone = unpaid_row.clone();
      data_id = unpaid_row.data("id");
      quantity = unpaid_row.find('td:nth-child(1)').html();

      if (quantity < 1) {
        return false;
      }

      unpaid_row.find("td.quantity").html(quantity - 1);
      unpaid_row.find('td:nth-child(1)').html(quantity - 1);


      var total = parseFloat(unpaid_row.find('td.total').text().slice(0,-1));

      total = roll_number(total);

      sumTotal += parseFloat(total);

      $('#interimSumPayment').text(sumTotal.toFixed(2) + '₺');

      var payable_row = $("tr[data-id=" + data_id + "]", payable_basket);
      if (!payable_row.length) {

        payable_clone.find("td.quantity").html(1);
        payable_clone.find('td:nth-child(1)').html(1);

        payable_basket.append(payable_clone);

      } else {

        val = payable_row.find('td:nth-child(1)').html();
        val = parseInt(val);

        payable_row.find("td.quantity").html(val + 1);
        payable_row.find('td:nth-child(1)').html(val + 1);

      }

      $("tr.interim_payments-info", payable_basket).removeClass("hidden");
      if ($("tr:not(tr.interim_payments-info)", payable_basket).length) {
        $("tr.interim_payments-info", payable_basket).addClass("hidden");
      }

      // Ödemesi yapılacak siparişlerin geri alınması
 


      });

        var payable_basket = "#interimPayable tbody";
        
        $("body").on("click", payable_basket + " tr:not(tr.interim_payments-info)", function(){
          var unpaid_basket = $("#interimPayment tbody");
          payable_row = $(this);
          data_id = payable_row.data("id");
          quantity = payable_row.find('td:nth-child(1)').html();
          quantity = parseInt(quantity);
          unpaid_row = $("tr[data-id=" + data_id + "]", unpaid_basket);


          unpaid_val = unpaid_row.find('td:nth-child(1)').html();
          unpaid_val = parseInt(unpaid_val);

          var total = parseFloat(unpaid_row.find('td.total').text().slice(0,-1));

          total = roll_number(total);

          sumTotal -= parseFloat(total);

      $('#interimSumPayment').text(sumTotal.toFixed(2) + '₺');

          unpaid_row.find("td.quantity").html(unpaid_val + 1);
          unpaid_row.find('td:nth-child(1)').html(unpaid_val + 1);

          if (quantity <= 1) {

            payable_row.remove();

          } else {

            payable_row.find("td.quantity").html(quantity - 1);
            payable_row.find('td:nth-child(1)').html(quantity - 1);
          }

          $(payable_basket + " tr.interim_payments-info").removeClass("hidden");
          if ($(payable_basket + " tr:not(tr.interim_payments-info)").length) {
            $(payable_basket + " tr.interim_payments-info").addClass("hidden");
          }

        });

        $('#interimPayable tbody').html("");
        $('#interimSumPayment').text('0.00₺');

        $('#backHomeInterimPayment').click(function(event) {
          $('#home').click();
        });

        
},



 on_click_order_pos_complete: function() {

    // kalan para üstünü hesapla   
    // kontrolleri yap 


    var tableId = $('#spanTableName').attr('data-table-id');
    // var sumAmount = $('#sumAmount').text();
    // var change = $('#posChange').text();
    $('#hiddenInputs').append('<input type="hidden" name="tableId" value="'+tableId+'">');
    // $('#hiddenInputs').append('<input type="hidden" name="sum-amount" value="'+sumAmount+'">');
    // $('#hiddenInputs').append('<input type="hidden" name="change" value="'+change+'">');
    $('input[name=is_order]').val('0');

    $.post(base_url+'mobilpos/ajax/setOrder', $(posmobil.posForm).serializeArray(),
   
    function(json) {

    if(json.result) {

      Pleasure.handleToastrSettings('Tebrikler!', json.message, 'success');

      $('#tablePrice-'+tableId).html("0.00₺");
      $('#tablePrice-'+tableId).parents('.table-image').css('background', 'white');
      $('#subtotalCheck').html("0.00₺");
      $('#kdv').html("0.00₺");
      $('#sumCheck').html("0.00₺");
      $('#subtotal').html("0.00₺");
      $('#sumAmount').html("0.00₺");
      $('#posRest').html("0.00₺");
      $('#posChange').html("0.00₺");
      $('input[name=temp-voucher]').val(0.00);
      $('input[name=temp-cash]').val(0.00);
      $('input[name=temp-credit]').val(0.00);
      $('input[name=temp-sodexo]').val(0.00);
      $('input[name=temp-ticket]').val(0.00);
      setTimeout(
      function() 
      {
        $('#home').click();
      }, 500);
      

    } else {

      Pleasure.handleToastrSettings('Hata!', json.message, 'error');

    }   

    
    },"json");


    
  },