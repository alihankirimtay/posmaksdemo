var Stocks = {

  countCriticalStocks: function () {

    $.post(base_url + "stocks/ajax", {target: "countCriticalStocks"}, function(json) {

      if (json.result === 1) {

        var menu = $("#stockAlarms"),
        text = menu.html();

        menu.html(text + '<span class="label label-danger pull-right">'+json.data.count+'</span>');
      }

    }, "json");
  },

  
};