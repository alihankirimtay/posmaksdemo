var Products = {

    configs: {
        editMode: false
    },

    init: function (configs) {

        if (typeof configs !== "undefined") {
            $.extend(Products.configs, configs);
        }
        
        $("input[name=type]").on("change", Products.onChangeType);
        $("select[name=location_id]").on("change", Products.onChangeLocation);
        $(".stocks").on("change", Products.onSelectStock);
        $(".price-with-tax, .price-without-tax").on("change input", Products.calculateTaxPrice);
        $("select[name=tax]").on("change", Products.triggerPriceChange);
        $("body").on("change", 'input[name*="status"]', Products.onChangeActive);
        $('#product-passive, #product-active').on("click", Products.onClickAllProductsActive);
        $('body').on('click', '#quicknotesmodal #saveQuickNotes', Products.onClickAddQuickNotes);
        $('#addStock').on('click', Products.onClickStockModal);
        $('body').on('click', '#saveQuickStock', Products.onClickAddStock);

        if (Products.configs.editMode === true) {
            $("select[name=location_id]").trigger("change");
            $(".price-without-tax").trigger('change');
            $("input[name=type]:checked").trigger('change');
        }

    },

    clearAddStockFormInputs: function (form) {

        $('input[name=location_id]', form).val(""); 
        $('input[name=title]', form).val("");
        $('select[name=unit]', form).val("").trigger("chosen:updated");
        $('input[name=sub_limit]', form).val(""); 

    },

    onClickAddQuickNotes: function () {
        let
        btn = $(this),
        form = btn.closest('form');

        Products.Helpers.postForm(form, "quicknotes/ajax/add", function (json) {
            if (json.result === 1) {
                $('#quicknotesmodal').modal('toggle');
                $("#quicknotesmodal input[name='title']").val("");

                $("select[name='quick_notes[]']")
                .append(Products.Templates.quickNotesRow(json.data.quick_note))
                .trigger("chosen:updated");

                Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
            } else {
                Pleasure.handleToastrSettings("Hata!", json.message, "error");                    
            }
        });
    },

    onClickStockModal: function () {
        let location = $("select[name=location_id]").val();
        if (location) {
            $("#addStockModal").modal('toggle');
            $("#addStockModal input[name=location_id]").val(location);
        } else {
            Pleasure.handleToastrSettings("Hata!", "Restorant seçmelisiniz", "error");
        }
    },

    onClickAddStock: function () {
        let
            btn = $(this),
            form = btn.closest('form');

        Products.Helpers.postForm(form, "stocks/add", function (json) {
            if (json.result === 1) {
                let id = json.data.id,
                    location_id = $("select[name=location_id]").val(),
                    title = $("#addStockModal input[name='title']").val(),
                    unit = $("#addStockModal select[name='unit'] option:selected").text(),
                    option = $('<option></option>').html(title).val(id).data('location_id', location_id).data('unit', unit); 

                $('.stocks').append(option).trigger("chosen:updated");
                Products.clearAddStockFormInputs(form);

                $('#addStockModal').modal('toggle');

                Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
            } else {
                Pleasure.handleToastrSettings("Hata!", json.message, "error");
            }
        });
    },

    onChangeActive: function () {

        let product_id = $(this).data("id"),
        val = $(this).val();

        $.post(base_url + '/products/ajax/productActive', {product_id: product_id, value : val}, function(json) {

            if(json.result) {
                Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
            } else {    
                Pleasure.handleToastrSettings('Hata!', json.message, 'error');
            }
        },"json");
    },

    onClickAllProductsActive: function () {

        let 
        rows = $('#product-table').DataTable().rows().data(),
        ids = [],
        button = $(this).attr('id'),
        status = "passive";

        $.each(rows, function(index, val) {

          let tr = $('input[name*=status]:checked', $(val[4]));
          ids.push(tr.attr('data-id'));
        });

        if (button === "product-active") {
            status = "active";
        }

        $.post(base_url + `/products/ajax/AllProductActiveOrPassive?status=${status}`, {id : ids}, function(json) {

            if(json.result) {
                Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
                location.reload();
            } else {    
                Pleasure.handleToastrSettings('Hata!', json.message, 'error');
            }
        });

    },

    onChangeType: function () {
        
        $("#products-container, #stocks-container").removeClass("hidden");

        $(this).val() == 'product'
            ? $("#products-container").addClass("hidden")
            : $("#stocks-container").addClass("hidden");
    },

    onChangeLocation: function () {
        
        let
        location_id = parseInt($(this).val()),
        prodcuts = $("select[name='products[]']"),
        stocks = $(".stocks"),
        additional_products = $("select[name='additional_products[]']"),
        portion_products = $("select[name='portion_products[]']");

        $("option", prodcuts).removeClass("hidden");
        $("option", stocks).removeClass("hidden");
        $("option", additional_products).removeClass("hidden");
        $("option", portion_products).removeClass("hidden");

        if (Products.configs.editMode === false) {
            $("option", prodcuts).prop('selected', false);
            $("option", stocks).prop('selected', false);
            $("option", additional_products).prop('selected', false);
            $("option", portion_products).prop('selected', false);
        }

        // Products
        $.each($("option", prodcuts), function(index, option) {
            let data_location_id = parseInt($(option).data("location_id"));
            if (data_location_id !== location_id)
                $(option).addClass("hidden");
        });

        // Stocks
        $.each($("option", stocks), function(index, option) {
            let data_location_id = parseInt($(option).data("location_id"));
            if (data_location_id !== location_id)
                $(option).addClass("hidden");
        });

        // Additional products
        $.each($("option", additional_products), function(index, option) {
            let data_location_id = parseInt($(option).data("location_id"));
            if (data_location_id !== location_id)
                $(option).addClass("hidden");
        });

        // Portion products
        $.each($("option", portion_products), function(index, option) {
            let data_location_id = parseInt($(option).data("location_id"));
            if (data_location_id !== location_id)
                $(option).addClass("hidden");
        });

        prodcuts.trigger("chosen:updated");
        stocks.trigger("chosen:updated");
        additional_products.trigger("chosen:updated");
        portion_products.trigger("chosen:updated");
    },

    onSelectStock: function (e, params) {

        if (params.selected !== undefined) {

            let
            id = params.selected,
            stock = $(`.stocks option[value=${id}]`),
            title = stock.text(),
            unit = stock.data('unit');

            $("#stocks-container").append(`
                <div class="form-group stock-values" id="stockContainer${id}">
                    <label class="control-label col-sm-3">${title}</label>
                    <div class="col-sm-3">
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input type="number" name="stocks[${id}][quantity]" class="form-control" step="0.01" value="0.00" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    <label class="col-sm-1"><b>${unit}</b></label>
                    <label class="col-sm-3">
                        <div class="checkboxer">
                            <input type="checkbox" name="stocks[${id}][optional]" value="1" id="stockCheckbox${id}">
                            <label for="stockCheckbox${id}">İsteğe bağlı siparişten çıkarılabilir</label>
                        </div> 
                    </label>
                </div>`);

        } else {
            let id = params.deselected;
            $(`#stocks-container #stockContainer${id}`).remove();
        }
    },

    calculateTaxPrice: function() {
        
        let val = parseFloat($(this).val()) || 0,
            tax = parseFloat($('select[name=tax] option:selected').val()),
            total = 0;
        if ($(this).hasClass('price-without-tax')) {

            total = val + (val*tax/100);
            total = Products.round(total,2);

            // total = addTax(val, tax);
            $('.price-with-tax').val(total);
            
        } else if($(this).hasClass('price-with-tax')) {
            total = val / (1 + tax / 100);
            total = Products.round(total,3);
            $('.price-without-tax').val(total);
        }

    },

    triggerPriceChange: function() {
        $(".price-without-tax").trigger('change');
    },

    round: function (value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        // return Number(Math.floor(value+'e'+decimals)+'e-'+decimals);
    },

    Templates: {
        quickNotesRow: function (quickNotes) {
            return `
                <option value="${quickNotes.id}" selected="selected">
                    ${quickNotes.title}
                </option>          
            `;
        }
    }, 
};


Products.Helpers = {

    postForm: function (form, url, callback) {
        $.post(base_url + url, $(form).serializeArray(), function (json) {
            callback(json);
        }, "json");
    },

};