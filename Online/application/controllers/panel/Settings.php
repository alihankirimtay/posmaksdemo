<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Panel {

	public function __construct()
	{
        parent::__construct();
        $this->load->model("backend/discounts_model");
        $this->load->model("backend/settings_model");
        $this->load->model("backend/payment_type_model");
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

    }

    public function index()
	{
        $discounts = $this->discounts_model->index();
        
        $payment_type = $this->payment_type_model->index();
        $payment_type_pos = $this->Posmaks->getPayment_Types();

        $logo = $this->settings_model->getLogo();
        $settings = $this->settings_model->index();
		
		$this->load->view("backend/settings",compact('discounts','payment_type','payment_type_pos','logo','settings'));
    }
    

    public function update()
    {
    	try
    	{

	    	$settings_id = $this->input->post('settings_id');
	    	$location_brand_name = $this->input->post('location_brand_name');
	    	$maps_api_key = $this->input->post('maps_api_key');
		    $this->settings_model->update($settings_id,$maps_api_key,$location_brand_name);


	        $discount_ids = (Array) $this->input->post('discount_id[]');

	        foreach($discount_ids as $id){
	            $which_discount =(int)$this->input->post("which_discount[{$id}]");
	            $discount_rate =(int)$this->input->post("discount_rate[{$id}]");
	            $active = $this->input->post("active[{$id}]") ? 1:0;

	            $data = array(
	            	'which_discount' => $which_discount,
	                'discount_rate'  => $discount_rate,
	                'active' 		 => $active
	            );

	            $this->db->where('id',$id)->update("{$this->database_name}discounts",$data);
	        }



	        $add_count = (Array) $this->input->post('add_count[]');
	        if(count($add_count) > 0)
	        {
	        	foreach ($add_count as $add_c) {
		        	$add_which_discount = (int)$this->input->post("add_which_discount[{$add_c}]");
			        $add_discount_rate = (int)$this->input->post("add_discount_rate[{$add_c}]");
			        $add_active = $this->input->post("add_active[{$add_c}]") ? 1:0;

			        // if($add_which_discount == null || $add_discount_rate == null)
			        // {
			        // 	throw new Exception("Promosyonlar Kısmında Boş Alanlar Var");
			        // }

			        $add_data = array(
			        	'which_discount' => $add_which_discount,
			        	'title'			 => "Sipariş İndirimi",
		                'discount_rate'  => $add_discount_rate,
		                'active' 		 => $add_active
			        );

			        $this->db->insert("{$this->database_name}discounts",$add_data);
		        }
	        }

	       

	        


	        $payment_type = $this->payment_type_model->index();
	        $payment_ids = $this->input->post('payment_id[]');
	        
	        if(count($payment_type) >= 1)
	        {
		        foreach($payment_ids as $id){
		            $active = $this->input->post("active_payment[{$id}]") ? 1:0;

		            $data = array(
		                'active' => $active
		            );

		            $this->db->where('id',$id)->update("{$this->database_name}payment_type",$data);
		        }
	        }
	        else
	        {
	        	foreach($payment_ids as $id){
		            $active = $this->input->post("active_payment[{$id}]") ? 1:0;

		            $data = array(
		            	'id' => $id,
		                'active' => $active
		            );

		            $this->db->insert("{$this->database_name}payment_type",$data);
		        }
	        }

        echo json_encode([
			'status' => 'success',
			'message' => 'Kayıt başarılı',
		]);
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
        

    }
    
    public function ImagesAdd()
    {
    	try
    	{

	        if (!isset($_FILES['image']['name'])) {
				exit(json_encode([
					'status' => 'error',
					'message' => 'dosya gerekli',
				]));
			}

			$path = 'assets/uploads/logo/';
			@mkdir(FCPATH . $path, 0755, true);

			$config = [
				'encrypt_name'  => true,
				'upload_path'   => FCPATH . $path,
				'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
			];

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('image')){
				exit(json_encode([
					'status' => 'error',
					'message' => $this->upload->display_errors(),
				]));
			}
			$image = $path . $this->upload->data('file_name');

			

			if (isset($_FILES['bodyimage']['name'])) 
			{
				$body_config = [
				'encrypt_name'  => true,
				'upload_path'   => FCPATH . $path,
				'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
				];

				$this->load->library('upload', $body_config);
				if (!$this->upload->do_upload('bodyimage')){
					$body_image = "";
				}
				else
				{
					$body_image = $path . $this->upload->data('file_name');				
				}
				
			}
			else
			{
				$body_image = "";
			}


	        $this->settings_model->add($image,$body_image);
			
			

			echo json_encode([
				'status' => 'success',
				'message' => 'Kayıt Başarılı',
				]);
			}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
    }

    public function logoUpdate()
    {
    	try
    	{
	        if (!isset($_FILES['image']['name'])) {
            	throw new Exception("dosya gerekli");
			}

			$path = 'assets/uploads/logo/';
			@mkdir(FCPATH . $path, 0755, true);

			$config = [
				'encrypt_name'  => true,
				'upload_path'   => FCPATH . $path,
				'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
			];

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('image')){
            	throw new Exception($this->upload->display_errors());
			}

			$image = $path . $this->upload->data('file_name');
	       
		    @unlink($this->website->image);
	        $this->settings_model->logo_update($image);
	        
	    
		
			echo json_encode([
			'status' => 'success',
			'message' => 'Kayıt Başarılı',
			]);
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
    }

    public function BodyImageUpdate()
    {
    	try
    	{
	        if (!isset($_FILES['bodyimage']['name'])) {
            	throw new Exception("dosya gerekli");
			}

			$path = 'assets/uploads/logo/';
			@mkdir(FCPATH . $path, 0755, true);

			$config = [
				'encrypt_name'  => true,
				'upload_path'   => FCPATH . $path,
				'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
			];

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('bodyimage')){
            	throw new Exception($this->upload->display_errors());
			}

			$image = $path . $this->upload->data('file_name');

	        @unlink($this->website->bodyimage);
	        $this->settings_model->bodyimage_update($image);
		
			echo json_encode([
			'status' => 'success',
			'message' => 'Kayıt Başarılı',
			]);
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
    }


    public function deleteLogo()
	{
		$img_src = $this->website->image;
		@unlink($img_src);
		$this->settings_model->deleteLogo();
		
	}
	 public function deleteBodyImage()
	{
		$img_src = $this->website->bodyimage;
		@unlink($img_src);
		$this->settings_model->deleteBodyImage();
	}

	public function delete_Discount()
	{
		$discount_id = $this->input->post("discount_id");
		$deleted_time = date('Y-m-d H:i:s');

		$delete = $this->discounts_model->delete($discount_id,$deleted_time);
	}
}