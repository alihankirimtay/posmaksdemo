<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slidemanagement extends Panel {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("backend/slide_model");
	}

	public function index()
	{
		$images = $this->slide_model->getImages();
		$this->load->view("backend/slideManagement",compact('images'));
	}

	public function process()
	{
		try
    	{

	        if (!isset($_FILES['image']['name'])) {
				exit(json_encode([
					'status' => 'error',
					'message' => 'dosya gerekli',
				]));
			}

			$path = 'assets/uploads/slides/';
			@mkdir(FCPATH . $path, 0755, true);

			$config = [
				'encrypt_name'  => true,
				'upload_path'   => FCPATH . $path,
				'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
			];

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('image')){
				exit(json_encode([
					'status' => 'error',
					'message' => $this->upload->display_errors(),
				]));
			}

			$image = $path . $this->upload->data('file_name');

			$data = array(
				"image" => $image
	        );

	        $this->slide_model->add($data);
			
			

			echo json_encode([
				'status' => 'success',
				'message' => 'Kayıt Başarılı',
				]);
			}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
		
	}
	public function updateImage()
	{
		try
    	{

		    $img_id = (Array) $this->input->post('slide_image_id[]');

			foreach($img_id as $id){
	            $img_src = $this->input->post("slide_image_src[{$id}]");
	            $active = $this->input->post("active[{$id}]") ? 1:0;

	            $data = array(
	                'image'  => $img_src,
	                'active' => $active
	            );

	            $this->db->where('id',$id)->update("{$this->database_name}slide_image",$data);
	        }
	        
        echo json_encode([
				'status' => 'success',
				'message' => 'Kayıt Başarılı',
				]);
			}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
	}

	public function deleteImage()
	{
		$img_src = $this->input->post("img_src");
		$img_id = $this->input->post("img_id");
		
		$this->slide_model->delete($img_id);
		@unlink($img_src);
	}
}