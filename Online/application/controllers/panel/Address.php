<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Panel {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/ilce_table_model");
		$this->load->model("backend/mahalle_table_model");
		$this->load->model("backend/restaurant_sehir_model");
		$this->load->model("backend/restaurant_ilce_model");
		$this->load->model("backend/restaurant_mahalle_model");
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    }

    public function get_sehirler()
    {
		$sehirler = $this->sehir_table_model->sehir();
	    echo json_encode($sehirler);
    }

    public function get_ilceler()
	{
		$sehir_key = $this->input->post('sehir_key');
		$ilceler = $this->ilce_table_model->ilce($sehir_key);
	    echo json_encode($ilceler);
	}

	public function get_mahalleler()
	{
		$ilce_key = $this->input->post('ilce_key');
		$mahalleler = $this->mahalle_table_model->mahalle($ilce_key);
		echo json_encode($mahalleler);
	}
	
	public function get_ilce_title()
	{
		$ilce_key = $this->input->post('ilce_key');
		$ilce_title = $this->ilce_table_model->ilce_title($ilce_key);
		echo json_encode($ilce_title);
	}
    


    public function Edit_Restaurant_Ilce()
    {
        $ilce = $this->restaurant_ilce_model->ilce($restaurant_id);
    }

    public function Edir_Restaurant_Mahalle()
    {
        $mahalle = $this->restaurant_mahalle_model->mahalle($restaurant_id);
    }

    public function locationAddress()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$sehir = $this->restaurant_sehir_model->sehir($restaurant_id);
		$ilce = $this->restaurant_ilce_model->ilce($restaurant_id); 
		$mahalle = $this->restaurant_mahalle_model->mahalle($restaurant_id);

		$data = array(
			'sehir' => $sehir,
			'ilce' => $ilce,
			'mahalle' => $mahalle
		);
		echo json_encode($data);
	}
}
