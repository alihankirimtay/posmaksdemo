<?php


class Settings_model extends CI_Model
{
    public function index()
    {
        $logo = $this->db->get("{$this->database_name}settings");
        return $logo->result();
    }

    public function getLogo()
    {
        $logo = $this->db->get("{$this->database_name}settings");
		return $logo->result();
    }

    public function add($image,$bodyimage)
    {
        $this->db->set('image', $image);
        $this->db->set('bodyimage', $bodyimage);
        $this->db->update("{$this->database_name}settings");
    }

    public function update($id,$api_key,$location_brand_name)
    {
        $this->db->set('api_key',$api_key);
        $this->db->set('restaurant_name',$location_brand_name);
        $this->db->where('id', $id);
        $this->db->update("{$this->database_name}settings");
    }

    public function logo_update($image)
    {
        $this->db->set('image', $image);
        $this->db->update("{$this->database_name}settings");
    }

    public function bodyimage_update($image)
    {
        $this->db->set('bodyimage', $image);
        $this->db->update("{$this->database_name}settings");
    }
    
    public function deleteLogo()
    {
        $this->db->set('image', '');
        $this->db->update("{$this->database_name}settings");
    }
    
    public function deleteBodyImage()
    {
        $this->db->set('bodyimage', '');
        $this->db->update("{$this->database_name}settings");
    }

    public function delete($id)
    {
        $delete =  $this->db->delete("{$this->database_name}settings",array('id' => $id));
    }

    


}