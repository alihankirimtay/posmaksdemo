<?php


class Restaurant_ilce_model extends CI_Model
{
	public function ilce($restaurant_id)
	{
		$this->db->select('*');
		$this->db->from("{$this->database_name}ilce");
		$this->db->join("{$this->database_name}restaurants_ilce", "{$this->database_name}restaurants_ilce.ilce_id = {$this->database_name}ilce.ilce_id");
		$this->db->where("restaurant_id",$restaurant_id);
		$ilce=$this->db->get();

		return $ilce->result();
	}
	
	
	
}

