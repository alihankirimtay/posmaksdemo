<?php


class Mahalle_table_model extends CI_Model
{
	public function mahalle($ilce_key)
	{
		$mahalle = $this->db->get_where("{$this->database_name}mahalle",array('mahalle_ilcekey' => $ilce_key));
		return $mahalle->result();
	}
	
}