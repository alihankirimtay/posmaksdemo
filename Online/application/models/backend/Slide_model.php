<?php


class Slide_model extends CI_Model
{

    public function getImages()
    {
        $images = $this->db->get("{$this->database_name}slide_image");
		return $images->result();
    }

    public function add($data)
    {
        $this->db->insert("{$this->database_name}slide_image",$data);
    }

    public function delete($id)
    {
        $delete =  $this->db->delete("{$this->database_name}slide_image",array('id' => $id));
    }

}