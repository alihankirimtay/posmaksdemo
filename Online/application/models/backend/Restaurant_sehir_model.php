<?php


class Restaurant_sehir_model extends CI_Model
{
	public function sehir($restaurant_id)
	{
        $this->db->select('*');
		$this->db->from("{$this->database_name}sehir");
		$this->db->join("{$this->database_name}restaurants_sehir", "{$this->database_name}restaurants_sehir.sehir_id = {$this->database_name}sehir.sehir_id");
		$this->db->where("restaurant_id",$restaurant_id);
		$sehir=$this->db->get();

		return $sehir->result();
	}
	
}

