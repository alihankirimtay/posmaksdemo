<?php


class Customers_model extends CI_Model
{
	public function customers()
	{
		$customer = $this->db->get("{$this->database_name}users");
		return $customer->result();
    }

    public function customer_groups()
    {
        $groups = $this->db->get("{$this->database_name}users_groups");
        return $groups->result();
    }
    
    public function delete($customer_id,$status)
    {
        $this->db->set('active', $status);
        $this->db->where('id', $customer_id);
        $this->db->update("{$this->database_name}users");
    }

    public function orders()
    {
    	$orders = $this->db->order_by("sale_id","DESC")->get("{$this->database_name}order");
    	return $orders->result();
    }

}