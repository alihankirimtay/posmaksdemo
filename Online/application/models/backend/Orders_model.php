<?php


class Orders_model extends CI_Model
{
    public function orders()
    {
        $this->db->limit(20);
    	$orders = $this->db->order_by("sale_id","DESC")->get("{$this->database_name}order");
    	return $orders->result();
    }

    public function order_search($start_date,$end_date)
    {
        $this->db->where('order_date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
        $search = $this->db->get("{$this->database_name}order");
        return $search->result();

    }

}