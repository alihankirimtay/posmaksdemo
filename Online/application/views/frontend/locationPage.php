<?php $this->load->view('frontend/template/header'); ?>
	
	<?php 
      	$logo = $this->settings->settings_model->getLogo();
      	foreach ($logo as $image) { 
    ?>
      	<center><img src="<?= base_url(); ?><?= $image->image; ?>" height="100px" width="300px" style="margin-top: 50px; margin-bottom: 30px;"></center>
    <?php } ?>


<div class="p-2 mb-0 w-100 bg-info text-white text-center">
	<h3 style="margin-top: 20px;">Lütfen sipariş vermek istediğiniz şubeyi seçiniz</h3>
</div>

<div class="location_select">
	<br>
	<?php foreach($restaurants as $restaurant) { ?>
		<?php foreach($restaurantPos as $restaurantP) { ?>
			<?php if($restaurant->id === $restaurantP['id']){ ?>

				<?php 
                    $open_time;
                    $close_time;
                    $now_time = date("H:i:s");

                    $open_time = $restaurant->open_time;
                    $close_time = $restaurant->close_time;    
                   
                ?>
				
				<div class=" bg-light text-black locations">
					<div class="row">
						<?php if($restaurant->status == 1) { ?>
							<?php if(($now_time > $open_time && $now_time < $close_time)) { ?>
								<div class="col-md-2 col-sm-12">
									<div class="openLocation">
										<p>Açık</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<p><?= $restaurantP['title'] ?></p>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="location_button">
										<a id="<?= $restaurant->id ?>" href="<?= base_url('home/main/'.$restaurant->id); ?>">
											<button type="button" class="btn btn-info btn-block" ><span>Sipariş Ver</span></button>
										</a>
									</div>
								</div>
							<?php } else{ ?>
								<div class="col-md-2 col-sm-12">
									<div class="closeLocation">
										<p>Kapalı</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<p><?= $restaurantP['title'] ?></p>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="location_button">
										<button type="button" class="btn btn-info btn-block" disabled="disabled" ><span>Sipariş Ver</span></button>
									</div>
								</div>
							<?php } ?>

						<?php } else{ ?>

							<div class="col-md-2 col-sm-12">
								<div class="closeLocation">
									<p>Kapalı</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
							<p><?= $restaurantP['title'] ?></p>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="location_button">
									<button type="button" class="btn btn-info btn-block" disabled="disabled" ><span>Sipariş Ver</span></button>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				
			<?php } ?>
		<?php } ?>
	<?php } ?>
</div>



<?php $this->load->view('frontend/template/footer'); ?>