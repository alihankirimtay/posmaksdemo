<div class="modal fade" id="productModal" role="dialog">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            

            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image product_image_bg"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Ürün Detayları</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>

            <div class="modal-product-details">
                <div class="row align-items-center">
                    
                </div>
            </div>
            <div class="modal-body panel-details-container">
            
                <!--  Quantity   -->
                <div class="productQuantity">
                   <span>Adet</span>
                        <input type="text" class="form-control" id="quantity" value="1">
                        <input type="button" name="quantity_plus" class="quantity_plus" value="+">
                        <input type="button" name="quantity_minus" class="quantity_minus" value="-">
                </div>


                <!-- Panel Details / Materials -->
                <div class="Materials panel-details">
                    <h5 class="panel-details-title">
                        <label class="custom-control custom-radio">
                            <input name="radio_title_additions" type="radio" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                        </label>
                        <a href="#panelDetailsMaterials" data-toggle="collapse">Malzemeler</a>
                    </h5>
                    <div id="panelDetailsMaterials" class="collapse">
                        <div class="panel-details-content">
                            <div class="row">
                                <div class="materials col-sm-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="Menu panel-details">
                    <h5 class="panel-details-title">
                        <label class="custom-control custom-radio">
                            <input name="radio_title_additions" type="radio" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                        </label>
                        <a href="#panelDetailsMenu" data-toggle="collapse">Menü İçeriği</a>
                    </h5>
                    <div id="panelDetailsMenu" class="collapse">
                        <div class="panel-details-content">
                            <div class="row">
                                <div class="menu col-sm-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Panel Details / Additions -->
                <div class="Additions panel-details">
                    <h5 class="panel-details-title">
                        <label class="custom-control custom-radio">
                            <input name="radio_title_additions" type="radio" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                        </label>
                        <a href="#panelDetailsAdditions" data-toggle="collapse">İlave Ürünler</a>
                    </h5>
                    <div id="panelDetailsAdditions" class="collapse">
                        <div class=" panel-details-content">
                            <div class="row">
                                <div class="additions col-sm-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Panel Details / Other -->
                <div class="panel-details">
                    <h5 class="panel-details-title">
                        <label class="custom-control custom-radio">
                            <input name="radio_title_other" type="radio" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                        </label>
                        <a href="#panelDetailsOther" data-toggle="collapse">Ürün Notları</a>
                    </h5>
                    <div id="panelDetailsOther" class="collapse">
                      <!--  <input type="textarea" id="productDetails" cols="30" rows="4" class="form-control" placeholder="Ürün ile ilgili notlarınız varsa yazınız"></textarea> -->
                        <textarea rows="4" cols="62" id="productDetails" value="" placeholder="Ürün ile ilgili notlarınız varsa yazınız"></textarea>
                    </div>
                </div>
            </div>

            <button id="add_cart" type="button" name="doSubmit" class="modal-btn btn btn-secondary btn-block btn-lg" data-dismiss="modal" data-restaurantid="1" data-productname="Cocaa Cola" data-price="15" data-productid="2"><span>Sepete Ekle</span></button> 

            

        </div>
    </div>
</div>