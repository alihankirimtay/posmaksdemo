<div class="mobile_slider_and_user">
  <div class="col-md-3" >
    <div class="bg-light">
        <form action="#" class="validate-form">

          <?php if ($this->ion_auth->logged_in()){ ?>
            
              
              <div class="form-group form-submit">
                 <center>
                    <div class="user_name">
                      <h3>
                        Hoşgeldin <br>
                        <?php
                          echo $this->user->first_name;
                        ?>
                      </h3>

                    </div>
                 </center> 
              </div>

              
              <div class="form-group form-submit">
                    <button type="button" id="userEditModal" class="btn btn-warning btn-block" href="#editModal" data-target="#editModal" data-toggle="modal"><span>BİLGİLERİ GÜNCELLE</span></button>
              </div>

              <div class="form-group form-submit">
                  <button type="button" id="userOrdersModal_" class="btn btn-success btn-block" href="#userOrdersModal" data-target="#userOrdersModal" data-toggle="modal"><span>SİPARİŞ GEÇMİŞİM</span></button>
              </div>

              <div class="form-group form-submit">
                  <a href="<?= base_url('auth/logout'); ?>">
                    <button type="button" class="btn btn-danger btn-block"><span>ÇIKIŞ YAP</span></button>
                  </a>
              </div>



          <?php } else{ ?>
               
               <style>
                  @media only screen and (max-width: 767px){
                      .mobile_slider_and_user {
                          margin-top: -470px;
                      }
                      #nav-main-mobile {
                          margin-top: 235px;
                      }
                  }
              </style>
              
               <div class="form-group form-submit">
                  <button type="button" class="btn btn-success btn-block" href="#LoginModal" data-target="#LoginModal" data-toggle="modal"><span>GİRİŞ YAP</span></button>
              </div>
              
              <div class="form-group form-submit">
                  <button type="button" class="btn btn-warning btn-block" href="#RegisterModal" data-target="#RegisterModal" data-toggle="modal" data-dismiss="modal"><span>KAYIT OL</span></button>
              </div> 
          
          <?php } ?>

          <div class="form-group form-submit">
              <a href="<?= base_url(); ?>">
                <button type="button" class="btn btn-secondary btn-block"><span>ŞUBE DEĞİŞTİR</span></button>
              </a>
          </div>

          
                             
        </form>
    </div>
  </div>
</div>


