
<!-- Modal / Register -->
<div class="modal fade" id="RegisterModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Kayıt Ol</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="example-box-content">


                    <form action="<?= base_url("Auth/signUp"); ?>" gourl="<?= $_SERVER['REQUEST_URI'] ?>">
                        <center>
                        
                            <div class="form-group">
                                <label>Adınız Soyadınız</label>
                                <input type="text" class="form-control" name="first_name" style="width: 75%" placeholder="Adınızı ve Soyadınızı Giriniz" value="<?= $this->input->post('first_name') ?>">
                            </div>
                        <!--
                            <div class="form-group">
                                <label>Soyad</label>
                                <input type="text" class="form-control" name="last_name" style="width: 75%" placeholder="Soyadınızı Giriniz"  value="<?= $this->input->post('last_name') ?>">
                            </div>
                        -->
                            <div class="form-group">
                                <label>E-Mail</label>
                                <input type="text" class="form-control" name="email" style="width: 75%" aria-describedby="emailHelp" placeholder="E-Mail Giriniz" value="<?= $this->input->post('email') ?>">
                            </div>
                            <div class="form-group">
                                <label>Telefon</label>
                                <input type="text" class="form-control" name="phone" style="width: 75%" placeholder="Telefon Giriniz" value="<?= $this->input->post('phone') ?>">
                            </div>
                            <div class="form-group">
                                <label>Şifre</label>
                                <input type="password" class="form-control" name="password" style="width: 75%" placeholder="Şifre Giriniz">
                            </div>
                            <div class="form-group">
                                <label>Şifre Onayı</label>
                                <input type="password" class="form-control" name="password_confirm" style="width: 75%" placeholder="Şifreyi Tekrar Giriniz">
                            </div>

                            <div class="result"></div>

                            <button type="button" name="doSubmit" class="btn btn-success" style="width: 25%"><span>Kayıt Ol</span></button>
                         
                        </center>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>