<?php $this->load->view('frontend/template/header'); ?>
	
	
	 <!--   mobile header panel -->
    <?php $this->load->view('frontend/template/header_panel_mobile'); ?>

	<!--  Opinions      -->
	<?php $this->load->view('frontend/opinions'); ?>

	<!--  ShoppingCart      -->
    <?php $this->load->view('frontend/elements/cart'); ?>

	<!--   ProductAttributes      -->
	<?php $this->load->view('frontend/elements/productAttributes'); ?>


	<div class="contact_info" style="margin-top: 150px; padding: 30px;">
	
	<center><h3>İletişim Bilgileri</h3></center>

		<table class="table table-sm">
			<thead>
				<tr>
					<th scope="col">Adres</th>
					<th scope="col">Telefon Numarası</th>
					<th scope="col">Mail Adresi</th>

				</tr>
			</thead>
			<tbody>
			
				<tr>
					<?php foreach ($restaurant_address as $address) { ?>
						<td><?= $address->sehir_title; ?> / <?= $address->ilce_title; ?> / <?= $address->mahalle_title; ?> / <?= $address->acik_adres; ?> </td>
					<?php } ?>
					<?php foreach ($location_info as $info) { ?>
						<td><?= $info->phone; ?></td>
						<td><?= $info->email; ?></td>

						<?php if($info->map_x != null && $info->map_y != null){ ?>
							<input type="hidden" name="lat" id="lat" value="<?= $info->map_x; ?>">
							<input type="hidden" name="lng" id="lng" value="<?= $info->map_y; ?>">
						<?php }else{ ?>
							<input type="hidden" name="lat" id="lat" value="40.99149345910838">
							<input type="hidden" name="lng" id="lng" value="29.022603929042816">
						<?php } ?>
						
					<?php } ?>
				</tr>
			
			</tbody>
		</table>

	</div>

	

	<div id="googleMap" style="width:100%;height:400px;"></div>

	<?php 
	    $settings = $this->settings->settings_model->index();
	    if($settings)
	    {
	        foreach ($settings as $map_api) {
	        	$map_api_key = $map_api->api_key;

	        }
	    } 
	?>

	<script>
		function myMap() {
			
			var lat_ = document.getElementById("lat");
			var lng_ = document.getElementById("lng");

			lat_ = lat_ ? parseFloat(lat_.value) : 40.99149345910838;
			lng_ = lng_ ? parseFloat(lng_.value) : 29.022603929042816;

			var map=new google.maps.Map(document.getElementById("googleMap"),{
				center:{
					lat: lat_,
					lng: lng_
				},
				zoom:15
			});

			var marker = new google.maps.Marker({
				position:{
					lat:lat_,
					lng:lng_
				},
				map:map,
				draggable: true
			});

			google.maps.event.addListener(marker,'dragend',function(){

				var lat = marker.getPosition().lat();
				var lng = marker.getPosition().lng();
				console.log(lat);
				console.log(lng);

				$('#lat').val(lat);
				$('#lng').val(lng);
			});

		
		}
	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=<?= "{$map_api_key}"; ?>&callback=myMap"></script>


<?php $this->load->view('frontend/template/footer'); ?>