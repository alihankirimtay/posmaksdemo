<?php $this->load->view('backend/template/header'); ?>

<link rel="stylesheet" href="<?= base_url('assets/css/adminpanel.css')?>">

<div class="content">
	<div class="row p-t-3">
		<div class="col-sm-12 col-md-6 col-lg-3">
	        <a target="_blank" rel="noopener noreferrer" href="<?= base_url(); ?>">
	            <div class="card card-iconic card-green">
	                <div class="card-full-icon fa fa-desktop"></div>
	                <div class="card-body text-left">
	                    <h4><b>WEB SİTEMİ GÖSTER</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/Locations/newLocationPage'); ?>">
	            <div class="card card-iconic card-grey">
	                <div class="card-full-icon fa fa-building"></div>
	                <div class="card-body text-left">
	                    <h4><b>YENİ LOKASYON EKLE</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/Locations/locations'); ?>">
	            <div class="card card-iconic card-purple">
	                <div class="card-full-icon fa fa-building-o"></div>
	                <div class="card-body text-left">
	                    <h4><b>LOKASYONLAR</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/customers/'); ?>">
	            <div class="card card-iconic card-blue-grey">
	                <div class="card-full-icon fa fa-users"></div>
	                <div class="card-body text-left">
	                    <h4><b>MÜŞTERİLER</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/customers/orders'); ?>">
	            <div class="card card-iconic card-pink">
	                <div class="card-full-icon fa fa-shopping-basket"></div>
	                <div class="card-body text-left">
	                    <h4><b>MÜŞTERİ SİPARİŞLERİ</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/customers/opinions/'); ?>">
	            <div class="card card-iconic card-amber">
	                <div class="card-full-icon fa fa-commenting-o"></div>
	                <div class="card-body text-left">
	                    <h4><b>MÜŞTERİ GÖRÜŞLERİ</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/slidemanagement'); ?>">
	            <div class="card card-iconic card-brown">
	                <div class="card-full-icon fa fa-file-photo-o"></div>
	                <div class="card-body text-left">
	                    <h4><b>SLAYT YÖNETİMİ</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="<?= base_url('panel/settings/'); ?>">
	            <div class="card card-iconic card-red">
	                <div class="card-full-icon fa fa-reorder"></div>
	                <div class="card-body text-left">
	                    <h4><b>AYARLAR</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
	    <div class="col-sm-12 col-md-6 col-lg-3">
	        <a href="" role="button" data-toggle="modal" data-target="#exampleModalCenter">
	            <div class="card card-iconic card-blue">
	                <div class="card-full-icon fa fa-magic"></div>
	                <div class="card-body text-left">
	                    <h4><b>YARDIM</b></h4>
	                </div> 
	            </div>
	        </a>
	    </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">YARDIM</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('backend/template/footer'); ?>