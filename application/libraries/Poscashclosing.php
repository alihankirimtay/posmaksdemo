<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Poscashclosing {

    protected $CI;
    private $error_message;
    private $payment_types;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Sessions_M');

        $this->setPaymentTypes();
    }

    public function errorMessage()
    {
        return $this->error_message;
    }

    public function getPaymentTypes()
    {
        return $this->payment_types;
    }

    private $session_id = 0;
    private $location_id = 0;
    private $user_id = 0;
    private $manager_id = 0;
    private $payments = [];
    private $cashs = [];
    private $cash_opening = 0;

    public function setSessionId($session_id = null)
    {
        $this->session_id = (Int) $session_id;
        return $this;
    }

    public function setLocationId($location_id = null)
    {
        $this->location_id = (Int) $location_id;
        return $this;
    }

    public function setUserId($user_id = null)
    {
        $this->user_id = (Int) $user_id;
        return $this;
    }

    public function setManagerId($manager_id = null)
    {
        $this->manager_id = (Int) $manager_id;
        return $this;
    }

    public function setPayments($payments = null)
    {
        $this->payments = (Array) $payments;
        return $this;
    }

    public function setCashs($cashs = null)
    {
        $this->cashs = (Array) $cashs;
        return $this;
    }

    public function setCashOpening($cash_opening = null)
    {
        $this->cash_opening = $cash_opening;
        return $this;
    }

    public function approve()
    {
        $location_id = $this->location_id;
        $user_id = $this->user_id;
        $manager_id = $this->manager_id;
        $cash_opening = doubleval(str_replace(',', '', $this->cash_opening));
        $payments = $this->payments;
        $cashs = $this->cashs;
        $datetime = date('Y-m-d H:i:s');
        foreach ($this->payment_types as $payment_type) {
            ${$payment_type->alias} = 0;
            if (isset($payments[$payment_type->alias])) {
                ${$payment_type->alias} = doubleval(str_replace(',', '', $payments[$payment_type->alias])); // credit, ticket, sodexo etc.
            }
        }

        $this->CI->db->where('location_id', $location_id);
        $session = $this->CI->Sessions_M->findOpenSession($user_id);
        if (!$session) {
            $this->error_message = 'Aktif oturum bulunamadı.';
            return false;
        }
        $session->values = json_decode($session->values);
        $next_key = count($session->values);

        $total_session_payments = $this->sessionCalculation([
            'completer_user_id' => $user_id,
            'location_id' => $location_id,
            'payment_date >=' => $session->created,
        ]);

        $this->sessionCashRequests();
        $cash = $this->cashs->total;

        
        $session->values[$next_key] = [
            'cashs' => $this->cashs->cash_amounts,
            'cash_opening' => $cash_opening,
            'manager_id' => $manager_id,
            'closed' => $datetime,
            'auto_closed' => 0,
        ];
        foreach ($this->payment_types as $payment_type) {
            $session->values[$next_key][$payment_type->alias] = ${$payment_type->alias};
        }

        // Kasa ilk defa kapanıyorsa, yani status null ise; kasadaki ve sistemdeki parasal değerler eşit mi?
        // Eşit ise kasa kapanır ve durum 1 olur. Değilse, tekrar düzenlemeye izin vermek üzere durum 0 yapılır.
        if (is_null($session->status)) {
            $is_equal = true;
            foreach ($this->payment_types as $payment_type) {

                if ($is_equal == false) break;

                if ($payment_type->alias == 'cash') {
                    $is_equal = intval($cash - $cash_opening) == intval($total_session_payments->cash);
                } else {
                    $is_equal = intval(${$payment_type->alias}) == intval($total_session_payments->{$payment_type->alias});
                }
            }
            $status = $is_equal ? 1 : 0;
        }
        // Son kayıt, başarılı yada başarısız olsada kapatılır.
        else if ($session->status == 0) {
            $status = 1;
        }

        $update_session = [
            'location_id' => $location_id,
            'manager_id' => $manager_id,
            'cash_opening' => $cash_opening,
            'values' => json_encode($session->values),
            'auto_closed' => 0,
            'closed' => $datetime,
            'modified' => $datetime,
            'status' => $status,
        ];
        foreach ($this->payment_types as $payment_type) {
            $update_session[$payment_type->alias] = $total_session_payments->{$payment_type->alias};
            $update_session["{$payment_type->alias}_exists"] = ${$payment_type->alias};
        }

        $this->CI->Sessions_M->update($update_session, [
            'id' => $session->id
        ]);

        if ($status) {
            $message = 'Kasa sayımı yapıldı.';
        } else {
            $message = 'Değerler eşleşmedi, tekrar deneyin.';
        }

        return (Object) compact('message', 'status', 'session');
    }

    public function update()
    {
        $session_id = $this->session_id;
        $manager_id = $this->manager_id;
        $cash_opening = doubleval(str_replace(',', '', $this->cash_opening));
        $payments = $this->payments;
        $cashs = $this->cashs;
        $datetime = date('Y-m-d H:i:s');
        foreach ($this->payment_types as $payment_type) {
            ${$payment_type->alias} = 0;
            if (isset($payments[$payment_type->alias])) {
                ${$payment_type->alias} = doubleval(str_replace(',', '', $payments[$payment_type->alias])); // credit, ticket, sodexo etc.
            }
        }


        $session = $this->CI->Sessions_M->where('location_id', $this->CI->user->locations)->get([
            'id' => $session_id
        ]);
        if (!$session) {
            $this->error_message = 'Aktif oturum bulunamadı.';
            return false;
        }
        $session->values = json_decode($session->values);
        $next_key = count($session->values);


        $this->sessionCashRequests();
        $cash = $this->cashs->total;


        $session->values[$next_key] = [
            'cashs' => $this->cashs->cash_amounts,
            'cash_opening' => $cash_opening,
            'manager_id' => $manager_id,
            'closed' => $datetime,
            'auto_closed' => 0,
        ];
        foreach ($this->payment_types as $payment_type) {
            $session->values[$next_key][$payment_type->alias] = ${$payment_type->alias};
        }

        $update_session = [
            'manager_id' => $manager_id,
            'cash_opening' => $cash_opening,
            'values' => json_encode($session->values),
            'auto_closed' => ($session->auto_closed) ? 2 : 0,
            'modified' => $datetime,
        ];
        foreach ($this->payment_types as $payment_type) {
            $update_session["{$payment_type->alias}_exists"] = ${$payment_type->alias};
        }

        $this->CI->Sessions_M->update($update_session, [
            'id' => $session->id
        ]);

        return true;
    }

    private function sessionCalculation(Array $where = [])
    {
        foreach ($this->payment_types as $payment_type) {
            ${$payment_type->alias} = 0;
        }

        $sales = $this->CI->Sales_M->where($where)->get_all([
            'status' => 'completed',
            'active !=' => 3,
        ]);

        foreach ($sales as $sale) {
            if ($sale->return_id) {
                foreach ($this->payment_types as $payment_type) {
                    ${$payment_type->alias} -= $sale->{$payment_type->alias};
                }
            } else {
                $total_paid = 0;
                foreach ($this->payment_types as $payment_type) {
                    if ($payment_type->alias != 'cash') {
                        ${$payment_type->alias} += $sale->{$payment_type->alias};
                        $total_paid += $sale->{$payment_type->alias};
                    }
                }
                $cash += $sale->cash - ($sale->cash + $total_paid - $sale->amount);
            }
        }

        $result = [];
        foreach ($this->payment_types as $payment_type) {
            $result[$payment_type->alias] = ${$payment_type->alias};
        }

        return (Object) $result;
    }

    private function sessionCashRequests()
    {
        $total = 0;
        $cash_types = $this->CI->Sessions_M->findCashTypes();
        $cash_amounts = array_map(function ($row) { return 0; }, $cash_types);
        $cash_amounts['drop'] = 0;

        foreach ($cash_types as $cash_alias => $cash_value) {

            if (!isset($this->cashs[$cash_alias]))
                continue;

            $total += $this->cashs[$cash_alias] * $cash_value;
            $cash_amounts[$cash_alias] = $this->cashs[$cash_alias];
        }

        if (isset($this->cashs['drop'])) {
            $total += $this->cashs['drop'];
            $cash_amounts['drop'] = $this->cashs[$cash_alias];
        }

        $this->cashs = (Object) [
            'cash_amounts' => $cash_amounts,
            'total' => $total,
        ];
    }

    private function setPaymentTypes()
    {
        $this->payment_types = $this->CI->db
        ->where(['active' => 1])
        ->get('payment_types')
        ->result();
    }
}