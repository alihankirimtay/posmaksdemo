<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Possession {

    protected $CI;
    private $error_message;

    public function __construct()
    {
        $this->CI =& get_instance();
    }


    /*
        The Setup is settings of pos and location.
    */
    public function getSetup()
    {
        $setup = $this->CI->session->userdata('pos_setup');

        if (!isset($setup->location_id) || !isset($setup->pos_id)) {
            return false;
        }

        return $setup;
    }

    public function setSetup($location_id, $pos_id)
    {
        $this->CI->session->set_userdata('pos_setup', (Object) [
            'location_id' => (int) $location_id,
            'pos_id' => (int) $pos_id,
        ]);
    }

    public function createSetup($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->CI->form_validation
        ->set_rules('location_id', 'Restoran', 'trim|required')
        ->set_rules('pos_id', 'Kasa', 'trim|required')
        ->set_rules('username', 'Kullanıcı adı/Eposta', 'trim|required')
        ->set_rules('password', 'Şifre', 'trim|required');

        if (!$this->CI->form_validation->run()) {
            $this->error_message = validation_errors();
            return false;
        }

        $location_id = $this->CI->input->post('location_id');
        $pos_id = $this->CI->input->post('pos_id');
        $username = $this->CI->input->post('username');
        $password = $this->CI->input->post('password');

        if (!$this->checkPermission($username, $password, 'pointofsales.setup')) {
            return false;
        }

        $location = $this->CI->Locations_M->where('id', $this->CI->user->locations)->get([
            'id' => $location_id,
            'active' => 1,
        ]);
        if (!$location) {
            $this->error_message = 'Geçersiz Restoran.';
            return false;
        }

        $pos = $this->CI->Pos_M->where('location_id', $this->CI->user->locations)->get([
            'id' => $pos_id,
            'location_id' => $location_id,
            'active' => 1,
        ]);
        if (!$pos) {
            $this->error_message = 'Geçersiz Kasa.';
            return false;
        }

        $this->setSetup($location_id, $pos_id);

        return true;
    }

    public function removeSetup()
    {

        $this->CI->session->unset_userdata('pos_setup');
    }




    /*
        The Session is settings of operator user.
    */
    public function getSession()
    {
        $session = $this->CI->session->userdata('pos_session');

        if (!isset($session->user_id)) {
            return false;
        }

        return $session;
    }

    public function setSession($user)
    {
        $this->CI->session->set_userdata('pos_session', (Object) [
            'user_id' => (int) $user->id,
            'user_name' => $user->username,
            'user_role' => $user->role_alias,
            'role_id' => $user->role_id,
            'permissions' => $user->permissions
        ]);
    }

    public function createSession($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->CI->form_validation
        ->set_rules('location_id', 'Restoran', 'trim|required')
        ->set_rules('password', 'Şifre', 'trim|required');

        if (!$this->CI->form_validation->run()) {
            $this->error_message = validation_errors();
            return false;
        }

        $location_id = $this->CI->input->post('location_id');
        $password = $this->CI->input->post('password');

        $user = $this->CI->db
        ->select('id,username,email,role_id')
        ->select('(select alias from roles where id = role_id) As role_alias')
        ->where([
            'active' => 1,
            'pin' => $password,
        ])->get('users', 1)->row();
        if (!$user) {
            $this->error_message = 'Hatalı giriş.';
            return false;
        }

        $user->permissions = [];

        if ($user->role_alias != 'admin') {
            $user_location = $this->CI->db->where([
                'user_id' => $user->id,
                'location_id' => $location_id,
            ])->get('user_has_locations', 1)->row();
            if (!$user_location) {
                $this->error_message = 'Bu Restorana erişim yetkiniz yok.';
                return false;
            }
            
            $user->permissions = $this->CI->Roles_M->findPermissionsList($user->role_id);

        }


        $this->setSession($user);

        return $user;
    }

    public function removeSession()
    {

        $this->CI->session->unset_userdata('pos_session');
    }




    /*
        The Discount key is using for discount on sale.
     */
    public function setDiscount($key, $amount, $type)
    {
        $this->CI->session->set_userdata('pos_discount', (Object) [
            'discount_key' => $key,
            'discount_amount' => $amount,
            'discount_type' => $type,
        ]);
    }

    public function getDiscount()
    {
        $discount = $this->CI->session->userdata('pos_discount');

        if (!isset($discount->discount_key) || !isset($discount->discount_amount) || !isset($discount->discount_type)) {
            return false;
        }

        return $discount;
    }

    public function checkDiscount($key)
    {
        $discount = $this->getDiscount();
        if (!$discount) {
            $this->error_message = 'İndirim bulunamadı.';
            return false;
        }

        if ($discount->discount_key != $key) {
            $this->error_message = 'Geçersiz indirim.';
            return false;
        }

        return $discount;
    }

    public function createDiscount($values, $login_required)
    {
        $_POST = array_merge($_POST, $values);

        if ($login_required) {

            $this->CI->form_validation
            ->set_rules('username' , 'Kullanıcı Adı/Eposta', 'trim|required')
            ->set_rules('password' , 'Şifre', 'trim|required');

        }

        $this->CI->form_validation
        ->set_rules('location_id' , 'Restoran', 'trim|required')
        ->set_rules('discount_amount' , 'İndirim miktarı', 'trim|required|is_money')
        ->set_rules('discount_type' , 'İndirim tipi', 'trim|required|in_list[amount,percent]');

        if (!$this->CI->form_validation->run()) {
            $this->error_message = validation_errors();
            return false;
        }

        $location_id = $this->CI->input->post('location_id');
        $username = $this->CI->input->post('username');
        $password = $this->CI->input->post('password');
        $discount_amount = $this->CI->input->post('discount_amount');
        $discount_type = $this->CI->input->post('discount_type');
        $discount_key = sha1(uniqid());

        if ($login_required) {
            if (!$this->checkPermission($username, $password, 'pointofsales.adddiscount')) {
                $this->error_message = 'Erişim yetkisi yok.';
                return false;
            }

        }
        

        $this->setDiscount($discount_key, $discount_amount, $discount_type);

        return $discount_key;
    }

    public function removeDiscount()
    {

        $this->CI->session->unset_userdata('pos_discount');
    }



    public function getReturn()
    {
        $return = $this->CI->session->userdata('pos_return');

        if (strlen($return) != 40) {
            return false;
        }

        return $return;
    }

    public function setReturn($session)
    {

        $this->CI->session->set_userdata('pos_return', $session);
    }

    public function createReturn($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->CI->form_validation
        ->set_rules('location_id' , 'Restoran', 'trim|required')
        ->set_rules('sale_id' , 'Satış No', 'trim|required')
        ->set_rules('username' , 'Kullanıcı Adı/Eposta', 'trim|required')
        ->set_rules('password' , 'Şifre', 'trim|required');

        if (!$this->CI->form_validation->run()) {
            $this->error_message = validation_errors();
            return false;
        }

        $location_id = $this->CI->input->post('location_id');
        $sale_id = $this->CI->input->post('sale_id');
        $username = $this->CI->input->post('username');
        $password = $this->CI->input->post('password');

        if (!$this->checkPermission($username, $password, 'pointofsales.return')) {
            return false;
        }


        $sale = $this->CI->Sales_M->where('location_id', $this->CI->user->locations)->get([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ]);
        if (!$sale) {
            $this->error_message = 'Geçersiz satış.';
            return false;
        }

        $session = sha1(uniqid());
        $this->setReturn($session);

        return $session;
    }

    public function removeReturn()
    {
        
        $this->CI->session->unset_userdata('pos_return');
    }




    public function errorMessage()
    {

        return $this->error_message;
    }

    public function checkPermission($username, $password, $permission)
    {
        $user = $this->CI->db
        ->select('users.id, users.password, users.role_id, roles.alias As role_alias')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $username)
                ->or_where('users.email', $username)
            ->group_end()
        ->join('roles', 'roles.id = users.role_id')
        ->where('roles.active', 1)
        ->get('users', 1)->row();
        if (!$user) {
            $this->error_message = 'Hatalı giriş.';
            return false;
        } 

        if ($user->password != sha1($password . $this->CI->config->config['salt'])) {
            $this->error_message = 'Hatalı giriş.';
            return false;
        }

        if ($user->role_alias != 'admin') {
            
            if (!$this->CI->Roles_M->hasPermission($user->role_id, $permission)) {
                $this->error_message = 'Erişim yetkisi yok.';
                return false;
            }
        }

        return $user;
    }

    public function checkPermissionByUserSession($user_session, $permission)
    {
        if (empty($user_session)) {
            return false;
        }
        
        if ($user_session->user_role != 'admin') {
        
            if (!$this->CI->Roles_M->hasPermission($user_session->role_id, $permission)) {
                $this->error_message = 'Erişim yetkisi yok.';
                return false;
            }
        }

        return true;
    }
}