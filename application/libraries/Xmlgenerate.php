<?php


class Xmlgenerate {

    public $xmlDoc;
    public $root;

    public $allingSpace = 46;

    function __construct()
    {   
        $this->xmlDoc = new DOMDocument();
        $this->root = $this->xmlDoc->appendChild(
            $this->xmlDoc->createElement("Invoice"));
    }

    public function addLogo($src){
        //create a tutorial element
        $element = $this->root->appendChild(
            $this->xmlDoc->createElement("logo"));
        
        //create the author attribute
        $element->appendChild(
        $this->xmlDoc->createAttribute("src"))->appendChild(
        $this->xmlDoc->createTextNode($src));
    }

    public function addAllingText($left , $rigth){
    	$total = strlen($left) + strlen($rigth);
    	$count = $this->allingSpace - $total;

    	$value = $left;



    	for($i = 0; $i<$count; $i++){
    		$value .= " ";
    	}

    	$value .= $rigth;

    	$element = $this->root->appendChild(
            $this->xmlDoc->createElement("text" , $this->replace_tr($value)));
        
        //create the author attribute
        $element->appendChild(
        $this->xmlDoc->createAttribute("alling"))->appendChild(
        $this->xmlDoc->createTextNode("center"));


    }

    public function addText($alling , $value){
        //create a tutorial element
        $element = $this->root->appendChild(
            $this->xmlDoc->createElement("text" , $this->replace_tr($value)));
        
        //create the author attribute
        $element->appendChild(
        $this->xmlDoc->createAttribute("alling"))->appendChild(
        $this->xmlDoc->createTextNode($alling));

    }

    public function boldText($value)
    {
        //create a tutorial element
        $element = $this->root->appendChild(
        $this->xmlDoc->createElement("bold" , $this->replace_tr($value)));

    }

    public function title($size , $value)
    {
        //create a tutorial element
        $element = $this->root->appendChild(
            $this->xmlDoc->createElement("text" , $this->replace_tr($value)));
        
        //create the author attribute
        $element->appendChild(
        $this->xmlDoc->createAttribute("size"))->appendChild(
        $this->xmlDoc->createTextNode($size));

    }

    public function seperator()
    {
        //create a tutorial element
        $element = $this->root->appendChild(
            $this->xmlDoc->createElement("separator"));

    }

    public function createList($header = [] , $items = [] , $space = 0 , $max_char = null)
    {
        //create a tutorial element
        $list = $this->root->appendChild(
            $this->xmlDoc->createElement("list"));
        
        //create the author attribute
        $list->appendChild(
        $this->xmlDoc->createAttribute("space"))->appendChild(
        $this->xmlDoc->createTextNode($space));

        $head = $list->appendChild(
            $this->xmlDoc->createElement('head')
        );

        foreach($header as $h){
            $head->appendChild(
                $this->xmlDoc->createElement('item' , $this->replace_tr($h))
            );
        }

        $body = $list->appendChild(
            $this->xmlDoc->createElement('body')
        );

        if($max_char != null){
            $body->appendChild(
            $this->xmlDoc->createAttribute("max-char"))->appendChild(
            $this->xmlDoc->createTextNode($max_char));
        }

        foreach($items as $item)
        {
            $row = $body->appendChild(
                $this->xmlDoc->createElement('row')
            );

            foreach($item as $e)
            {
                $row->appendChild(
                    $this->xmlDoc->createElement('item' , $this->replace_tr($e))
                );

            }
            
        }
        

    }


    public function Create()
    {   
        $this->root->appendChild(
            $this->xmlDoc->createElement("cutpaper"));
        echo $this->xmlDoc->saveXML();
    }

    private function replace_tr($text) {
        $text = trim($text);
        $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ');
        $replace = array('c','c','g','g','i','i','o','o','s','s','u','u',' ');
        $new_text = str_replace($search,$replace,$text);
        return $new_text;
    } 

}



// Örnek Kullanımı
/*$xml = new GenerateInvoice();

$xml->addLogo("example");
$xml->addText('left' , 'Deneme1');
$xml->addText('right' , 'Deneme2');
$xml->addText('center' , 'Deneme3');
$xml->boldText('Kalın Yazı');
$xml->title('1' , 'Başlık 1');
$xml->title('2' , 'Başlık 2');
$xml->title('3' , 'Başlık 3');
$xml->seperator();

$head_items = [
    'Urun',
    'Adet',
    'Tutar'
];

$items = [
    0 => [
        'Yemek',
        '1',
        '15.00TL'
    ],
    1 => [
        'Icicek',
        '2',
        '6.00 TL'
    ]
];

$xml->createList($head_items , $items , 15 , 14);

header("Content-Type: application/xml");
$xml->Create();
*/
?>