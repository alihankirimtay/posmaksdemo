<?php
/**
 * File to load generated classes once at once time
 * @package YemekSepeti
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
require_once dirname(__FILE__) . '/YemekSepetiWsdlClass.php';
require_once dirname(__FILE__) . '/Order/States/YemekSepetiEnumOrderStates.php';
require_once dirname(__FILE__) . '/Update/Response/YemekSepetiStructUpdateOrderResponse.php';
require_once dirname(__FILE__) . '/Set/Time/YemekSepetiStructSetRestaurantServiceTime.php';
require_once dirname(__FILE__) . '/Update/Order/YemekSepetiStructUpdateOrder.php';
require_once dirname(__FILE__) . '/Message/Response/YemekSepetiStructMessageSuccessfulResponse.php';
require_once dirname(__FILE__) . '/Get/Messages/YemekSepetiStructGetTopMessages.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetTopMessagesResponse.php';
require_once dirname(__FILE__) . '/Message/Successful/YemekSepetiStructMessageSuccessful.php';
require_once dirname(__FILE__) . '/Set/Response/YemekSepetiStructSetRestaurantServiceTimeResponse.php';
require_once dirname(__FILE__) . '/Set/Order/YemekSepetiStructSetRestaurantForOrder.php';
require_once dirname(__FILE__) . '/Get/Promotions/YemekSepetiStructGetRestaurantPromotions.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetRestaurantPromotionsResponse.php';
require_once dirname(__FILE__) . '/Get/Result/YemekSepetiStructGetRestaurantPromotionsResult.php';
require_once dirname(__FILE__) . '/Get/Result/YemekSepetiStructGetMenuResult.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetMenuResponse.php';
require_once dirname(__FILE__) . '/Set/Response/YemekSepetiStructSetRestaurantForOrderResponse.php';
require_once dirname(__FILE__) . '/Get/Menu/YemekSepetiStructGetMenu.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetAllMessagesResponse.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetAllMessagesV2Response.php';
require_once dirname(__FILE__) . '/Get/Messages/YemekSepetiStructGetAllMessages.php';
require_once dirname(__FILE__) . '/Get/Messages/YemekSepetiStructGetAllMessagesV2.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetRestaurantListResponse.php';
require_once dirname(__FILE__) . '/Get/Result/YemekSepetiStructGetRestaurantListResult.php';
require_once dirname(__FILE__) . '/Is/Open/YemekSepetiStructIsRestaurantOpen.php';
require_once dirname(__FILE__) . '/Get/List/YemekSepetiStructGetRestaurantList.php';
require_once dirname(__FILE__) . '/Auth/Header/YemekSepetiStructAuthHeader.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetPaymentTypesResponse.php';
require_once dirname(__FILE__) . '/Get/Result/YemekSepetiStructGetPaymentTypesResult.php';
require_once dirname(__FILE__) . '/Is/Response/YemekSepetiStructIsRestaurantOpenResponse.php';
require_once dirname(__FILE__) . '/Get/Comments/YemekSepetiStructGetRestaurantPointsAndComments.php';
require_once dirname(__FILE__) . '/Update/Response/YemekSepetiStructUpdateRestaurantStateResponse.php';
require_once dirname(__FILE__) . '/Get/Message/YemekSepetiStructGetMessage.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetMessageResponse.php';
require_once dirname(__FILE__) . '/Restaurant/States/YemekSepetiEnumRestaurantStates.php';
require_once dirname(__FILE__) . '/Update/State/YemekSepetiStructUpdateRestaurantState.php';
require_once dirname(__FILE__) . '/Get/Response/YemekSepetiStructGetRestaurantPointsAndCommentsResponse.php';
require_once dirname(__FILE__) . '/Get/Result/YemekSepetiStructGetRestaurantPointsAndCommentsResult.php';
require_once dirname(__FILE__) . '/Get/Types/YemekSepetiStructGetPaymentTypes.php';
require_once dirname(__FILE__) . '/Get/YemekSepetiServiceGet.php';
require_once dirname(__FILE__) . '/Is/YemekSepetiServiceIs.php';
require_once dirname(__FILE__) . '/Update/YemekSepetiServiceUpdate.php';
require_once dirname(__FILE__) . '/Message/YemekSepetiServiceMessage.php';
require_once dirname(__FILE__) . '/Set/YemekSepetiServiceSet.php';
require_once dirname(__FILE__) . '/YemekSepetiClassMap.php';
