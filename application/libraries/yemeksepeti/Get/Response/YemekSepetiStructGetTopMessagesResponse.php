<?php
/**
 * File for class YemekSepetiStructGetTopMessagesResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetTopMessagesResponse originally named GetTopMessagesResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetTopMessagesResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetTopMessagesResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $GetTopMessagesResult;
    /**
     * Constructor method for GetTopMessagesResponse
     * @see parent::__construct()
     * @param string $_getTopMessagesResult
     * @return YemekSepetiStructGetTopMessagesResponse
     */
    public function __construct($_getTopMessagesResult = NULL)
    {
        parent::__construct(array('GetTopMessagesResult'=>$_getTopMessagesResult),false);
    }
    /**
     * Get GetTopMessagesResult value
     * @return string|null
     */
    public function getGetTopMessagesResult()
    {
        return $this->GetTopMessagesResult;
    }
    /**
     * Set GetTopMessagesResult value
     * @param string $_getTopMessagesResult the GetTopMessagesResult
     * @return string
     */
    public function setGetTopMessagesResult($_getTopMessagesResult)
    {
        return ($this->GetTopMessagesResult = $_getTopMessagesResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetTopMessagesResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
