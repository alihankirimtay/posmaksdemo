<?php
/**
 * File for class YemekSepetiStructGetAllMessagesResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetAllMessagesResponse originally named GetAllMessagesResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetAllMessagesResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetAllMessagesResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $GetAllMessagesResult;
    /**
     * Constructor method for GetAllMessagesResponse
     * @see parent::__construct()
     * @param string $_getAllMessagesResult
     * @return YemekSepetiStructGetAllMessagesResponse
     */
    public function __construct($_getAllMessagesResult = NULL)
    {
        parent::__construct(array('GetAllMessagesResult'=>$_getAllMessagesResult),false);
    }
    /**
     * Get GetAllMessagesResult value
     * @return string|null
     */
    public function getGetAllMessagesResult()
    {
        return $this->GetAllMessagesResult;
    }
    /**
     * Set GetAllMessagesResult value
     * @param string $_getAllMessagesResult the GetAllMessagesResult
     * @return string
     */
    public function setGetAllMessagesResult($_getAllMessagesResult)
    {
        return ($this->GetAllMessagesResult = $_getAllMessagesResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetAllMessagesResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
