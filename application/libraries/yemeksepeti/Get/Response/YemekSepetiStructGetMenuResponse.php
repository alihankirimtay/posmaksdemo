<?php
/**
 * File for class YemekSepetiStructGetMenuResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetMenuResponse originally named GetMenuResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetMenuResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetMenuResult
     * @var YemekSepetiStructGetMenuResult
     */
    public $GetMenuResult;
    /**
     * Constructor method for GetMenuResponse
     * @see parent::__construct()
     * @param YemekSepetiStructGetMenuResult $_getMenuResult
     * @return YemekSepetiStructGetMenuResponse
     */
    public function __construct($_getMenuResult = NULL)
    {
        parent::__construct(array('GetMenuResult'=>$_getMenuResult),false);
    }
    /**
     * Get GetMenuResult value
     * @return YemekSepetiStructGetMenuResult|null
     */
    public function getGetMenuResult()
    {
        return $this->GetMenuResult;
    }
    /**
     * Set GetMenuResult value
     * @param YemekSepetiStructGetMenuResult $_getMenuResult the GetMenuResult
     * @return YemekSepetiStructGetMenuResult
     */
    public function setGetMenuResult($_getMenuResult)
    {
        return ($this->GetMenuResult = $_getMenuResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetMenuResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
