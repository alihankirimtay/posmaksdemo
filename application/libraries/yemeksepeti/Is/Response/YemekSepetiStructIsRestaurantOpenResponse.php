<?php
/**
 * File for class YemekSepetiStructIsRestaurantOpenResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructIsRestaurantOpenResponse originally named IsRestaurantOpenResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructIsRestaurantOpenResponse extends YemekSepetiWsdlClass
{
    /**
     * The IsRestaurantOpenResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $IsRestaurantOpenResult;
    /**
     * Constructor method for IsRestaurantOpenResponse
     * @see parent::__construct()
     * @param boolean $_isRestaurantOpenResult
     * @return YemekSepetiStructIsRestaurantOpenResponse
     */
    public function __construct($_isRestaurantOpenResult)
    {
        parent::__construct(array('IsRestaurantOpenResult'=>$_isRestaurantOpenResult),false);
    }
    /**
     * Get IsRestaurantOpenResult value
     * @return boolean
     */
    public function getIsRestaurantOpenResult()
    {
        return $this->IsRestaurantOpenResult;
    }
    /**
     * Set IsRestaurantOpenResult value
     * @param boolean $_isRestaurantOpenResult the IsRestaurantOpenResult
     * @return boolean
     */
    public function setIsRestaurantOpenResult($_isRestaurantOpenResult)
    {
        return ($this->IsRestaurantOpenResult = $_isRestaurantOpenResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructIsRestaurantOpenResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
