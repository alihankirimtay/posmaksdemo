<?php
/**
 * File for class YemekSepetiStructSetRestaurantServiceTimeResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructSetRestaurantServiceTimeResponse originally named SetRestaurantServiceTimeResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructSetRestaurantServiceTimeResponse extends YemekSepetiWsdlClass
{
    /**
     * The SetRestaurantServiceTimeResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $SetRestaurantServiceTimeResult;
    /**
     * Constructor method for SetRestaurantServiceTimeResponse
     * @see parent::__construct()
     * @param boolean $_setRestaurantServiceTimeResult
     * @return YemekSepetiStructSetRestaurantServiceTimeResponse
     */
    public function __construct($_setRestaurantServiceTimeResult)
    {
        parent::__construct(array('SetRestaurantServiceTimeResult'=>$_setRestaurantServiceTimeResult),false);
    }
    /**
     * Get SetRestaurantServiceTimeResult value
     * @return boolean
     */
    public function getSetRestaurantServiceTimeResult()
    {
        return $this->SetRestaurantServiceTimeResult;
    }
    /**
     * Set SetRestaurantServiceTimeResult value
     * @param boolean $_setRestaurantServiceTimeResult the SetRestaurantServiceTimeResult
     * @return boolean
     */
    public function setSetRestaurantServiceTimeResult($_setRestaurantServiceTimeResult)
    {
        return ($this->SetRestaurantServiceTimeResult = $_setRestaurantServiceTimeResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructSetRestaurantServiceTimeResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
