<?php
/**
 * File for class YemekSepetiStructUpdateOrder
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructUpdateOrder originally named UpdateOrder
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructUpdateOrder extends YemekSepetiWsdlClass
{
    /**
     * The orderId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var decimal
     */
    public $orderId;
    /**
     * The orderState
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var YemekSepetiEnumOrderStates
     */
    public $orderState;
    /**
     * The reason
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $reason;
    /**
     * Constructor method for UpdateOrder
     * @see parent::__construct()
     * @param decimal $_orderId
     * @param YemekSepetiEnumOrderStates $_orderState
     * @param string $_reason
     * @return YemekSepetiStructUpdateOrder
     */
    public function __construct($_orderId,$_orderState,$_reason = NULL)
    {
        parent::__construct(array('orderId'=>$_orderId,'orderState'=>$_orderState,'reason'=>$_reason),false);
    }
    /**
     * Get orderId value
     * @return decimal
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    /**
     * Set orderId value
     * @param decimal $_orderId the orderId
     * @return decimal
     */
    public function setOrderId($_orderId)
    {
        return ($this->orderId = $_orderId);
    }
    /**
     * Get orderState value
     * @return YemekSepetiEnumOrderStates
     */
    public function getOrderState()
    {
        return $this->orderState;
    }
    /**
     * Set orderState value
     * @uses YemekSepetiEnumOrderStates::valueIsValid()
     * @param YemekSepetiEnumOrderStates $_orderState the orderState
     * @return YemekSepetiEnumOrderStates
     */
    public function setOrderState($_orderState)
    {
        if(!YemekSepetiEnumOrderStates::valueIsValid($_orderState))
        {
            return false;
        }
        return ($this->orderState = $_orderState);
    }
    /**
     * Get reason value
     * @return string|null
     */
    public function getReason()
    {
        return $this->reason;
    }
    /**
     * Set reason value
     * @param string $_reason the reason
     * @return string
     */
    public function setReason($_reason)
    {
        return ($this->reason = $_reason);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructUpdateOrder
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
