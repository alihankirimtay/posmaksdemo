<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_helper {

    protected $CI;

    private $type;
    private $bank_id = null;
    private $pos_id = null;
    private $amount = 0;
    private $increment;
    private $customer_id = 0;
    private $payment_date;
    private $desc;
    private $sender_type;
    private $recipient_type;
    private $sender_bank_id = null;
    private $sender_pos_id = null;
    private $recipient_pos_id = null;
    private $recipient_bank_id = null;



    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Transactions_M');
        $this->CI->load->model('Customers_M');
        $this->CI->load->model('Banks_M');
        $this->CI->load->model('Pos_M');

    }

    public function errorMessage()
    {
        return $this->error_message;
    }

    public function setType($type)
    {
        $this->type = (String) $type;
        return $this;
    }

    public function setBankId($bank_id)
    {
        $this->bank_id = (int) $bank_id;
        return $this;
    }

    public function setPosId($pos_id)
    {
        $this->pos_id = (int) $pos_id;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setIncrement($increment)
    {
        $this->increment = $increment;
        return $this;
    }

    public function setCustomerId($customer_id)
    {
        $this->customer_id = (int) $customer_id;
        return $this;
    }

    public function setTypeId($type_id)
    {
        $this->type_id = (int) $type_id;
        return $this;
    }

    public function setPaymentDate($payment_date)
    {
        $this->payment_date = $payment_date;
        return $this;
    }

    public function setDesc($desc)
    {
        $this->desc = $desc;
        return $this;
    }

    //Banka ve Kasalar arası transfer işlemleri için eklenen kısımlar
    public function setSenderAccountType($type)
    {
        $this->sender_type = (String) $type;
        return $this;
    }

    public function setRecipientAccountType($type)
    {
        $this->recipient_type = (String) $type;
        return $this;
    }

    public function setSenderBankId($sender_bank_id)
    {
        $this->sender_bank_id = (int) $sender_bank_id;
        return $this;
    }

    public function setSenderPosId($sender_pos_id)
    {
        $this->sender_pos_id = (int) $sender_pos_id;
        return $this;
    }

    public function setRecipientBankId($recipient_bank_id)
    {
        $this->recipient_bank_id = (int) $recipient_bank_id;
        return $this;
    }

    public function setRecipientPosId($recipient_pos_id)
    {
        $this->recipient_pos_id = (int) $recipient_pos_id;
        return $this;
    }

    public function processTransactionCustomer($isExpense = false)
    {
        $post_data = [
            'amount' => $this->amount,
            'date' => $this->payment_date,
            'increment' => $this->increment,
            'desc'  => $this->desc,
            'pos_amount' => 0.00,
            'bank_amount' => 0.00,
            'customer_amount' => 0.00,
            'type_id' => $this->type_id
        ];

        if ($this->type == "bank") {

            $post_data['bank_id'] = $this->bank_id;

        } else if($this->type == "pos") {
            
            $post_data['pos_id'] = $this->pos_id;

        }

        $this->CI->form_validation->reset_validation()->set_rules($this->CI->Transactions_M->setType($this->type)->rules['insert'])->set_data($post_data);
        if(!$this->CI->form_validation->run()) {
            throw new Exception(validation_errors());               
        }

        if (!$this->CI->Customers_M->where('id', $this->customer_id)->get()) {
            throw new Exception("Geçersiz kullanıcı.");
        }

        $data = [
            'bank_id' => null,
            'pos_id' => null,
            'customer_id' => $this->customer_id,
            'type_id' => $this->type_id,
            'related_transaction_id' => null,
            'amount' => $this->amount,
            'desc' => $this->desc,
            'increment' => null,
            'date' => dateConvert2($this->payment_date, 'server', dateFormat()),
        ];

        if ($this->increment) {
            $data['increment'] = 0; 

            if(!$isExpense) {
                $this->CI->db->where('id', $this->customer_id)->set('amount', "amount - {$this->amount}", false)->update('customers', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
            }

            if (isset($post_data['bank_id'])) {
                $this->CI->db->where('id', $this->bank_id)->set('amount', "amount + {$this->amount}", false)->update('banks', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
                
            } else if(isset($post_data['pos_id'])) {
                $this->CI->db->where('id', $this->pos_id)->set('amount', "amount + {$this->amount}", false)->update('pos', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
            }

        } else {
            $data['increment'] = 1; 
            
            if(!$isExpense) {
                $this->CI->db->where('id', $this->customer_id)->set('amount', "amount + {$this->amount}", false)->update('customers', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
            }
            
            if (isset($post_data['bank_id'])) {
                $this->CI->db->where('id', $this->bank_id)->set('amount', "amount - {$this->amount}", false)->update('banks', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
            } else if (isset($post_data['pos_id'])) {
                $this->CI->db->where('id', $this->pos_id)->set('amount', "amount - {$this->amount}", false)->update('pos', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
            }
        }

        if (isset($post_data['bank_id'])) {
            $bank_amount = $this->CI->db->select('amount')->where('id', $this->bank_id)->get('banks')->row();
            $post_data['bank_amount'] = $bank_amount->amount;
        } else if (isset($post_data['pos_id'])) {
            $pos_amount = $this->CI->db->select('amount')->where('id', $this->pos_id)->get('pos')->row();
            $post_data['pos_amount'] = $pos_amount->amount;
        }

        $customer_amount = $this->CI->db->select('amount')->where('id', $this->customer_id)->get('customers')->row();
        $data['customer_amount'] = $customer_amount->amount;

        $this->CI->db->reset_query();
        $id = $this->CI->Transactions_M->insert($post_data);

        $data['related_transaction_id'] = $id;

        $related_transaction_id = $this->CI->Transactions_M->insert($data);

        $this->CI->Transactions_M->where('id', $id)->update([
            'related_transaction_id' => $related_transaction_id
        ]);

        return $id;

    }

    public function processTransactionAccounts()
    {
        $transaction_sender = [
            'type_id' => 5,
            'amount' => $this->amount,
            'desc' => $this->desc,
            'date' => $this->payment_date,
            'increment' => 0,
            'pos_amount' => 0.00,
            'bank_amount' => 0.00
        ];

        $transaction_recipient = [
            'type_id' => 5,
            'amount' => $this->amount,
            'desc' => $this->desc,
            'date' => $this->payment_date,
            'increment' => 1,
            'pos_amount' => 0.00,
            'bank_amount' => 0.00
        ];

        if($this->sender_type == "bank") {
            $transaction_sender['bank_id'] = $this->sender_bank_id;
        } else if ($this->sender_type == "pos") {
            $transaction_sender['pos_id'] = $this->sender_pos_id;
        }

        if ($this->recipient_type == "bank") {
            $transaction_recipient['bank_id'] = $this->recipient_bank_id;
        } else if($this->recipient_type == "pos") {
            $transaction_recipient['pos_id'] = $this->recipient_pos_id;
        }
        

        $this->CI->form_validation->reset_validation()->set_rules($this->CI->Transactions_M->setType($this->sender_type)->rules['insert'])->set_data($transaction_sender);
        if(!$this->CI->form_validation->run()) {
            throw new Exception(validation_errors());               
        }

        $this->CI->form_validation->reset_validation()->set_rules($this->CI->Transactions_M->setType($this->recipient_type)->rules['insert'])->set_data($transaction_recipient);
        if(!$this->CI->form_validation->run()) {
            throw new Exception(validation_errors());               
        }

        if($this->sender_type == "bank") {

            $this->CI->db->where('id', $this->sender_bank_id)->set('amount', "amount - {$this->amount}", false)->update('banks', [
                'modified' => date("Y-m-d H:i:s"),
            ]);

            $bank_amount = $this->CI->db->select('amount')->where('id', $this->sender_bank_id)->get('banks')->row();
            $transaction_sender['bank_amount'] = $bank_amount->amount;

        } else if ($this->sender_type == "pos") {

            $this->CI->db->where('id', $this->sender_pos_id)->set('amount', "amount - {$this->amount}", false)->update('pos', [
                'modified' => date("Y-m-d H:i:s"),
            ]);

            $pos_amount = $this->CI->db->select('amount')->where('id', $this->sender_pos_id)->get('pos')->row();
            $transaction_sender['pos_amount'] = $pos_amount->amount;

        }

        $this->CI->db->reset_query();
        $transaction_sender_id = $this->CI->Transactions_M->insert($transaction_sender);

        if($this->recipient_type == "bank") {

            $this->CI->db->where('id', $this->recipient_bank_id)->set('amount', "amount + {$this->amount}", false)->update('banks', [
                'modified' => date("Y-m-d H:i:s"),
            ]);

            $bank_amount = $this->CI->db->select('amount')->where('id', $this->recipient_bank_id)->get('banks')->row();
            $transaction_recipient['bank_amount'] = $bank_amount->amount;

        } else if ($this->recipient_type == "pos") {

            $this->CI->db->where('id', $this->recipient_pos_id)->set('amount', "amount + {$this->amount}", false)->update('pos', [
                'modified' => date("Y-m-d H:i:s"),
            ]);

            $pos_amount = $this->CI->db->select('amount')->where('id', $this->recipient_pos_id)->get('pos')->row();
            $transaction_recipient['pos_amount'] = $pos_amount->amount;

        }

        $transaction_recipient['related_transaction_id'] = $transaction_sender_id;

        $transaction_recipient_id = $this->CI->Transactions_M->insert($transaction_recipient);

        $this->CI->Transactions_M->where('id', $transaction_sender_id)->update([
            'related_transaction_id' => $transaction_recipient_id
        ]);

        return $transaction_sender_id;
        
    }

}
