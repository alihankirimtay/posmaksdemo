-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.2.115    Database: posmaks_demoapp
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ys_id` varchar(32) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `address` text,
  `tax_office` varchar(100) DEFAULT NULL,
  `tax_no` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ys_id` (`ys_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `digital_keys`
--

DROP TABLE IF EXISTS `digital_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `digital_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_digital_keys_sales1_idx` (`sale_id`),
  KEY `fk_digital_keys_users1_idx` (`user_id`),
  CONSTRAINT `fk_digital_keys_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_digital_keys_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `digital_keys`
--

LOCK TABLES `digital_keys` WRITE;
/*!40000 ALTER TABLE `digital_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `digital_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount_keys`
--

DROP TABLE IF EXISTS `discount_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `session` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount_keys`
--

LOCK TABLES `discount_keys` WRITE;
/*!40000 ALTER TABLE `discount_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donations`
--

DROP TABLE IF EXISTS `donations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `ticket` varchar(45) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_donations_locations1_idx` (`location_id`),
  CONSTRAINT `fk_donations_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donations`
--

LOCK TABLES `donations` WRITE;
/*!40000 ALTER TABLE `donations` DISABLE KEYS */;
/*!40000 ALTER TABLE `donations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endofdays`
--

DROP TABLE IF EXISTS `endofdays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `endofdays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `cash` decimal(15,2) NOT NULL,
  `credit` decimal(15,2) DEFAULT NULL,
  `ticket` decimal(15,2) DEFAULT NULL,
  `sodexo` decimal(15,2) DEFAULT NULL,
  `multinet` decimal(15,2) DEFAULT NULL,
  `setcart` decimal(15,2) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_endofdays_locations1_idx` (`location_id`),
  CONSTRAINT `fk_endofdays_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endofdays`
--

LOCK TABLES `endofdays` WRITE;
/*!40000 ALTER TABLE `endofdays` DISABLE KEYS */;
/*!40000 ALTER TABLE `endofdays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_attachments`
--

DROP TABLE IF EXISTS `expense_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expense_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_id` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_expense_attachments_expenses1_idx` (`expense_id`),
  CONSTRAINT `fk_expense_attachments_expenses1` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_attachments`
--

LOCK TABLES `expense_attachments` WRITE;
/*!40000 ALTER TABLE `expense_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_types`
--

DROP TABLE IF EXISTS `expense_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expense_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_types`
--

LOCK TABLES `expense_types` WRITE;
/*!40000 ALTER TABLE `expense_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `expense_type_id` int(11) DEFAULT NULL,
  `payment_type_id` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `amount` decimal(19,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(45) DEFAULT NULL,
  `has_stock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_expenses_locations1_idx` (`location_id`),
  KEY `fk_expenses_expense_types1_idx` (`expense_type_id`),
  KEY `fk_expenses_users1_idx` (`user_id`),
  KEY `fk_expenses_payment_types1_idx` (`payment_type_id`),
  CONSTRAINT `fk_expenses_expense_types1` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_expenses_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_expenses_payment_types1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_expenses_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floors`
--

DROP TABLE IF EXISTS `floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pos_points_locations1_idx` (`location_id`),
  CONSTRAINT `fk_pos_points_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floors`
--

LOCK TABLES `floors` WRITE;
/*!40000 ALTER TABLE `floors` DISABLE KEYS */;
/*!40000 ALTER TABLE `floors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text,
  `barcode` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `warranty_exp` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_inventory_locations1_idx` (`location_id`),
  CONSTRAINT `fk_inventory_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_histories`
--

DROP TABLE IF EXISTS `inventory_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int(11) NOT NULL,
  `history` text,
  PRIMARY KEY (`id`),
  KEY `fk_inventory_histories_inventory1_idx` (`inventory_id`),
  CONSTRAINT `fk_inventory_histories_inventory1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_histories`
--

LOCK TABLES `inventory_histories` WRITE;
/*!40000 ALTER TABLE `inventory_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_template`
--

DROP TABLE IF EXISTS `invoice_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_template` (
  `location_id` int(11) NOT NULL,
  `logo` text,
  `header` text,
  `footer` text,
  KEY `fk_ticket_template_locations1_idx` (`location_id`),
  CONSTRAINT `fk_ticket_template_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_template`
--

LOCK TABLES `invoice_template` WRITE;
/*!40000 ALTER TABLE `invoice_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` text,
  `json` text,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `width` float(5,2) NOT NULL,
  `height` float(5,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invoices_locations1_idx` (`location_id`),
  CONSTRAINT `fk_invoices_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_has_products`
--

DROP TABLE IF EXISTS `location_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_has_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(15,4) DEFAULT NULL,
  `tax_inclusive` tinyint(1) NOT NULL DEFAULT '1',
  `master` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ürün bu lokasyon için master ürün mü?\nestimated sale hesaplarken kullanılıyor.',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_locations_has_products_products1_idx` (`product_id`),
  KEY `fk_locations_has_products_locations1_idx` (`location_id`),
  CONSTRAINT `fk_locations_has_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_locations_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_has_products`
--

LOCK TABLES `location_has_products` WRITE;
/*!40000 ALTER TABLE `location_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_losts`
--

DROP TABLE IF EXISTS `location_losts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_losts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `unsold` int(11) NOT NULL DEFAULT '0' COMMENT 'pictures printed but not sold',
  `waste` int(11) NOT NULL DEFAULT '0' COMMENT 'test pictures',
  `comps` int(11) NOT NULL DEFAULT '0' COMMENT 'pictures given for free',
  `unseen` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_donations_locations1_idx` (`location_id`),
  CONSTRAINT `fk_donations_locations1000` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_losts`
--

LOCK TABLES `location_losts` WRITE;
/*!40000 ALTER TABLE `location_losts` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_losts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `currency` varchar(5) DEFAULT NULL,
  `prepaid_limit` int(11) NOT NULL DEFAULT '0' COMMENT 'kullanıcının resimleri bu limit sayısına eriştiğinde zip-boox üzerinde albüm oluşturlulur.',
  `force_sale_via_ticket` tinyint(1) NOT NULL DEFAULT '0',
  `autoalbum_time` time DEFAULT NULL,
  `autoalbum_active` tinyint(1) NOT NULL DEFAULT '0',
  `dtz` varchar(100) NOT NULL DEFAULT 'UTC',
  `yemeksepeti` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'POSMAKS','','₺',0,0,'17:01:00',0,'Europe/Istanbul','{\"username\":\"\",\"password\":\"\"}','2016-08-11 14:02:05','2018-04-11 13:04:11',1);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `message` text,
  `json` text,
  `ip` varchar(16) DEFAULT NULL,
  `url_coming` varchar(255) DEFAULT NULL,
  `url_current` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_logs_users1_idx` (`user_id`),
  KEY `fk_logs_locations1_idx` (`location_id`),
  CONSTRAINT `fk_logs_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_logs_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reciever` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  `viewed` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_messages_users1_idx` (`reciever`),
  KEY `fk_messages_users2_idx` (`sender`),
  CONSTRAINT `fk_messages_users1` FOREIGN KEY (`reciever`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_messages_users2` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_has_products`
--

DROP TABLE IF EXISTS `package_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_has_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `production_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `price` decimal(15,2) NOT NULL,
  `content` tinyint(1) NOT NULL DEFAULT '1',
  `digital` tinyint(1) DEFAULT NULL,
  `is_video` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `completed` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `tax` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_package_has_products_sale_has_products1_idx` (`package_id`),
  KEY `fk_package_has_products_products1_idx` (`product_id`),
  KEY `fk_package_has_products_productions1_idx` (`production_id`),
  CONSTRAINT `fk_package_has_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_package_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_package_has_products_sale_has_products1` FOREIGN KEY (`package_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_has_products`
--

LOCK TABLES `package_has_products` WRITE;
/*!40000 ALTER TABLE `package_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `package_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paxnegs`
--

DROP TABLE IF EXISTS `paxnegs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paxnegs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `pax` int(11) NOT NULL DEFAULT '0',
  `negs` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_donations_locations1_idx` (`location_id`),
  CONSTRAINT `fk_donations_locations100` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paxnegs`
--

LOCK TABLES `paxnegs` WRITE;
/*!40000 ALTER TABLE `paxnegs` DISABLE KEYS */;
/*!40000 ALTER TABLE `paxnegs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS `payment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_types`
--

LOCK TABLES `payment_types` WRITE;
/*!40000 ALTER TABLE `payment_types` DISABLE KEYS */;
INSERT INTO `payment_types` VALUES (1,'Nakit','cash','fa fa-money','btn-success','2015-11-02 00:00:00',NULL,1),(2,'Kredi Kartı','credit','fa fa-credit-card','btn-primary','2015-11-06 00:00:00',NULL,1),(3,'Ticket','ticket','fa fa-ticket','btn-danger','2015-11-06 00:00:00',NULL,1),(4,'Sodexo','sodexo','fa fa-scribd','btn-light-blue','2015-12-26 00:00:00',NULL,1),(5,'Multinet','multinet','fa fa-credit-card','btn-light-green','2015-12-26 00:00:00',NULL,1),(6,'Setcart','setcart','fa fa-credit-card-alt','btn-primary','2016-12-07 00:00:00',NULL,1);
/*!40000 ALTER TABLE `payment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(100) NOT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'locations.index','Restoranlar','2017-12-12 00:00:00',NULL,1),(2,'locations.add','Restoran oluştur','2017-12-12 00:00:00',NULL,1),(3,'locations.edit','Restoran düzenle','2017-12-12 00:00:00',NULL,1),(4,'locations.delete','Restoran sil','2017-12-12 00:00:00',NULL,1),(5,'points.index','Masalar','2017-12-12 00:00:00',NULL,1),(6,'points.add','Masa oluştur','2017-12-12 00:00:00',NULL,1),(7,'points.edit','Masa düzenle','2017-12-12 00:00:00',NULL,1),(8,'points.delete','Masa sil','2017-12-12 00:00:00',NULL,1),(9,'employes.index','Personeller','2017-12-12 00:00:00',NULL,1),(10,'employes.add','Personel oluştur','2017-12-12 00:00:00',NULL,1),(11,'employes.edit','Personel düzenle','2017-12-12 00:00:00',NULL,1),(12,'employes.delete','Personel sil','2017-12-12 00:00:00',NULL,1),(13,'customers.index','Müşteriler','2017-12-12 00:00:00',NULL,1),(14,'customers.add','Müşteri oluştur','2017-12-12 00:00:00',NULL,1),(15,'customers.edit','Müşteri düzenle','2017-12-12 00:00:00',NULL,1),(16,'customers.delete','Müşteri sil','2017-12-12 00:00:00',NULL,1),(17,'products.index','Ürünler','2017-12-12 00:00:00',NULL,1),(18,'products.add','Ürün oluştur','2017-12-12 00:00:00',NULL,1),(19,'products.edit','Ürün düzenle','2017-12-12 00:00:00',NULL,1),(20,'products.delete','Ürün sil','2017-12-12 00:00:00',NULL,1),(21,'reports.index','Raporlar Özet','2017-12-12 00:00:00',NULL,1),(22,'reports.home','Raporlar Satışlar','2017-12-12 00:00:00',NULL,1),(23,'reports.daily','Raporlar Kasiyerler','2017-12-12 00:00:00',NULL,1),(24,'reports.monthly','Raporlar Aylık Kasa','2017-12-12 00:00:00',NULL,1),(25,'reports.status','Raporlar Gelir-Gider','2017-12-12 00:00:00',NULL,1),(26,'reports.stock','Raporlar Stok','2017-12-12 00:00:00',NULL,1),(27,'reports.endofday','Raporlar Z-Raporu','2017-12-12 00:00:00',NULL,1),(28,'reports.fullness','Raporlar Yoğunluk','2017-12-12 00:00:00',NULL,1),(29,'reports.prim','Raporlar Prim Raporu','2017-12-12 00:00:00',NULL,1),(30,'roles.index','Yetkiler','2017-12-12 00:00:00',NULL,1),(31,'roles.add','Rol oluştur','2017-12-12 00:00:00',NULL,1),(32,'roles.edit','Rol düzenle','2017-12-12 00:00:00',NULL,1),(33,'roles.delete','Rol sil','2017-12-12 00:00:00',NULL,1),(34,'roles.permission','Yetkileri yönet','2017-12-12 00:00:00',NULL,1),(35,'configuration.index','Uygulama Ayarları','2017-12-12 00:00:00',NULL,1),(36,'expenses.index','Giderler','2017-12-12 00:00:00',NULL,1),(37,'expenses.add','Gider oluştur','2017-12-12 00:00:00',NULL,1),(38,'expenses.edit','Gider düzenle','2017-12-12 00:00:00',NULL,1),(39,'expenses.addwithstock','Gider oluştur (Faturalı)','2017-12-12 00:00:00',NULL,1),(40,'expenses.editwithstock','Gider düzenle (Faturalı)','2017-12-12 00:00:00',NULL,1),(41,'pointofsales.index','POS','2017-12-12 00:00:00',NULL,1),(42,'pointofsales.setup','POS kasa kurulumu','2017-12-12 00:00:00',NULL,1),(43,'pointofsales.return','POS ürün iade','2017-12-12 00:00:00',NULL,1),(44,'pointofsales.adddiscount','POS indirim ekleme','2017-12-12 00:00:00',NULL,1),(45,'pointofsales.orders','POS mutfak siparişleri','2017-12-12 00:00:00',NULL,1),(46,'pointofsales.packageserviceorders','POS paket servis takibi','2017-12-12 00:00:00',NULL,1),(47,'expensetypes.index','Gider tipleri','2017-12-12 00:00:00',NULL,1),(48,'expensetypes.add','Gider tipi oluştur','2017-12-12 00:00:00',NULL,1),(49,'expensetypes.edit','Gider tipi düzenle','2017-12-12 00:00:00',NULL,1),(50,'expensetypes.delete','Gider tipi sil','2017-12-12 00:00:00',NULL,1),(51,'sales.index','Satışlar','2017-12-12 00:00:00',NULL,1),(52,'sales.add','Satış oluştur','2017-12-12 00:00:00',NULL,1),(53,'sales.edit','Satış düzenle','2017-12-12 00:00:00',NULL,1),(54,'sales.delete','Satış sil','2017-12-12 00:00:00',NULL,1),(55,'sales.view','Satış faturası','2017-12-12 00:00:00',NULL,1),(56,'pos.index','Kasalar','2017-12-12 00:00:00',NULL,1),(57,'pos.add','Kasa oluştur','2017-12-12 00:00:00',NULL,1),(58,'pos.edit','Kasa düzenle','2017-12-12 00:00:00',NULL,1),(59,'pos.delete','Kasa sil','2017-12-12 00:00:00',NULL,1),(60,'inventory.index','Envanterler','2017-12-12 00:00:00',NULL,1),(61,'inventory.add','Envanter oluştur','2017-12-12 00:00:00',NULL,1),(62,'inventory.edit','Envanter düzenle','2017-12-12 00:00:00',NULL,1),(63,'inventory.delete','Envanter sil','2017-12-12 00:00:00',NULL,1),(64,'inventory.history','Envanter geçmişi','2017-12-12 00:00:00',NULL,1),(65,'stocks.index','Stoklar','2017-12-12 00:00:00',NULL,1),(66,'stocks.add','Stok oluştur','2017-12-12 00:00:00',NULL,1),(67,'stocks.edit','Stok düzenle','2017-12-12 00:00:00',NULL,1),(68,'stocks.delete','Stok sil','2017-12-12 00:00:00',NULL,1),(69,'stockcontents.index','Stok işlemleri','2017-12-12 00:00:00',NULL,1),(70,'stockcontents.add','Stok işlemi oluştur','2017-12-12 00:00:00',NULL,1),(71,'stockcontents.edit','Stok işlemi düzenle','2017-12-12 00:00:00',NULL,1),(72,'stockcontents.delete','Stok işlemi sil','2017-12-12 00:00:00',NULL,1),(73,'floors.index','Katlar','2017-12-12 00:00:00',NULL,1),(74,'floors.add','Kat oluştur','2017-12-12 00:00:00',NULL,1),(75,'floors.edit','Kat düzenle','2017-12-12 00:00:00',NULL,1),(76,'floors.delete','Kat sil','2017-12-12 00:00:00',NULL,1),(77,'producttypes.index','Ürün tipleri','2017-12-12 00:00:00',NULL,1),(78,'producttypes.add','Ürün tipi oluştur','2017-12-12 00:00:00',NULL,1),(79,'producttypes.edit','Ürün tipi düzenle','2017-12-12 00:00:00',NULL,1),(80,'producttypes.delete','Ürün tipi sil','2017-12-12 00:00:00',NULL,1),(81,'zones.index','Bölgeler','2017-12-12 00:00:00',NULL,1),(82,'zones.add','Bölge ekle','2017-12-12 00:00:00',NULL,1),(83,'zones.edit','Bölge düzenle','2017-12-12 00:00:00',NULL,1),(84,'zones.delete','Bölge sil','2017-12-12 00:00:00',NULL,1),(85,'shiftschedule.index','Vardiyalar','2017-12-12 00:00:00',NULL,1),(86,'shiftschedule.add','Vardiya oluştur','2017-12-12 00:00:00',NULL,1),(87,'shiftschedule.edit','Vardiya düzenle','2017-12-12 00:00:00',NULL,1),(88,'shiftschedule.delete','Vardiya sil','2017-12-12 00:00:00',NULL,1),(89,'sales.scoreboard','Günlük Performans','2017-12-12 00:00:00',NULL,1),(90,'mobilpos.index','Garson Terminal','2017-12-12 00:00:00',NULL,1),(91,'mobilpos.tablet','Müşteri Terminal','2017-12-12 00:00:00',NULL,1),(92,'points.planner','Masa düzeni','2017-12-12 00:00:00',NULL,1),(93,'pointofsales.tables','Açık Hesaplar','2017-12-12 00:00:00',NULL,1),(94,'syslogs.index','Loglar','2017-12-12 00:00:00',NULL,1),(95,'invoices.index','Fatura şablonları','2017-12-12 00:00:00',NULL,1),(96,'invoices.add','Fatura şablonu oluştur','2017-12-12 00:00:00',NULL,1),(97,'invoices.edit','Fatura şablonu düzenle','2017-12-12 00:00:00',NULL,1),(98,'invoices.delete','Fatura şablonu sil','2017-12-12 00:00:00',NULL,1),(99,'productions.index','Üretim yerleri','2017-12-12 00:00:00',NULL,1),(100,'productions.add','Üretim yeri oluştur','2017-12-12 00:00:00',NULL,1),(101,'productions.edit','Üretim yeri düzenle','2017-12-12 00:00:00',NULL,1),(102,'productions.delete','Üretim yeri sil','2017-12-12 00:00:00',NULL,1),(103,'pointofsales.approvesession','Kasa sayımı','2017-12-12 00:00:00',NULL,1),(104,'pointofsales.packageservicebutton','POS paket servis','2017-12-12 00:00:00',NULL,1),(105,'pointofsales.selfservicebutton','POS self servis','2017-12-12 00:00:00',NULL,1),(106,'pointofsales.productdelete','POS ürün silme','2017-12-12 00:00:00',NULL,1),(107,'pointofsales.productedit','POS ürün düzenleme','2017-12-12 00:00:00',NULL,1),(108,'pointofsales.check','POS Hesap Alma','2017-12-12 00:00:00',NULL,1),(109,'pointofsales.interimpayment','POS ara ödeme','2017-12-12 00:00:00',NULL,1),(110,'pointofsales.tablemove','POS Masa taşıma','2017-12-12 00:00:00',NULL,1),(111,'pointofsales.tablecancel','POS Masa İptal','2017-12-12 00:00:00',NULL,1),(112,'pointofsales.changepos','POS Kasa değiştirme','2017-12-12 00:00:00',NULL,1),(113,'pointofsales.forceproductdecrement','POS Hazırlanmış ürünü silme','2017-12-12 00:00:00',NULL,1);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `folder` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_photos_locations1_idx` (`location_id`),
  CONSTRAINT `fk_photos_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_histories`
--

DROP TABLE IF EXISTS `point_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `point_id` int(11) NOT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pıint_histories_points1_idx` (`point_id`),
  CONSTRAINT `fk_pıint_histories_points1` FOREIGN KEY (`point_id`) REFERENCES `points` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_histories`
--

LOCK TABLES `point_histories` WRITE;
/*!40000 ALTER TABLE `point_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `point_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `planner_settings` text COMMENT 'masa düzeni düzenleyicisindeki ayarları',
  `status` tinyint(1) NOT NULL COMMENT '1: dolu, 0: boş',
  `opened_at` datetime DEFAULT NULL,
  `service_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=table, 2=package, 3=self',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_cameras_locations1_idx` (`location_id`),
  KEY `fk_points_zones1_idx` (`zone_id`),
  KEY `fk_points_floors1_idx` (`floor_id`),
  KEY `fk_points_sales1_idx` (`sale_id`),
  CONSTRAINT `fk_cameras_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_points_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_points_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_points_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points`
--

LOCK TABLES `points` WRITE;
/*!40000 ALTER TABLE `points` DISABLE KEYS */;
/*!40000 ALTER TABLE `points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos`
--

DROP TABLE IF EXISTS `pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pos_points_locations1_idx` (`location_id`),
  CONSTRAINT `fk_pos_points_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos`
--

LOCK TABLES `pos` WRITE;
/*!40000 ALTER TABLE `pos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer_has_counts`
--

DROP TABLE IF EXISTS `printer_has_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer_has_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `printer_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_printer_has_counts_printers1_idx` (`printer_id`),
  CONSTRAINT `fk_printer_has_counts_printers1` FOREIGN KEY (`printer_id`) REFERENCES `printers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printer_has_counts`
--

LOCK TABLES `printer_has_counts` WRITE;
/*!40000 ALTER TABLE `printer_has_counts` DISABLE KEYS */;
/*!40000 ALTER TABLE `printer_has_counts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printers`
--

DROP TABLE IF EXISTS `printers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `desc` text,
  `equal` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(150) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_donations_locations1_idx` (`location_id`),
  CONSTRAINT `fk_donations_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printers`
--

LOCK TABLES `printers` WRITE;
/*!40000 ALTER TABLE `printers` DISABLE KEYS */;
/*!40000 ALTER TABLE `printers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_has_stocks`
--

DROP TABLE IF EXISTS `product_has_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_stocks` (
  `product_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` decimal(15,2) DEFAULT NULL,
  `optional` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_id`,`stock_id`),
  KEY `fk_products_stocks_stocks1_idx` (`stock_id`),
  KEY `fk_products_stocks_products1_idx` (`product_id`),
  CONSTRAINT `fk_products_stocks_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_stocks`
--

LOCK TABLES `product_has_stocks` WRITE;
/*!40000 ALTER TABLE `product_has_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_has_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL COMMENT 'parent product type id',
  `title` varchar(45) NOT NULL,
  `desc` text,
  `sort` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_types_product_types1_idx` (`parent_id`),
  CONSTRAINT `fk_product_types_product_types1` FOREIGN KEY (`parent_id`) REFERENCES `product_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_types`
--

LOCK TABLES `product_types` WRITE;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productions`
--

DROP TABLE IF EXISTS `productions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productions`
--

LOCK TABLES `productions` WRITE;
/*!40000 ALTER TABLE `productions` DISABLE KEYS */;
/*!40000 ALTER TABLE `productions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_id` int(11) NOT NULL,
  `portion_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `type` enum('package','product','ves') NOT NULL COMMENT 'ves: özel indirimli paket ürünüdür.',
  `digital` tinyint(1) NOT NULL DEFAULT '0',
  `is_video` tinyint(1) NOT NULL DEFAULT '0',
  `content` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ürün içeriğindeki sayı',
  `color` varchar(15) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `tax_inclusive` tinyint(1) DEFAULT NULL,
  `master` tinyint(1) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `tax` decimal(15,4) NOT NULL,
  `bonus` decimal(15,2) NOT NULL,
  `bonus_type` varchar(10) NOT NULL DEFAULT 'value' COMMENT 'value or percent',
  PRIMARY KEY (`id`),
  KEY `fk_products_locations1_idx` (`location_id`),
  KEY `fk_products_product_types1_idx` (`product_type_id`),
  KEY `fk_products_productions1_idx` (`production_id`),
  KEY `fk_products_products1_idx` (`portion_id`),
  CONSTRAINT `fk_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_product_types1` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_products1` FOREIGN KEY (`portion_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_additional_products`
--

DROP TABLE IF EXISTS `products_additional_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_additional_products` (
  `product_id` int(11) NOT NULL,
  `additional_product_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`additional_product_id`),
  KEY `fk_products_products_products2_idx` (`additional_product_id`),
  KEY `fk_products_products_products1_idx` (`product_id`),
  CONSTRAINT `fk_products_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_products_products2` FOREIGN KEY (`additional_product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_additional_products`
--

LOCK TABLES `products_additional_products` WRITE;
/*!40000 ALTER TABLE `products_additional_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_additional_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_has_products`
--

DROP TABLE IF EXISTS `products_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_has_products` (
  `package_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  KEY `fk_products_has_products_products2_idx` (`product_id`),
  KEY `fk_products_has_products_products1_idx` (`package_id`),
  CONSTRAINT `fk_products_has_products_products1` FOREIGN KEY (`package_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_has_products_products2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_has_products`
--

LOCK TABLES `products_has_products` WRITE;
/*!40000 ALTER TABLE `products_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `return_keys`
--

DROP TABLE IF EXISTS `return_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `return_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `return_keys`
--

LOCK TABLES `return_keys` WRITE;
/*!40000 ALTER TABLE `return_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `return_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  KEY `fk_roles_has_permissions_permissions1_idx` (`permission_id`),
  KEY `fk_roles_has_permissions_roles1_idx` (`role_id`),
  CONSTRAINT `fk_roles_has_permissions_permissions1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_roles_has_permissions_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `editable` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','admin',0,'2015-10-23 00:00:00',NULL,1),(2,'Kurye','courier',0,'2016-08-04 07:48:53','2016-08-04 07:51:08',1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_attachments`
--

DROP TABLE IF EXISTS `sale_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sale_attachments_sales1_idx` (`sale_id`),
  CONSTRAINT `fk_sale_attachments_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_attachments`
--

LOCK TABLES `sale_attachments` WRITE;
/*!40000 ALTER TABLE `sale_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_has_products`
--

DROP TABLE IF EXISTS `sale_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_has_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `additional_sale_product_id` int(11) DEFAULT NULL,
  `production_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL COMMENT 'vergisiz fiyatı',
  `content` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ürün içeriğindeki sayı',
  `digital` tinyint(1) DEFAULT NULL COMMENT 'ürün usb mi?',
  `is_video` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('package','product','ves') DEFAULT NULL COMMENT 'ves:özel indirimli paket ürünüdür.',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `return_quantity` int(11) NOT NULL COMMENT 'iade edilen adet',
  `completed` int(11) NOT NULL COMMENT 'tamamlanan sipariş(quantity) adedi',
  `delivered` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL COMMENT 'garson siparişi gördüğünde (delivered den dolayı ihtiyaç kalmadı)',
  `paid_cash` int(11) NOT NULL COMMENT 'ara ödeme adedi',
  `paid_credit` int(11) NOT NULL COMMENT 'ara ödeme adedi',
  `paid_ticket` int(11) NOT NULL COMMENT 'ara ödeme adedi',
  `paid_sodexo` int(11) NOT NULL,
  `paid_multinet` int(11) NOT NULL,
  `paid_setcart` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `tax` decimal(15,4) NOT NULL,
  `bonus` decimal(15,2) NOT NULL,
  `bonus_type` varchar(10) NOT NULL DEFAULT 'value' COMMENT 'value or percent',
  `is_gift` tinyint(1) NOT NULL COMMENT 'ikram',
  PRIMARY KEY (`id`),
  KEY `fk_sales_has_products_products1_idx` (`product_id`),
  KEY `fk_sales_has_products_sales1_idx` (`sale_id`),
  KEY `fk_sale_has_products_sale_has_products1_idx` (`additional_sale_product_id`),
  KEY `fk_sale_has_products_users1_idx` (`user_id`),
  KEY `fk_sale_has_products_productions1_idx` (`production_id`),
  CONSTRAINT `fk_sale_has_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_has_products_sale_has_products1` FOREIGN KEY (`additional_sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_has_products_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sales_has_products_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_has_products`
--

LOCK TABLES `sale_has_products` WRITE;
/*!40000 ALTER TABLE `sale_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_packages_stocks`
--

DROP TABLE IF EXISTS `sale_packages_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_packages_stocks` (
  `sale_package_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `in_use` tinyint(1) NOT NULL,
  `optional` tinyint(1) NOT NULL,
  PRIMARY KEY (`sale_package_id`,`stock_id`),
  KEY `fk_sale_packages_stocks_package_has_products1_idx` (`sale_package_id`),
  KEY `fk_sale_packages_stocks_stocks1_idx` (`stock_id`),
  CONSTRAINT `fk_sale_packages_stocks_package_has_products1` FOREIGN KEY (`sale_package_id`) REFERENCES `package_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_packages_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_packages_stocks`
--

LOCK TABLES `sale_packages_stocks` WRITE;
/*!40000 ALTER TABLE `sale_packages_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_packages_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_product_notes`
--

DROP TABLE IF EXISTS `sale_product_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_product_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_product_id` int(11) DEFAULT NULL,
  `note` text,
  `sound` varchar(255) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1= text, 2= sound',
  `viewed` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sale_product_notes_sale_has_products1_idx` (`sale_product_id`),
  CONSTRAINT `fk_sale_product_notes_sale_has_products1` FOREIGN KEY (`sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_product_notes`
--

LOCK TABLES `sale_product_notes` WRITE;
/*!40000 ALTER TABLE `sale_product_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_product_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_products_stocks`
--

DROP TABLE IF EXISTS `sale_products_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_products_stocks` (
  `sale_product_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  `in_use` tinyint(1) NOT NULL,
  `optional` tinyint(1) NOT NULL,
  PRIMARY KEY (`sale_product_id`,`stock_id`),
  KEY `fk_sale_products_stocks_sale_has_products1_idx` (`sale_product_id`),
  KEY `fk_sale_products_stocks_stocks1_idx` (`stock_id`),
  CONSTRAINT `fk_sale_products_stocks_sale_has_products1` FOREIGN KEY (`sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_products_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_products_stocks`
--

LOCK TABLES `sale_products_stocks` WRITE;
/*!40000 ALTER TABLE `sale_products_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_products_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_types`
--

DROP TABLE IF EXISTS `sale_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_types`
--

LOCK TABLES `sale_types` WRITE;
/*!40000 ALTER TABLE `sale_types` DISABLE KEYS */;
INSERT INTO `sale_types` VALUES (1,'Payment','Payment','2015-11-06 00:00:00','2015-11-06 13:38:26',1),(2,'Sales Receipt','Sales Receipt','2015-11-06 00:00:00','2015-11-06 13:39:44',1),(3,'Invoice','Invoice','2015-11-06 13:24:59','2015-11-06 13:38:06',1),(5,'Estimate','Estimate','2015-11-06 13:37:56',NULL,1);
/*!40000 ALTER TABLE `sale_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `point_id` int(11) DEFAULT NULL COMMENT 'masa',
  `sale_type_id` int(11) NOT NULL,
  `payment_type_id` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `completer_user_id` int(11) DEFAULT NULL COMMENT 'hesabı alan kullanıcı',
  `customer_id` int(11) DEFAULT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `ticket_number` varchar(45) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL COMMENT 'toplam tutar',
  `subtotal` decimal(15,2) DEFAULT NULL COMMENT 'aratoplam= vergisiz ve indirimsiz hali',
  `taxtotal` decimal(15,2) NOT NULL,
  `change` decimal(15,2) DEFAULT NULL COMMENT 'ödenen para üstü',
  `gift_amount` decimal(15,2) NOT NULL,
  `interim_payment_amount` decimal(15,2) NOT NULL,
  `cash` decimal(15,2) DEFAULT NULL COMMENT 'ödenen nakit miktarı',
  `credit` decimal(15,2) DEFAULT NULL COMMENT 'ödenen k kart''ı miktarı',
  `ticket` decimal(15,2) DEFAULT NULL COMMENT 'bina kasasına ödenen miktar',
  `sodexo` decimal(15,2) DEFAULT NULL COMMENT 'ödenen kupon ',
  `multinet` decimal(15,2) DEFAULT NULL,
  `setcart` decimal(15,2) DEFAULT NULL,
  `discount_amount` decimal(15,2) DEFAULT NULL,
  `discount_type` enum('amount','percent') DEFAULT NULL COMMENT 'yüzdelik veya parasal değer indirimi.',
  `title` varchar(45) NOT NULL,
  `desc` text,
  `return` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'asıl satışta iade var mı?',
  `return_id` int(11) DEFAULT NULL COMMENT 'iadenin ait olduğu asıl satışın id si.',
  `status` enum('pending','cancelled','completed','shipped') DEFAULT NULL,
  `service_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:table, 2:package, 3:self (check points table)',
  `call_account` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_sales_locations1_idx` (`location_id`),
  KEY `fk_sales_payment_types1_idx` (`payment_type_id`),
  KEY `fk_sales_users1_idx` (`user_id`),
  KEY `fk_sales_sale_types1_idx` (`sale_type_id`),
  KEY `fk_sales_sales1_idx` (`return_id`),
  KEY `index_payment_date` (`payment_date`),
  KEY `fk_sales_pos1_idx` (`pos_id`),
  KEY `fk_sales_points1_idx` (`point_id`),
  KEY `fk_sales_floors1_idx` (`floor_id`),
  KEY `fk_sales_zones1_idx` (`zone_id`),
  KEY `fk_sales_users2_idx` (`completer_user_id`),
  KEY `fk_sales_customers1_idx` (`customer_id`),
  KEY `fk_sales_users3_idx` (`courier_id`),
  KEY `fk_sales_invoices1_idx` (`invoice_id`),
  CONSTRAINT `fk_sales_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_invoices1` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_payment_types1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_points1` FOREIGN KEY (`point_id`) REFERENCES `points` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_pos1` FOREIGN KEY (`pos_id`) REFERENCES `pos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_sale_types1` FOREIGN KEY (`sale_type_id`) REFERENCES `sale_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_sales1` FOREIGN KEY (`return_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sales_users2` FOREIGN KEY (`completer_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_users3` FOREIGN KEY (`courier_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_ys_orders`
--

DROP TABLE IF EXISTS `sales_ys_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_ys_orders` (
  `sale_id` int(11) NOT NULL,
  `ys_order_id` varchar(32) NOT NULL,
  KEY `idx_ys_order_id` (`ys_order_id`),
  KEY `fk_sales_ys_orders_sales1_idx` (`sale_id`),
  CONSTRAINT `fk_sales_ys_orders_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_ys_orders`
--

LOCK TABLES `sales_ys_orders` WRITE;
/*!40000 ALTER TABLE `sales_ys_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_ys_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `manager_id` int(11) DEFAULT NULL COMMENT '	kasayı teslim alan yönetici',
  `status` tinyint(1) DEFAULT NULL COMMENT '1=approved, 0=disapproved, NULL=open',
  `cash` decimal(15,2) NOT NULL,
  `credit` decimal(15,2) NOT NULL,
  `ticket` decimal(15,2) NOT NULL,
  `sodexo` decimal(15,2) NOT NULL,
  `multinet` decimal(15,2) NOT NULL,
  `setcart` decimal(15,2) NOT NULL,
  `cash_exists` decimal(15,2) NOT NULL COMMENT 'Eldeki Gerçek değerler (tüm _exists ler)',
  `credit_exists` decimal(15,2) NOT NULL,
  `ticket_exists` decimal(15,2) NOT NULL,
  `sodexo_exists` decimal(15,2) NOT NULL,
  `multinet_exists` decimal(15,2) NOT NULL,
  `setcart_exists` decimal(15,2) NOT NULL,
  `cash_opening` decimal(15,2) NOT NULL,
  `values` text COMMENT 'money data as json',
  `description` text,
  `auto_closed` tinyint(1) NOT NULL COMMENT 'auto closed by cron job 1: auto closed, 2: edited	',
  `closed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sessions_locations1_idx` (`location_id`),
  KEY `fk_sessions_users1_idx` (`user_id`),
  KEY `fk_sessions_users2_idx` (`manager_id`),
  CONSTRAINT `fk_sessions_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessions_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessions_users2` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `company` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `dtz` varchar(45) DEFAULT NULL,
  `date_long` varchar(45) DEFAULT NULL,
  `date_short` varchar(45) DEFAULT NULL,
  `screen_saver` varchar(255) DEFAULT NULL,
  `smtp_host` varchar(150) DEFAULT NULL,
  `smtp_user` varchar(150) DEFAULT NULL,
  `smtp_pass` varchar(150) DEFAULT NULL,
  `smtp_port` int(3) DEFAULT NULL,
  `is_ssl` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `virtual_keyboard` tinyint(1) NOT NULL,
  `date_expire` datetime DEFAULT NULL COMMENT 'Sistem üyelik bitiş tarihi',
  `api_token` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'POSMAKS','POSMAKS','http://localhost','eoruc@kodar.com.tr','America/New_York','F d, Y','j-m-Y','assets/uploads/screen_saver/06ebecb427cf0764067a6335b48d062b.jpg','http://in-v3.mailjet.com','31fe03caa4df1ba6001151033000b9f6','8fd4c121094e9ca5e36e9a12fe3beca5',587,0,1,1,'2018-10-31 18:07:38','7b52009b64fd0a2a49e6d8a939753077792b0554');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shift_schedule`
--

DROP TABLE IF EXISTS `shift_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shift_schedule_users1_idx` (`user_id`),
  KEY `fk_shift_schedule_locations1_idx` (`location_id`),
  CONSTRAINT `fk_shift_schedule_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shift_schedule_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shift_schedule`
--

LOCK TABLES `shift_schedule` WRITE;
/*!40000 ALTER TABLE `shift_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `shift_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sound_notes`
--

DROP TABLE IF EXISTS `sound_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sound_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_new` tinyint(1) DEFAULT NULL COMMENT '1: new, 0 or NULL: old',
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sound_notes_sales1_idx` (`sale_id`),
  CONSTRAINT `fk_sound_notes_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sound_notes`
--

LOCK TABLES `sound_notes` WRITE;
/*!40000 ALTER TABLE `sound_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sound_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_contents`
--

DROP TABLE IF EXISTS `stock_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `expense_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,2) NOT NULL COMMENT 'eklenen miktar',
  `price` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `desc` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_contents_users1_idx` (`user_id`),
  KEY `fk_stock_contents_stocks1_idx` (`stock_id`),
  KEY `fk_stock_contents_expenses1_idx` (`expense_id`),
  CONSTRAINT `fk_stock_contents_expenses1` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_contents_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_contents_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_contents`
--

LOCK TABLES `stock_contents` WRITE;
/*!40000 ALTER TABLE `stock_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_counts`
--

DROP TABLE IF EXISTS `stock_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_counts_locations1_idx` (`location_id`),
  KEY `fk_stock_counts_users1_idx` (`user_id`),
  CONSTRAINT `fk_stock_counts_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_counts_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_counts`
--

LOCK TABLES `stock_counts` WRITE;
/*!40000 ALTER TABLE `stock_counts` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_counts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text,
  `quantity` decimal(15,2) NOT NULL COMMENT 'güncel miktar',
  `sub_limit` decimal(15,2) NOT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stocks_locations1_idx` (`location_id`),
  CONSTRAINT `fk_stocks_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks_stock_counts`
--

DROP TABLE IF EXISTS `stocks_stock_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks_stock_counts` (
  `stock_id` int(11) NOT NULL,
  `stock_count_id` int(11) NOT NULL,
  `hand_amount` decimal(15,2) NOT NULL,
  `system_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`stock_id`,`stock_count_id`),
  KEY `fk_stocks_stock_counts_stock_counts1_idx` (`stock_count_id`),
  CONSTRAINT `fk_stocks_stock_counts_stock_counts1` FOREIGN KEY (`stock_count_id`) REFERENCES `stock_counts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stocks_stock_counts_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks_stock_counts`
--

LOCK TABLES `stocks_stock_counts` WRITE;
/*!40000 ALTER TABLE `stocks_stock_counts` DISABLE KEYS */;
/*!40000 ALTER TABLE `stocks_stock_counts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_template`
--

DROP TABLE IF EXISTS `ticket_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_template` (
  `location_id` int(11) NOT NULL,
  `logo` text,
  `header` text,
  `footer` text,
  KEY `fk_ticket_template_locations1_idx` (`location_id`),
  CONSTRAINT `fk_ticket_template_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_template`
--

LOCK TABLES `ticket_template` WRITE;
/*!40000 ALTER TABLE `ticket_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_activation`
--

DROP TABLE IF EXISTS `user_has_activation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_activation` (
  `user_id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activation_code` varchar(45) NOT NULL,
  KEY `fk_user_has_activation_users1_idx` (`user_id`),
  CONSTRAINT `fk_user_has_activation_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_activation`
--

LOCK TABLES `user_has_activation` WRITE;
/*!40000 ALTER TABLE `user_has_activation` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_has_activation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_locations`
--

DROP TABLE IF EXISTS `user_has_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_locations` (
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  KEY `fk_users_has_locations_locations1_idx` (`location_id`),
  KEY `fk_users_has_locations_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_locations_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_locations_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_locations`
--

LOCK TABLES `user_has_locations` WRITE;
/*!40000 ALTER TABLE `user_has_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_has_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  `password_token` varchar(50) DEFAULT NULL COMMENT 'new password key.\nForgot password template: forgot.{value}\nChange password template: change.{value}',
  `image` varchar(150) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'isim/soyisim',
  `lastip` varchar(16) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `dtz` varchar(45) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL COMMENT 'POS password',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_users_roles1_idx` (`role_id`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','admin@posmaks.com','95d1c5b3317c423586cb932a096d0e4e761cb895',NULL,'1-YU7K-UHQF-7RIK-HC0O-1525342260_8.jpg','PATRON-','85.105.193.172','2018-05-16 14:39:15','Asia/Kuwait',1231,'2016-07-21 16:23:24','2016-12-08 07:19:34',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_zones`
--

DROP TABLE IF EXISTS `users_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_zones` (
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`zone_id`),
  KEY `fk_users_zones_zones1_idx` (`zone_id`),
  KEY `fk_users_zones_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_zones_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_zones_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_zones`
--

LOCK TABLES `users_zones` WRITE;
/*!40000 ALTER TABLE `users_zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_zones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_waits`
--

DROP TABLE IF EXISTS `video_waits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_waits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `ticket` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_photos_locations1_idx` (`location_id`),
  CONSTRAINT `fk_photos_locations100` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Galactic tv modu için videosu düzenlenmiş ve satışı bekleyen videolar.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_waits`
--

LOCK TABLES `video_waits` WRITE;
/*!40000 ALTER TABLE `video_waits` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_waits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `url` varchar(50) NOT NULL,
  `ticket` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_photos_locations1_idx` (`location_id`),
  CONSTRAINT `fk_photos_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ys_products`
--

DROP TABLE IF EXISTS `ys_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ys_products` (
  `id` varchar(40) NOT NULL,
  `location_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stocks_id` int(11) DEFAULT NULL,
  `portion_id` int(11) DEFAULT NULL,
  `add_product_id` int(11) DEFAULT NULL,
  KEY `fk_ys_products_locations1_idx` (`location_id`),
  KEY `fk_ys_products_products1_idx` (`product_id`),
  KEY `fk_ys_products_stocks1_idx` (`stocks_id`),
  KEY `fk_ys_products_products2_idx` (`portion_id`),
  KEY `fk_ys_products_products3_idx` (`add_product_id`),
  CONSTRAINT `fk_ys_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ys_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ys_products_products2` FOREIGN KEY (`portion_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ys_products_products3` FOREIGN KEY (`add_product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ys_products_stocks1` FOREIGN KEY (`stocks_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ys_products`
--

LOCK TABLES `ys_products` WRITE;
/*!40000 ALTER TABLE `ys_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `ys_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zones`
--

DROP TABLE IF EXISTS `zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `desc` text,
  `background` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_zones_floors1_idx` (`floor_id`),
  KEY `fk_zones_locations1_idx` (`location_id`),
  CONSTRAINT `fk_zones_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_zones_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones`
--

LOCK TABLES `zones` WRITE;
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-22 11:56:48
