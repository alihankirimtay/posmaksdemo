<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Videos_Model extends CI_Model {

		const videos = 'videos';

		function __construct()
		{
			parent::__construct();
		}

		function add($values)
		{
			$values['ticket'] = ltrim($values['ticket'], '#');

			// Find sales of the ticket
			$sales = $this->db->select('id')
			->where([
				'location_id' => $values['location'],
				'return'      => 0,
				'active'      => 1,
				'payment_date >=' => date('Y-m-d H:i:s', strtotime('-48 hours'))
			])
			->where("ticket REGEXP '^{$values['ticket']}(_)?$'")
			->order_by('id', 'desc')
			->get('sales')->result_array();
			if (!$sales) {
				api_messageError( logs_(NULL, 'No sale found. . x001', $values, NULL, $values['location']) );
			}

			// Find packages and products of the sales
			$sale_has_products = $this->db->select('id, type, digital, is_video, quantity')
				->where('active', 1)
				->where_in('sale_id', array_column($sales, 'id'))
			->get('sale_has_products')->result_array();
			if (!$sale_has_products) {
				api_messageError( logs_(NULL, 'No product found in sale. x002', $values + ['sales' => $sales], NULL, $values['location']) );
			}

			$video_counter = 0;
			$package_ids = null;
			$ves_ids = null;
			foreach ($sale_has_products as $product_in_sale) {
				if ($product_in_sale['type'] == 'package') {
					$package_ids[] = $product_in_sale['id'];
				} elseif ($product_in_sale['type'] == 'ves') {
					$ves_ids[] = $product_in_sale['id'];
				} elseif ($product_in_sale['digital'] && $product_in_sale['is_video']) {
					$video_counter += $product_in_sale['quantity'];
				}
			}

			// find vidoes of package
			$package_has_video = [];
			if ($package_ids) {
				$package_has_video = $this->db->select('id')
					->where('active', 1)
					->where('digital', 1)
					->where('is_video', 1)
					->where_in('package_id', $package_ids)
				->get('package_has_products')->result_array();
				if (!$package_has_video) {
					api_messageError( logs_(NULL, 'No product found in sale. x003', $values + ['package_has_video' => $package_has_video], NULL, $values['location']) );
				}
			}

			// find vidoes of VES
			$ves_has_video = [];
			if ($ves_ids) {
				$ves_has_video = $this->db->select('id')
					->where('active', 0)
					->where('digital', 1)
					->where('is_video', 1)
					->where_in('package_id', $ves_ids)
				->get('package_has_products')->result_array();
			}

			// Find uploaded all videos for ticket
			$videos = $this->db->where([
				'ticket'      => $values['ticket'],
				'location_id' => $values['location'],
				'active'      => 1
			])->get(self::videos)->result_array();

			if (count($videos) >= ($video_counter + count($package_has_video) + count($ves_has_video))) {
				api_messageError( logs_(NULL, 'No more sold videos found. x004', $values, NULL, $values['location']) );
			}


			// İnsert video
			$data = [
				'location_id' => $values['location'],
				'url'         => $values['url'],
				'ticket'      => $values['ticket'],
				'created'     => _date(),
				'active'      => 1
			];
			$this->db->insert(self::videos, $data);

			// Delete video in video_waits
			$this->db->where('ticket', $values['ticket'])->delete('video_waits');

			//Check ves video as given.
			if ($ves_has_video) {
				$this->db
					->where_in('id', array_column($ves_has_video, 'id'))
					->where('active', 0)
					->where('digital', 1)
					->where('is_video', 1)
					->limit(1)
				->update('package_has_products', ['active' => 1, 'modified' => _date()]);
			}

			// Send GalacticTV api
			$this->load->library('curl');

			$galactic_tv = $this->curl->simple_post(galacticTvURL() . 'Api/videos/add', [
				'ticket'      => $values['ticket'],
				'url'         => $values['url'],
				'location_id' => $values['location']
			]);
			$galactic_tv = json_decode($galactic_tv, true);

			$galactic_tv_result = null;
			if (!isset($galactic_tv['status'])) {
				$galactic_tv_result = 'An error occurred.';
			} elseif (strtoupper($galactic_tv['status']) == 'SUCCESS') {
				$galactic_tv_result = 'SUCCESS';
			} elseif (strtoupper($galactic_tv['status']) == 'ERROR') {
				$galactic_tv_result = $galactic_tv['message'];
			}

			api_messageOk(logs_(NULL, 'Video has been created. [GalacticTV:'.$galactic_tv_result.']', $data, NULL, $values['location']));
		}


		function uploaded_videos($values)
		{
			
			if (!count($this->input->post('tickets[]'))) {
				api_messageError('Tickets array required.');
			}

			$values['tickets'] = null;
			foreach ($this->input->post('tickets[]') as $ticket) {
				$ticket = ltrim($ticket, '#');

				if (!strlen($ticket)) {
					api_messageError('Ticket can not be empty.');
				}
				$values['tickets'][] = $ticket;
			}


			
			$this->db->where_in('ticket', $values['tickets']);
			$this->db->where('location_id', $values['location']);
			$videos = $this->db->get('video_waits')->result();

			$uploaded = null;
			$unloaded = null;
			if ($videos) {
				
				foreach ($videos as $video) {

					$pos = stripos($video->ticket, '_');
					$pure_ticket = $pos ? substr($video->ticket, 0, $pos) : $video->ticket;

					// check sale
					$sale = $this->db->select('id')
					->where([
						'ticket' 	  => $pure_ticket,
						'location_id' => $values['location'],
						'return'      => 0,
						'active'      => 1,
						'payment_date >=' => date('Y-m-d H:i:s', strtotime('-48 hours'))
					])
					// ->where("ticket REGEXP '^{$video->ticket}(_)?$'")
					->order_by('id', 'desc')
					->get('sales', 1)->row();

					if ($sale) {

						$this->db->where_in('ticket', $video->ticket);
						$this->db->where('location_id', $values['location']);
						$this->db->where('active', 1);
						$video_uploaded = $this->db->get(self::videos, 1)->row();

						if ($video_uploaded) {
							$uploaded[] = $video->ticket;
						} else {
							$unloaded[] = $video->ticket;
						}
						
					} else {
						$uploaded[] = $video->ticket;
					}
				}

			} else {
				// $uploaded = $values['tickets'];

				foreach ($values['tickets'] as $value) {

					$this->db->where_in('ticket', $value);
					$this->db->where('location_id', $values['location']);
					$this->db->where('active', 1);
					$video_uploaded = $this->db->get(self::videos, 1)->row();

					if (!$video_uploaded) {

						$unloaded[] = $value;

						$this->db->insert('video_waits', [
							'ticket'      => $value,
							'location_id' => $values['location'],
							'created'     => date('Y-m-d H:i:s')
						]);
					} else {
						$uploaded[] = $value;
					}
					
				}
			}

			api_messageOk(['items' => ['uploaded' => $uploaded, 'unloaded' => $unloaded]]);
		}
	}    
	?>
