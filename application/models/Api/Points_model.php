<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Points_Model extends CI_Model {

		const points = 'points';

		function __construct()
		{
			parent::__construct();
		}

		function index($location)
		{
			$this->db->select('C.*');
			$this->db->where('C.active', 1);
			$this->db->where('C.location_id', $location);
			$points = $this->db->get(self::points.' AS C')->result();

			if(!$points) api_messageError('No point found.');

			$json['count'] = count($points);

			foreach ($points as $key => $point) {
				$json['items'][] = array(
					'id' 	=> $point->id,
					'tag' 	=> encode_id($point->id),
					'title' => $point->title,
					'desc'  => $point->desc,
				);
			}

			api_messageOk($json);
		}


	}    
	?>