	<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Locations_Model extends CI_Model {

		const locations = 'locations';

		function __construct()
		{
			parent::__construct();
		}

		function index()
		{
			$this->db->where('active', 1);
			$locations = $this->db
			->select(
				self::locations.'.* ,
				tt.header as ticket_header ,
				tt.footer as ticket_footer ,
				tt.logo as ticket_logo')
			->join('ticket_template as tt' , 'tt.location_id = '.self::locations.'.id' , 'left')
			->group_by('id')
			->get(self::locations)->result();

			if(!$locations) api_messageError('No location found.');

			$json['count'] = count($locations);

			foreach ($locations as $key => $location) {
				$json['items'][] = array(
					'id' 	 => $location->id,
					'title'  => $location->title,
					'desc'   => $location->desc,
					'ticket' => array(
						'header' => '<html><body style="background-color: rgb(255, 0, 0, 0);">'
									.$location->ticket_header
									.'<p><center><img id="barcode" src="" style="width:100%;"></img></center></p>'
									.$location->ticket_footer.'</body></html>',
						'footer' => '',
    					'logo'	 => 'http://loremflickr.com/320/240/sport'//$location->ticket_logo
    					)
					);
			}

			api_messageOk($json);
		}
	}    
	?>
