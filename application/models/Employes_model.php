<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Employes_model extends CI_Model {
    /**
     * @name string EMPLOYE_TABLE Holds the name of the table in use by this model
     */
    const EMPLOYE_TABLE = 'users';
    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';
    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
        $this->db->select(self::EMPLOYE_TABLE.'.* , r.title as role');
        
        if($this->user->role_id != 1):
            $this->db->where('role_id !=' , 1);
            $this->db->join('user_has_locations as ul' , 'ul.location_id IN('.implode(',',$this->user->locations).') AND ul.user_id = users.id');
            $this->db->group_by('id');
        endif;
        
        $this->db->from(self::EMPLOYE_TABLE);
        $this->db->join('roles as r' , 'r.id = '.self::EMPLOYE_TABLE.'.role_id');


        if ($where !== NULL) {
            if (is_array($where)) {
                foreach ($where as $field=>$value) {
                    $this->db->where($field, $value);
                }
            } else {
                $this->db->where(self::EMPLOYE_TABLE.'.'.self::PRI_INDEX, $where);
            }
        }
        $result = $this->db->get()->result();
        // pr($this->db->last_query() ,1);
        if ($result) {
            if ($where !== NULL) {
                return array_shift($result);
            } else {
                return $result;
            }
        } else {
            return false;
        }
    }

    public function getLocation($location)
    {
        return     $this->db
                        ->select('u.* , r.title as role')
                        ->join('user_has_locations ul' , 'ul.location_id = '.$location.' AND ul.user_id = u.id' )
                        ->join('roles as r' , 'r.id = u.role_id')
                        ->get(self::EMPLOYE_TABLE.' u')->result();
    }


    public function roles()
    {
        return $this->db->get('roles')->result();
        
    }

    public function locations()
    {
        $this->db->where('active', 1);
        return $this->db->get('locations')->result();
    }


    public function user_location($user_id){
        return $this->db->select('location_id')->where('user_id' , $user_id)->get('user_has_locations')->result();
    }
    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
        if ($this->db->insert(self::EMPLOYE_TABLE, $data)) {
            $insert_id = $this->db->insert_id();

            $data['id'] = $insert_id;

            logs_($this->user->id , 'Çalışan oluşturuldu' , $data);

            return $insert_id;
        } else {
            return false;
        }
    }

    public function insert_location($id , $locations = NULL)
    {
        $this->db
             ->where('user_id' , $id )
             ->delete('user_has_locations');

        if($locations != NULL):
            foreach($locations as $location):
                $this->db->insert('user_has_locations' , array('user_id' => $id , 'location_id' => $location));
            endforeach;   
        endif;  
    }


    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {


        if (!is_array($where)) {
            $where = array(self::PRI_INDEX => $where);
        }
        $this->db->update(self::EMPLOYE_TABLE, $data, $where);


        $data['id'] = $where[self::PRI_INDEX];

        logs_($this->user->id , 'Çalışan güncellendi' , $data);

        return $this->db->affected_rows();
    }
    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
        if (!is_array($where)) {
            $where = array(self::PRI_INDEX => $where);
        }
        logs_($this->user->id , 'Çalışan silindi ', $this->get($where[self::PRI_INDEX]));
        $this->db->delete(self::EMPLOYE_TABLE, $where);


        return $this->db->affected_rows();
    }
}

