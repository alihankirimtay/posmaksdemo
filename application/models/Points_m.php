<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Points_M extends M_Model {
	
	    public $table = 'points';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Kat',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'zone_id' => [
                        'field' => 'zone_id',
                        'label' => 'Bölge',
                        'rules' => 'required|trim|search_table[zones.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'service_type' => [
                        'field' => 'service_type',
                        'label' => 'Masa Tipi',
                        'rules' => 'required|trim|in_list[1,2,3]',
                    ],
                    'planner_settings' => [
                        'field' => 'planner_settings',
                        'label' => 'Planner Settings',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Kat',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'zone_id' => [
                        'field' => 'zone_id',
                        'label' => 'Bölge',
                        'rules' => 'required|trim|search_table[zones.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'description' => [
                        'field' => 'description',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'service_type' => [
                        'field' => 'service_type',
                        'label' => 'Masa Tipi',
                        'rules' => 'required|trim|in_list[1,2,3]',
                    ],
                    'planner_settings' => [
                        'field' => 'planner_settings',
                        'label' => 'Planner Settings',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function findServiceTypes()
        {
            return [
                1 => (Object)[
                    'id' => 1,
                    'title' => 'Normal servis',
                    'type' => 'normal',
                ],
                2 => (Object)[
                    'id' => 2,
                    'title' => 'Paket servis',
                    'type' => 'package',
                ],
                3 => (Object)[
                    'id' => 3,
                    'title' => 'Self servis',
                    'type' => 'self',
                ],
            ];
        }

        public function findServiceTypeByType($type)
        {
            foreach ($this->findServiceTypes() as $service_type) {
                if ($service_type->type == $type) {
                    return $service_type;
                }
            }
        }
	}    
?>
