<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Productions_Model extends CI_Model {
	
	    public $table = 'productions';

	    function __construct()
	    {
            $this->rules = [
                'insert' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'description' => [
                        'field' => 'description',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'description' => [
                        'field' => 'description',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function getAll($conditions = [])
        {
            return $this->_get($conditions)->result();
        }

        public function get($conditions = [])
        {
            return $this->_get($conditions)->row();
        }

        private function _get($conditions = [])
        {
            if ($conditions) {
                $this->db->where($conditions);
            }
            return $this->db->where('active !=', 3)->get($this->table);
        }

        public function findOrFail($conditions = [])
        {
            if (!is_array($conditions)) {
                $conditions = [
                    'id' => (int) $conditions
                ];
            }

            if (!$data = $this->get($conditions)) {
                if ($this->input->is_ajax_request()) {
                    messageAJAX('error', 'İçerik bulunamadı.');
                }

                show_404();
            }

            return $data;
        }

        public function insert()
        {
            $this->load->helper('security');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->rules['insert']);
            if($this->form_validation->run()) {
                
                $data['created'] = date('Y-m-d H:i:s');
                foreach ($this->rules['insert'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

                $this->db->insert($this->table, $data);

                return $this->db->insert_id();
            }
        }

        public function update($conditions = [])
        {
            $this->load->helper('security');
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->rules['update']);
            if($this->form_validation->run()) {
                
                $data['modified'] = date('Y-m-d H:i:s');
                foreach ($this->rules['update'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

               $this->db->where($conditions);
               $this->db->update($this->table, $data);

                return true;
            }
        
        }    

        public function delete($conditions = [])
        {
            $this->db->where($conditions);
            $this->db->update($this->table,[
                'active' => 3
            ]);
        }    

	}    
?>
