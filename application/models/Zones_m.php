<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Zones_M extends M_Model {
	
	    public $table = 'zones';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['points'] = [
                'foreign_model' => 'Points_m',
                'foreign_table' => 'points',
                'foreign_key'   => 'zone_id',
                'local_key'     => 'id'
            ];

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Kat',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Kat',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function findUsersByZone($zone_id) {
            $users = $this->db
                ->select('u.id, uz.zone_id, u.username')
                ->join('users_zones as uz', 'u.id = uz.user_id', 'inner')
                ->where('uz.zone_id', $zone_id)
                ->where('u.active !=', 3)
                ->get('users u')->result();
            return $users;

        }

        public function findZonesByUser($user_id)
        {
            $zones = $this->db
                ->select('zones.*')
                ->where([
                    'users_zones.user_id' => $user_id,
                    'zones.active !=' => 3
                ])
                ->join('users_zones', 'zones.id = users_zones.zone_id')
                ->get('zones')->result();
            return $zones;
        }

        public function findByLocation($where)      
        { 

            return $this->fields(['*', '(select title from locations where id = location_id) as location_title'])->get_all($where);
        
        }
	}    
?>
