<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Pos_Model extends CI_Model {
	
	    const pos = 'pos';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	if($location && !in_array($location, $this->user->locations)) error_('access');

	    	$this->db->select('P.*, (SELECT title FROM locations WHERE id = P.location_id LIMIT 1) AS location');
	    	if($location) {

	    		$this->db->where('P.location_id', $location);
	    	}
    		else {

    			$this->db->where_in('P.location_id', $this->user->locations);
    		}

	    	return $this->db->get(self::pos.' P')->result();	    	
	    }

	    function add()
	    {
	    	$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['created']  	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz Restoran. ', $values));
            }

            if( $this->db->insert(self::pos, $values) ) {

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Kasa oluşturuldu. ', $values));
            }
	    }

	    function edit($id)
	    {
    		$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']  = (int) $this->input->post('location');
            $values['modified'] = _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz Restoran. ', ['id'=>$id]+$values));
            }

       		$this->db->where('id', $id);
            if( $this->db->update(self::pos, $values) ) {

            	messageAJAX('success', logs_($this->user->id, 'Kasa güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete($id, $pos)
	    {
	    	$id   = (int) $id;
	    	$data = [ 'active' => 3, 'modified' => _date() ];

	    	$this->db->where('id', $id);
            if( $this->db->update(self::pos, $data) ) {

            	messageAJAX('success', logs_($this->user->id, 'Kasa silindi. ', $pos));
            }
	    }
	}    
?>
