<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StocksStockCounts_M extends M_Model {

	public $table = 'stocks_stock_counts';
	public $soft_deletes = false;
    public $timestamps = false;

	public function __construct()
	{

		$this->rules = [
			// 'insert' => [
			// 	'stock_id' => [
//                     'field' => 'stock_id',
//                     'label' => 'Stock Id',
//                     'rules' => 'required|trim|search_table[stocks.id]',
//                 ],
//                  'stock_count_id' => [
//                     'field' => 'stock_count_id',
//                     'label' => 'Stock Count Id',
//                     'rules' => 'required|trim|search_table[stock_counts.id]',
//                 ],
//                 'hand_amount' => [
//                     'field' => 'hand_amount',
//                     'label' => 'Elle girilen adet',
//                     'rules' => 'required|trim',
//                 ],
//                 'system_amount' => [
//                     'field' => 'system_amount',
//                     'label' => 'Sistem Adeti',
//                     'rules' => 'required|trim|',
//                 ],
               
			// ],
			// 'update' => [
			// 	'stock_id' => [
//                     'field' => 'stock_id',
//                     'label' => 'Stock Id',
//                     'rules' => 'required|trim|search_table[stocks.id]',
//                 ],
//                  'stock_count_id' => [
//                     'field' => 'stock_count_id',
//                     'label' => 'Stock Count Id',
//                     'rules' => 'required|trim|search_table[stock_counts.id]',
//                 ],
//                 'hand_amount' => [
//                     'field' => 'hand_amount',
//                     'label' => 'Elle girilen adet',
//                     'rules' => 'required|trim',
//                 ],
//                 'system_amount' => [
//                     'field' => 'system_amount',
//                     'label' => 'Sistem Adeti',
//                     'rules' => 'required|trim|',
//                 ],
               
			// ]
		];
	
		parent::__construct();

	}

	

}

/* End of file Stocks_stock_counts_M.php */
/* Location: ./application/models/Stocks_stock_counts_M.php */