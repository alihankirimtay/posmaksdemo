<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Sessions_Model extends CI_Model {
		
		private $table = 'sessions';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    public function find($where = [])
	    {
	    	return $this->db
	    	->where($where)
	    	->where('deleted', null)
	    	->get($this->table)->row();
	    }

	    public function findAll($where = [])
	    {
	    	return $this->db
	    	->where($where)
	    	->where('deleted', null)
	    	->get($this->table)->result();
	    }

	    public function findOpenSession($user_id)
	    {
	    	return $this->db
	    	->where([
	    		'user_id' => $user_id,
	    		'deleted' => null,
	    	])
	    	->group_start()
	    		->where('status', null)
	    		->or_where('status', 0)
	    	->group_end()
	    	->order_by('id', 'desc')
	    	->get($this->table, 1)->row();
	    }

	    public function createSession($user_id)
	    {
	    	if (!$this->findOpenSession($user_id)) {
	    		
	    		$this->db->insert($this->table, [
	    			'user_id' => $user_id,
	    			'created' => date('Y-m-d H:i:s'),
	    			'closed' => date('Y-m-d H:i:s'),
	    		]);
	    	}
	    }

	    public function createSessionForLocations($user_id)
	    {
	    	$now = date('Y-m-d H:i:s');

	    	$locations = $this->db
	    	->select('locations.id')
	    	->where([
	    		'user_has_locations.user_id' => $user_id,
	    		'locations.active' => 1,
	    	])
	    	->join('locations', 'locations.id = user_has_locations.location_id')
	    	->get('user_has_locations')
	    	->result();

	    	if (!$locations)
	    		return false;

	    	$location_ids = array_map(function ($row) {
	    		return $row->id;
	    	}, $locations);

	    	// Find Open sessions
	    	$sessions = $this->db
	    	->where([
	    		'user_id' => $user_id,
	    		'deleted' => null,
	    	])
	    	->group_start()
	    		->where('status', null)
	    		->or_where('status', 0)
	    	->group_end()
	    	->where_in('location_id', $location_ids)
	    	->get($this->table)
	    	->result();

	    	$insert_sessions = [];
	    	foreach ($locations as $location) {

	    		$sessionExists = false;
	    		foreach ($sessions as $session) {
	    			if ($location->id == $session->location_id) {
	    				$sessionExists = true;
	    				break;
	    			}
	    		}

	    		if (!$sessionExists) {
	    			$insert_sessions[$location->id] = [
		    			'location_id' => $location->id,
		    			'user_id' => $user_id,
		    			'created' => $now,
		    			'closed' => $now,
		    		];
	    		}
	    	}

	    	if ($insert_sessions) {
	    		$this->db->insert_batch($this->table, $insert_sessions);
	    	}

	    	return true;
	    }
	}    
?>
