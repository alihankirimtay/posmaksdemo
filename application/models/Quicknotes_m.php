<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class QuickNotes_M extends M_Model {
	
	    public $table = 'quick_notes';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->soft_deletes = true;
            
	    	$this->rules = [
                'insert' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];
	    	parent::__construct();
	    }
	}    
