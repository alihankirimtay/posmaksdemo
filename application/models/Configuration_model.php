<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


    public function save($values)
    {

        $control  = $this->db->select('id')->get('settings');

        if($control->num_rows() > 0):

            if (isset($_FILES['screen_saver'])) {
                $values['screen_saver'] = $this->uploadScreenSaver();
            }

            unset($values[ 'dosubmit' ]);
        
            $this->db->where('id' , $control->row()->id);
            $this->db->update('settings' , $values);


            logs_($this->user->id , 'Uygulama ayarları kaydedildi ', $values );
            return TRUE;

        else:

            if(!$this->db->insert('settings' , $values)):

                return FALSE;
            else:

                logs_($this->user->id , 'Uygulama ayarları oluşturldu', $values );
				

                return TRUE;
            endif;

        endif;
    }


    public function uploadScreenSaver()
    {
        $avatar_path  = 'assets/uploads/screen_saver/';

        @mkdir(FCPATH . $avatar_path);

        $this->load->library('upload', [
            'encrypt_name' => true,
            'upload_path' => FCPATH . $avatar_path,
            'allowed_types' => 'gif|jpg|jpeg|png',
            'max_size' => 2048, // 2Mb
        ]);

        if (!$this->upload->do_upload('screen_saver')) {
            messageAJAX('error', $this->upload->display_errors());
        }
        
        return $avatar_path . $this->upload->data()['file_name'];
    }
    

}

/* End of file Configuration_model.php */
/* Location: ./application/models/Configuration_model.php */
