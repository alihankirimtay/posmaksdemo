<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class YsProducts_M extends M_Model {

		public $table = 'ys_products';
		public $soft_deletes = TRUE;
        public $timestamps = false;
		

		public function __construct()
		{
			
			$this->rules = [
				'insert' => [
					'id' => [
                        'field' => 'id',
                        'label' => 'Id',
                        'rules' => 'required|trim',
                    ],
                     'product_id' => [
                        'field' => 'product_id',
                        'label' => 'Product id',
                        'rules' => 'required|trim|search_table[products.id]',
                    ],
                   
				],
				'update' => [
					'id' => [
                        'field' => 'id',
                        'label' => 'Id',
                        'rules' => 'required|trim',
                    ],
                     'product_id' => [
                        'field' => 'product_id',
                        'label' => 'Product id',
                        'rules' => 'required|trim|search_table[products.id]',
                    ],
                   
				]
			];
		
			parent::__construct();

		}

	}

/* End of file Stockcounts_M.php */
/* Location: ./application/models/Stockcounts_M.php */