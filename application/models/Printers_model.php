<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Printers_Model extends CI_Model {
	
        const printers = 'printers';
        const printerhascounts = 'printer_has_counts';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index($location = NULL)
        {
            $location = (int) $location;

            if($location && !in_array($location, $this->user->locations)) error_('access');

            $this->db->select('P.*, (SELECT title FROM locations WHERE id = P.location_id LIMIT 1) AS location');

            ($location)
            ? $this->db->where('P.location_id', $location)
            : $this->db->where_in('P.location_id', $this->user->locations);
            return $this->db->get(self::printers.' P')->result();  
        }

	    function add()
	    {
            if ($this->input->is_ajax_request()) {

    	    	$values['title']        = $this->input->post('title', TRUE);
                $values['desc']         = $this->input->post('desc', TRUE);
                $values['equal']        = $this->input->post('equal');
                $values['active']       = $this->input->post('active');
                $values['location_id']  = $this->input->post('location');
                $values['created']      = _date();

                if( $this->db->insert(self::printers, $values) ) {

                    $id = $this->db->insert_id();

                    $values_count['printer_id'] = $id;
                    $values_count['count']      = $this->input->post('counter');
                    $values_count['date']       = $values['created'];
                    $values_count['created']    = $values['created'];
                    $values_count['active']     = 1;

                    // PRINTERIN BAŞLANGIÇ DEĞERİ GİRİLİYOR.
                    $this->db->insert(self::printerhascounts, $values_count);

                    messageAJAX('success', logs_($this->user->id, 'Printer has been added. ', ['id'=>$id]+$values));
                }

            }               
	    }

	    function edit($id = NULL)
	    {
            $id = (int) $id;

            if ($this->input->is_ajax_request()) {

                $values['title']        = $this->input->post('title', TRUE);
                $values['desc']         = $this->input->post('desc', TRUE);
                $values['active']       = $this->input->post('active');
                $values['equal']        = $this->input->post('equal');
                $values['location_id']  = $this->input->post('location');
                $values['modified']     = _date();

                $this->db->where('id', $id);
                if( $this->db->update(self::printers, $values) ) {

                    messageAJAX('success', logs_($this->user->id, 'Printer has been edited.', ['id'=>$id]+$values));
                }

            }
	    }

        function waste($location_id = NULL, $id = NULL)
        {
            $id          = (int) $id;
            $location_id = (int) $location_id;

            if ($this->input->is_ajax_request()) {

                $values['date']     = dateConvert($this->input->post('date'), 'server');
                $values['waste']    = $this->input->post('waste');
                $values['unsold']   = $this->input->post('unsold');
                $values['comps']    = $this->input->post('comps');
                $values['unseen']   = $this->input->post('unseen');
                $values['modified'] = _date();


                $client_day_number = dateReFormat($this->input->post('date'), dateFormat(), 'Y-m-d');
                $start_of_day      = dateConvert2($client_day_number . ' 00:00:00', 'server');
                $end_of_day        = dateConvert2($client_day_number . ' 23:59:59', 'server');


                $has_date = $this->db
                    ->where('id !=', $id)
                    ->where('location_id', $location_id)
                    ->where('date >=', $start_of_day)
                    ->where('date <=', $end_of_day)
                    ->get('location_losts', 1)->row();


                if ($has_date) {
                    messageAJAX('error', 'Location already has value on the date.');
                }


                $this->db->where('id', $id);
                if( $this->db->update('location_losts', $values) ) {

                    messageAJAX('success', logs_($this->user->id, 'Waste has been edited.', [ 'id' => $id ]+$values));
                }

            }
        }

	    function delete($id, $printer)
	    {
            $id = (int) $id;
            
	    	$this->db->where('id', $id);
            if( $this->db->delete(self::printers) ) {

                $this->db->where('printer_id', $id);
                $this->db->delete(self::printerhascounts);

                messageAJAX('success', logs_($this->user->id, 'Printer has been deleted. ', $printer));
            }
	    }
	}    
?>
