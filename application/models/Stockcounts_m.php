<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Stockcounts_M extends M_Model {

		public $table = 'stock_counts';
		public $soft_deletes = TRUE;

		public function __construct()
		{
			$this->before_create = ['_setDates'];
            $this->before_update = ['_setDates'];
			
			$date_format = dateFormat();

			$this->rules = [
				'insert' => [
					'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                     'user_id' => [
                        'field' => 'user_id',
                        'label' => 'User Id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'start_date' => [
                        'field' => 'start_date',
                        'label' => 'Başlangıç Tarihi',
                        'rules' => 'required|trim|is_date[Y-m-d H:i:s]',
                    ],
                    'end_date' => [
                        'field' => 'end_date',
                        'label' => 'Bitiş Tarihi',
                        'rules' => 'required|trim|is_date['.$date_format.']',
                    ],
                   
				],
				'update' => [
					'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                     'user_id' => [
                        'field' => 'user_id',
                        'label' => 'User Id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'start_date' => [
                        'field' => 'start_date',
                        'label' => 'Başlangıç Tarihi',
                        'rules' => 'required|trim|is_date[Y-m-d H:i:s]',
                    ],
                    'end_date' => [
                        'field' => 'end_date',
                        'label' => 'Bitiş Tarihi',
                        'rules' => 'required|trim|is_date['.$date_format.']',
                    ],
                   
				]
			];
		
			parent::__construct();

		}

		public function _setDates($data)
		{
			$data['end_date'] = dateConvert($data['end_date'], 'server');

			if (strtotime($data['start_date']) >= strtotime($data['end_date'])) {
				messageAjax('error', 'Başlangıç tarihi bitiş tarihinden büyük olamaz');
			}
			return $data;
		}

		public function findLastStockCountDate($location_id) 
		{
			$location_id = (int) $location_id;
			$stock_count = $this->order_by('id', 'DESC')->get([
				'location_id' => $location_id
			]);

			if ($stock_count) {
				return $stock_count->end_date;
			} 
			

			$location = $this->db->where('id', $location_id)->get('locations')->row();

			if ($location) {

				return $location->created;

			}

		}
	}

/* End of file Stockcounts_M.php */
/* Location: ./application/models/Stockcounts_M.php */