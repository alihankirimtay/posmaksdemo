<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_M extends M_Model {

      public $table = 'users';

      public function __construct()
      {
            $this->before_create = ['_setPassword', '_setPin'];
            $this->before_update = ['_setPassword', '_setPin'];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many_pivot['locations'] = [
                'foreign_model'     =>'Locations_M',
                'pivot_table'       =>'user_has_locations',
                'local_key'         =>'id',
                'pivot_local_key'   =>'user_id',
                'pivot_foreign_key' =>'location_id',
                'foreign_key'       =>'id',
            ];

            $this->has_many_pivot['zones'] = [
                'foreign_model'     =>'Zones_M',
                'pivot_table'       =>'users_zones',
                'local_key'         =>'id',
                'pivot_local_key'   =>'user_id',
                'pivot_foreign_key' =>'zone_id',
                'foreign_key'       =>'id',
            ];


            $this->rules = [
                  'insert' => [
                        'role_id' => [
                              'field' => 'role_id',
                              'label' => 'Rol',
                              'rules' => 'required|trim|search_table[roles.id]',
                        ],
                        'username' => [
                              'field' => 'username',
                              'label' => 'Hesap adı',
                              'rules' => 'required|trim|is_username|is_unique[users.username]',
                        ],
                        'email' => [
                              'field' => 'email',
                              'label' => 'Eposta',
                              'rules' => 'required|trim|valid_email|is_unique[users.email]',
                        ],
                        'password' => [
                              'field' => 'password',
                              'label' => 'Oturum Şifresi',
                              'rules' => 'required|trim|min_length[6]',
                        ],
                        'pin' => [
                              'field' => 'pin',
                              'label' => 'Pin',
                              'rules' => 'trim|integer|greater_than_equal_to[1000]|is_unique[users.pin]',
                        ],
                        'name' => [
                              'field' => 'name',
                              'label' => 'Ad Soyad',
                              'rules' => 'required|trim|xss_clean',
                        ],
                        'active' => [
                              'field' => 'active',
                              'label' => 'Durum',
                              'rules' => 'required|trim|in_list[1,0]',
                        ],
                  ],
                  'update' => [
                        'role_id' => [
                              'field' => 'role_id',
                              'label' => 'Rol',
                              'rules' => 'required|trim|search_table[roles.id]',
                        ],
                        'username' => [
                              'field' => 'username',
                              'label' => 'Hesap adı',
                              'rules' => 'required|trim|is_username|is_unique_update[users.username.'.$this->uri->segment(3).']',
                        ],
                        'email' => [
                              'field' => 'email',
                              'label' => 'Eposta',
                              'rules' => 'required|trim|valid_email|is_unique_update[users.email.'.$this->uri->segment(3).']',
                        ],
                        'password' => [
                              'field' => 'password',
                              'label' => 'Oturum Şifresi',
                              'rules' => 'trim|min_length[6]',
                        ],
                        'pin' => [
                              'field' => 'pin',
                              'label' => 'Pin',
                              'rules' => 'trim|integer|greater_than_equal_to[1000]|is_unique_update[users.pin.'.$this->uri->segment(3).']',
                        ],
                        'name' => [
                              'field' => 'name',
                              'label' => 'Ad Soyad',
                              'rules' => 'required|trim|xss_clean',
                        ],
                        'active' => [
                              'field' => 'active',
                              'label' => 'Durum',
                              'rules' => 'required|trim|in_list[1,0]',
                        ],
                  ]
            ];

            parent::__construct();
      }

      public function _whereNonDeleted()
      {
            $this->where('active !=', 3);
      }

      public function _setPassword($data)
      {
            if ($this->input->post('password')) {
                  $data['password'] = $this->hashPassword($data['password']);
            } else {
                  unset($data['password']);
            }

            return $data;
      }

      public function _setPin($data)
      {
            if ($this->router->fetch_method() != "delete") {
                  if (!$data['pin']) {
                        $data['pin'] = null;
                  }
            }

            return $data;
      }

      public function hashPassword($password)
      {
            return sha1($password . $this->config->item('salt'));
      }

      public function handleUserLocations($user_id = null)
      {
            $locations = (Array) $this->input->post('locations[id][]');

            $data = [];
            foreach ($locations as $location_id) {

                  $location_id = (int) $location_id;

                  if (!$this->db->where('id', $location_id)->get('locations', 1)->row()) {
                        messageAJAX('error', 'Geçersiz lokasyon');
                  }

                  $data[] = [
                        'user_id' => $user_id,
                        'location_id' => $location_id,
                  ];
            }

            if (!$data) {
                messageAJAX('error' , 'Restoran alanı gerekli');
            }

            return $data;
      }

      public function handleUserZones($user_id = null)
      {
            $zones = (Array) $this->input->post('zones[id][]');
            $data = [];

            foreach ($zones as $zone_id) {

                  $zone_id = (int) $zone_id;

                  if (!$this->db->where('id', $zone_id)->get('zones', 1)->row()) {
                        messageAJAX('error', 'Geçersiz bölge');
                  }

                  $data[] = [
                        'user_id' => $user_id,
                        'zone_id' => $zone_id,
                  ];
            }

            return $data;


            
      }

      public function findByLocation($location_id = null)
      {
            return $this->db
            ->where([
                  'users.active !=' => 3,
                  'locations.active !=' => 3,
                  'locations.id' => $location_id,
            ])
            ->join('user_has_locations', 'user_has_locations.user_id = users.id')
            ->join('locations', 'locations.id = user_has_locations.location_id')
            ->group_by('users.id')
            ->get('users')
            ->result();
      }
}    

