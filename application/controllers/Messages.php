<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        $this->load->model('Messages_model');

    }

    // Get All User
    public function index(){

        $users = $this->Messages_model->getUsers();

        echo json_encode($users);
    }


    //Start Chat
    public function start($id)
    {   
        $id = (int)$id;

        if(!$id) exit();

        $historys = $this->Messages_model->startChat($id);
        if($historys != NULL):
            foreach($historys as $history){

                $data[] = array(
                    'id' => $history->id,
                    'reciever' => $history->reciever,
                    'sender'    => $history->sender,
                    'message'   => $history->message,
                    'time'      => time_ago($history->created)
                );
            }
        else:
            $data = NULL;
        endif;

        echo json_encode($data);
    }

    //Show User New Message Control
    public function msgControl($id , $msgID)
    {
        $newMsg = $this->Messages_model->msgControl($id , $msgID);

        if($newMsg != NULL):
            foreach($newMsg as $msg){
                $data[] = array(
                    'id' => $msg->id,
                    'message' => $msg->message,
                    'time'    => time_ago($msg->created)
                );
            }
        else:
            $data['result'] = 0;
        endif;

        echo json_encode($data);
    }

    // New Message Control
    public function NewControl()
    {
       $row = $this->Messages_model->NewControl();

       $data['result'] = $row;

       echo json_encode($data);
    }

   

    // Add a new item
    public function send($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('security');

        $this->form_validation->set_rules('message', 'Mesaj', 'trim|required|min_length[1]|xss_clean');

        if($this->form_validation->run() === FALSE):
            messageAJAX('error' , validation_errors());
        else:

            $values = array(
                'sender'    => $this->user->id,
                'reciever'  => $id,
                'message'   => $this->input->post('message'),
                'created'   => _date()
            );


           $post = $this->Messages_model->insert($values);

            if($post != FALSE):
                messageAJAX('success' , __('Mesaj gönderildi.'));
            else:
                messageAJAX('error' , 'Database Error');
            endif;

        endif;
    }


    //Delete one item
    public function delete( $id = NULL )
    {

    }
}

/* End of file Messages.php */
/* Location: ./application/controllers/Messages.php */
