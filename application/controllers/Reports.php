<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Auth_Controller {

    private $payment_types;

    private $location_id;
    private $date_start;
    private $date_end;


    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => [ 'ajax', 'deleteEndofday', 'index' ]
        ]);

        $this->load->model('Sessions_M');
        $this->load->model('Sales_M');
        $this->load->model('Expenses_M');
        $this->load->model('Paymenttypes_M', 'PaymentTypes_M');
        $this->load->model('Producttypes_M', 'ProductTypes_M');
        $this->load->model('Reports_model');

        $this->payment_types = $this->PaymentTypes_M->get_all([
            'active' => 1
        ]);
    }

    public function ajax($event = null)
    {
        if(!method_exists(__CLASS__, (String) $event) || !$this->input->is_ajax_request()) {
            messageAJAX('error', __('Hatalı istek [404]'));
        }

        $this->{$event}();
    }


    public function index()
    {
        $tab = $this->input->get('tab');

        if (!hasPermisson("reports.{$tab}")) {
            error_("access");
        }

        switch ($tab) {
            case 'status':
                $this->tab_status();
                break;

            case 'home':
                $this->tab_home();
                break;

            case 'stock':
                $this->tab_stock();
                break;

            case 'endofday':
                $this->tab_endofday();
                break; 

            case 'fullness':
                $this->tab_fullness();
                break; 

            case 'primdetails':
                $this->tab_prim_details();
                break; 

            case 'prim':
                $this->tab_prim();
                break;

            case 'daily':
                $this->tab_daily();
                break;

            case 'monthly':
                $this->tab_monthly();
                break;
            
            default:
                $this->tab_summary();
                break;
        }
 
    }

    // New
    private function filters()
    {
        $this->location_id = $this->input->get('location_id');
        $this->date_start = $this->input->get('date_start');
        $this->date_end = $this->input->get('date_end');

        if (!$this->location_id) {
            $this->location_id = $this->user->locations[0];
        }

        if (!isValidDate($this->date_start, dateFormat())) {
            $this->date_start = date('Y-m-d 00:00:00');
        } else {
            $this->date_start = dateReFormat($this->date_start, dateFormat(), 'Y-m-d H:i:s');
        }

        if (!isValidDate($this->date_end, dateFormat())) {
            $this->date_end = date('Y-m-d 23:59:59');
        } else {
            $this->date_end = dateReFormat($this->date_end, dateFormat(), 'Y-m-d H:i:s');
        }

        $this->date_start = dateConvert2($this->date_start, 'server');
        $this->date_end = dateConvert2($this->date_end, 'server');
    }

    // New
    private function tab_daily()
    {
        $location_id = $this->input->get('location_id');
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');

        if (!$location_id) {
            $location_id = $this->user->locations[0];
        }

        if (!isValidDate($date_start, dateFormat())) {
            $date_start = date('Y-m-d 00:00:00');
        } else {
            $date_start = dateReFormat($date_start, dateFormat(), 'Y-m-d H:i:s');
        }

        if (!isValidDate($date_end, dateFormat())) {
            $date_end = date('Y-m-d 23:59:59');
        } else {
            $date_end = dateReFormat($date_end, dateFormat(), 'Y-m-d H:i:s');
        }

        $date_start = dateConvert2($date_start, 'server');
        $date_end = dateConvert2($date_end, 'server');

        $sessions = $this->Sessions_M
        ->fields('*')
        ->select('(SELECT username FROM users WHERE id = sessions.user_id LIMIT 1) as user_name')
        ->where('location_id', $this->user->locations)
        ->get_all([
            'location_id' => $location_id,
            'created >='  => $date_start,
            'closed <='  => $date_end
        ]);
        $payment_types = $this->payment_types;
        $cash_types = $this->Sessions_M->findCashTypes();

        $tab_content = $this->load->view('Reports/tab_daily', compact('sessions', 'payment_types', 'location_id', 'date_start', 'date_end'), true);

        $this->theme_plugin['js'] = ['panel/js/view/reports.js'];
        $this->theme_plugin['start'] = '
            TablesDataTables.init(); 
            MyFunction.datetimepicker("'.dateFormat(TRUE).'"); 
            Reports.Daily.init({
                payment_types: '.json_encode($payment_types).',
                cash_types: '.json_encode($cash_types).',
            });';
        $this->load->template('Reports/index', compact('tab_content'));
    }

    // New
    private function tab_status()
    {
        $this->filters();

        $location_id = $this->location_id;
        $date_start = $this->date_start;
        $date_end = $this->date_end;
        $currency = $this->user->locations_array[$location_id]['currency'] . ' ';
        $payment_types = $this->payment_types;

        $total_sale = 0;
        $total_refund = 0;
        $total_expense = 0;
        $total_gift = 0;
        $payments = (Object) [];

        foreach ($payment_types as $payment_type) {
            $payments->{$payment_type->alias} = 0;
        }

        $sales = $this->Sales_M->findSalesWithProducts([
            'payment_date >=' => $date_start,
            'payment_date <=' => $date_end,
            'location_id'=> $location_id,
            'status' =>'completed',
            'active' => 1
        ]);

        $expenses = $this->Expenses_M
        ->fields('*')
        ->select('(SELECT title FROM expense_types WHERE id = expense_type_id LIMIT 1) AS type_title')
        ->select('(SELECT username FROM users WHERE id = user_id LIMIT 1) AS user_name')
        ->select('(SELECT title FROM payment_types WHERE id = payment_type_id LIMIT 1) AS payment_type_title')
        ->get_all([
            'payment_date >=' => $date_start,
            'payment_date <=' => $date_end,
            'location_id' => $location_id,
            'active' => 1
        ]);

        foreach ($sales as $sale) {

            if ($sale->return_id) {
                $total_sale -= $sale->amount;
                $total_refund += $sale->amount;

                foreach ($payment_types as $payment_type) {
                    $payments->{$payment_type->alias} -= $sale->{$payment_type->alias};
                }

            } else {
                $total_sale += $sale->amount;

                foreach ($payment_types as $payment_type) {
                    $payments->{$payment_type->alias} += $sale->{$payment_type->alias};
                }

                foreach ($sale->products as $product) {

                    if ($product->is_gift) {
                        if ($product->type == 'product') {
                            $total_gift += addTax($product->price, $product->tax);
                        }
                    }
                }
            }
        }

        foreach ($expenses as $expense) {
            $total_expense += $expense->amount + $expense->amount*$expense->tax / 100 ;
        }


        $tab_content = $this->load->view('Reports/tab_status', 
            compact('total_sale', 'total_refund', 'total_expense', 'total_gift', 'payments', 'expenses', 'payment_types', 'location_id', 'date_start', 'currency'), true);

        $this->theme_plugin['js'] = array('panel/js/view/reports.js');
        $this->theme_plugin['start'] = '
            TablesDataTables.init();
            MyFunction.datetimepicker("'.dateFormat(TRUE).'");
        ';

        $this->load->template('Reports/index', compact('tab_content'));
    }

    // New
    private function tab_monthly()
    {
        $location_id = $this->input->get('location_id');
        $date_start = $this->input->get('date_start');
        $date_end = null;
        $date_format = yearMonthFormat();

        if (!$location_id) {
            $location_id = $this->user->locations[0];
        }

        if (!isValidDate($date_start, $date_format)) {
            $date_start = date('Y-m-01 00:00:00');
        } else {
            $date_start = dateReFormat($date_start, $date_format, 'Y-m-01 00:00:00');
        }
        $date_end = date('Y-m-d 23:59:59', strtotime("{$date_start} last day of this month"));

        $countdays = date('t', strtotime($date_start));
        $date_start = dateConvert2($date_start, 'server');
        $date_end = dateConvert2($date_end, 'server');

        $sessions = $this->Sessions_M
        ->where('location_id', $this->user->locations)
        ->get_all([
            'location_id' => $location_id,
            'created >='  => $date_start,
            'closed <='  => $date_end
        ]);

        $payment_types = $this->payment_types;

        $days = [];
        for ($i=1; $i <= $countdays; $i++) {

            if ($i === 1) {
                $day_start = strtotime($date_start);
                $day_end = $day_start + (24*60*60 - 1); // +24 hours -1 second
            } else {
                $day_start = $day_start + (24*60*60); // +24 hours
                $day_end = $day_end + (24*60*60); // +24 hours
            }

            $days[$i] = (Object) [
                'day_start' => date('Y-m-d H:i:s', $day_start),
                'day_end' => date('Y-m-d H:i:s', $day_end),
                'day_start_unix' => $day_start,
                'day_end_unix' => $day_end,
                'cash_opening' => 0,
                'total' => 0,
                'total_exists' => 0,
                'over_short' => 0,
            ];

            foreach ($payment_types as $payment_type) {
                $days[$i]->{$payment_type->alias} = 0;
                $days[$i]->{"{$payment_type->alias}_exists"} = 0;
            }


            foreach ($sessions as $session) {

                if (!between(strtotime($session->created), $days[$i]->day_start_unix, $days[$i]->day_end_unix)) {
                    continue;
                }

                $total =
                $total_exists = 0;
                foreach ($payment_types as $payment_type) {
                    $days[$i]->{$payment_type->alias} += $session->{$payment_type->alias};
                    $days[$i]->{"{$payment_type->alias}_exists"} += $session->{"{$payment_type->alias}_exists"};

                    $total += $session->{$payment_type->alias};
                    $total_exists += $session->{"{$payment_type->alias}_exists"};
                }

                $days[$i]->cash_opening += $session->cash_opening;
                $days[$i]->total += $total;
                $days[$i]->total_exists += $total_exists;

                $days[$i]->over_short += $total_exists - $total - $session->cash_opening;
            }

        }

        $tab_content = $this->load->view('Reports/tab_monthly', compact('days', 'payment_types', 'location_id', 'date_start', 'date_format'), true);

        $this->theme_plugin['start'] = '
            TablesDataTables.init(); 
            MyFunction.datetimepicker("'.yearMonthFormat(TRUE).'");
        ';
        $this->load->template('Reports/index', compact('tab_content'));
    }

    // New
    private function tab_home()
    {
        $this->filters();

        $location_id = $this->location_id;
        $date_start = $this->date_start;
        $date_end = $this->date_end;
        $currency = $this->user->locations_array[$location_id]['currency'] . ' ';
        $payment_types = $this->payment_types;

        $total_sale = 0;
        $total_refund = 0;
        $total_gift = 0;
        $payments = (Object) [];

        foreach ($payment_types as $payment_type) {
            $payments->{$payment_type->alias} = 0;
        }

        $sales = $this->Sales_M->findSalesWithProducts([
            'payment_date >=' => $date_start,
            'payment_date <=' => $date_end,
            'location_id' => $location_id,
            'status' =>'completed',
            'active' => 1
        ]);


        $main_categories = $this->ProductTypes_M
        ->fields(['*', '0 As amount', '0 As count', '0 As return_amount', '0 As return_count'])
        ->with_subCategories([
            'fields' => ['*', '0 As amount', '0 As count'],
            'where' => [
                'parent_id !=' => null,
                'active' => 1,
            ],
            'with' => [
                'relation' => 'products',
                'fields' => ['*', '0 As amount', '0 As count', '0 As return_amount', '0 As return_count'],
                'where' => [
                    'location_id' => $location_id,
                    'active' => 1,
                ]
            ]
        ])
        ->get_all([
            'parent_id' => null,
            'active' => 1,
        ]);


        foreach ($sales as $sale) {

            if ($sale->return_id) {
                $total_sale -= $sale->amount;
                $total_refund += $sale->amount;

                foreach ($payment_types as $payment_type) {
                    $payments->{$payment_type->alias} -= $sale->{$payment_type->alias};
                }

                foreach ($sale->products as $product) {

                    $product->amount = addTax($product->price, $product->tax);

                    $this->calcCategories($main_categories, $product, '-');

                    foreach ($product->additional_products as $additional) {   
                        $additional->amount = addTax($additional->price, $additional->tax);
                        $this->calcCategories($main_categories, $additional, '-'); 
                    }
                }

            } else {
                $total_sale += $sale->amount;

                foreach ($payment_types as $payment_type) {
                    $payments->{$payment_type->alias} += $sale->{$payment_type->alias};
                }

                foreach ($sale->products as $product) {

                    $product->amount = addTax($product->price, $product->tax);

                    $this->calcCategories($main_categories, $product, '+');

                    foreach ($product->additional_products as $additional) {   
                        $additional->amount = addTax($additional->price, $additional->tax);
                        $this->calcCategories($main_categories, $additional, '+'); 

                        if ($product->is_gift) {
                            $total_gift += addTax($additional->price * $product->quantity, $additional->tax);
                        }

                    }

                    if ($product->is_gift) {
                        $total_gift += addTax($product->price * $product->quantity, $product->tax);
                    }
                }
            }
        }

        $tab_content = $this->load->view('Reports/tab_home', 
            compact('total_sale', 'total_refund', 'total_gift', 'payment_types', 'payments', 'main_categories', 'location_id', 'date_start', 'date_end', 'currency'), true);
        
        $this->theme_plugin = [
            'js'    => [
                'globals/plugins/jquery.print/print.js',
                'panel/js/view/reports.js'
            ],
            'start' => '
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Reports.Home.init();
                TablesDataTables.init();'
        ];


        $this->load->template('Reports/index', compact('tab_content'));
    }

    // New
    private function updateSession()
    {
        $this->load->library('Poscashclosing', null, 'PosCashClosing');

        $result = $this->PosCashClosing
        ->setSessionId($this->input->post('session_id'))
        ->setManagerId($this->user->id)
        ->setPayments($this->input->post('payments[]'))
        ->setCashs($this->input->post('cashs[]'))
        ->setCashOpening($this->input->post('cash_opening'))
        ->update();

        if (!$result) {
            messageAJAX('error', $this->PosCashClosing->errorMessage());
        }

        messageAJAX('success', 'Success');
    }

    // New
    protected function calcCategories(&$main_categories, $sold_product, $operator = '+ OR -')
    {
        $operator = ($operator == '-' ? -1 : +1);

        foreach ($main_categories as &$main_category) {
            foreach ($main_category->subCategories as &$sub_category) {
                foreach ($sub_category->products as &$product) {
                    if ($product->id == $sold_product->product_id) {
                        if (!$sold_product->is_gift) {

                            $amount = $sold_product->amount * $sold_product->quantity * $operator;
                            $count = $sold_product->quantity * $operator;

                            $main_category->amount += $amount;
                            $sub_category->amount += $amount;
                            $product->amount += $amount;

                            $main_category->count += $count;
                            $sub_category->count += $count;
                            $product->count += $count;

                            if ($operator === -1) {
                                $main_category->return_amount += $amount; 
                                $product->return_amount += $amount; 

                                $main_category->return_count += $count;
                                $product->return_count += $count;
                            }
                        }
                    }
                }
                unset($product);
            }
            unset($sub_category);
        }
        unset($main_category);
    }


    public function tab_prim() {

        $this->load->model('Reports_model');
        $this->load->model('Sales_M');

        $this->theme_plugin = array(
            'css'   => array(
            ),
            'js'    => array(
                'globals/plugins/jquery.print/print.js',
                'panel/js/view/prim.js'
            ),
            'start' => '
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Prim.init(); '
        );

        if (isset($_GET[ 'l' ])) {
            $data[ 'location_id' ] = array_key_exists($_GET[ 'l' ], $this->user->locations_array)
                ? (int) $_GET[ 'l' ]
                : 0;
        } else {
            $data[ 'location_id' ] = (int) @current($this->user->locations_array)[ 'id' ];
        }

        // DATES
        if (isset($_GET[ 'd' ])) {
            $data[ 'date_from' ] = isValidDate($_GET[ 'd' ], dateFormat())
                ? dateConvert($_GET[ 'd' ], 'server', 'Y-m-d H:i:00')
                : date('Y-m-d H:i:00', strtotime('-24 hours'));
        } else {
            $data[ 'date_from' ] = date('Y-m-d H:i:00', strtotime('-24 hours'));
        }

        if (isset($_GET[ 'dTo' ])) {
            $data[ 'date_to' ] = isValidDate($_GET[ 'dTo' ], dateFormat())
                ? dateConvert($_GET[ 'dTo' ], 'server', 'Y-m-d H:i:59')
                : date('Y-m-d H:i:59', strtotime('-24 hours'));
        } else {
            $data[ 'date_to' ] = date('Y-m-d H:i:59');
        }

        if(isset($_GET['garsonFilter'])) {
            $userId = $_GET['garsonFilter'];
        } else {
            $userId = 0;
        }

            $this->db
            ->where('location_id', $data['location_id'])
            ->where('active', 1);
        $users = $this->Reports_model->usersandlocations();


        $allSales = $this->Sales_M->findSalesWithProducts([
            'payment_date >=' => $data[ 'date_from' ],
            'payment_date <=' => $data[ 'date_to' ],
            'location_id' => $data['location_id'],
            'status' =>'completed',
            'active' => 1
        ]);
        
        foreach ($users as &$user) {
            $user['quantity'] = 0;
            $user['bonus'] = 0;
            $user['sum_sale'] = 0;
            foreach ($allSales as $sale) {         
                foreach ($sale->products as $product) {
                    if ($product->user_id == $user['id']) { 
                        if ($product->bonus_type == 'value') {
                            $sumBonus = $product->bonus;         
                        } else {
                            $sumPrice = $product->price + ($product->price*$product->tax)/100;
                            $sumBonus = $sumPrice*$product->bonus/100;
                        }
                        if ($sale->return) {  
                            $user['quantity'] -= $product->quantity;    
                            $user['bonus'] -= $sumBonus * $product->quantity;   
                            $user['sum_sale']--;
                        } else {
                            $user['quantity'] += $product->quantity;    
                            $user['bonus'] += $sumBonus * $product->quantity; 
                            $user['sum_sale']++;
                        }
                    }
                }
            }
        }

    
        $data['tab_content'] = $this->load->view('Reports/tab_prim', compact('users', 'data'), true);
        $this->load->template('Reports/index', $data);
    }

    public function tab_prim_details()
    {
        $data['payment_types'] = $this->payment_types;
        
        if (isset($_GET[ 'l' ])) {
            $data[ 'location_id' ] = array_key_exists($_GET[ 'l' ], $this->user->locations_array)
                ? (int) $_GET[ 'l' ]
                : 0;
        } else {
            $data[ 'location_id' ] = (int) @current($this->user->locations_array)[ 'id' ];
        }

        // DATES
        if (isset($_GET[ 'd' ])) {
            $data[ 'date_from' ] = isValidDate($_GET[ 'd' ], dateFormat())
                ? dateConvert($_GET[ 'd' ], 'server', 'Y-m-d H:i:00')
                : date('Y-m-d H:i:00', strtotime('-24 hours'));
        } else {
            $data[ 'date_from' ] = date('Y-m-d H:i:00', strtotime('-24 hours'));
        }

        if (isset($_GET[ 'dTo' ])) {
            $data[ 'date_to' ] = isValidDate($_GET[ 'dTo' ], dateFormat())
                ? dateConvert($_GET[ 'dTo' ], 'server', 'Y-m-d H:i:59')
                : date('Y-m-d H:i:59', strtotime('-24 hours'));
        } else {
            $data[ 'date_to' ] = date('Y-m-d H:i:59');
        }

        if(isset($_GET['garsonFilter'])) {
            $userId = $_GET['garsonFilter'];
        } else {
            $userId = 0;
        }

        $this->db
            ->select('id, return_id')
            ->where([
                'S.location_id' => $data['location_id'],
                'S.payment_date >=' => $data[ 'date_from' ],
                'S.payment_date <=' => $data[ 'date_to' ]
                ]);

        $salesAll = $this->Reports_model->sales();

        if($salesAll) {
            $sales = [];
            foreach ($salesAll as $key => $sale) {
                $sales[] = $sale['id'];
            }

            $this->db
                ->where_in('SHP.sale_id', $sales )
                ->where('SHP.user_id', $userId )
                ->order_by('SHP.id', 'DESC');
            $product_sold = $this->Reports_model->sold_products('*');

            $products = [];

            foreach ($salesAll as $key => $sale) {
                foreach ($product_sold as $key => $product) {
                    if($sale['id'] == $product['sale_id']) {
                        $sumPrice = $product['price'] + $product['price']*$product['tax'] / 100;
                        if($product['bonus_type'] == "value") {
                            $productBonus = $product['bonus'];
                        } else {
                            $productBonus = ($sumPrice * $product['bonus']) / 100;
                        }
                        if(!isset($products[$product['id']])) {
                            $products[$product['id']] = [
                                'quantity' => 0,
                                'title' => $product['title'],
                                'total' => 0,
                                'price' => $sumPrice,
                                'bonus' => $product['bonus'],
                                'totalbonus' => 0,
                            ]; 
                        }        

                        if($sale['return_id']) {

                            $products[$product['id']]['quantity'] -= $product['quantity'];
                            $products[$product['id']]['total'] -= $product['quantity'] * $sumPrice;
                            $products[$product['id']]['totalbonus'] -= $productBonus * $product['quantity'];

                        } else {

                            $products[$product['id']]['quantity'] += $product['quantity'];
                            $products[$product['id']]['total'] += $product['quantity'] * $sumPrice;
                            $products[$product['id']]['totalbonus'] += $productBonus * $product['quantity'];
                        }
                    }
                }             
            }

            $data['products'] = $products;
        }

        $this->theme_plugin = array(
            'css'   => array(
            ),
            'js'    => array(
                'globals/plugins/jquery.print/print.js',
                'panel/js/view/prim.js'
            ),
            'start' => '
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Prim.init(); '
        );

        $data['tab_content'] = $this->load->view('Reports/tab_primdetails', $data, true);
        $this->load->template('Reports/index', $data);
    }









   /*
   * TAB - SUMMARY
    */
   private function tab_summary()
   {
       $this->load->model('Reports_model');

        $this->theme_plugin = [
			'css'   => ['globals/plugins/c3js-chart/c3.min.css'],
            'js'    => ['https://www.gstatic.com/charts/loader.js', 'panel/js/view/reports.js', 'globals/plugins/c3js-chart/d3.min.js', 'globals/plugins/c3js-chart/c3.min.js','globals/plugins/chartjs/Chart.bundle.min.js', 'globals/plugins/jquery.print/print.js'],
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                        Reports.Summary.init();'
        ];



        // LOCATION
        $data[ 'location_id' ] = array_key_exists($this->input->get('location'), $this->user->locations_array) 
            ? (int) $this->input->get('location')
            : (int) @current($this->user->locations_array)[ 'id' ];

        // DATES
        if (isValidDate($this->input->get('date'), dateFormat())) {

            $data[ 'date_client_today' ]        = $this->input->get('date');
            $data[ 'date_server_today' ]        = dateConvert($data[ 'date_client_today' ], 'server');
            $data[ 'date_client_today_ymdhis' ] = dateReFormat($data[ 'date_client_today' ], dateFormat(), 'Y-m-d H:i:s');
        } else {

            $data[ 'date_client_today_ymdhis' ] = date('Y-m-d 05:00:00');
            $data[ 'date_server_today' ]        = dateConvert2($data[ 'date_client_today_ymdhis' ], 'server');
            $data[ 'date_client_today' ]        = dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', dateFormat());
        }

        // // // today
        $data[ 'date_client_today_from' ]      = date('Y-m-d H:i:00', strtotime($data[ 'date_client_today_ymdhis' ]));
        $data[ 'date_server_today_from' ]      = dateConvert2($data[ 'date_client_today_from' ], 'server');
        $data[ 'date_server_today_from_unix' ] = strtotime($data[ 'date_server_today_from' ]);
        $data[ 'date_client_today_to' ]        = date('Y-m-d H:00:00', strtotime($data[ 'date_client_today_ymdhis' ] . ' +24 hours'));
        $data[ 'date_server_today_to' ]        = dateConvert2($data[ 'date_client_today_to' ], 'server');
        $data[ 'date_server_today_to_unix' ]   = strtotime($data[ 'date_server_today_to' ]);

        // // // this month
        $data[ 'date_client_tmonth_from' ]      = date('Y-m-01 H:i:s', strtotime($data[ 'date_client_today_from' ]));
        $data[ 'date_server_tmonth_from' ]      = dateConvert2($data[ 'date_client_tmonth_from' ], 'server');
        $data[ 'date_server_tmonth_from_unix' ] = strtotime($data[ 'date_server_tmonth_from' ]);
        $data[ 'date_client_tmonth_to' ]        = date('Y-m-t H:i:s', strtotime($data[ 'date_client_today_to' ]));
        $data[ 'date_server_tmonth_to' ]        = dateConvert2($data[ 'date_client_tmonth_to' ], 'server');
        $data[ 'date_server_tmonth_to_unix' ]   = strtotime($data[ 'date_server_tmonth_to' ]);

        // // // this year
        $data[ 'date_client_tyear_from' ]      = date('Y-01-01 H:i:s', strtotime($data[ 'date_client_today_from' ]));
        $data[ 'date_server_tyear_from' ]      = dateConvert2($data[ 'date_client_tyear_from' ], 'server');
        $data[ 'date_server_tyear_from_unix' ] = strtotime($data[ 'date_server_tyear_from' ]);
        $data[ 'date_client_tyear_to' ]        = date('Y-12-31 H:i:s', strtotime($data[ 'date_client_today_to' ]));
        $data[ 'date_server_tyear_to' ]        = dateConvert2($data[ 'date_client_tyear_to' ], 'server');
        $data[ 'date_server_tyear_to_unix' ]   = strtotime($data[ 'date_server_tyear_to' ]);

        // // // last year from-to 
        $data[ 'date_client_lyear_from' ]      = date('Y-01-01 H:i:s', strtotime($data[ 'date_client_today_from' ] . ' -1 year'));
        $data[ 'date_server_lyear_from' ]      = dateConvert2($data[ 'date_client_lyear_from' ], 'server');
        $data[ 'date_server_lyear_from_unix' ] = strtotime($data[ 'date_server_lyear_from' ]);
        $data[ 'date_client_lyear_to' ]        = date('Y-12-31 H:i:s', strtotime($data[ 'date_client_today_to' ] . ' -1 year'));
        $data[ 'date_server_lyear_to' ]        = dateConvert2($data[ 'date_client_lyear_to' ], 'server');
        $data[ 'date_server_lyear_to_unix' ]   = strtotime($data[ 'date_server_lyear_to' ]);

        // // // last year month from-to 
        $data[ 'date_client_lyear_month_from' ]      = date('Y-m-01 H:i:s', strtotime($data[ 'date_client_today_from' ] . ' -1 year'));
        $data[ 'date_server_lyear_month_from' ]      = dateConvert2($data[ 'date_client_lyear_month_from' ], 'server');
        $data[ 'date_server_lyear_month_from_unix' ] = strtotime($data[ 'date_server_lyear_month_from' ]);
        $data[ 'date_client_lyear_month_to' ]        = date('Y-m-t H:i:s', strtotime($data[ 'date_client_today_to' ] . ' -1 year'));
        $data[ 'date_server_lyear_month_to' ]        = dateConvert2($data[ 'date_client_lyear_month_to' ], 'server');
        $data[ 'date_server_lyear_month_to_unix' ]   = strtotime($data[ 'date_server_lyear_month_to' ]);

        // // // last year today
        $date = new DateTime($data[ 'date_client_today_ymdhis' ]);
        $day = $date->format('l');
        $date->sub(new DateInterval('P1Y'));
        $date->modify('next ' . $day);
        $data[ 'date_client_lyear_today_ymdhis' ] = $date->format('Y-m-d ' . dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', 'H:i:s'));
        $data[ 'date_client_lyear_today' ]        = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', dateFormat());
        $data[ 'date_server_lyear_today' ]        = dateConvert2($data[ 'date_client_lyear_today_ymdhis' ], 'server');
        // $data[ 'date_client_lyear_today_format' ] = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', 'D m/d/y h:i A');
        // // // last year today from-to
        $data[ 'date_client_lyear_today_from' ]      = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', 'Y-m-d H:i:00');
        $data[ 'date_server_lyear_today_from' ]      = dateConvert2($data[ 'date_client_lyear_today_from' ], 'server');
        $data[ 'date_server_lyear_today_from_unix' ] = strtotime($data[ 'date_server_lyear_today_from' ]);
        $data[ 'date_client_lyear_today_to' ]        = dateReFormat(date('Y-m-d H:i:s', strtotime($data[ 'date_client_lyear_today_ymdhis' ] . ' +24 hours')), 'Y-m-d H:i:s', 'Y-m-d H:00:00');
        $data[ 'date_server_lyear_today_to' ]        = dateConvert2($data[ 'date_client_lyear_today_to' ], 'server');
        $data[ 'date_server_lyear_today_to_unix' ]   = strtotime($data[ 'date_server_lyear_today_to' ]);
        // END - DATES
// pr($data,1);

        $data[ 'sales_today' ]               = 0;
        $data[ 'sales_this_month' ]          = 0;
        $data[ 'sales_this_year' ]           = 0;
        $data[ 'sales_last_year_today' ]     = 0;
        $data[ 'sales_last_year_month' ]     = 0;
        $data[ 'sales_last_year' ]           = 0;

        $data[ 'pax_today' ]                 = 0;
        $data[ 'pax_this_month' ]            = 0;
        $data[ 'pax_this_year' ]             = 0;
        $data[ 'pax_last_year_today' ]       = 0;
        $data[ 'pax_last_year_month' ]       = 0;
        $data[ 'pax_last_year' ]             = 0;

        $data[ 'negs_today' ]                = 0;
        $data[ 'negs_this_month' ]           = 0;
        $data[ 'negs_this_year' ]            = 0;
        $data[ 'negs_last_year_today' ]      = 0;
        $data[ 'negs_last_year_month' ]      = 0;
        $data[ 'negs_last_year' ]            = 0;
        
        $data[ 'gallery' ]                   = 0;
        $data[ 'gallery_printed' ]           = 0;
        $data[ 'master_product_sold' ]       = 0;
        
        $data[ 'categorized_products' ]      = [];
        $data[ 'categorized_entrances' ]     = [];
        $data[ 'categorized_product_sales' ] = [];
        $data[ 'currency' ]                  = $this->user->locations_array[ $data[ 'location_id' ] ][ 'currency' ];

        //ana ekran iade ve ikram 

        $data[ 'sales_today_return' ]        = 0;
        $data[ 'sales_today_gift' ]          = 0;

        //açık kapalı adisyon 
        $data['sale_today_open_count']       = 0;
        $data['sale_today_close_count']      = 0;

        //toplam ortalama kazanç
        $data['sale_today_alignment']        = 0;

        $data['sale_today_service_types']   =  [
            '1' => 0, // Normal
            '2' => 0, // Paket
            '3' => 0 // Self
        ];

        $data['sale_today_service_types_amount'] = [
            '1' => 0, // Normal servis tutarı
            '2' => 0, // Paket servis tutarı
            '3' => 0 // Self servis tutarı
        ];

        //payment types

        $data['payment_types'] = $this->payment_types;
        $payment_types_query = '';

        foreach ($data['payment_types'] as $payment_type) {
            $payment_types_query .= "S.{$payment_type->alias},";
            $data['payment'][$payment_type->alias] = 0;
        }


        // sales
        $this->db->where([
            'S.location_id'     => $data[ 'location_id' ],
            'S.payment_date >=' => $data[ 'date_server_lyear_from' ],
            'S.payment_date <=' => $data[ 'date_server_tyear_to' ],
            'S.status'          => "completed",
            'SHP.active !='     => 3,
            'P.active !='       => 3,
        ]);
        $this->db->join('sale_has_products SHP', 'SHP.sale_id = S.id');
        $this->db->join('products P', 'P.id = SHP.product_id');
        $data[ 'sales' ] = $this->Reports_model->sales('
            S.id sale_id, S.return_id sale_return, S.pos_id sale_pos_id, S.amount sale_amount, S.subtotal sale_subtotal, S.gift_amount sale_gift_amount, S.discount_amount discount_amount, S.discount_type discount_type, '.$payment_types_query.' S.status sales_status, S.service_type service_type, UNIX_TIMESTAMP(S.payment_date) payment_date_unix, S.payment_date sale_payment,
            SHP.product_id product_id, SHP.price product_price, SHP.tax product_tax, SHP.quantity product_quantity, SHP.is_gift product_is_gift, SHP.content product_content, SHP.type product_type, SHP.digital product_digital,
            P.product_type_id category_id');

        // PAX+NEGS
        // $this->db->where([
        //     'P.location_id' => $data[ 'location_id' ],
        //     'P.date >='     => $data[ 'date_server_lyear_from' ],
        //     'P.date <='     => $data[ 'date_server_tyear_to' ]
        // ]);
        $data[ 'paxnegs' ] = [];//$this->Reports_model->paxnegs_all('*, UNIX_TIMESTAMP(P.date) date_unix');
        // $data[ 'pax' ]     = (int) @$data[ 'paxnegs' ]->pax ? $data[ 'paxnegs' ]->pax : 1;
        // $data[ 'negs' ]    = (int) @$data[ 'paxnegs' ]->negs;

        // Products
        $this->db
            ->where('P.location_id', $data[ 'location_id' ])
            ->order_by('P.title', 'ASC');
        $data[ 'products' ] = $this->Reports_model->products('P.id, P.title');

        // Product Categories
        $this->db->where('active !=', 3);
        $data[ 'categories' ] = $this->db->get('product_types')->result_array();

        // Entrances
        $this->db->where([
            'location_id' => $data[ 'location_id' ],
            'active !='   => 3,
        ]);
        $data[ 'entrances' ] = $this->db->get('pos')->result_array();

        // WASTE, COMPS, UNSOLD
        $this->db->where([
            'L.location_id' => $data[ 'location_id' ],
            'L.date >='     => $data[ 'date_server_today_from' ],
            'L.date <='     => $data[ 'date_server_today_to' ]
        ]);
        $data[ 'losts' ]  = [];//$this->Reports_model->printer_waste('SUM(L.unsold) as unsold, SUM(L.waste) as waste, SUM(L.comps) as comps, SUM(L.unseen) as unseen');
        $data[ 'waste' ]  = 0;//(int) @$data[ 'losts' ]->waste;
        $data[ 'unsold' ] = 0;//(int) @$data[ 'losts' ]->unsold;
        $data[ 'comps' ]  = 0;//(int) @$data[ 'losts' ]->comps;
        $data[ 'unseen' ] = 0;//(int) @$data[ 'losts' ]->unseen;

        // PRINTER COUNTS
        $data[ 'printers' ] = [];
        // $this->db->where('P.location_id', $data[ 'location_id' ]);
        // $this->db->order_by('P.title', 'ASC');
        // $data[ 'printers' ] = $this->Reports_model->printers('P.id, P.title, P.equal,
        //     IFNULL(
        //         (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date < "'.$data[ 'date_server_today_from' ].'" ORDER BY PHC.date DESC LIMIT 1),
        //         (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date BETWEEN "'.$data[ 'date_server_today_from' ].'" AND "'.$data[ 'date_server_today_to' ].'" ORDER BY PHC.date ASC LIMIT 1)
        //     ) as `countMin`,
        //     (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date BETWEEN "'.$data[ 'date_server_today_from' ].'" AND "'.$data[ 'date_server_today_to' ].'" ORDER BY PHC.date DESC LIMIT 1) as `countMax`');


        $data[ 'master_product' ] = [ 'id' => 0, 'content' => 0];
        foreach ($data[ 'products' ] as $product) {
            if ($product[ 'master' ]) {
                $data[ 'master_product' ] = $product;
                break;
            }
        }
        
        // CATEGORIES
        if ($data[ 'categories' ]) {
            foreach ($data[ 'categories' ] as $category) {
                $data[ 'categorized_products' ][ $category[ 'id' ] ]            = $category;
                $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] = 0;
            }
        }


        // PRODUCTS
        if ($data[ 'products' ]) {
            foreach ($data[ 'products' ] as $product) {
                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ]            = $product;
                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] = 0;
            }
        }

        // ENTRANCE
        if ($data[ 'entrances' ]) {
            foreach ($data[ 'entrances' ] as $entrance) {
                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ]            = $entrance;
                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] = 0;
            }
        }





        $data[ 'sales_today_processed' ]           = NULL;
        $data[ 'sales_this_month_processed' ]      = NULL;
        $data[ 'sales_this_year_processed' ]       = NULL;
        $data[ 'sales_last_year_today_processed' ] = NULL;
        $data[ 'sales_last_year_processed' ]       = NULL;
        $data[ 'sales_last_year_month_processed' ] = NULL;


        // SALES for 2 Years 
        foreach ($data[ 'sales' ] as $sale) {

            

            // TODAY
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_today_from_unix' ], $data[ 'date_server_today_to_unix' ])) {

                // SALES
                if (!isset($data[ 'sales_today_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_today_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    if ($sale[ 'sales_status' ] == "pending") {
                        $data['sale_today_open_count']++;
                        continue;
                    }


                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_today' ] -= $sale[ 'sale_amount' ];
                        $data[ 'sales_today_return' ] += $sale[ 'sale_amount' ];
                        $data['sale_today_service_types_amount'][$sale['service_type']] -= $sale['sale_amount'];


                        foreach ($data['payment_types'] as $payment_type) {
                            $data['payment'][$payment_type->alias] -= $sale[$payment_type->alias];
                        }


                    } else {
                        $data[ 'sales_today' ] += $sale[ 'sale_amount' ];
                        $data['sale_today_service_types_amount'][$sale['service_type']] += $sale['sale_amount'];

                        foreach ($data['payment_types'] as $payment_type) {
                            $data['payment'][$payment_type->alias] += $sale[$payment_type->alias];
                        }
                    } 

                    //GIFT CALCULATE
                    
                    $data[ 'sales_today_gift' ] += $sale[ 'sale_gift_amount' ];

                    $data['sale_today_close_count']++;

                    $data['sale_today_service_types'][$sale['service_type']]++;
                    
                    
                }

                if ($sale[ 'sales_status' ] == "pending") {
                    continue;
                }


                // CATEGORIES
                if ($data[ 'categories' ]) {
                    foreach ($data[ 'categories' ] as $category) {

                        if ($sale[ 'category_id' ]  == $category[ 'id' ]) {
                            if (!$sale[ 'product_is_gift' ]) {

                                $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                                $total = $sale[ 'product_tax' ] * $total / 100 + $total;

                                if ($sale['discount_amount']) {
                                    if ($sale['discount_type'] == "percent") {
                                        $total -= $total*$sale['discount_amount']/100;
                                    } else {
                                        $total = $total - $sale['discount_amount'];
                                    }
                                } 


                                // SALES AND REFUND
                                if ($sale[ 'sale_return' ]) {
                                    $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] -= $total;


                                } else {
                                    $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] += $total;

                                } 
                            
                            }

                            

                        }
                    }
                }    
                // END - CATEGORIES
                
                

                // PRODUCTS
                if ($data[ 'products' ]) {
                    foreach ($data[ 'products' ] as $product) {

                        if ($sale[ 'product_id' ] == $product[ 'id' ]) {

                            if (!$sale[ 'product_is_gift' ]) {

                                $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                                $total = $sale[ 'product_tax' ] * $total / 100 + $total;

                                if ($sale['discount_amount']) {
                                    if ($sale['discount_type'] == "percent") {
                                        $total -= $total*$sale['discount_amount']/100;
                                    } else {
                                        $total = $total - $sale['discount_amount'];
                                    }
                                }   


                                $total = round($total, 2);

                                // SALES AND REFUND
                                if ($sale[ 'sale_return' ]) {
                                    $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] -= $total;

                                } else {
                                    $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] += $total;
                                }

                            }


                                             
                        }
                    }
                }
                // END - PRODUCTS
                


                // ENTRANCE
                if ($data[ 'entrances' ]) {
                    foreach ($data[ 'entrances' ] as $entrance) {

                        if ($sale[ 'sale_pos_id' ] == $entrance[ 'id' ]) {

                            if (!$sale[ 'product_is_gift' ]) {

                                $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                                $total = $sale[ 'product_tax' ] * $total / 100 + $total;
                                $total = round($total, 2);

                                if ($sale['discount_amount']) {
                                    if ($sale['discount_type'] == "percent") {
                                        $total -= $total*$sale['discount_amount']/100;
                                    } else {
                                        $total = $total - $sale['discount_amount'];
                                    }
                                } 

                                // SALES AND REFUND
                                if ($sale[ 'sale_return' ]) {
                                    $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] -= $total;

                                } else {
                                    $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] += $total;
                                }   
                            }                 
                        }
                    }
                }
                // END - ENTRANCE
            


            }
            // END - TODAY

            if ($sale[ 'sales_status' ] == "pending") {
                continue;
            }


            // THIS MONTH
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_tmonth_from_unix' ], $data[ 'date_server_tmonth_to_unix' ])) {
                


                // THIS MONTH
                if (!isset($data[ 'sales_this_month_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_this_month_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_this_month' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_this_month' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - THIS MONTH
            
             

            // THIS YEAR
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_tyear_from_unix' ], $data[ 'date_server_tyear_to_unix' ])) {
                


                // SALES THIS YEAR
                if (!isset($data[ 'sales_this_year_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_this_year_processed' ][ $sale[ 'sale_id' ] ] = 1;
                    
                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_this_year' ] -= round($sale[ 'sale_amount' ], 2);

                    } else {
                        $data[ 'sales_this_year' ] += round($sale[ 'sale_amount' ], 2);
                    } 

                }



            }
            // END - THIS YEAR
            
            

            // LAST YEAR TODAY
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_today_from_unix' ], $data[ 'date_server_lyear_today_to_unix' ])) {
                


                // SALES LAST YEAR TODAY
                if (!isset($data[ 'sales_last_year_today_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_today_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year_today' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year_today' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - LAST YEAR TODAY


            // LAST YEAR
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_from_unix' ], $data[ 'date_server_lyear_to_unix' ])) {
                

                // SALES LAST YEAR
                if (!isset($data[ 'sales_last_year_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year' ] += $sale[ 'sale_amount' ];
                    } 

                }


            }
            // END - LAST YEAR
            


            // LAST YEAR MONTH
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_month_from_unix' ], $data[ 'date_server_lyear_month_to_unix' ])) {
                


                // LAST YEAR MONTH
                if (!isset($data[ 'sales_last_year_month_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_month_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year_month' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year_month' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - LAST YEAR MONTH

    
        }


        // PAX and NEGS data for 2 YEARS
        foreach ($data[ 'paxnegs' ] as $paxnegs) {
            
            // TODAY
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_today_from_unix' ], $data[ 'date_server_today_to_unix' ])) {
                $data[ 'pax_today' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_today' ] += $paxnegs[ 'negs' ];
            }
            // END - TODAY

            // THIS MONTH
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_tmonth_from_unix' ], $data[ 'date_server_tmonth_to_unix' ])) {
                $data[ 'pax_this_month' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_this_month' ] += $paxnegs[ 'negs' ];
            }
            // END - THIS MONTH
            
            // THIS YEAR
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_tyear_from_unix' ], $data[ 'date_server_tyear_to_unix' ])) {
                $data[ 'pax_this_year' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_this_year' ] += $paxnegs[ 'negs' ];
            }
            // END - THIS YEAR
            
            // LAST YEAR TODAY
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_today_from_unix' ], $data[ 'date_server_lyear_today_to_unix' ])) {
                $data[ 'pax_last_year_today' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year_today' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR TODAY
            
            // LAST YEAR
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_from_unix' ], $data[ 'date_server_lyear_to_unix' ])) {
                $data[ 'pax_last_year' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR
            
            // LAST YEAR MONTH
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_month_from_unix' ], $data[ 'date_server_lyear_month_to_unix' ])) {
                $data[ 'pax_last_year_month' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year_month' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR MONTH
        }

        $data[ 'pax_today' ]                 = $data[ 'pax_today' ] ? $data[ 'pax_today' ] : 1;
        $data[ 'pax_this_month' ]            = $data[ 'pax_this_month' ] ? $data[ 'pax_this_month' ] : 1;
        $data[ 'pax_this_year' ]             = $data[ 'pax_this_year' ] ? $data[ 'pax_this_year' ] : 1;
        $data[ 'pax_last_year_today' ]       = $data[ 'pax_last_year_today' ] ? $data[ 'pax_last_year_today' ] : 1;
        $data[ 'pax_last_year_month' ]       = $data[ 'pax_last_year_month' ] ? $data[ 'pax_last_year_month' ] : 1;
        $data[ 'pax_last_year' ]             = $data[ 'pax_last_year' ] ? $data[ 'pax_last_year' ] : 1;

        $data[ 'negs_today' ]                = $data[ 'negs_today' ] ? $data[ 'negs_today' ] : 1;
        $data[ 'negs_this_month' ]           = $data[ 'negs_this_month' ] ? $data[ 'negs_this_month' ] : 1;
        $data[ 'negs_this_year' ]            = $data[ 'negs_this_year' ] ? $data[ 'negs_this_year' ] : 1;
        $data[ 'negs_last_year_today' ]      = $data[ 'negs_last_year_today' ] ? $data[ 'negs_last_year_today' ] : 1;
        $data[ 'negs_last_year_month' ]      = $data[ 'negs_last_year_month' ] ? $data[ 'negs_last_year_month' ] : 1;
        $data[ 'negs_last_year' ]            = $data[ 'negs_last_year' ] ? $data[ 'negs_last_year' ] : 1;



        $data[ 'total_print' ] = 0;
        if ($data[ 'printers' ]) {
            foreach ($data[ 'printers' ] as $p) {
                if (!is_null($p[ 'countMax' ]) && !is_null($p[ 'countMin' ])) {
                    $data[ 'total_print' ] += ($p[ 'countMax' ] - $p[ 'countMin' ]) / $p[ 'equal' ];
                } 
            }
        }


        // GALLERY
        if ($data[ 'gallery' ] && $data[ 'master_product' ][ 'content' ]) {

            // Yeni formul için eklendi
            $data[ 'gallery_printed' ] = ($data[ 'total_print' ] - $data[ 'waste' ] - $data[ 'gallery' ]) / $data[ 'master_product' ][ 'content' ];


            $data[ 'gallery' ] = $data[ 'gallery' ] + ($data[ 'waste' ] + $data[ 'unsold' ] + $data[ 'comps' ]);
            $data[ 'gallery' ] = $data[ 'total_print' ] - $data[ 'gallery' ];
            $data[ 'gallery' ] = $data[ 'gallery' ] / $data[ 'master_product' ][ 'content' ];
        } else {
            $data[ 'gallery' ] = 1;
            $data[ 'gallery_printed' ] = 1;
        }


        // CHARTS
        $piechart_category[] = ['Başlık', 'Değer'];
        foreach ($data['categorized_products'] as $val) {
            $piechart_category[] = [$val['title'], $val['total']];
        }
        $piechart_categorized_product_sales[] = ['Başlık', 'Değer'];
        foreach ($data['categorized_product_sales'] as $val) {
            $piechart_categorized_product_sales[] = [$val['title'], $val['total']];
        }
        $piechart_categorized_entrances[] = ['Başlık', 'Değer'];
        foreach ($data['categorized_entrances'] as $val) {
            $piechart_categorized_entrances[] = [$val['title'], $val['total']];
        }
        $this->theme_plugin['start'] .= '
        Reports.init();
        Reports.Adisyon.init({
            sales_status_pie: {
                open: '.$data['sale_today_open_count'].',
                close: '.$data['sale_today_close_count'].'
            },
            sale_service_type_doughnut: '.json_encode($data['sale_today_service_types']).',
            sale_service_type_amount: '.json_encode($data['sale_today_service_types_amount']).',
            currency: '.json_encode($data['currency']).',

        });
        $(window).resize(function(){
          Reports.charts("piechart-category", '.json_encode($piechart_category).');

          Reports.charts("piechart-category-sales", '.json_encode($piechart_categorized_product_sales).');

          Reports.charts("piechart-entrance", '.json_encode($piechart_categorized_entrances).');
        });';

        //Günlük ortalama kazanç hesaplama 

        if ($data['sales_today']) {
            $data['sale_today_alignment'] =$data['sales_today'] / $data['sale_today_close_count'];
        }

        $data['tab_content'] = $this->load->view('Reports/tab_summary', $data, true);
        $this->load->template('Reports/index', $data);
   }







   private function tab_stock()
   {

        $this->theme_plugin = [
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");'
        ];


        // LOCATION
        $data[ 'location_id' ] = array_key_exists($this->input->get('location'), $this->user->locations_array) 
            ? (int) $this->input->get('location')
            : (int) @current($this->user->locations_array)[ 'id' ];

        // DATES
        if (isValidDate($this->input->get('date_from'), dateFormat())) {

            $data[ 'date_client_from' ] = $this->input->get('date_from');
            $data[ 'date_server_from' ] = dateConvert($data[ 'date_client_from' ], 'server');
        } else {
            $data[ 'date_server_from' ] = date('Y-m-d H:i:s');
            $data[ 'date_client_from' ] = dateConvert($data[ 'date_server_from' ], 'client', dateFormat());
        }
        if (isValidDate($this->input->get('date_to'), dateFormat())) {

            $data[ 'date_client_to' ] = $this->input->get('date_to');
            $data[ 'date_server_to' ] = dateConvert($data[ 'date_client_to' ], 'server');
        } else {
            $data[ 'date_server_to' ] = date('Y-m-d H:i:s', strtotime('+24 hours'));
            $data[ 'date_client_to' ] = dateConvert($data[ 'date_server_to' ], 'client', dateFormat());
        }

        $data['stocks'] = $this->db
            ->where('active', 1)
            ->where('location_id', $data['location_id'])
            ->order_by('title', 'asc')
            ->get('stocks')->result_array();

        $data['input']  = [];
        $data['output'] = [];
        foreach ($data['stocks'] as $stock) {

            // INPUT
            $input = $this->db
            ->select('SUM(quantity) as total')
            ->where([
                'created >=' => $data['date_server_from'],
                'created <=' => $data['date_server_to'],
                'active'     => 1,
                'stock_id'   => $stock['id']
            ])
            ->get('stock_contents')->row();

            $data['input'][$stock['id']] = $input->total;
            // END - INPUT
        }






        // OUTPUT
        $data['sales'] = $this->db
        ->select('id')
        ->where([
            'payment_date >=' => $data['date_server_from'],
            'payment_date <=' => $data['date_server_to'],
            'status'          => 'completed',
            'return_id'       => null,
            'active'          => 1,
        ])->get('sales')->result_array();

        if ($data['sales']) {
            foreach ($data['sales'] as $sale) {





                $data['sale_has_products'] = $this->db
                ->select('id, product_id, quantity, type')
                ->where([
                    'sale_id' => $sale['id'],
                    'active'  => 1,
                ])->get('sale_has_products')->result_array();

                if ($data['sale_has_products']) {
                    foreach ($data['sale_has_products'] as $sale_has_product) {

                        
                        if ($sale_has_product['type'] == 'product') {
                            
                            $data['product_has_stocks'] = $this->db
                            ->select('quantity, stock_id')
                            ->where([
                                'product_id' => $sale_has_product['product_id'],
                            ])->get('product_has_stocks')->result_array();

                            if ($data['product_has_stocks']) {
                                foreach ($data['product_has_stocks'] as $product_has_stock) {
                                    @$data['output'][$product_has_stock['stock_id']] += $product_has_stock['quantity'] * $sale_has_product['quantity'];
                                }
                            }

                            unset($data['product_has_stocks']);

                        } else {


                            $data['package_has_products'] = $this->db
                            ->select('quantity, product_id')
                            ->where([
                                'package_id' => $sale_has_product['id'],
                                'active'  => 1,
                            ])->get('package_has_products')->result_array();

                            if ($data['package_has_products']) {
                                
                                foreach ($data['package_has_products'] as $package_has_product) {
                                    $data['product_has_stocks'] = $this->db
                                    ->select('quantity, stock_id')
                                    ->where([
                                        'product_id' => $package_has_product['product_id'],
                                    ])->get('product_has_stocks')->result_array();

                                    if ($data['product_has_stocks']) {
                                        foreach ($data['product_has_stocks'] as $product_has_stock) {
                                            @$data['output'][$product_has_stock['stock_id']] += $product_has_stock['quantity'] * $package_has_product['quantity'];
                                        }
                                    }

                                    unset($data['product_has_stocks']);
                                }
                            }

                        }

                    }
                }



            }
        }
        // END - OUTPUT

        
        $data['tab_content'] = $this->load->view('Reports/tab_stock', $data, true);
        $this->load->template('Reports/index', $data); 
   }

    private function tab_endofday()
    {
        $data['current_location'] = (int) $this->input->get('location');

        if (!$data['current_location']) {
            $data['current_location'] = $this->user->locations[0];
        }

        $data['payment_types'] = $this->payment_types;

        $data['endofdays'] = $this->db->where([
            'location_id' => $data['current_location'],
            'active' => 1,
        ])
        ->order_by('id', 'desc')
        ->get('endofdays')->result();

        $this->theme_plugin['js']    = array('panel/js/view/reports.js');
        $this->theme_plugin['start'] = 'TablesDataTables.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'"); Reports.endOfDay();';

        $data['tab_content'] = $this->load->view('Reports/tab_endofday', $data, true);
        $this->load->template('Reports/index', $data); 
    }


    protected function tab_fullness()
    {
        $this->load->model('Reports_model');

        // LOCATION
        $data[ 'location_id' ] = array_key_exists($this->input->get('location'), $this->user->locations_array) 
            ? (int) $this->input->get('location')
            : (int) @current($this->user->locations_array)[ 'id' ];


        // DATES
        if (isValidDate($this->input->get('date'), dateFormat1())) {

            $data[ 'date_client_today' ]        = $this->input->get('date');
            $data[ 'date_client_today_ymdhis' ] = dateReFormat($data[ 'date_client_today' ], dateFormat1(), 'Y-m-d 00:00:00');
            $data[ 'date_client_today_format' ] = dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', dateFormat1());
            $data[ 'date_server_today' ]        = dateConvert2($data[ 'date_client_today_ymdhis' ], 'server');
        } else {
            $data[ 'date_server_today' ]        = date('Y-m-d H:i:s');
            $data[ 'date_client_today' ]        = dateConvert2($data[ 'date_server_today' ], 'client', 'Y-m-d H:i:s', dateFormat1());
            $data[ 'date_client_today_ymdhis' ] = dateReFormat($data[ 'date_client_today' ], dateFormat1(), 'Y-m-d 00:00:00');
            $data[ 'date_client_today_format' ] = dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', dateFormat1());
            $data[ 'date_server_today' ]        = dateConvert2($data[ 'date_client_today_ymdhis' ], 'server');
        }

        // // today
        $data[ 'date_client_today_from' ]      = date('Y-m-d H:i:00', strtotime($data[ 'date_client_today_ymdhis' ]));
        $data[ 'date_server_today_from' ]      = dateConvert2($data[ 'date_client_today_from' ], 'server');
        $data[ 'date_server_today_from_unix' ] = strtotime($data[ 'date_server_today_from' ]);
        $data[ 'date_client_today_to' ]        = date('Y-m-d H:00:00', strtotime($data[ 'date_client_today_ymdhis' ] . ' +24 hours'));
        $data[ 'date_server_today_to' ]        = dateConvert2($data[ 'date_client_today_to' ], 'server');
        $data[ 'date_server_today_to_unix' ]   = strtotime($data[ 'date_server_today_to' ]);


        // Hours of today
        $data['times'] = null;
        for ($i = 0; $i <= 23; $i++) {

            if ($i == 0) {

                $data['times'][$i] = [
                    'format' => ($i<10?'0':'') . $i,
                    'from'   => $data['date_server_today_from_unix'],
                    'to'     => $data['date_server_today_from_unix'] + 1*60*60,
                    'total'  => 0
                ];
            } else {

                $data['times'][$i] = [
                    'format' => ($i<10?'0':'') . $i,
                    'from'   => $data['times'][$i-1]['from'] + 1*60*60 ,
                    'to'     => $data['times'][$i-1]['to'] + 1*60*60,
                    'total'  => 0
                ];
            }

        }

        // sales
        $this->db
        ->group_start()
            ->where([
                'S.payment_date >=' => $data[ 'date_server_today_from' ],
                'S.payment_date <=' => $data[ 'date_server_today_to' ],
            ])
            ->or_where([
                'S.created >=' => $data[ 'date_server_today_from' ],
                'S.created <=' => $data[ 'date_server_today_to' ],
            ])

        ->group_end()
        ->where_in('S.status', ['completed', 'pending'])
        ->where([
            'S.location_id'     => $data[ 'location_id' ],
            'S.return_id'       => null,
        ]);
        $sales = $this->Reports_model->sales('S.id sale_id, S.return_id sale_return,
            UNIX_TIMESTAMP(S.created) opened_date_unix, UNIX_TIMESTAMP(S.payment_date) closed_date_unix');

        foreach ($sales as $sale) {
            foreach ($data['times'] as &$time) {
                if (between($sale['opened_date_unix'], $time['from'], $time['to'] - 1) || 
                    between($sale['closed_date_unix'], $time['from'], $time['to'] - 1) ||
                    between($time['from'], $sale['opened_date_unix'], $sale['closed_date_unix'] - 1) ||
                    between($time['to'], $sale['opened_date_unix'], $sale['closed_date_unix'] - 1))
                {
                    $time['total']++;
                }
            }
        }

        $chart[] = ['Time', 'Fullness', ['role' => 'annotation'], ['role' => 'tooltip']];
        foreach ($data['times'] as $_time) {
            $chart[] = [$_time['format'], $_time['total'], $_time['total'], "Masa Sayısı"];
        }

        $this->theme_plugin = [
            'js'    => ['https://www.gstatic.com/charts/loader.js', 'panel/js/view/reports.js'],
            'start' => 'MyFunction.datetimepicker("'.dateFormat1(TRUE).'");'
        ];

        $this->theme_plugin['start'] .= '
        Reports.init_Fullness();
        $(window).resize(function(){
            Reports.charts_Fullness("chart-fullness", '.json_encode($chart).');
        });';



        $data['tab_content'] = $this->load->view('Reports/tab_fullness', $data, true);
        $this->load->template('Reports/index', $data);
    }


    public function ajax1()
    {
        $event = $this->input->post('event');

        switch ($event) {
            case 'createEndOfDay':
                $this->createEndOfDay();
                break;

            case 'users':
                $this->users();
                break;        
            
            default:
                # code...
                break;
        }
    }

    public function users() 
    {
        $this->load->model('Reports_model');        

        $location_id = $this->input->post('l');

        $this->db
            ->where('UHL.location_id', $location_id)
            ->order_by('U.username', 'ASC');
        $data = $this->Reports_model->usersandlocations('U.id, U.username');


        messageAJAX('success','', $data);

    }

    public function deleteEndofday($id = null)
    {
        $id = (int) $id;

        $this->db->where('id', $id);
        $this->db->update('endofdays', ['active' => 3, 'modified' => date('Y-m-d H:i:s')]);

        messageAJAX('success', __('Gün Sonu silindi.'));
    }

    protected function createEndOfDay()
    {
        $locaiton = (int) $this->input->post('location');

        if (!array_key_exists($locaiton, $this->user->locations_array)) {
            messageAJAX('error', __('Restoran Seçiniz.'));
        }


        $endofday = $this->db->where([
            'location_id' => $locaiton,
            'active' => 1
        ])->order_by('id', 'desc')->get('endofdays', 1)->row();

        if ($endofday) {
            $data['start_date'] = date('Y-m-d H:i:00', strtotime($endofday->end_date));
        } else {

            $data['start_date'] = $this->input->post('first_date');

            if (!isValidDate($data['start_date'], dateFormat())) {
                messageAJAX('error', __('Hatalı tarih formatı.'));
            }

            $data['start_date'] = dateConvert2($data['start_date'], 'server', dateFormat(), 'Y-m-d H:i:00');
        }

        $data['end_date'] = date('Y-m-d H:i:00');


        if (strtotime($data['end_date']) <= strtotime($data['start_date'])) {
            messageAJAX('error', __('Başlangıç tarihi bitiş tarihinden büyük veya eşit olamaz.'));
        }

        

        $sales = $this->db->where([
            'location_id'     => $locaiton,
            'payment_date >=' => $data['start_date'],
            'payment_date <=' => $data['end_date'],
            'status'          => 'completed',
            'return_id'       => null,
            'active'          => 1,
        ])->get('sales')->result_array();


        $data += [
            'location_id' => $locaiton,
            'amount'      => 0,
            'created'     => $data['end_date'],
            'active'      => 1
        ];

        foreach ($this->payment_types as $payment_type) {
            $data[$payment_type->alias] = 0;
        }


        if ($sales) {
            foreach ($sales as $sale) {

                if ($sale[ 'return_id' ]) {

                    $data[ 'amount' ]  -= $sale[ 'amount' ];

                    foreach ($this->payment_types as $payment_type) {
                         $data[$payment_type->alias] -= $sale[$payment_type->alias];
                    }

                } else {
                    $data['amount']  += $sale[ 'amount' ];

                    foreach ($this->payment_types as $payment_type) {
                        $data[$payment_type->alias] += $sale[$payment_type->alias];
                    }
                }
                
            }
        }

        $this->db->insert('endofdays', $data);
        messageAJAX('success');
    }
}
?>
