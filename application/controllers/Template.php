<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        $this->load->model('Template_model');

    }

    //Ticket Template
    public function ticket($id)
    {
        $id = (int)$id;

        if(!$id) redirect(base_url(),'refresh');

        if(!$_POST):

            $this->theme_plugin = array(
                'css' => array('globals/plugins/components-summernote/dist/summernote.css'),
                'js' => array('globals/plugins/components-summernote/dist/summernote.min.js' , 'globals/scripts/forms-wysiwyg.js'),
                'start' => 'FormsWysiwyg.summernoteDefault();',
                );

            $data['tmp'] = $this->Template_model->get_ticket($id);

            $this->load->template('Template/ticket' , $data);

        else:
            $this->load->library('form_validation');
            $this->load->helper('security');

            $this->form_validation->set_rules('header', 'Sayfa başlığı', 'trim|required');
            $this->form_validation->set_rules('footer', 'Sayfa altlığı', 'trim|required');

            if($this->form_validation->run() === FALSE):
                messageAJAX('error' , validation_errors() );
            else:

                $this->Template_model->save_ticket($id);

            endif;

        endif;
        
    }

        //Ticket Template
    public function invoice($id)
    {
        $id = (int)$id;

        if(!$id) redirect(base_url(),'refresh');

        if(!$_POST):

            $this->theme_plugin = array(
                'css' => array('globals/plugins/components-summernote/dist/summernote.css'),
                'js' => array('globals/plugins/components-summernote/dist/summernote.min.js' , 'globals/scripts/forms-wysiwyg.js'),
                'start' => 'FormsWysiwyg.summernoteDefault();',
                );

            $data['tmp'] = $this->Template_model->get_invoice($id);

            $this->load->template('Template/invoice' , $data);

        else:
            $this->load->library('form_validation');
            $this->load->helper('security');

            $this->form_validation->set_rules('header', 'Sayfa başlığı', 'trim|required');
            $this->form_validation->set_rules('footer', 'Sayfa altlığı', 'trim|required');

            if($this->form_validation->run() === FALSE):
                messageAJAX('error' , validation_errors() );
            else:

                $this->Template_model->save_invoice($id);

            endif;

        endif;
        
    }

}

/* End of file Tickets.php */
/* Location: ./application/controllers/Tickets.php */
