<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banks extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        $this->check_auth();

        $this->load->model('Banks_M');
        $this->load->model('Pos_M');
        $this->load->model('Transactions_M');

    }

    public function index()
    {
        $location_id = $this->input->get('location_id');
        if ($location_id) {
            $this->db->where(['location_id' => $location_id]);
        } 
        $banks = $this->Banks_M->where('location_id', $this->user->locations)->get_all();
        if ($location_id) {
            $this->db->where(['location_id' => $location_id]);
        } 

        $pos = $this->Pos_M->where('location_id', $this->user->locations)->get_all();

        $accounts = [];
        $i= 0;
        foreach ($banks as $bank) {
            $accounts[$i] = $bank;
            $accounts[$i]->type = "bank";
            $i++;
        }

        foreach ($pos as $single_pos) {
            $accounts[$i] = $single_pos;
            $accounts[$i]->type = "pos";
            $i++;
        }

        $this->theme_plugin = [
            'css' => ['panel/css/view/cari.css'],
            'js' => ['panel/js/view/cari.js'],
            'start' => 'TablesDataTables.init(); Cari.init();'
        ];
        $this->load->template('Cari/Banks/index', compact('banks', 'location_id', 'accounts'));

    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($id = $this->Banks_M->from_form()->insert()) {

                 $datetime = date("Y-m-d H:i:s");

                 $data = [
                    'bank_id' => $id,
                    'pos_id' => null, 
                    'type_id' => 6,
                    'related_transaction_id' => null,
                    'amount'  => $this->input->post('amount'),
                    'bank_amount' => $this->input->post('amount'),
                    'desc'  => null,
                    'increment' => 1,
                    'date' => $datetime,
                    'created' => $datetime,
                 ];

                 if($this->db->insert('transactions', $data)) {
                    messageAJAX('success', __('Banka başarıyla oluşturuldu.'));
                 }


            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Cari/Banks/add');
    }

    public function edit($id = NULL)
    {
        $bank = $this->Banks_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Banks_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Banka başarıyla güncellendi.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Cari/Banks/edit', compact('bank'));    
    }

    public function delete($id = NULL)
    {
        $this->Banks_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Banks_M->update([
            'active' => 3
        ], ['id' => $id]);

        messageAJAX('success', __('Kat başarıyla silindi.'));
    }

    public function detail($id = NULL)  
    {
        $bank = $this->Banks_M->where('location_id', $this->user->locations)->findOrFail($id);
        $types = $this->Transactions_M->getTypes();

        $dFrom = date('Y-m-d H:i:00', strtotime('-24 hours'));
        $dFrom = $this->input->get('d');

        $dTo = date('Y-m-d H:i:59');
        $dTo = $this->input->get('dTo');

        $date['date_from'] = isValidDate($dFrom, dateFormat()) ? dateConvert($dFrom, 'server', 'Y-m-d H:i:00') : date('Y-m-d H:i:00', strtotime('-24 hours'));
        $date['date_to'] = isValidDate($dTo, dateFormat()) ? dateConvert($dTo, 'server', 'Y-m-d H:i:00') : date('Y-m-d H:i:00');

        $transactions = $this->Transactions_M
        ->fields('*')
        ->with_related_transaction([
            'fields' => [
                '*',
                '(
                    CASE 
                        WHEN customer_id IS NOT NULL THEN (SELECT name FROM customers where customers.id = transactions.customer_id)
                        WHEN bank_id IS NOT NULL THEN (SELECT title FROM banks where banks.id = transactions.bank_id)
                        WHEN pos_id IS NOT NULL THEN (SELECT title FROM pos where pos.id = transactions.pos_id)
                    END
                ) as account_title'
            ]
        ])
        ->where('date >=', $date['date_from'])
        ->where('date <=', $date['date_to'])
        ->get_all([
            'bank_id' => $id
        ]);

        $this->theme_plugin = [
            'start' => 'TablesDataTables.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'");'
        ];

        $this->load->template('Cari/Banks/detail', compact('transactions', 'types', 'date', 'bank'));
    }

}

/* End of file Configuration.php */
/* Location: ./application/controllers/Configuration.php */
