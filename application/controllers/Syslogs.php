<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Syslogs extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Syslogs_model');
    }

    function index($page = 1)
    {
        $this->check_auth();
        
        $page = (int)$page ? $page : 1;

        $this->theme_plugin = array(
            'js'    => ['panel/js/view/syslogs.js'],
            'start' => 'Syslogs.index();'
        );

        $data['logs'] = $this->Syslogs_model->index($page);

        $this->load->template('Syslogs/index', $data);
    }

    function ajax()
    {
        $target = $this->input->post('target');
        $logId = (int) $this->input->post('log');

        if($target == 'json') {

            $this->db->select('json');
            $this->db->where('id', $logId);
            if( $log = $this->db->get('logs', 1)->row() ) {

                exit($log->json);
            }
        }
    }
}
?>