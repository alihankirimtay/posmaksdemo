<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Producttypes extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->check_auth();
        
        $this->load->model('ProductTypes_model');
    }

    function index()
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $this->db->select('*, (SELECT title FROM product_types p WHERE p.id = product_types.parent_id LIMIT 1) as parent');
        $data['producttypes'] = $this->ProductTypes_model->index();

    	$this->load->template('ProductTypes/index', $data);
    }

    function add()
    {
        $types = $this->db->where([
            'parent_id' => null
        ])
        ->order_by('title')
        ->get('product_types')->result_array();

        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('parent_id', 'Üst Kategori', 'trim|in_list['.implode(',', array_column($types, 'id')).']');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('sort', 'Sıra', 'trim|integer');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->ProductTypes_model->add();
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('ProductTypes/add', compact('types'));
        }       
    }

    function view($id = NULL)
    {
        $id = (int) $id;

        $data = $this->ProductTypes_model->view($id);
    }

    function edit($id = NULL)
    {
        if(!$data['producttype'] = $this->exists('product_types', "id = $id", '*')) {

            show_404();
        }

        $data['types'] = $this->db->where([
            'id !=' => $id,
            'parent_id' => null,
        ])
        ->order_by('title')
        ->get('product_types')->result_array();

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('parent_id', 'Üst Kategori', 'trim|in_list['.implode(',', array_column($data['types'], 'id')).']');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('sort', 'Sıra', 'trim|integer');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->ProductTypes_model->edit($id);
            }
            
            messageAJAX('error', validation_errors());

        } else {
            
            $this->ProductTypes_model->edit($id);

            $this->load->template('ProductTypes/edit', $data);
        }
    }

    function delete($id = NULL)
    {
        $id = (int) $id;

        if(!$product = $this->exists('product_types', "id = $id")) {

            show_404();
        }

        $this->db
        ->set('active', 3)
        ->where('id', $id)
        ->update('product_types');

        messageAJAX('success', __('Ürün kategorisi silindi.'));
        
        
    }
}
?>
