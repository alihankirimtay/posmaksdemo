<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);
        $this->load->model('Customers_M');
    }

    function index()
    {
        $customers = $this->Customers_M->where([
            'isSupplier' => 1,
            'active !='  => 3
        ])->get_all();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

    	$this->load->template('Suppliers/index', compact('customers'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Customers_M->from_form()->insert()) {
                messageAJAX('success', __('Tedarikçi başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Suppliers/add');
    }

    public function edit($id = null)
    {
        $customer = $this->Customers_M->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Customers_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Tedarikçi başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Suppliers/edit', compact('customer'));
    }

    public function delete($id = null)
    {
        $this->Customers_M->findOrFail($id);

        $this->Customers_M->delete(['id' => $id]);

        messageAJAX('success', __('Tedarikçi başarıyla silindi.'));
    }

}