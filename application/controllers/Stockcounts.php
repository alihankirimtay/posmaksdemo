<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stockcounts extends Auth_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Stocks_model', 'Stocks');
        $this->load->model('Stocks_M');
        $this->load->model('Sales_M');
        $this->load->model('Stockcounts_M', 'StockCounts_M');
        $this->load->model('Stocksstockcounts_M', 'StocksStockCounts_M');
        $this->check_auth([
            'allowed' => ['ajax']
        ]);  		
    }

    public function ajax($event = null)
	{
		if(method_exists(__CLASS__, (String) $event)) {
			$this->{$event}();
		} else {
			messageAJAX('error', 'Invalid request!');
		}
	}

	public function index()
	{

        $location_id = $this->input->get('l');
        $start_date = $this->input->get('d');
        $end_date = $this->input->get('dTo');

        if (!in_array($location_id, $this->user->locations)) {
           $location_id = $this->user->locations[0];
        }

        if (!isValidDate($start_date, dateFormat())) {
            $start_date = date('Y-m-d H:i:00', strtotime('-24 hours'));
        } else {
           $start_date = dateConvert($start_date, 'server');
        }

        if (!isValidDate($end_date, dateFormat())) {
            $end_date = date('Y-m-d H:i:00', strtotime('-24 hours'));
        } else {
           $end_date = dateConvert($end_date, 'server');
        }

        $stock_counts = $this->StockCounts_M
        ->fields('*, (SELECT name FROM users WHERE stock_counts.user_id = id) as user_name')
        ->get_all([
            'start_date >=' => $start_date,
            'end_date <=' => $end_date,
            'location_id' => $location_id
        ]); 


        $this->theme_plugin = ['start' => 'TablesDataTables.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'");'];
        $this->load->template('Stockcounts/index', compact('stock_counts', 'start_date', 'end_date', 'location_id'));
	}

	public function add($location = NULL)
	{	            
        $units = $this->units; 
        
		if ($this->input->is_ajax_request()) {

            $location_id = $this->input->post('location_id');    

			$_POST['user_id'] = $this->user->id;
            $_POST['start_date'] = $this->StockCounts_M->findLastStockCountDate($location_id);

            if ($id = $this->StockCounts_M->from_form()->insert()) {

                $stocks = $this->Stocks_M->where('location_id', $location_id)->get_all();

                $stocks_stock_counts = $this->handleStockCounts($stocks);
                $stocks_stock_counts = $this->systemStockCounts($stocks_stock_counts, $stocks, $location_id);

                foreach ($stocks_stock_counts as $stock_stock_count) {
                    $stock_stock_count['stock_count_id'] = $id;
                    $this->StocksStockCounts_M->insert($stock_stock_count);
                }

                messageAJAX('success', __('Stok sayımı oluşturuldu.'));
            }

            messageAJAX('error', validation_errors());
        }

        $location_id = (int) @current($this->user->locations_array)[ 'id' ];
	    $date_to = date('Y-m-d H:i:59');

       $this->theme_plugin = array(
            'js'    => array('panel/js/view/stockcounts.js'),
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'"); Stockcounts.init({
                units: '.json_encode($units).',
            });'
        );

		$this->load->template('Stockcounts/add', compact('location_id', 'date_to'));
	}

	public function edit($id = null) 
	{
        
        $stock_count = $this->StockCounts_M->findOrFail($id);
        $units = $this->units;

		if ($this->input->is_ajax_request()) {

			$_POST['user_id'] = $this->user->id;
            $_POST['location_id'] = $stock_count->location_id;
            $_POST['start_date'] = $stock_count->start_date;


            if ($this->StockCounts_M->from_form(null, null, ['id' => $id])->update()) {

                $stocks = $this->Stocks_M->where('location_id', $stock_count->location_id)->get_all();
                $stocks_stock_counts = $this->handleStockCounts($stocks);
                $stocks_stock_counts = $this->systemStockCounts($stocks_stock_counts, $stocks, $stock_count->location_id);

                $this->StocksStockCounts_M->delete(['stock_count_id' => $id]);

                foreach ($stocks_stock_counts as $stock_stock_count) {
                    $stock_stock_count['stock_count_id'] = $id;
                    $this->StocksStockCounts_M->insert($stock_stock_count);
                }
                messageAJAX('success', __('Stok sayımı güncellendi.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->theme_plugin = array(
            'js'    => array('panel/js/view/stockcounts.js'),
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'"); Stockcounts.init({
                units: '.json_encode($units).',
            });'
        );


		$this->load->template('Stockcounts/edit', compact('stock_count'));
	}

	public function getStockCounts() {

		$location = $this->input->post('location_id');
        $stock_count_id = (int)$this->input->post('id');

        $stocks = $this->Stocks_M->where('location_id', $location)->get_all();

        if ($stock_count_id) {
            $stock_counts = $this->StocksStockCounts_M->where([
                'stock_count_id' => $stock_count_id
            ])->get_all();  

            foreach ($stocks as $key => &$stock) {
                $stock->hand_amount = '';
                foreach ($stock_counts as $stock_count_key => $stock_count) {

                    if ($stock->id == $stock_count->stock_id ) {
                        $stock->hand_amount = $stock_count->hand_amount;
                        unset($stock_counts[$stock_count_key]);
                    }
                
                }
                
            }
            unset($stock);
        } else {
            $date_start = $this->StockCounts_M->findLastStockCountDate($location);
            $date_start = dateConvert2($date_start, 'client', 'Y-m-d H:i:s', dateFormat());
        }        

		messageAJAX('success','', compact('stocks', 'date_start'));
		
	}

	private function handleStockCounts($system_stocks)
    {
        $stocks = (Array) $this->input->post('stocks');
        $data = [];



        foreach ($system_stocks as $key => $system_stock) {
            

            foreach ($stocks as $stock_id => $stock_quantity) {

                if ($system_stock->id == $stock_id) {

                
                    $stock_quantity = doubleval(str_replace(',', '', $stock_quantity));

                    $data[$stock_id] = [
                        'stock_id' => $stock_id,
                        'stock_count_id' => null,
                        'hand_amount' => $stock_quantity,
                        'system_amount' => 0
                    ];

                    unset($stocks[$stock_id]);
                    break;
                }          
            }
        }
        return $data;
    }

    private function systemStockCounts($stocks_stock_counts, $system_stocks, $location_id) {

        foreach ($system_stocks as $stock) {
            $stocks_stock_counts[$stock->id]['system_amount'] += $stock->quantity;
        }

        return $stocks_stock_counts;
    }

    // private function systemStockCounts($stocks_stock_counts, $system_stocks, $location_id) {

        // $sales = $this->Sales_M->findSalesWithProducts($where);



        // if ($sales) {
        //     foreach ($sales->products as $product) {

        //         if ($product->type == "product") {
        //             foreach ($product->stocks as $key => $stock) {
        //                 if ($stock->in_use == 1) {
        //                     foreach ($system_stocks as $system_stock) {
        //                         if ($system_stock->id == $stock->stock_id) {
        //                             $stocks_stock_counts[$stock->stock_id]['system_amount'] += $system_stock->quantity - $product->quantity * $stock->quantity;
        //                             break;
        //                         }
        //                     }

        //                     }
        //             }
        
        //                 foreach ($product->additional_products as $key => $add_products) {
        //                     foreach ($add_products->stocks as $key => $stock) {
        //                         if ($stock->in_use == 1) {
        //                             foreach ($system_stocks as $system_stock) {
        //                                 if ($system_stock->id == $stock->stock_id) {
        //                                     $stocks_stock_counts[$stock->stock_id]['system_amount'] += $system_stock->quantity - $add_products->quantity * $stock->quantity;
        //                                     break;
        //                                 }
        //                             }
        //                          }
        //                     }             

        //                 }

        //         } else {
        //             foreach ($product->products as $p_product) {
        //                 foreach ($p_product->stocks as $key => $stock) {
        //                     if ($stock->in_use == 1) {
        //                         foreach ($system_stocks as $system_stock) {
        //                             if ($system_stock->id == $stock->stock_id) {
        //                                 $stocks_stock_counts[$stock->stock_id]['system_amount'] += $system_stock->quantity - $p_product->quantity * $stock->quantity;
        //                                 break;
        //                             }
        //                         }
        //                     }
        //                 }
        //                     foreach ($product->additional_products as $key => $p_add_products) {

        //                         foreach ($p_add_products->stocks as $key => $stock) {
        //                             if ($stock->in_use == 1) {
        //                                 foreach ($system_stocks as $system_stock) {
        //                                     if ($system_stock->id == $stock->stock_id) {
        //                                         $stocks_stock_counts[$stock->stock_id]['system_amount'] += $system_stock->quantity - $p_add_products->quantity*$stock->quantity;
        //                                         break;
        //                                     }
        //                                 }
        //                              }
        //                         }   
        //                     }
        //             }

        //         }

        //     }
        // }
    //     return $stocks_stock_counts;

    // }

    public function view($id=null) {

        $stock_counts =  $this->StocksStockCounts_M
        ->fields('*, (SELECT title FROM stocks WHERE stocks_stock_counts.stock_id = id) as stock_name , (SELECT unit FROM stocks WHERE stocks_stock_counts.stock_id = id) as unit')
        ->where('stock_count_id', $id)->get_all();

         $this->theme_plugin = array(
            'css'   => array(
            ),
            'js'    => array('panel/js/view/stockcounts.js'
            ),
            'start' => 'TablesDataTables.init(); Stockcounts.init();'
        );

        $this->load->template('Stockcounts/view', compact('stock_counts', 'id'));
    }

    public function systemSync() {

        if ($this->input->is_ajax_request()) {

            $id = $this->input->post('id');
            $datetime = _date();

            $stock_counts =  $this->StocksStockCounts_M
            ->fields('*,(SELECT quantity FROM stocks WHERE stocks_stock_counts.stock_id = id) as quantity')
            ->where('stock_count_id', $id)->get_all();   

            foreach ($stock_counts as $stock_count) {
                                
                $diff = $stock_count->hand_amount - $stock_count->quantity;

                $this->Stocks_M->where('id', $stock_count->stock_id)
                ->update([
                    'quantity' => $stock_count->hand_amount
                ]);


                $this->db->insert('stock_contents', [
                    'user_id' => $this->user->id,
                    'stock_id' => $stock_count->stock_id,
                    'quantity' => $diff,
                    'created' =>  $datetime,
                    'active' => 1
                ]);
            }
              
            messageAJAX('success', __('Stoklarınız güncellendi')); 

        } else {
            messageAJAX('error', validation_errors()); 

        }                 
    
    }


}
