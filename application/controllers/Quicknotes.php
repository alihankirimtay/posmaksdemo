<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Quicknotes extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('QuickNotes_M');
    }

    public function ajax($event = null)
    {
        if (method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index()
    {
        $quickNotes = $this->QuickNotes_M->get_all();

        $this->theme_plugin = [
            'start' => 'TablesDataTables.init();'];

        $this->load->template('QuickNotes/index', compact('quickNotes'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {

            if ($id = $this->QuickNotes_M->from_form()->insert()) {

                $quick_note = $this->QuickNotes_M->get($id);

                messageAJAX('success', __('Hızlı Not oluşturuldu.'), compact('quick_note'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('QuickNotes/add');
    }

    public function edit($id = null)
    {
        $quickNote = $this->QuickNotes_M->findOrFail($id);
        
        if ($this->input->is_ajax_request()) {

            if ($this->QuickNotes_M->from_form(null, null, ['id' => $id])->update()) {

               messageAJAX('success', __('Hızlı Not düzenlendi.'));
            }

            messageAJAX('error', validation_errors());

        }

        $this->load->template('QuickNotes/edit', compact('quickNote'));
    }

    public function delete($id = NULL)
    {
        $this->QuickNotes_M->findOrFail((int) $id);

        $this->QuickNotes_M->update(['deleted' => date('Y-m-d H:i:s')], $id);

        messageAJAX('success', 'Success');
    }

}
