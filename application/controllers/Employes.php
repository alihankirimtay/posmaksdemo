<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employes extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth();
        
        $this->load->model('Users_M');
        $this->load->model('Roles_M');
        $this->load->model('Zones_M');

    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index($location_id = null)
    {
        if (!in_array($location_id, $this->user->locations)) {
            $location_id = $this->user->locations[0];
        }

        $admins = $this->Users_M->fields(['*', '(select title from roles where id = role_id) As role_title'])->get_all(['role_id' => 1]);

        $this->db->where('users.role_id != 1');
        $users = $this->Users_M->select('users.*, (select title from roles where id = role_id) As role_title')->findByLocation($location_id);

        $this->theme_plugin = [
            'start' => 'TablesDataTables.init();'
        ];

        $this->load->template('Employes/index' , compact('users', 'admins', 'location_id'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            
            $users_locations = $this->Users_M->handleUserLocations();

            $zones_locations = $this->Users_M->handleUserZones();


            if ($id = $this->Users_M->from_form()->insert()) {

                foreach ($users_locations as $user_location) {
                    $user_location['user_id'] = $id;
                    $this->db->insert('user_has_locations', $user_location);
                }

                foreach ($zones_locations as $zone_location) {
                    $zone_location['user_id'] = $id;
                    $this->db->insert('users_zones', $zone_location);
                }

                messageAJAX('success' , __('Personel oluşturldu.'));
            }

            messageAJAX('error' , validation_errors());
        }

        $roles = $this->Roles_M->get_all(['active !=' => 3]);

         $this->theme_plugin = [
            'js'    => ['panel/js/view/employes.js'],
            'start' => 'Employes.init();'
        ];

        $this->load->template('Employes/add' , compact('roles'));
    }

    public function edit($id = null)
    {
        $user = $this->Users_M->with_locations()->with_zones()->findOrFail(['id' => $id]);


        if ($this->input->is_ajax_request()) {
            
            $users_locations = $this->Users_M->handleUserLocations($id);
            $zones_locations = $this->Users_M->handleUserZones($id);


            if ($this->Users_M->from_form(null, null, ['id' => $id])->update()) {
                
                $this->db->where('user_id', $id)->delete('user_has_locations');
                $this->db->where('user_id', $id)->delete('users_zones');

                foreach ($users_locations as $user_location) {
                    $this->db->insert('user_has_locations', $user_location);
                }

                foreach ($zones_locations as $zone_locations) {
                    $this->db->insert('users_zones', $zone_locations);
                }
                
                messageAJAX('success' , __('Personel güncellendi.'));
            }

            messageAJAX('error' , validation_errors());
        }

        $roles = $this->Roles_M->get_all(['active !=' => 3]);
        $user_zones = $user->zones;

         $this->theme_plugin = [
            'js'    => ['panel/js/view/employes.js'],
            'start' => 'Employes.init({
                users_zones: '.json_encode($user_zones).',
            });'
        ];


        $this->load->template('Employes/edit' , compact('user', 'roles'));
    }


    public function delete($id = null)
    {
        $this->Users_M->findOrFail(['id' => $id]);

        $this->Users_M->where(['id' => $id])->update(['active' => 3]);

        messageAJAX('success' , __('Personel silindi.'));
    }

    private function getZonesByLocationId()
    {

        $locations = (array) $this->input->post('locations[id]');

        $zones = [];

        foreach ($locations as $location_id) {

            $zones[] = $this->Zones_M->findByLocation([
                'location_id' => $location_id,
                'active !=' => 3
            ]);

        }


        messageAJAX('success', 'Success', compact('zones'));

        
    }

}