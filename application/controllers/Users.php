<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('User_model');
        $this->load->model('Locations_M');
        $this->load->model('Users_M');
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index(){}


    public function edit()
    {


        $this->load->library('form_validation');
        $this->load->helper('security');

        $language = $this->input->post('language');
        $this->lang->set($language);


        // Change Password 
        if($this->input->post('password')):
            $this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[5]|max_length[32]|xss_clean');

            if($this->form_validation->run() === FALSE):
                messageAJAX('error' ,  validation_errors());
            else:

                $result =  $this->User_model->edit();

                if($result === FALSE):
                    messageAJAX('error' , 'Database ERROR');
                elseif($result === TRUE):

                    $values = $this->User_model->activationData();

                $mail_tmp = ' Dear '.$this->user->username.', we received a request for password change for username '.$this->user->username.' at '.$this->website->name.'.<br />
                <a href="'.base_url("login/activation".'/'.$values['activation_code']).'" target="_blank">Go to this page</a> to set your new password. The link will be active.<br />
                <br />
                Regards,<br />
                The '.$this->website->name.' team';


                $this->sendMail($this->website->email , $this->user->email , __('Yeni Şifre İsteği') , $mail_tmp);

                 messageAJAX('success' , 'Success');


                else:
                    messageAJAX('success', __('Kullanıcı güncellenemedi'));
                endif;

            endif;
        // Change Profile Image
        elseif(@$_FILES['pimage']['name']):

            $this->load->helper('string');


            $config['upload_path']   =  FCPATH.'assets/uploads/profile';
            $config['allowed_types'] =  'gif|jpg|jpeg|png';
            $config['file_name']     =  sprintf('%d-%s-%s-%s-%s-%s',
                                        $this->user->id,
                                        strtoupper(random_string('alnum', 4)),
                                        strtoupper(random_string('alnum', 4)),
                                        strtoupper(random_string('alnum', 4)),
                                        strtoupper(random_string('alnum', 4)),
                                        str_replace('.', '_', microtime(1)));
            $config['max_size']     =   '5000';
            
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('pimage')){
                messageAJAX('error' , $this->upload->display_errors());
            }
            else{
                $data = array( 'image' => $this->upload->data('file_name'));
                $result = $this->User_model->edit($data);

                if($result == TRUE):
                    messageAJAX('success' , __('Profil resmi güncellendi.'));
                else:
                    messageAJAX('error' , 'Something Went Wrong');
                endif;
            }

        elseif ($this->input->post('dtz')):

            $this->form_validation->set_rules('dtz',
                'Timezone',
                'trim|required|in_list['.implode(',', DateTimeZone::listIdentifiers()).']',
                ['in_list'=>__('Saat dilmini seçin.')]
            );

            if (! $this->form_validation->run()) {
                messageAJAX('error', validation_errors());
            }
            
            $this->db->where('id', $this->user->id);
            $this->db->update('users', ['dtz' => $this->input->post('dtz', TRUE), 'modified' => _date()]);
            messageAJAX('success', __('Başarılı'));
                 
        endif;

        messageAJAX('success', __('Kullanıcı güncellenemedi.'));
        
    }

    private function getAdminsAndUsersByLocationId()
    {
        $location_id = $this->input->post('location_id');

        $location = $this->Locations_M
        ->with_users([
            'fields' => 'id,username,email',
            'where' => [
                'users.role_id !=' => 1,
                'users.active !=' => 3
            ]
        ])
        ->findOrFail([
            'id' => $location_id 
        ]);

        $users = $location->users;

        $super_users = $this->Users_M->fields('id,username,email')->get_all([
            'role_id' => 1,
            'active !=' => 3
        ]);

        $tmp_users = [];
        foreach ($users as $user) {
            $tmp_users[$user->id] = $user;
        }
        foreach ($super_users as $super_user) {
            $tmp_users[$super_user->id] = $super_user;
        }
        $users = $tmp_users;
        

        messageAJAX('success', 'Success', compact('users'));
    }
}