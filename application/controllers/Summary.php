<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Summary extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->check_auth();
    }

   

    function index()
    {
        $this->load->model('Reports_model');

        $this->theme_plugin = [
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");'
        ];


        // LOCATION
        $data[ 'location_id' ] = array_key_exists($this->input->get('location'), $this->user->locations_array) 
            ? (int) $this->input->get('location')
            : (int) @current($this->user->locations_array)[ 'id' ];

        // DATES
        if (isValidDate($this->input->get('date'), dateFormat())) {

            $data[ 'date_client_today' ]        = $this->input->get('date');
            $data[ 'date_server_today' ]        = dateConvert($data[ 'date_client_today' ], 'server');
            $data[ 'date_client_today_ymdhis' ] = dateReFormat($data[ 'date_client_today' ], dateFormat(), 'Y-m-d H:i:s');
            $data[ 'date_client_today_format' ] = dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', 'D m/d/y h:i A');
        } else {
            $data[ 'date_server_today' ]        = date('Y-m-d H:i:s');
            $data[ 'date_client_today' ]        = dateConvert($data[ 'date_server_today' ], 'client', dateFormat());
            $data[ 'date_client_today_ymdhis' ] = dateReFormat($data[ 'date_client_today' ], dateFormat(), 'Y-m-d H:i:s');
            $data[ 'date_client_today_format' ] = dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', 'D m/d/y h:i A');
        }

        // // // today
        $data[ 'date_client_today_from' ]      = date('Y-m-d H:i:00', strtotime($data[ 'date_client_today_ymdhis' ]));
        $data[ 'date_server_today_from' ]      = dateConvert2($data[ 'date_client_today_from' ], 'server');
        $data[ 'date_server_today_from_unix' ] = strtotime($data[ 'date_server_today_from' ]);
        $data[ 'date_client_today_to' ]        = date('Y-m-d H:00:00', strtotime($data[ 'date_client_today_ymdhis' ] . ' +23 hours'));
        $data[ 'date_server_today_to' ]        = dateConvert2($data[ 'date_client_today_to' ], 'server');
        $data[ 'date_server_today_to_unix' ]   = strtotime($data[ 'date_server_today_to' ]);

        // // // this month
        $data[ 'date_client_tmonth_from' ]      = date('Y-m-01 H:i:s', strtotime($data[ 'date_client_today_from' ]));
        $data[ 'date_server_tmonth_from' ]      = dateConvert2($data[ 'date_client_tmonth_from' ], 'server');
        $data[ 'date_server_tmonth_from_unix' ] = strtotime($data[ 'date_server_tmonth_from' ]);
        $data[ 'date_client_tmonth_to' ]        = date('Y-m-t H:i:s', strtotime($data[ 'date_client_today_to' ]));
        $data[ 'date_server_tmonth_to' ]        = dateConvert2($data[ 'date_client_tmonth_to' ], 'server');
        $data[ 'date_server_tmonth_to_unix' ]   = strtotime($data[ 'date_server_tmonth_to' ]);

        // // // this year
        $data[ 'date_client_tyear_from' ]      = date('Y-01-01 H:i:s', strtotime($data[ 'date_client_today_from' ]));
        $data[ 'date_server_tyear_from' ]      = dateConvert2($data[ 'date_client_tyear_from' ], 'server');
        $data[ 'date_server_tyear_from_unix' ] = strtotime($data[ 'date_server_tyear_from' ]);
        $data[ 'date_client_tyear_to' ]        = date('Y-12-31 H:i:s', strtotime($data[ 'date_client_today_to' ]));
        $data[ 'date_server_tyear_to' ]        = dateConvert2($data[ 'date_client_tyear_to' ], 'server');
        $data[ 'date_server_tyear_to_unix' ]   = strtotime($data[ 'date_server_tyear_to' ]);

        // // // last year from-to 
        $data[ 'date_client_lyear_from' ]      = date('Y-01-01 H:i:s', strtotime($data[ 'date_client_today_from' ] . ' -1 year'));
        $data[ 'date_server_lyear_from' ]      = dateConvert2($data[ 'date_client_lyear_from' ], 'server');
        $data[ 'date_server_lyear_from_unix' ] = strtotime($data[ 'date_server_lyear_from' ]);
        $data[ 'date_client_lyear_to' ]        = date('Y-12-31 H:i:s', strtotime($data[ 'date_client_today_to' ] . ' -1 year'));
        $data[ 'date_server_lyear_to' ]        = dateConvert2($data[ 'date_client_lyear_to' ], 'server');
        $data[ 'date_server_lyear_to_unix' ]   = strtotime($data[ 'date_server_lyear_to' ]);

        // // // last year month from-to 
        $data[ 'date_client_lyear_month_from' ]      = date('Y-m-01 H:i:s', strtotime($data[ 'date_client_today_from' ] . ' -1 year'));
        $data[ 'date_server_lyear_month_from' ]      = dateConvert2($data[ 'date_client_lyear_month_from' ], 'server');
        $data[ 'date_server_lyear_month_from_unix' ] = strtotime($data[ 'date_server_lyear_month_from' ]);
        $data[ 'date_client_lyear_month_to' ]        = date('Y-m-t H:i:s', strtotime($data[ 'date_client_today_to' ] . ' -1 year'));
        $data[ 'date_server_lyear_month_to' ]        = dateConvert2($data[ 'date_client_lyear_month_to' ], 'server');
        $data[ 'date_server_lyear_month_to_unix' ]   = strtotime($data[ 'date_server_lyear_month_to' ]);

        // // // last year today
        $date = new DateTime($data[ 'date_client_today_ymdhis' ]);
        $day = $date->format('l');
        $date->sub(new DateInterval('P1Y'));
        $date->modify('next ' . $day);
        $data[ 'date_client_lyear_today_ymdhis' ] = $date->format('Y-m-d ' . dateReFormat($data[ 'date_client_today_ymdhis' ], 'Y-m-d H:i:s', 'H:i:s'));
        $data[ 'date_client_lyear_today' ]        = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', dateFormat());
        $data[ 'date_server_lyear_today' ]        = dateConvert2($data[ 'date_client_lyear_today_ymdhis' ], 'server');
        $data[ 'date_client_lyear_today_format' ] = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', 'D m/d/y h:i A');
        // // // last year today from-to
        $data[ 'date_client_lyear_today_from' ]      = dateReFormat($data[ 'date_client_lyear_today_ymdhis' ], 'Y-m-d H:i:s', 'Y-m-d H:i:00');
        $data[ 'date_server_lyear_today_from' ]      = dateConvert2($data[ 'date_client_lyear_today_from' ], 'server');
        $data[ 'date_server_lyear_today_from_unix' ] = strtotime($data[ 'date_server_lyear_today_from' ]);
        $data[ 'date_client_lyear_today_to' ]        = dateReFormat(date('Y-m-d H:i:s', strtotime($data[ 'date_client_lyear_today_ymdhis' ] . ' +23 hours')), 'Y-m-d H:i:s', 'Y-m-d H:00:00');
        $data[ 'date_server_lyear_today_to' ]        = dateConvert2($data[ 'date_client_lyear_today_to' ], 'server');
        $data[ 'date_server_lyear_today_to_unix' ]   = strtotime($data[ 'date_server_lyear_today_to' ]);
        // END - DATES
// pr($data,1);

        $data[ 'sales_today' ]               = 0;
        $data[ 'sales_this_month' ]          = 0;
        $data[ 'sales_this_year' ]           = 0;
        $data[ 'sales_last_year_today' ]     = 0;
        $data[ 'sales_last_year_month' ]     = 0;
        $data[ 'sales_last_year' ]           = 0;

        $data[ 'pax_today' ]                 = 0;
        $data[ 'pax_this_month' ]            = 0;
        $data[ 'pax_this_year' ]             = 0;
        $data[ 'pax_last_year_today' ]       = 0;
        $data[ 'pax_last_year_month' ]       = 0;
        $data[ 'pax_last_year' ]             = 0;

        $data[ 'negs_today' ]                = 0;
        $data[ 'negs_this_month' ]           = 0;
        $data[ 'negs_this_year' ]            = 0;
        $data[ 'negs_last_year_today' ]      = 0;
        $data[ 'negs_last_year_month' ]      = 0;
        $data[ 'negs_last_year' ]            = 0;
        
        $data[ 'gallery' ]                   = 0;
        $data[ 'gallery_printed' ]           = 0;
        $data[ 'master_product_sold' ]       = 0;
        
        $data[ 'categorized_products' ]      = [];
        $data[ 'categorized_entrances' ]     = [];
        $data[ 'categorized_product_sales' ] = [];
        $data[ 'currency' ]                  = $this->user->locations_array[ $data[ 'location_id' ] ][ 'currency' ];

        // sales
        $this->db->where([
            'S.location_id'     => $data[ 'location_id' ],
            'S.payment_date >=' => $data[ 'date_server_lyear_from' ],
            'S.payment_date <=' => $data[ 'date_server_tyear_to' ],
            'SHP.active !='     => 3,
            'P.active !='       => 3,
        ]);
        $this->db->join('sale_has_products SHP', 'SHP.sale_id = S.id');
        $this->db->join('products P', 'P.id = SHP.product_id');
        $data[ 'sales' ] = $this->Reports_model->sales('
            S.id sale_id, S.return sale_return, S.tax sale_tax, S.pos_id sale_pos_id, S.amount sale_amount, S.subtotal sale_subtotal, UNIX_TIMESTAMP(S.payment_date) payment_date_unix, S.payment_date sale_payment,
            SHP.product_id product_id, SHP.price product_price, SHP.quantity product_quantity, SHP.content product_content, SHP.type product_type, SHP.digital product_digital,
            P.product_type_id category_id');

        // PAX+NEGS
        $this->db->where([
            'P.location_id' => $data[ 'location_id' ],
            'P.date >='     => $data[ 'date_server_lyear_from' ],
            'P.date <='     => $data[ 'date_server_tyear_to' ]
        ]);
        $data[ 'paxnegs' ] = $this->Reports_model->paxnegs_all('*, UNIX_TIMESTAMP(P.date) date_unix');
        // $data[ 'pax' ]     = (int) @$data[ 'paxnegs' ]->pax ? $data[ 'paxnegs' ]->pax : 1;
        // $data[ 'negs' ]    = (int) @$data[ 'paxnegs' ]->negs;

        // Products
        $this->db
            ->where('P.location_id', $data[ 'location_id' ])
            ->order_by('P.title', 'ASC');
        $data[ 'products' ] = $this->Reports_model->products('P.id, P.title');

        // Product Categories
        $this->db->where('active !=', 3);
        $data[ 'categories' ] = $this->db->get('product_types')->result_array();

        // Entrances
        $this->db->where([
            'location_id' => $data[ 'location_id' ],
            'active !='   => 3,
        ]);
        $data[ 'entrances' ] = $this->db->get('pos')->result_array();

        // WASTE, COMPS, UNSOLD
        $this->db->where([
            'L.location_id' => $data[ 'location_id' ],
            'L.date >='     => $data[ 'date_server_today_from' ],
            'L.date <='     => $data[ 'date_server_today_to' ]
        ]);
        $data[ 'losts' ]  = $this->Reports_model->printer_waste('SUM(L.unsold) as unsold, SUM(L.waste) as waste, SUM(L.comps) as comps, SUM(L.unseen) as unseen');
        $data[ 'waste' ]  = (int) @$data[ 'losts' ]->waste;
        $data[ 'unsold' ] = (int) @$data[ 'losts' ]->unsold;
        $data[ 'comps' ]  = (int) @$data[ 'losts' ]->comps;
        $data[ 'unseen' ] = (int) @$data[ 'losts' ]->unseen;

        // PRINTER COUNTS
        $this->db->where('P.location_id', $data[ 'location_id' ]);
        $this->db->order_by('P.title', 'ASC');
        $data[ 'printers' ] = $this->Reports_model->printers('P.id, P.title, P.equal,
            IFNULL(
                (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date < "'.$data[ 'date_server_today_from' ].'" ORDER BY PHC.date DESC LIMIT 1),
                (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date BETWEEN "'.$data[ 'date_server_today_from' ].'" AND "'.$data[ 'date_server_today_to' ].'" ORDER BY PHC.date ASC LIMIT 1)
            ) as `countMin`,
            (SELECT PHC.count FROM printer_has_counts PHC WHERE PHC.printer_id = P.id AND PHC.date BETWEEN "'.$data[ 'date_server_today_from' ].'" AND "'.$data[ 'date_server_today_to' ].'" ORDER BY PHC.date DESC LIMIT 1) as `countMax`');


        $data[ 'master_product' ] = [ 'id' => 0, 'content' => 0];
        foreach ($data[ 'products' ] as $product) {
            if ($product[ 'master' ]) {
                $data[ 'master_product' ] = $product;
                break;
            }
        }
        
        // CATEGORIES
        if ($data[ 'categories' ]) {
            foreach ($data[ 'categories' ] as $category) {
                $data[ 'categorized_products' ][ $category[ 'id' ] ]            = $category;
                $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] = 0;
            }
        }

        // PRODUCTS
        if ($data[ 'products' ]) {
            foreach ($data[ 'products' ] as $product) {
                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ]            = $product;
                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] = 0;
            }
        }

        // ENTRANCE
        if ($data[ 'entrances' ]) {
            foreach ($data[ 'entrances' ] as $entrance) {
                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ]            = $entrance;
                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] = 0;
            }
        }





        $data[ 'sales_today_processed' ]           = NULL;
        $data[ 'sales_this_month_processed' ]      = NULL;
        $data[ 'sales_this_year_processed' ]       = NULL;
        $data[ 'sales_last_year_today_processed' ] = NULL;
        $data[ 'sales_last_year_processed' ]       = NULL;
        $data[ 'sales_last_year_month_processed' ] = NULL;

        // SALES for 2 Years 
        foreach ($data[ 'sales' ] as $sale) {


            // TODAY
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_today_from_unix' ], $data[ 'date_server_today_to_unix' ])) {

                // SALES
                if (!isset($data[ 'sales_today_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_today_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_today' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_today' ] += $sale[ 'sale_amount' ];
                    } 

                }


                // Teslim edilen VES ürünler
                $delivered_ves_content = 0;
                if ($sale[ 'product_type' ] == 'ves') {
                    
                    $delivered_ves_products = $this->db
                        ->select('content product_content, quantity product_quantity')
                        ->where('active', 1)
                        ->where('digital', 0)
                        ->where('package_id', $sale[ 'product_id' ])
                    ->get('package_has_products')->result_array();

                    foreach ($delivered_ves_products as $ves_product) {
                        $delivered_ves_content += $ves_product[ 'product_content' ] * $ves_product[ 'product_quantity' ];
                    }
                } 

                // GALLERY - Digital ve Master olmayan ürünler
                if (!$sale[ 'product_digital' ] && $data[ 'master_product' ][ 'id' ] != $sale[ 'product_id' ]) {
                    if ($sale[ 'sale_return' ]) {

                        if ($delivered_ves_content) {
                            $data[ 'gallery' ] -= $delivered_ves_content;
                        } else {
                            $data[ 'gallery' ] -= $sale[ 'product_content' ] * $sale[ 'product_quantity' ];
                        }
                        

                    } else {

                        if ($delivered_ves_content) {
                            $data[ 'gallery' ] += $delivered_ves_content;
                        } else {
                            $data[ 'gallery' ] += $sale[ 'product_content' ] * $sale[ 'product_quantity' ];
                        }

                    }

                } else if ($data[ 'master_product' ][ 'id' ] == $sale[ 'product_id' ]) {
                    if ($sale[ 'sale_return' ]) {

                        if ($delivered_ves_content) {
                            $data[ 'master_product_sold' ] -= $delivered_ves_content;
                        } else {
                            $data[ 'master_product_sold' ] -= $sale[ 'product_content' ] * $sale[ 'product_quantity' ];
                        }
                        

                    } else {

                        if ($delivered_ves_content) {
                            $data[ 'master_product_sold' ] += $delivered_ves_content;
                        } else {
                            $data[ 'master_product_sold' ] += $sale[ 'product_content' ] * $sale[ 'product_quantity' ];
                        }

                    }
                }



                // CATEGORIES
                if ($data[ 'categories' ]) {
                    foreach ($data[ 'categories' ] as $category) {

                        if ($sale[ 'category_id' ]  == $category[ 'id' ]) {

                            $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                            $total = $sale[ 'sale_tax' ] * $total / 100 + $total;

                            // SALES AND REFUND
                            if ($sale[ 'sale_return' ]) {
                                $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] -= $total;

                            } else {
                                $data[ 'categorized_products' ][ $category[ 'id' ] ][ 'total' ] += $total;
                            }                    
                        }
                    }
                }    
                // END - CATEGORIES
                
                

                // PRODUCTS
                if ($data[ 'products' ]) {
                    foreach ($data[ 'products' ] as $product) {

                        if ($sale[ 'product_id' ] == $product[ 'id' ]) {

                            $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                            $total = $sale[ 'sale_tax' ] * $total / 100 + $total;
                            $total = round($total, 2);

                            // SALES AND REFUND
                            if ($sale[ 'sale_return' ]) {
                                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] -= $total;

                            } else {
                                $data[ 'categorized_product_sales' ][ $product[ 'id' ] ][ 'total' ] += $total;
                            }                    
                        }
                    }
                }
                // END - PRODUCTS
                


                // ENTRANCE
                if ($data[ 'entrances' ]) {
                    foreach ($data[ 'entrances' ] as $entrance) {

                        if ($sale[ 'sale_pos_id' ] == $entrance[ 'id' ]) {

                            $total = $sale[ 'product_quantity' ] * $sale[ 'product_price' ];
                            $total = $sale[ 'sale_tax' ] * $total / 100 + $total;
                            $total = round($total, 2);

                            // SALES AND REFUND
                            if ($sale[ 'sale_return' ]) {
                                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] -= $total;

                            } else {
                                $data[ 'categorized_entrances' ][ $entrance[ 'id' ] ][ 'total' ] += $total;
                            }                    
                        }
                    }
                }
                // END - ENTRANCE
            


            }
            // END - TODAY



            // THIS MONTH
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_tmonth_from_unix' ], $data[ 'date_server_tmonth_to_unix' ])) {
                


                // THIS MONTH
                if (!isset($data[ 'sales_this_month_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_this_month_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_this_month' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_this_month' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - THIS MONTH
            
             

            // THIS YEAR
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_tyear_from_unix' ], $data[ 'date_server_tyear_to_unix' ])) {
                


                // SALES THIS YEAR
                if (!isset($data[ 'sales_this_year_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_this_year_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_this_year' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_this_year' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - THIS YEAR
            
            

            // LAST YEAR TODAY
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_today_from_unix' ], $data[ 'date_server_lyear_today_to_unix' ])) {
                


                // SALES LAST YEAR TODAY
                if (!isset($data[ 'sales_last_year_today_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_today_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year_today' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year_today' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - LAST YEAR TODAY


            // LAST YEAR
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_from_unix' ], $data[ 'date_server_lyear_to_unix' ])) {
                

                // SALES LAST YEAR
                if (!isset($data[ 'sales_last_year_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year' ] += $sale[ 'sale_amount' ];
                    } 

                }


            }
            // END - LAST YEAR
            


            // LAST YEAR MONTH
            if (between($sale[ 'payment_date_unix' ], $data[ 'date_server_lyear_month_from_unix' ], $data[ 'date_server_lyear_month_to_unix' ])) {
                


                // LAST YEAR MONTH
                if (!isset($data[ 'sales_last_year_month_processed' ][ $sale[ 'sale_id' ] ])) {

                    $data[ 'sales_last_year_month_processed' ][ $sale[ 'sale_id' ] ] = 1;

                    // SALES AND REFUND
                    if ($sale[ 'sale_return' ]) {
                        $data[ 'sales_last_year_month' ] -= $sale[ 'sale_amount' ];

                    } else {
                        $data[ 'sales_last_year_month' ] += $sale[ 'sale_amount' ];
                    } 

                }



            }
            // END - LAST YEAR MONTH

    
        }


        // PAX and NEGS data for 2 YEARS
        foreach ($data[ 'paxnegs' ] as $paxnegs) {
            
            // TODAY
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_today_from_unix' ], $data[ 'date_server_today_to_unix' ])) {
                $data[ 'pax_today' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_today' ] += $paxnegs[ 'negs' ];
            }
            // END - TODAY

            // THIS MONTH
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_tmonth_from_unix' ], $data[ 'date_server_tmonth_to_unix' ])) {
                $data[ 'pax_this_month' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_this_month' ] += $paxnegs[ 'negs' ];
            }
            // END - THIS MONTH
            
            // THIS YEAR
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_tyear_from_unix' ], $data[ 'date_server_tyear_to_unix' ])) {
                $data[ 'pax_this_year' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_this_year' ] += $paxnegs[ 'negs' ];
            }
            // END - THIS YEAR
            
            // LAST YEAR TODAY
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_today_from_unix' ], $data[ 'date_server_lyear_today_to_unix' ])) {
                $data[ 'pax_last_year_today' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year_today' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR TODAY
            
            // LAST YEAR
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_from_unix' ], $data[ 'date_server_lyear_to_unix' ])) {
                $data[ 'pax_last_year' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR
            
            // LAST YEAR MONTH
            if (between($paxnegs[ 'date_unix' ], $data[ 'date_server_lyear_month_from_unix' ], $data[ 'date_server_lyear_month_to_unix' ])) {
                $data[ 'pax_last_year_month' ] += $paxnegs[ 'pax' ];
                $data[ 'negs_last_year_month' ] += $paxnegs[ 'negs' ];
            }
            // END - LAST YEAR MONTH
        }

        $data[ 'pax_today' ]                 = $data[ 'pax_today' ] ? $data[ 'pax_today' ] : 1;
        $data[ 'pax_this_month' ]            = $data[ 'pax_this_month' ] ? $data[ 'pax_this_month' ] : 1;
        $data[ 'pax_this_year' ]             = $data[ 'pax_this_year' ] ? $data[ 'pax_this_year' ] : 1;
        $data[ 'pax_last_year_today' ]       = $data[ 'pax_last_year_today' ] ? $data[ 'pax_last_year_today' ] : 1;
        $data[ 'pax_last_year_month' ]       = $data[ 'pax_last_year_month' ] ? $data[ 'pax_last_year_month' ] : 1;
        $data[ 'pax_last_year' ]             = $data[ 'pax_last_year' ] ? $data[ 'pax_last_year' ] : 1;

        $data[ 'negs_today' ]                = $data[ 'negs_today' ] ? $data[ 'negs_today' ] : 1;
        $data[ 'negs_this_month' ]           = $data[ 'negs_this_month' ] ? $data[ 'negs_this_month' ] : 1;
        $data[ 'negs_this_year' ]            = $data[ 'negs_this_year' ] ? $data[ 'negs_this_year' ] : 1;
        $data[ 'negs_last_year_today' ]      = $data[ 'negs_last_year_today' ] ? $data[ 'negs_last_year_today' ] : 1;
        $data[ 'negs_last_year_month' ]      = $data[ 'negs_last_year_month' ] ? $data[ 'negs_last_year_month' ] : 1;
        $data[ 'negs_last_year' ]            = $data[ 'negs_last_year' ] ? $data[ 'negs_last_year' ] : 1;



        $data[ 'total_print' ] = 0;
        if ($data[ 'printers' ]) {
            foreach ($data[ 'printers' ] as $p) {
                if (!is_null($p[ 'countMax' ]) && !is_null($p[ 'countMin' ])) {
                    $data[ 'total_print' ] += ($p[ 'countMax' ] - $p[ 'countMin' ]) / $p[ 'equal' ];
                } 
            }
        }


        // GALLERY
        if ($data[ 'gallery' ] && $data[ 'master_product' ][ 'content' ]) {

            // Yeni formul için eklendi
            $data[ 'gallery_printed' ] = ($data[ 'total_print' ] - $data[ 'waste' ] - $data[ 'gallery' ]) / $data[ 'master_product' ][ 'content' ];


            $data[ 'gallery' ] = $data[ 'gallery' ] + ($data[ 'waste' ] + $data[ 'unsold' ] + $data[ 'comps' ]);
            $data[ 'gallery' ] = $data[ 'total_print' ] - $data[ 'gallery' ];
            $data[ 'gallery' ] = $data[ 'gallery' ] / $data[ 'master_product' ][ 'content' ];
        } else {
            $data[ 'gallery' ] = 1;
            $data[ 'gallery_printed' ] = 1;
        }

        $this->load->template('Summary/index', $data);

    }
}
?>
