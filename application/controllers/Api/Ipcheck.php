<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Ipcheck extends API_Controller {

	function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');  
    }

	public function index()
	{
		$json = array( 'result' => "IP Success");

		api_messageOk($json);
	}

}

/* End of file Ipcheck.php */
/* Location: ./application/controllers/Api/Ipcheck.php */