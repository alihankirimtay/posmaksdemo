<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class App extends API_Controller {

  function __construct()
    {
        parent::__construct();

        //$this->load->model('Api/App_model');
        
    }

  function index()
    {
        
    }
  
  function version()
    {
    $json['count'] = 1;
    $json['items'][] = array(
            'version' => 1,
            'title'   => base_url('assets/uploads/versions/APP_v_1.exe')
          );

    api_messageOk($json);
    }

    function trigger()
    {
     
      ob_end_clean(); //if our framework have turn on ob_start() we don't need bufering respond up to this script will be finish 
      header("Connection: close\r\n"); //send information to curl is close
      header("Content-Encoding: none\r\n"); //extra information 
      header("Content-Length: 1"); //extra information
      ignore_user_abort(true); //script will be exisits up to finish below query even web browser will be close before sender get respond

      $isProcesses = false;
      $processes = [];

      try {
       
        $archives = postRequest(posmaksCmsUrl("api/?updatesGetArchives&api_token={$this->website->api_token}"), [
          'version' => $this->website->version,
          'branch' => $this->website->branch
        ]); 
         
        if (!$archives) {
          return;
         } 
        
        
        foreach ($archives as $archive) {

          $archive_name = basename($archive['path']);
          $target = FCPATH . 'assets/updates/' . $archive_name;

          $archive_data = file_get_contents($archive['path']);
          if (!$archive_data) {
            throw new Exception("Arşiv indirilemedi");  
          }

          if (!file_put_contents($target, $archive_data)) {
            throw new Exception("Arşiv kaydedilemedi");
          }

          $zip = new ZipArchive();

          if ($zip->open($target , ZipArchive::CREATE) === false) {
            throw new Exception("Arşiv açılamadı");
          }

          $processes = json_decode($zip->getFromName('info.json'));
          $isProcesses = true;
          mkdir(FCPATH . "assets/updates/temp", 0777, true);//çalışma mantığını tam olarak öğren

          if ($processes->deleted) {

            foreach ($processes->deleted as $deleted) {
           
              if ($deleted && file_exists(FCPATH . $deleted)) {
                  
                if (rename(FCPATH . $deleted, FCPATH . 'assets/updates/temp/' . basename($deleted)) === FALSE){
                  throw new Exception("Silinen dosyalar geçici klasöre kopyalanamadı.");
                }
                
                unlink(FCPATH . $deleted);
              }
              
            }

          }

          if ($processes->modified) {

            foreach ($processes->modified as $modified) {

              $file_content = $zip->getFromName($modified);      

              if ($file_content === FALSE) {
                throw new Exception("Dosya okunamadı veya bir hata oluştu: " . $zip->getStatusString());
              }

              if ($modified || file_exists(FCPATH . $modified)) { 
                
                if(rename(FCPATH . $modified, FCPATH . 'assets/updates/temp/' . basename($modified)) === FALSE){
                  throw new Exception("Eklenilen/Değiştirilen dosyalar geçici klasöre kopyalanamadı.");
                }

                file_put_contents(FCPATH . $modified, $file_content);
    
              }

            }

          }  
          
          $zip->close();

          //Migration
         /*exec('php ' . base_url('index.php'). ' migrate latest 2>&1', $exec_output);
          $exec_output = implode(' ', (Array) $exec_output);
          if (!preg_match('/\[Success\]/', $exec_output)) {
            throw new Exception("Migrate Error: ");
          }
          */
          $this->deleteDir(FCPATH . 'assets/updates/temp');
          $this->db->update('settings',[
            'version' => $archive['id']
          ]);
          
          unlink($target);
        }
        logs_(1, date("Y-m-d H:i:s").'Güncellleme Gerçekleştirildi ', NULL, 'selfupdate', NULL);

      } catch (Exception $e) {
          
        if ($isProcesses === false) {
          $e .= 'Güncelleme gerçekleştirilemedi lütfen sistem yöneticinize başvurunuz. Hata: ' . $e;
          return logs_(1, $e, NULL, 'selfupdate', NULL);
        }
        //RESTORE 
        
        $tempOpen = opendir("assets/updates/temp/");
        if ($tempOpen === FALSE){
           $e .= "Güncellemede bir hata oluştu ve dosyalar geri yüklenme sırasında açılamadı sistem yöneticinize başvurunuz.";
         }

        $rdtemps = readdir($tempOpen);
      
        if ($rdtemps === FALSE){
           $e .= "Güncellemede bir hata oluştu ve dosyalar geri yüklenme sırasında okunamadı sistem yöneticinize başvurunuz.";
           $this->deleteDir(FCPATH . 'assets/updates/temp'); 
           return logs_(1, $e, NULL, 'selfupdate', NULL);
        }
        
        $mergeprocceses = array_merge($processes->deleted, $processes->modified);

        foreach ($mergeprocceses as $rProcces) {
          foreach ($rdtemps as $sendfile) {

            $baseprocces = basename($rProcces);
            $basesenfile = basename($sendfile);

            if ($baseprocces == $basesenfile) {
               file_put_contents(FCPATH . $rProcces, file_get_contents($sendfile));
            }
          }   
        }

        closedir($tempOpen);
        $this->deleteDir(FCPATH . 'assets/updates/temp');
        logs_(1, $e, NULL, 'selfupdate', NULL);
       
      }
    
    }

  function deleteDir($dirPath) 
  {
      if (! is_dir($dirPath)) {
        throw new Exception($dirPath . " must be a directory");
      }
      if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
      }
      $files = glob($dirPath . '*', GLOB_MARK);
      foreach ($files as $file) {
        if (is_dir($file)) {
          self::deleteDir($file);
        } else {
          unlink($file);
        }
      }
      rmdir($dirPath);
    }
}