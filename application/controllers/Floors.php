<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Floors extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Floors_M');
        $this->load->model('Locations_M');
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index($location_id = null)
    {
        if ($location_id) {
            $this->db->where('location_id', $location_id);
        }

        $floors = $this->Floors_M
        ->fields('*')
        ->select('(SELECT title FROM locations where locations.id = floors.location_id) as location_title')
        ->where('location_id', $this->user->locations)->get_all();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $this->load->template('Floors/index', compact('floors', 'location_id'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Floors_M->from_form()->insert()) {
                messageAJAX('success', __('Kat başarıyla oluşturuldu.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Floors/add');     
    }

    public function edit($id = NULL)
    {
        $floor = $this->Floors_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Floors_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Kat başarıyla güncellendi.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Floors/edit', compact('floor'));    
    }

    public function delete($id = NULL)
    {
        $this->Floors_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Floors_M->update([
            'active' => 3
        ], ['id' => $id]);

        messageAJAX('success', __('Kat başarıyla silindi.'));
    }

    private function getFloorsByLocationId()
    {
        $location_id = $this->input->post('location_id');

        $floors = $this->Floors_M->where('location_id', $this->user->locations)->get_all([
            'location_id' => $location_id
        ]);

        messageAJAX('success', 'Success', compact('floors'));
    }

    public function getFloorById()
    {
        $id = $this->input->get('id');
        
        $floor = $this->Floors_M->where('id',$id)->get();
        
        messageAJAX('success', 'Success', compact('floor'));
        
    }
}