<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_version_branch_to_settings extends CI_Migration {


	public function up() {
		$this->dbforge->add_column('settings', [
           'version' => [
               'type' => 'INT',
               'constraint' => '11',
               'null' => false,
           ],
           'branch' => [
               'type' => 'VARCHAR',
               'constraint' => '255',
               'null' => false,
           ]
       ]);
	}

	public function down() {
		$this->dbforge->drop_column('settings', 'version');
		$this->dbforge->drop_column('settings', 'branch');
	}

}

/* End of file 201812201544_add_version_branch_to_settings.php */
/* Location: ./application/migrations/201812201544_add_version_branch_to_settings.php */