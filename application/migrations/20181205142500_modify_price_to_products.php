
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_modify_price_to_products extends CI_Migration
{
	public function up()
    {
       $this->dbforge->modify_column('products', [
           'price' => [
               'name' => 'price',
               'type' => 'decimal(15,3)',
               'null' => true,
           ]
       ]);

       $this->dbforge->modify_column('sale_has_products', [
           'price' => [
               'name' => 'price',
               'type' => 'decimal(15,3)',
               'null' => true,
           ]
       ]);

    }

    public function down()
    {
       $this->dbforge->modify_column('products', [
           'price' => [
               'name' => 'price',
               'type' => 'decimal(15,2)',
               'null' => true,
           ]
       ]);

       $this->dbforge->modify_column('sale_has_products', [
           'price' => [
               'name' => 'price',
               'type' => 'decimal(15,2)',
               'null' => true,
           ]
       ]);
    }

}