<div class="row" id="invoice-wrapper">
    <div class="col-xs-9">
        <div id="invoice-page"></div>
        <div id="invoice-component-configs"></div>
    </div>
    <div class="col-xs-3" id="invoice-components">

    	<div id="accordion" class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#page-size">
                            <i class="fa fa-arrow-down pull-right"></i> <?= __("Kağıt Boyutu"); ?>
                        </a>
                    </h4>
                </div>
                <div id="page-size" class="panel-collapse collapse in">
                    <div class="panel-body p-a-1">
                        <select id="pageSize" class="form-control"> 
                            <option value="0" data-height="29.7cm" data-width="21cm">A4</option>
                            <option value="1" data-height="21cm" data-width="14.8cm">A5</option>
                            <option value="2"><?= __("Özel Boyut"); ?></option>
                        </select>
                        <div id="special" style="display: none;">
                            <div class="col-lg-12" style="padding:0px;">
                                <div class="col-lg-4" style="padding: 15px 0px;">
                                    <?= __("Genişlik :"); ?>
                                </div>
                                <div class="col-lg-6" style="padding: 0;">
                                    <input type="text" id="pageW" class="form-control" style="text-align: right;">
                                </div>

                                <div class="col-lg-2" style="padding: 5px 10px;">
                                    <?= __("cm"); ?>
                                </div>
                            </div>


                            <div class="col-lg-12" style="padding: 0;">
                                <div class="col-lg-4" style="padding: 15px 0px;">
                                    <?= __("Yükseklik :"); ?>
                                </div>
                                <div class="col-lg-6" style="padding: 0;">
                                    <input type="text" id="pageH"  class="form-control" style="text-align: right;">
                                </div> 

                                <div class="col-lg-2" style="padding: 5px 10px;">
                                    <?= __("cm"); ?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <a id="customSizeApply" class="btn btn-success pull-right"><?= __("Uygula"); ?></a>
                            </div>
                        </div>

                         
                    </div>
                </div>
            </div>
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="panel-title">
    					<a data-toggle="collapse" data-parent="#accordion" href="#list-customer">
    						<i class="fa fa-arrow-down pull-right"></i> <?= __("Müşteri Bilgileri"); ?>
    					</a>
    				</h4>
    			</div>
    			<div id="list-customer" class="panel-collapse collapse">
    				<div class="panel-body p-a-1">
    					<ul class="list-group">
    					</ul>
    				</div>
    			</div>
    		</div>
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="panel-title">
    					<a data-toggle="collapse" data-parent="#accordion" href="#list-invoice">
    						<i class="fa fa-arrow-down pull-right"></i> <?= __("Fatura Bilgileri"); ?>
    					</a>
    				</h4>
    			</div>
    			<div id="list-invoice" class="panel-collapse collapse">
    				<div class="panel-body p-a-1">
    					<ul class="list-group">
    					</ul>
    				</div>
    			</div>
    		</div>
			<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="panel-title">
    					<a data-toggle="collapse" data-parent="#accordion" href="#list-product">
    						<i class="fa fa-arrow-down pull-right"></i> <?= __("Hizmet/Ürün Bilgileri"); ?>
    					</a>
    				</h4>
    			</div>
    			<div id="list-product" class="panel-collapse collapse">
    				<div class="panel-body p-a-1">
    					<ul class="list-group">
    					</ul>
    				</div>
    			</div>
    		</div>
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="panel-title">
    					<a data-toggle="collapse" data-parent="#accordion" href="#list-amount">
    						<i class="fa fa-arrow-down pull-right"></i> <?= __("Toplam Miktar Bilgileri"); ?>
    					</a>
    				</h4>
    			</div>
    			<div id="list-amount" class="panel-collapse collapse">
    				<div class="panel-body p-a-1">
    					<ul class="list-group">
    					</ul>
    				</div>
    			</div>
    		</div>
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h4 class="panel-title">
    					<a data-toggle="collapse" data-parent="#accordion" href="#list-note">
    						<i class="fa fa-arrow-down pull-right"></i> <?= __("Özel Not Bilgileri"); ?>
    					</a>
    				</h4>
    			</div>
    			<div id="list-note" class="panel-collapse collapse">
    				<div class="panel-body p-a-1">
    					<ul class="list-group">
    					</ul>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>