<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->page_title . ' | ' . $this->website->name; ?></title>

    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- BEGIN CORE CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/panel/css/admin1.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/elements.css">
    <!-- END CORE CSS -->

    <!-- BEGIN PLUGINS CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/plugins/bootstrap-social/bootstrap-social.css">
    <!-- END PLUGINS CSS -->

    <!-- FIX PLUGINS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/plugins.css">
    <!-- END FIX PLUGINS -->

    <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/globals/img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/globals/img/icons/apple-touch-icon.png">
    <!-- END SHORTCUT AND TOUCH ICONS -->

    <script src="<?= base_url(); ?>assets/globals/plugins/modernizr/modernizr.min.js"></script>
</head>
<body class="bg-login printable">

    <?php if($status['result'] === TRUE): ?>
        <div class="col-md-12" style="margin-top:20%; z-index:9; opacity: 0.9;">
                <div class="card card-iconic card-green">
                    <div class="card-full-icon fa fa-check-circle"></div>

                    <div class="card-body text-left">
                        <i class="card-icon fa fa-check-circle"></i>
                        <h4><?= __("Şifre Aktivasyonu"); ?></h4>
                        <p><?= $status['message'] ?></p>
                        <a href="<?= base_url('login'); ?>"> <?= __("Giriş sayfasına git"); ?><i class="fa fa-sign-in"></i></a>
                    </div><!--.card-body-->

                </div><!--.card-->
            </div>
    <?php else: ?>

        <div class="col-md-12" style="margin-top:20%; z-index:9; opacity: 0.9;">
                <div class="card card-iconic card-red">
                    <div class="card-full-icon fa fa-exclamation-triangle"></div>

                    <div class="card-body text-left">
                        <i class="card-icon fa fa-exclamation-triangle"></i>
                        <h4><?= __("Şifre Aktivasyonu"); ?></h4>
                        <p><?= $status['message'] ?></p>
                        <a href="<?= base_url('login'); ?>"> <?= __("Giriş sayfasına git"); ?><i class="fa fa-sign-in"></i></a>
                    </div><!--.card-body-->

                </div><!--.card-->
            </div>

    <?php endif; ?>

  

    <!-- BEGIN GLOBAL AND THEME VENDORS -->
    <script src="<?= base_url(); ?>assets/globals/js/global-vendors.js"></script>
    <!-- END GLOBAL AND THEME VENDORS -->

    <!-- JQUERY FORM -->
    <script src="<?= base_url(); ?>assets/globals/scripts/jquery.form.min.js"></script>
    <!-- END JQUERY FORM -->

    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <script src="<?= base_url(); ?>assets/globals/scripts/user-pages.js"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->

    <!-- PLEASURE Initializer -->
    <script src="<?= base_url(); ?>assets/globals/js/pleasure.js"></script>
    <!-- ADMIN 1 Layout Functions -->
    <script src="<?= base_url(); ?>assets/panel/js/layout.js"></script>
    <script src="<?= base_url(); ?>assets/globals/js/master.js"></script>

    <!-- BEGIN INITIALIZATION-->
    <script type="text/javascript">
    $(function() {
        Pleasure.init();
        Layout.init();
        UserPages.login();
        <?php if(isset($error)): ?>
            Pleasure.handleToastrSettings('Error!', "<?php echo preg_replace('/\s+/', ' ', trim($error)); ?>", 'error');
        <?php endif; ?>
    });
    </script>
    <!-- END INITIALIZATION-->
</body>
</html>
