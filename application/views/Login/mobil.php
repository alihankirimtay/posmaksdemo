<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->page_title . ' | ' . $this->website->name; ?></title>

    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
    </script>

    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- BEGIN CORE CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/panel/css/admin1.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/elements.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/panel/css/login.css">
    <!-- END CORE CSS -->

    <!-- BEGIN PLUGINS CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/plugins/bootstrap-social/bootstrap-social.css">
    <!-- END PLUGINS CSS -->

    <!-- FIX PLUGINS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/plugins.css">
    
    <!-- END FIX PLUGINS -->

    <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/globals/img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/globals/img/icons/apple-touch-icon.png">
    <!-- END SHORTCUT AND TOUCH ICONS -->

    <script src="<?= base_url(); ?>assets/globals/plugins/modernizr/modernizr.min.js"></script>
    
</head>

<body>
    <div class="container-fluid body-container">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div>
                <h1 class="posmaks-title">POSMAKS <br> <?= __("GİRİŞ"); ?></h1>
            </div>
            <form id="pane-login" role="form" class="col-xs-12 col-lg-12 col-md-12 col-sm-12" method="post">
                <fieldset>
                    <div class="label-login"><?= __("Kullanıcı Adı"); ?></div>
                    <div class="form-group">
                        <input class="form-control input-background input-lg" name="name" type="text" value="<?php echo $this->input->post('name', TRUE); ?>" autofocus>
                    </div>
                    <div class="label-login"><?= __("Şifre"); ?></div>
                    <div class="form-group">
                        <input class="form-control input-background input-lg" name="password" type="password" value="">
                    </div>
                    <div class="form-button clearfix">
                    <button type="submit" name="login" class="btn btn-success btn-block btn-login btn-xxl"><?= __("GİRİŞ YAP"); ?></button>
                    </div>
                </fieldset>
            </form>
        </div>

<!--         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-footer pl-pr-0">
            <a href="#" id="terminalSettings" class="btn btn-primary btn-xxl btn-ripple col-xs-12 col-lg-12 col-md-12 col-sm-12"><i class="fa fa-cogs"></i> TERMİNAL AYARLARI</a>
        </div> -->


    </div>


    <!-- BEGIN GLOBAL AND THEME VENDORS -->
    <script src="<?= base_url(); ?>assets/globals/js/global-vendors.js"></script>
    <!-- END GLOBAL AND THEME VENDORS -->

    <!-- JQUERY FORM -->
    <script src="<?= base_url(); ?>assets/globals/scripts/jquery.form.min.js"></script>
    <!-- END JQUERY FORM -->

    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <script src="<?= base_url(); ?>assets/globals/scripts/user-pages.js"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->

    <!-- PLEASURE Initializer -->
    <script src="<?= base_url(); ?>assets/globals/js/pleasure.js"></script>
    <!-- ADMIN 1 Layout Functions -->
    <script src="<?= base_url(); ?>assets/panel/js/layout.js"></script>
    <script src="<?= base_url(); ?>assets/globals/js/master.js"></script>
    

    <!-- BEGIN INITIALIZATION-->
    <script type="text/javascript">
    $(function() {
        Pleasure.init();
        Layout.init();
        UserPages.login();
        <?php if(isset($error)): ?>
            Pleasure.handleToastrSettings('Error!', "<?php echo preg_replace('/\s+/', ' ', trim($error)); ?>", 'error');
        <?php endif; ?>
    });
    </script>
    <!-- END INITIALIZATION-->
</body>
</html>
