<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->page_title . ' | ' . $this->website->name; ?></title>

    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
    </script>

    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- BEGIN CORE CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/panel/css/admin1.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/elements.css">
    <!-- END CORE CSS -->

    <!-- BEGIN PLUGINS CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/plugins/bootstrap-social/bootstrap-social.css">
    <!-- END PLUGINS CSS -->

    <!-- FIX PLUGINS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/plugins.css">
    <!-- END FIX PLUGINS -->

    <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/globals/img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/globals/img/icons/apple-touch-icon.png">
    <!-- END SHORTCUT AND TOUCH ICONS -->

    <script src="<?= base_url(); ?>assets/globals/plugins/modernizr/modernizr.min.js"></script>
</head>
<body class="bg-login printable">

    <div class="login-screen">
	<div class="login-logo">
	<img src="<?= base_url(); ?>assets/posmaks-login-logo.png">
	</div>
        <div class="panel-login blur-content">
		
            <div class="panel-heading"></div>

            <form id="pane-login" class="panel-body active" method="post">
                <h2><?= __("POSMAKS GİRİŞ"); ?></h2>
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input name="name" type="text" class="form-control text-white" placeholder="<?= __("Kullanıcı Adı/Eposta"); ?>" value="<?php echo $this->input->post('name', TRUE); ?>"> 
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input name="password" type="password" class="form-control text-white" placeholder="<?= __("Şifre"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="clearfix">
                    <button name="login" type="submit" class="btn btn-lg btn-block btn-ripple btn btn-success" style="margin-top: 20px;"><?= __("Giriş"); ?></button>
                </div><!--.form-buttons-->


                <ul class="extra-links">
                    <li><a href="#" class="show-pane-forgot-password"><?= __("Şifremi Unuttum"); ?></a></li>
                </ul>
            </form><!--#login.panel-body-->

            <form id="pane-forgot-password" class="panel-body" method="post">
                <h2><?= __("Şifremi Unuttum"); ?></h2>
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input name="email" type="email" class="form-control  text-white" placeholder="<?= __("E-Posta Adresiniz"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-buttons clearfix">
                    <button type="submit" class="btn btn-white pull-left show-pane-login"><?= __("İptal"); ?></button>
                    <button type="submit" class="btn btn-success pull-right submit" data-loading-text="Gönderiyor"><?= __("Gönder"); ?></button>
                </div><!--.form-buttons-->
            </form><!--#pane-forgot-password.panel-body-->

            <div id="pane-create-account" class="panel-body">
                <h2><?= __("Hesap Oluştur"); ?></h2>
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" placeholder="<?= __("İsminiz"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="email" class="form-control" placeholder="<?= __("Eposta"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" placeholder="<?= __("Şifre"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-group">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" placeholder="<?= __("Şifre tekrar"); ?>">
                        </div>
                    </div>
                </div><!--.form-group-->
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="remember" value="1"> <?= __("Sözleşmeyi okudum ve kabul ediyorum."); ?>
                    </label>
                </div>
                <div class="form-buttons clearfix">
                    <button type="submit" class="btn btn-white pull-left show-pane-login"><?= __("İptal"); ?></button>
                    <button type="submit" class="btn btn-success pull-right"><?= __("Kaydet"); ?></button>
                </div><!--.form-buttons-->
            </div><!--#login.panel-body-->

        </div><!--.blur-content-->
    </div><!--.login-screen-->

    <div class="bg-blur dark">
        <div class="overlay"></div><!--.overlay-->
    </div><!--.bg-blur-->

    <svg version="1.1" xmlns='http://www.w3.org/2000/svg'>
        <filter id='blur'>
            <feGaussianBlur stdDeviation='7' />
        </filter>
    </svg>

    <!-- BEGIN GLOBAL AND THEME VENDORS -->
    <script src="<?= base_url(); ?>assets/globals/js/global-vendors.js"></script>
    <!-- END GLOBAL AND THEME VENDORS -->

    <!-- JQUERY FORM -->
    <script src="<?= base_url(); ?>assets/globals/scripts/jquery.form.min.js"></script>
    <!-- END JQUERY FORM -->

    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <script src="<?= base_url(); ?>assets/globals/scripts/user-pages.js"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->

    <!-- PLEASURE Initializer -->
    <script src="<?= base_url(); ?>assets/globals/js/pleasure.js"></script>
    <!-- ADMIN 1 Layout Functions -->
    <script src="<?= base_url(); ?>assets/panel/js/layout.js"></script>
    <script src="<?= base_url(); ?>assets/globals/js/master.js"></script>

    <!-- BEGIN INITIALIZATION-->
    <script type="text/javascript">
    $(function() {
        Pleasure.init();
        Layout.init();
        UserPages.login();
        <?php if(isset($error)): ?>
            Pleasure.handleToastrSettings('Error!', "<?php echo preg_replace('/\s+/', ' ', trim($error)); ?>", 'error');
        <?php endif; ?>
        <?php if(isset($this->theme_plugin['start'])): ?>
            <?= $this->theme_plugin['start']; ?>
        <?php endif; ?>


        $("#pane-forgot-password .submit").on("click", function(e) {
            e.preventDefault();

            let button = $(this);
            button.button('loading');

            $.post(base_url + "login/forgotPassword", $("#pane-forgot-password").serializeArray(), function(json) {
                if (!json.result) {
                    button.button('reset');
                    Pleasure.handleToastrSettings('Hata!', json.message, 'error');
                } else {
                    $("#pane-forgot-password").html(`<h2 class="p-a-3 m-t-3">${json.message}<br><i class="fa fa-envelope fa-4x"></i></h2>`);
                }
            }, "json");
        });
    });
    </script>
    <!-- END INITIALIZATION-->
</body>
</html>
