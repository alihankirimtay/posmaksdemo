<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Yemek Sepeti') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Yemek Sepeti') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?=dropdownMenu($this->user->locations_array, $location_id, 'yemeksepetisync/index/', 'Restoranlar')?>
                    </div>
                </div>
                <form action="#" class="form-horizontal">
                    <div class="panel-body">
                        <div class="overflow-table">
                            <table id="ysProduct" class="table">
                                <thead>
                                    <tr>
                                        <th><?= __('Yemek Sepeti Ürün Adı') ?></th>
                                        <th class ="text-right"><?= __('Eşleştirilecek Ürün') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="form-buttons-fixed">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url("yemeksepetisync/update/{$location_id}");?>" ><?= __('Kaydet') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
     


</div>