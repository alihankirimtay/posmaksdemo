<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Fiş Düzeni') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('locations'); ?>" class="active"><?= __('Restoranlar') ?></a></li>
                    <li><a href="sales" class="active"><?= __('Fiş Düzeni') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title"><h4><?= __('Fiş Bilgileri') ?></h4></div>
                    </div><!--.panel-heading-->
                    <div class="panel-body">

                        <form action="#" class="form-horizontal">
                            <div class="form-content">

                            <div class="row example-row">
                            <div class="col-md-3"><?= __('Logo') ?></div><!--.col-md-3-->
                            <div class="col-md-9">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!$tmp): ?>
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                <?php else: ?>
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"><img src="<?= base_url("$tmp->logo"); ?>"></div>
                                <?php endif ?>

                                <div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new"><?= __('Resim Seç') ?></span>
                                        <span class="fileinput-exists"><?= __('Değiştir') ?></span>
                                        <input type="file" name="image">
                                    </span>
                                </div>
                            </div>
                                
                                    

                            </div><!--.col-md-9-->
                        </div><!--.row-->
                        <hr/>

                        <div class="row example-row">
                            <div class="col-md-3"><?= __('Üst Başlık') ?></div><!--.col-md-3-->
                            <div class="col-md-9">

                                <textarea name="header" class="summernote-default">
                                    <?= ($tmp != NULL ? $tmp->header : NULL); ?>
                                </textarea>
                                    

                            </div><!--.col-md-9-->
                        </div><!--.row-->
                        <hr/>
                        <div class="row example-row">
                            <div class="col-md-3"><?= __('Alt Başlık') ?></div><!--.col-md-3-->
                            <div class="col-md-9">

                                <textarea name="footer" class="summernote-default">
                                    <?= ($tmp != NULL ? $tmp->footer : NULL); ?>
                                </textarea>
                                    

                            </div><!--.col-md-9-->
                        </div><!--.row-->


                            </div><!--.form-content-->

                            <div class="form-buttons">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="dosubmit" class="btn btn-blue btn-ripple"><?= __('Kaydet') ?></button>
                                        <a href="<?= base_url('locations')?>" class="btn btn-flat btn-default btn-ripple"><?= __('İptal') ?></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div><!--.panel-body-->
                </div><!--.panel-->

            </div><!--.col-md-12-->
        </div>


    

</div>
