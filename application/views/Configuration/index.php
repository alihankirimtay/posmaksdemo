<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Uygulama Ayarları"); ?></small></h1>
            </div><!--.col-->
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(''); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Ayarlar"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Yapılandırma"); ?></a></li>
                </ol>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.page-header-->
    <!-- content -->
    <div class="row">
        <div class="col-md-12">

            <form action="#" class="form-horizontal">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title"><h4><?= __("Uygulama Yapılandırma"); ?></h4></div>
                    </div><!--.panel-heading-->
                    <div class="panel-body">


                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Uygulama Adı"); ?></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="name" value="<?= $this->website->name; ?>" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div><!--.form-group-->

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Firma Adı"); ?></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="company" value="<?= $this->website->company; ?>" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div><!--.form-group-->


                            <div class="form-group">
                               <label class="control-label col-md-3"><?= __("Eposta"); ?></label>
                               <div class="col-md-5">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="email" value="<?= $this->website->email; ?>" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div><!--.form-group-->

                        <div class="form-group">
                            <label class="control-label col-md-3"><?= __("URL"); ?></label>
                            <div class="col-md-5">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="url" value="<?= $this->website->url; ?>" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><?= __("POS Duvar Kağıdı"); ?></label>
                            <div class="col-md-5">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                        <img src="<?= base_url($this->website->screen_saver) ?>" width="100%">
                                    </div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new"><?= __("Resim Seç"); ?></span>
                                            <span class="fileinput-exists"><?= __("Değiştir"); ?></span>
                                            <input type="file" name="screen_saver">
                                        </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= __("Kaldır"); ?></a>
                                        <small class="help-block"><?= __("Dosya en fazla 2MB boyutunda ve Jpg, Png veya Gif uzantılı olmalı."); ?></small>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3"><?= __("POS Ekranında Sanal Klavye Kullan"); ?></label>
                            <div class="col-md-5">
                                <div class="radioer">
                                    <input type="radio" name="virtual_keyboard" id="virtual_keyboard1" value="1" <?= checked($this->website->virtual_keyboard, 1) ?>>
                                    <label for="virtual_keyboard1"><?= __("Evet"); ?></label>
                                </div>
                                <div class="radioer">
                                    <input  type="radio" name="virtual_keyboard" id="virtual_keyboard0" value="0" <?= checked($this->website->virtual_keyboard, 0) ?>>
                                    <label for="virtual_keyboard0"><?= __("Hayır"); ?></label>
                                </div>
                            </div>
                        </div>


                    </div><!--.form-content-->
                </div>


                <div class="panel-heading">
                    <div class="panel-title"><h4><?= __("Lokasyon & Tarih"); ?></h4></div>
                </div><!--.panel-heading-->
                <div class="panel-body">

                    <?php 

                    $arrS = array(
                     'm-d-Y' => date('m-d-Y') . ' (MM-DD-YYYY)',
                     'Y-m-d' => date('Y-m-d') . ' (YYYY-MM-DD)',
                     'j-m-Y' => date('j-m-Y') . ' (D-MM-YYYY)',
                     'm-j-y' => date('m-j-y') . ' (MM-D-YY)',
                     'j-M-y' => date('j-M-y') . ' (D-MMM-YY)',
                     'd M Y' => date('d M Y')
                     );

                    $short_date = '';
                    foreach ($arrS as $key => $val) {
                        if($this->website->date_short == $key):
                            $short_date .= "<option selected=\"selected\" value=\"" . $key . "\">" . $val . "</option>\n";
                        else:
                            $short_date .= "<option value=\"" . $key . "\">" . $val . "</option>\n";
                        endif;
                        
                    }

                    $arrL = array(
                      'F d, Y h:i:s' => date('F d, Y h:i:s'),
                      'Y-m-d H:i:s' => date('Y-m-d H:i:s'),
                      'd F Y  h:i:s' => date('d F Y  h:i:s'),
                      'F d, Y' => date('F d, Y'),
                      'd F, Y' => date('d F, Y'),
                      'l d F Y' => date('l d F Y'),
                      'l d F Y h:i' => date('l d F Y h:i'),
                      'D d, F' => date('D d, F'));

                    $long_time = '';
                    foreach ($arrL as $key => $val) {
                        if($this->website->date_long == $key):
                            $long_time .= "<option selected='selected' value=\"" . $key . "\">" . $val . "</option>\n";
                        else:
                            $long_time .= "<option value=\"" . $key . "\">" . $val . "</option>\n";
                        endif;
                  }
                  ?>

                  <div class="form-group">
                    <label class="control-label col-md-3"><?= __("Kısa Tarih"); ?></label>
                    <div class="col-md-5">
                        <select name="date_short" class="chosen-select">
                           <?= $short_date; ?>
                       </select>
                   </div>
               </div><!--.form-group-->

               <div class="form-group">
                <label class="control-label col-md-3"><?= __("Uzun Tarih"); ?></label>
                <div class="col-md-5">
                    <select name="date_long" class="chosen-select">
                       <?= $long_time; ?>
                   </select>
               </div>
           </div><!--.form-group-->
    
        <?php if (0): // DISABLED?>
            <div class="form-group">
                <label class="control-label col-md-3"><?= __("Zaman dilimi"); ?></label>
                <div class="col-md-5">
                    <select name="dtz" data-live-search="true" class="chosen-select">
                        <option value=""><?= __("Zaman dilimi seç"); ?></option>
                        <?php foreach(DateTimeZone::listIdentifiers() as $key => $timezone): ?>
                            <option <?= ($this->website->dtz == $timezone ? 'selected="selected"' : ''); ?> value="<?= $timezone; ?>"><?= $timezone; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div><!--.form-group-->
        <?php endif; ?>
    </div>


    <div class="panel-heading">
        <div class="panel-title"><h4><?= __("Eposta Ayarlar"); ?></h4></div>
    </div><!--.panel-heading-->
    <div class="panel-body">


        <div class="form-group">
         <label class="control-label col-md-3"><?= __("SMTP Hostname"); ?></label>
         <div class="col-md-5">
            <div class="inputer">
                <div class="input-wrapper">
                    <input type="text" name="smtp_host" value="<?= $this->website->smtp_host?>" class="form-control" />
                </div>
            </div>
        </div>
    </div><!--.form-group-->

    <div class="form-group">
        <label class="control-label col-md-3"><?= __("SMTP Kullanıcı"); ?></label>
        <div class="col-md-5">
            <div class="inputer">
                <div class="input-wrapper">
                    <input type="text" name="smtp_user" value="<?= $this->website->smtp_user?>" class="form-control" />
                </div>
            </div>
        </div>
    </div><!--.form-group-->

    <div class="form-group">
        <label class="control-label col-md-3"><?= __("SMTP Şifre"); ?></label>
        <div class="col-md-5">
            <div class="inputer">
                <div class="input-wrapper">
                    <input type="text" name="smtp_pass" value="<?= $this->website->smtp_pass?>" class="form-control" />
                </div>
            </div>
        </div>
    </div><!--.form-group-->

    <div class="form-group">
        <label class="control-label col-md-3"><?= __("SMTP Port"); ?></label>
        <div class="col-md-5">
            <div class="inputer">
                <div class="input-wrapper">
                    <input type="text"value="<?= $this->website->smtp_port?>" name="smtp_port" class="form-control" />
                </div>
            </div>
        </div>
    </div><!--.form-group-->

    <div class="form-group">
    <label class="control-label col-md-3"><?= __("SSL Gerekli"); ?></label>
        <div class="col-md-5">
            <div class="radioer">
                <input <?= ($this->website->is_ssl == 1 ? 'checked="checked"' : ''); ?> type="radio" name="is_ssl" id="radioColor13" value="1" >
                <label for="radioColor13"><?= __("Evet"); ?></label>
            </div>
            <div class="radioer">
                <input <?= ($this->website->is_ssl == 0 ? 'checked="checked"' : ''); ?>  type="radio" name="is_ssl" id="radioColor14" value="0">
                <label for="radioColor14"><?= __("Hayır"); ?></label>
            </div>
        </div>
    </div>



    <div class="form-buttons">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" name="dosubmit" data-return="" data-url="<?=base_url('configuration');?>" data-tutorialize="saveConfiguration" class="btn btn-blue"><?= __("Kaydet"); ?></button>
                <a  href="" class="btn btn-flat btn-default"><?= __("İptal"); ?></a>
            </div>
        </div>
    </div>

</div><!--.panel-body-->
</div><!--.panel-->
</form>
</div><!--.col-md-12-->
</div><!--.row-->
<!-- content -->

</div>
