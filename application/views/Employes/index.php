<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Çalışanlar"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url() ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Çalışanlar"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

            	<div class="panel-heading">
            		<div class="panel-title">
            			<?= dropdownMenu($this->user->locations_array, $location_id, 'employes/index/', 'Restoranlar') ?>
            		</div>
            	</div>

            	<div class="panel-heading">
            		<div class="panel-title">
            			<h4><?= __("Tüm Çalışanlar"); ?></h4>
            			<div class="btn-group pull-right">
            				<a id="calisanOlustur" href="<?= base_url('employes/add/') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __("Çalışan Oluştur"); ?></a>
            			</div>
            		</div>
            	</div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("İsim Soyisim"); ?></th>
                                    <th><?= __("Hesap Adı"); ?></th>
                                    <th><?= __("Eposta"); ?></th>
                                    <th><?= __("Rol"); ?></th>
                                    <th><?= __("Oluşturludu"); ?></th>
                                    <th><?= __("Durum"); ?></th>
                                    <th><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?= $user->name ?></td>
                                        <td><?= $user->username ?></td>
                                        <td><?= $user->email ?></td>
                                        <td><?= $user->role_title ?></td>
                                        <td><?= dateConvert($user->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($user->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('employes/edit/'.$user->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('employes/delete/'.$user->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php foreach ($admins as $user): ?>
                                    <tr>
                                        <td><?= $user->name ?></td>
                                        <td><?= $user->username ?></td>
                                        <td><?= $user->email ?></td>
                                        <td><?= $user->role_title ?></td>
                                        <td><?= dateConvert($user->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($user->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('employes/edit/'.$user->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('employes/delete/'.$user->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
