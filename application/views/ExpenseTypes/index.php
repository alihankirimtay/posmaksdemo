<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Gider Türü"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("İşlemler"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Gider Türü"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Tüm Gider Türleri"); ?></h4>
                        <div class="btn-group pull-right">
                            <a id="giderTuru" href="<?php ECHO base_url('expensetypes/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __("Gider Türü Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-5"><?= __("İsim"); ?></th>
                                    <th class="col-md-4"><?= __("Oluşturuldu"); ?></th>
                                    <th class="col-md-1"><?= __("Durum"); ?></th>
                                    <th class="col-md-2"><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($expensetypes as $expensetype): ?>
                                    <tr>
                                        <td><?php ECHO $expensetype->title; ?></td>
                                        <td><?=dateConvert($expensetype->created, 'client', dateFormat())?></td>
                                        <td><?php ECHO get_status($expensetype->active); ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('expensetypes/edit/'.$expensetype->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <!-- <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('expensetypes/delete/'.$expensetype->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button> -->
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
