<?php 
    if(!isset($this->user->locations[0])) {
        $locations_temp = [
            'dtz' => "Europe/Istanbul",
            'currency' => "₺",
            'active' => 1
        ];
    } else {
        $location_id = $this->user->locations[0];
    }
?>
<div id="wizardModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="wizard-header">
            <div class="col-lg-10 header-title">
                <span>HIZLI KURULUM SİHİRBAZI</span>
            </div>
            <div class="col-lg-2 close-button" data-dismiss="modal">
                <span>X</span>
            </div>
        </div>
        <div class="modal-body">
            <div class="col-lg-12 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center">
                    <h4 class="glyphicon glyphicon-pencil"></h4><br/>Restoran Adı
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-unchecked"></h4><br/>Masalar
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-print"></h4><br/>Üretim Yerleri
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-list"></h4><br/>Gider Kategorileri
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-credit-card"></h4><br/>Faturalı Stok Girişi
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-align-justify"></h4><br/>Ürün Kategorileri
                    </a>
                    <a href="#" class="list-group-item text-center">
                    <h4 class="glyphicon glyphicon-tags"></h4><br/>Ürünler
                    </a>
                </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active location">
                        <form action="#" class="form-horizontal">
                        <div class="form-content">
                            
                            <div class="form-group">
                                <div class="col-lg-offset-3">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?= $this->user->locations_array[$location_id]['title']; ?>">
                                            <input type="hidden" name="dtz" value="<?php echo (!isset($location_temp)) ? $this->user->dtz : $location_temp['dtz'] ?>">
                                            <input type="hidden" name="currency" value="<?php echo (!isset($location_temp)) ? $this->user->locations_array[$location_id]['currency'] : $location_temp['currency'] ?>">
                                            <input type="hidden" name="active" value="<?php echo (!isset($location_temp)) ? 1 : $location_temp['active'] ?>">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-buton-fixed">
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn-ripple btn-xxl" name="dosubmit" data-url="<?= (!isset($location_temp)) ? base_url('locations/edit/'.$this->user->locations_array[$location_id]['id']) : base_url('locations/edit/'.$this->user->locations_array[$location_id]['id']) ?>">İLERİ</button>
                                </div>
                            </div>

                        </div>
                    </form>
                    </div>
                    <!-- train section -->
                    <div class="bhoechie-tab-content">
                        <center>
                        <h1 class="glyphicon glyphicon-road" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Train Reservation</h3>
                        </center>
                    </div>
        
                    <!-- hotel search -->
                    <div class="bhoechie-tab-content">
                        <center>
                        <h1 class="glyphicon glyphicon-home" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Hotel Directory</h3>
                        </center>
                    </div>
                    <div class="bhoechie-tab-content">
                        <center>
                        <h1 class="glyphicon glyphicon-cutlery" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Restaurant Diirectory</h3>
                        </center>
                    </div>
                    <div class="bhoechie-tab-content">
                        <center>
                        <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div>
</div>