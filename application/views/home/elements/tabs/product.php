<div class="panel">
    
    <div class="hidden">
        <a href="#indexProduct" data-toggle="tab" >1.</a>
        <a href="#addProduct" data-toggle="tab">2.</a>
        <a href="#editProduct" data-toggle="tab">3.</a>
        <a href="#newCategoryTab" data-toggle="tab">4.</a>
    </div>

    <div class="tab-content p-a-0 b-0" style="overflow-y:auto;height: 400px">

        <div class="tab-pane active" id="indexProduct">

            <div class="panel-heading bb-0">
                <div class="panel-title p-l-0">
                    <h4 class="content-header col-md-10"><?= __('HIZLI ÜRÜN EKLEME') ?></h4>
                    <a id="createProduct" href="#" class="col-md-2 btn btn-success btn-xs btn-ripple pull-right text-color-white"><i class="fa fa-plus"></i><?= __('Ürün Oluştur') ?></a>
                </div>
                <div class="col-md-12">
                    <h6 style="color: black"><?= __('Bu aşamada ürünlerinizi ekleyebilirsiniz.'); ?></h6>
                </div>
            </div>

            <div class="table-responsive col-md-12" >
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?= __('Ürün Adı') ?></th>
                            <th style="text-align:center;"><?= __('Üretim Yeri') ?></th>
                            <th class="text-center"><?= __('Ürün Kategorisi') ?></th>
                            <th><?= __('Fiyat') ?></th>
                            <th><?= __('İşlem') ?></th>
                        </tr>
                    </thead>
                </table>
                <div class="product-table-scroll">
                    <table id="fillWizardProductTable" class="table table-striped">
                        <tbody>
                        </tbody>
                    </table>
                </div>     
            </div> 

        </div>





        <div class="tab-pane" id="addProduct">

            <div class="panel-body">

                <form action="#" class="form-horizontal">

                    <div class="form-content">
                        <div class="form-group">
                            <label class="control-label col-md-2"><?= __('Ürün Adı') ?></label>
                            <div class="col-md-10">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Kategori') ?></label>
                        <div class="col-md-6">
                            <select class="chosen-select" name="product_type_id">
                            </select>
                        </div>
                        <button type="button" class="col-md-3 btn btn-primary" id="newCategory">Yeni Kategori Ekle</button>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Üretim Yeri') ?></label>
                        <div class="col-md-10">
                            <select class="chosen-select" name="production_id">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Vergi') ?></label>
                        <div class="col-md-10">
                            <select class="chosen-select" name="tax">
                                <?php foreach ($this->taxes as $tax_key => $tax): ?>
                                    <?php if($tax_key == 0): ?>
                                        <option value="<?= $tax_key; ?>" selected="selected" ><?= $tax ?></option>
                                    <?php else: ?>
                                        <option value="<?= $tax_key; ?>" ><?= $tax ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Fiyat') ?></label>
                        <div class="col-md-10">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input type="number" name="price" class="form-control price-without-tax" step="0.01" value="0.00" placeholder="0.00">
                                </div>
                                <p class="help-block"><small class="text-danger"><?= __('Vergi hariç') ?></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input type="number" name="price_tax" class="form-control price-with-tax" step="0.01" value="0.00" placeholder="0.00">
                                </div>
                                <p class="help-block"><small class="text-danger"><?= __('Vergi dahil') ?></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="wizard-stocks-container">
                        <div class="form-group">
                            <label class="control-label col-md-2"><?= __('Stok İçeriği') ?></label>
                            <div class="col-md-10">
                                <select multiple="multiple" class="chosen-select stocks">
                                    
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="btn-group pull-right p-a-3">
                        <button id="saveProduct" type="button" class="br-0 btn btn-ripple btn-success btn-xl" ><?= __('Kaydet') ?></button>
                        <button id="cancelProduct" type="button" class="br-0 btn btn-danger btn-ripple btn-xl" ><?= __('İptal') ?></button>
                    </div>

                    <div class="hidden">
                        <input type="hidden" name="active" value="1">
                        <input type="radio" name="type" id="type0" checked="checked" value="product">
                        <input type="hidden" name="bonus_type" value="value">
                        <input type="hidden" name="sort" value="0">
                        <input type="hidden" name="bonus" value="0">

                        
                    </div>

                </form>

            </div>

        </div>

        <div class="tab-pane" id="editProduct">


            <div class="panel-body">

                <form action="#" class="form-horizontal">

                    <div class="form-content">
                        <div class="form-group">
                            <label class="control-label col-md-2"><?= __('Ürün Adı') ?></label>
                            <div class="col-md-10">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Kategori') ?></label>
                        <div class="col-md-6">
                            <select class="chosen-select" name="product_type_id">
                            </select>
                        </div>
                        <button type="button" class="col-md-3 btn btn-primary" id="newEditCategory">Yeni Kategori Ekle</button>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Üretim Yeri') ?></label>
                        <div class="col-md-10">
                            <select class="chosen-select" name="production_id">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Vergi') ?></label>
                        <div class="col-md-10">
                            <select class="chosen-select" name="tax">
                                <?php foreach ($this->taxes as $tax_key => $tax): ?>
                                    <option value="<?= $tax_key; ?>" ><?= $tax ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2"><?= __('Fiyat') ?></label>
                        <div class="col-md-10">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input type="number" name="price" class="form-control price-without-tax" step="0.01" value="0.00" placeholder="0.00">
                                </div>
                                <p class="help-block"><small class="text-danger"><?= __('Vergi hariç') ?></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="inputer">
                                <div class="input-wrapper">
                                    <input type="number" name="price_tax" class="form-control price-with-tax" step="0.01" value="0.00" placeholder="0.00">
                                </div>
                                <p class="help-block"><small class="text-danger"><?= __('Vergi dahil') ?></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="wizard-stocks-container">
                        <div class="form-group">
                            <label class="control-label col-md-2"><?= __('Stok İçeriği') ?></label>
                            <div class="col-md-10">
                                <select multiple="multiple" class="chosen-select stocks">
                                    
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="btn-group pull-right">
                        <button id="saveEditProduct" type="button" class="br-0 btn btn-ripple btn-success btn-xl" ><?= __('Kaydet') ?></button>
                        <button id="cancelEditProduct" type="button" class="br-0 btn btn-danger btn-ripple btn-xl" ><?= __('İptal') ?></button>
                    </div>

                    <div class="hidden">
                        <input type="hidden" name="active" value="">
                        <input type="radio" name="type" checked="checked" value="product">
                        <input type="hidden" name="bonus_type" value="">
                        <input type="hidden" name="sort" value="">
                        <input type="hidden" name="bonus" value="">
                        <input type="hidden" name="product_id" value="">

                        <select name="additional_products[]" class="chosen-select" multiple="multiple" style="display: none;">
                        </select>
                        <select name="portion_products[]" class="chosen-select" multiple="multiple" style="display: none;">
                        </select>
                        
                    </div>


                    



                </form>

            </div>



        </div>




        <div class="tab-pane" id="newCategoryTab">

            <div class="panel-body">

                <h4 class="content-header"><?= __('Yeni Kategori Ekleme Sayfası'); ?></h4>
                <h6 style="color: black"><?= __('Bu bölümde ürünlerinizi gruplayabileceğiniz kategorileri oluşturabilirsiniz.'); ?></h6>

                <form action="#" class="form-horizontal m-t-3 p-t-3">

                    <div class="form-content">
                        <div class="form-group">
        
                            <div class="input-wrapper col-md-6">
                                <input type="text" name="title" class="form-control" placeholder="<?= __('Kategori Adı') ?>">
                                <input type="hidden" name="active" value="1">                                
                                <input type="hidden" name="sort" value="0">                                
                            </div>
                            <div class="col-md-6">
                                <select name="parent_id" class="chosen-select">
                                    
                                </select>
                            </div>
                            
                        </div>
                        
                        <label class="label-ex-floor"><?= __('Örnek: Kebaplar, Köfteler, Salatalar, Sıcak İçecekler, Soğuk İçecekler, Tatlılar vs.'); ?></label>

                        <div class="btn-group col-md-12 p-a-0">
                            <button id="saveCategory" type="button" class="br-0 btn btn-ripple btn-success btn-xl col-md-6" ><?= __('Kaydet') ?></button>
                            <button id="cancelCategory" type="button" class="br-0 btn btn-danger btn-ripple btn-xl col-md-6" ><?= __('İptal') ?></button>
                        </div>
                    </div>

                </form>

            </div>

        </div>

        <button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl"><?= __('İLERİ'); ?></button>


        
    </div>


</div>
