<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <h4 class="content-header"><?= __('ÜRETİM YERLERİ') ?></h4>
            <h6 style="color: black"><?= __('Siparişlerin hazırlandığı alanları oluşturunuz.'); ?></h6>
        </div>
    </div>

    <div class="p-l-0 p-r-0 p-t-0 panel-body" style="overflow-x: hidden;overflow-y: auto;height: 320px;">
        <div class="well p-a-2">
            <div class="hidden">
                <a href="#productionAdd" data-toggle="tab" >1.</a>
                <a href="#productionEdit" data-toggle="tab">2.</a>
            </div>
            <div class="tab-content p-a-0 b-0">
                <div class="tab-pane active" id="productionAdd">
                    <label for=""><?= __('Üretim Yeri Adı') ?></label>
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-inline" style="width: calc(100% - 200px);">
                                <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title" placeholder="<?= __('Üretim Yeri Adı') ?>">
                                <input type="hidden" name="active" value="1">
                            </div>
                            <div class="form-inline pull-right">   
                                <button id="addProduction" type="button" class="btn btn-success input-lg br-0"><?= __('KAYDET') ?></button>   
                            </div>
                        </div>
                    </form>
                    <label class="label-ex-floor"><?= __('Örnek : Mutfak, Izgara, Bar vs.'); ?></label>
                </div>

                <div class="tab-pane b-0 p-a-0" id="productionEdit">
                    <label for=""><?= __('Üretim Yerini Düzenleyiniz') ?></label>
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-inline" style="width: calc(100% - 200px);">
                                <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title">
                                <input type="hidden" name="active" value="">
                                <input type="hidden" name="production_id" value="">
                            </div>
                            <div class="form-inline pull-right">   
                                <button id="btnProductionEdit" type="button" class="btn btn-success input-lg br-0"><?= __('KAYDET') ?></button>   
                                <button id="btnProductionCancel" type="button" class="btn btn-danger input-lg br-0"><?= __('İPTAL') ?></button>   
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="production-list">
            
        </div> 
        <button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl">İLERİ</button>
               

    </div>

</div>
