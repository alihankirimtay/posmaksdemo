<div class="modal full-height" id="modal-invoice">
	<div class="modal-dialog modal-lg1">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?= __('Fatura') ?></h4>
			</div>
			<div class="modal-body">
				<form role="form">
					
					<div class="form-group">
						<label><?= __('Müşteri') ?></label>
						<select name="customer_id" class="chosen-select">
							<option value=""><?= __('Müşteri Seç') ?></option>
							<?php foreach ($customers as $customer): ?>
								<option value="<?= $customer->id ?>">
									<?= $customer->name .' - '. $customer->phone ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<label><?= __('Düzenlenme Tarihi') ?></label>
						<input type="text" name="date" class="form-control datetimepicker" format="DD-MM-YYYY" value="">
						<label><?= __('Düzenlenme Saati') ?></label>
						<input type="text" name="time" class="form-control datetimepicker" format="HH:mm" value="">
					</div>

					<div class="form-group text-right">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?= __('İptal') ?></button>
						<button type="button" class="btn btn-primary" id="process_invoice"><?= __('Kaydet') ?></button>
					</div>
					
					<input type="hidden" name="sale_id" value="">
				</form>
			</div>
		</div>
	</div>
</div>