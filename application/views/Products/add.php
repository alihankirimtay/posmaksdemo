    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Ürün Oluştur') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('products'); ?>"><?= __('Ürünler') ?></a></li>
                    <li><a href="#" class="active"><?= __('Ürün Oluştur') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Ürün Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="location_id">
                                        <option value="" data-tax="0"><?= __('Restoran Seç') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id'] ?>"><?= $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Kategori') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="product_type_id">
                                        <option value=""><?= __('Kategori Seçin') ?></option>
                                        <?php foreach ($product_types as $product_type): ?>
                                            <option value="<?= $product_type->id ?>"><?= $product_type->title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Üretim Yeri') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="production_id">
                                        <option value=""><?= __('Üretim Yeri Seç') ?></option>
                                        <?php foreach ($productions as $production): ?>
                                            <option value="<?= $production->id ?>"><?= $production->title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İsim') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Açıklama') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Barkod') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="bar_code" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Vergi Oranı') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="tax">
                                        <?php foreach ($this->taxes as $tax_key => $tax): ?>
                                            <option value="<?= $tax_key; ?>" ><?= $tax ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Fiyat') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price" class="form-control price-without-tax" step="0.001" value="0.00" placeholder="0.00">
                                        </div>
                                        <p class="help-block"><small class="text-danger"><?= __('Vergi hariç') ?></small></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price_tax" class="form-control price-with-tax" step="0.01" value="0.00" placeholder="0.00">
                                        </div>
                                        <p class="help-block"><small class="text-danger"><?= __('Vergi dahil') ?></small></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Ürün Sıralaması</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="sort" class="form-control" placeholder="Ürününüzün görünmesini istediğini sırayı seçiniz.">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary-transparent">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= __('Ürün Tipi') ?></label>
                                    <div class="col-md-9">
                                        <div class="radioer form-inline">
                                            <input type="radio" name="type" id="type0" checked="checked" value="product" >
                                            <label for="type0"><i class="fa fa-square"></i> <?= __('Ürün') ?></label>
                                        </div>
                                        <div class="radioer form-inline">
                                            <input type="radio" name="type" id="type1" value="package" >
                                            <label for="type1"><i class="fa fa-th-large"></i> <?= __('Menü') ?></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="products-container" class="hidden">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?= __('Menü içerisindeki ürünler') ?></label>
                                        <div class="col-md-9">
                                            <select multiple="multiple" name="products[]" class="chosen-select">
                                                <?php foreach ($products as $product): ?>
                                                    <?php if ($product->type == 'product'): ?>
                                                        <option 
                                                            value="<?= $product->id ?>"  
                                                            data-location_id="<?= $product->location_id ?>" 
                                                            class="hidden">
                                                            <?= $product->title ?>
                                                        </option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="stocks-container">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?= __('Stok İçeriği') ?></label>
                                        <div class="col-md-7">
                                            <select multiple="multiple" class="chosen-select stocks">
                                                <?php foreach ($stocks as $stock): ?>
                                                    <option 
                                                        value="<?= $stock->id ?>" 
                                                        data-location_id="<?= $stock->location_id ?>"
                                                        data-unit="<?= @$this->units[$stock->unit] ?>"
                                                        class="hidden">
                                                        <?= $stock->title ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <a id="addStock" class="btn btn-success btn-xs" style="color: white;"><i class="fa fa-plus-square"></i> <?= __('Stok Oluştur') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İlave seçilebilir ürünler') ?></label>
                                <div class="col-md-9">
                                    <select name="additional_products[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($products as $product): ?>
                                            <?php if ($product->type == 'product'): ?>
                                                <option value="<?= $product->id ?>"
                                                data-location_id="<?= $product->location_id ?>"
                                                class="hidden">
                                                    <?= $product->title ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ürüne ait diğer porsiyonlar') ?></label>
                                <div class="col-md-9">
                                    <select name="portion_products[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($products as $product): ?>
                                            <?php if ($product->type == 'product'): ?>
                                                <option value="<?= $product->id ?>"
                                                data-location_id="<?= $product->location_id ?>"
                                                class="hidden">
                                                    <?= $product->title ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Hızlı Notlar') ?></label>
                                <div class="col-sm-7">
                                    <select name="quick_notes[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($quickNotes as $quickNote): ?>
                                            <option value="<?= $quickNote->id ?>">
                                                <?= $quickNote->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#quicknotesmodal" data-toggle="modal" class="btn btn-warning"><i class="fa fa-sticky-note-o"></i><?= __('Hızlı Not Ekle') ?></a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3"><?= __('Prim') ?></label>
                                <div class="col-sm-7">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="bonus" class="form-control" placeholder="<?= __('Nakit veya Yüzdelik değer giriniz..') ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" name="bonus_type">
                                        <option value="value"><?= __('Miktar') ?></option>
                                        <option value="percent"><?= __('Yüzdelik') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="row example-row">
                                <div class="col-md-3 control-label"><?= __('Renk') ?></div>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="color" name="color" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Resim') ?></label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new"><?= __('Resim Seç') ?></span>
                                                <span class="fileinput-exists"><?= __('Değiştir') ?></span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= __('Kaldır') ?></a>
                                                <input type="file" name="image">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Durum') ?></label>
                                <div class="col-md-9">
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active1" checked="checked" value="1" >
                                        <label for="active1"><?= __('Aktif') ?></label>
                                    </div>
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active0" value="0" >
                                        <label for="active0"><?= __('Pasif') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('products'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('products/add');?>" ><?= __('Kaydet') ?></button>
                                    <button id="kaydetVeCik" type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('products/add');?>" data-return="<?= base_url('products'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<?php $this->load->view('Products/elements/quicknotesmodal'); ?>
<?php $this->load->view('Products/elements/addStockModal'); ?>

