<div class="container-fluid tab-body body-bg">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav-row">

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-10">      
      <img class="home-click icon-svg-size" src="<?= asset_url('home-01.svg'); ?>"></img>
    </div> <!-- col-lg-3 close -->

    <div class="col-lg-9 col-md-9 col-offset-1 col-sm-9 col-offset-1 col-xs-9 col-offset-1 nav-span-home p-10">
      <span class="notification-title text-center"><?= __("BİLDİRİMLER"); ?></span>    
    </div>

  </div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <ul class="list-group" id="product-list">

    </ul>
  </div>

</div>