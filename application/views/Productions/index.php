<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Üretim Yerleri') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Üretim Yerleri') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Üretim Yerleri') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="uretimYeriOlustur" href="<?php ECHO base_url('productions/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Üretim Yeri Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('Başlık') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                    <th><?= __('Durum') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($productions as $production): ?>
                                    <tr>
                                        <td><?= $production->title ?></td>
                                        <td><?= dateConvert($production->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($production->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('productions/edit/'.$production->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('productions/delete/'.$production->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
