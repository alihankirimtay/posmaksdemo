<?php

$ves = [];

// SALES
foreach ($ves_packages as $key1 => $sale) {
  // PACKAGES
  foreach ($ves_packages as $key2 => $package) {

    if ($sale[ 'sale_id' ] != $package[ 'sale_id' ] && $sale[ 'package_id' ] != $package[ 'package_id' ]) {
      continue;
    }

    // PRODUCTS
    foreach ($ves_packages as $key3 => $product) {

      if ($product[ 'product_id' ] != $package[ 'product_id' ]) {
        continue;
      }

      if (!isset($ves[ $sale[ 'sale_id' ] ])) {
        $ves[ $sale[ 'sale_id' ] ] = [
          'sale_id'     => $sale[ 'sale_id' ],
          'sale_ticket' => $sale[ 'sale_ticket' ],
          'sale_date'   => $sale[ 'sale_date' ],
          'sale_amount' => $sale[ 'sale_amount' ],
          'packages'    => []
        ];
      }

      if (!isset($ves[ $sale[ 'sale_id' ] ][ 'packages' ][ $package[ 'package_id' ] ])) {
        $ves[ $sale[ 'sale_id' ] ][ 'packages' ][ $package[ 'package_id' ] ] = [
          'package_id'       => $package[ 'package_id' ],
          'package_title'    => $package[ 'package_title' ],
          'package_quantity' => $package[ 'package_quantity' ],
          'products'         => []
        ];
      }

      if (!isset($ves[ $sale[ 'sale_id' ] ][ 'packages' ][ $package[ 'package_id' ] ][ 'products' ][ $product[ 'product_id' ] ])) {
        $ves[ $sale[ 'sale_id' ] ][ 'packages' ][ $package[ 'package_id' ] ][ 'products' ][ $product[ 'product_id' ] ] = [
          'product_id'         => $product[ 'product_id' ],
          'product_title'      => $product[ 'product_title' ],
          'product_quantity'   => $product[ 'product_quantity' ],
          'product_checked_in' => $product[ 'product_checked_in' ]
        ];
      }
    }
  }
}

?>

  
<div class="modal-dialog modal-fullscreen">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">VES Packages</h4>
    </div>
    <div class="modal-body">


     <div class="panel-group" id="accordionSale" role="tablist" aria-multiselectable="true">
      <?php $key1 = 0; ?>
       <?php foreach ($ves as $sale_id => $sale): ?>
         <div class="panel panel-success">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordionSale" href="#collapseSale<?=$key1?>" aria-expanded="true" aria-controls="collapseSale<?=$key1?>">
                <?= $sale[ 'sale_ticket' ] ?>, <?= @$this->user->locations_array[ $location ][ 'currency' ] . number_format($sale[ 'sale_amount' ], 2) ?><span class="pull-right"><?= dateConvert($sale[ 'sale_date' ], 'client', dateFormat()) ?></span>
              </a>
            </h4>
          </div>
          <div id="collapseSale<?=$key1?>" class="panel-collapse collapse <?=($key1 ?: 'in')?>" role="tabpanel" aria-labelledby="headingSale<?=$key1?>">
            <div class="panel-body">

              <div class="panel-group" id="accordionPackage" role="tablist" aria-multiselectable="true">
              <?php $key2 = 0; ?>
                <?php foreach ($sale[ 'packages' ] as $package_id => $package): ?>
                  <div class="panel panel-danger">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordionPackage" href="#collapsePackage<?=$key2?>" aria-expanded="true" aria-controls="collapsePackage<?=$key2?>">
                          <?= $package[ 'package_title' ] ?><span class="pull-right">x<?= $package[ 'package_quantity' ] ?></span>
                        </a>
                      </h4>
                    </div>
                    <div id="collapsePackage<?=$key2?>" class="panel-collapse collapse <?=($key2 ?: 'in')?>" role="tabpanel" aria-labelledby="headingPackage<?=$key2?>">
                      <div class="panel-body">


                        <ul class="list-group">
                          <?php foreach ($package[ 'products' ] as $product_id => $product): ?>
                            <li class="list-group-item">
                              <div class="checkboxer">
                                <input type="checkbox" value='<?= json_encode(["s" => $sale_id, "pa" => $package_id, "pr" => $product_id]) ?>' id="checkProduct<?= $product_id ?>" <?php checked($product[ 'product_checked_in' ], 1) ?>>
                                <label for="checkProduct<?= $product_id ?>"><h4><b><?= $product[ 'product_title' ] ?></b></h4></label>
                                <span class="pull-right">x<?= $product[ 'product_quantity' ] ?></span>
                              </div>
                            </li>
                          <?php endforeach;?>
                        </ul>


                      </div>
                    </div>
                  </div>
                  <?php $key2++; ?>
                <?php endforeach;?>
              </div>

            </div>
          </div>
        </div>
        <?php $key1++; ?>
      <?php endforeach;?>
    </div>


  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
  </div>
</div>
</div>