<!-- Unpaid products -->
<table class="table table-hover interim_payments-unpaid">
    <thead>
        <tr>
            <th class="active col-sm-3"><?= __("Adet"); ?></th>
            <th class="active col-sm-6"><?= __("Ürün"); ?></th>
            <th class="active col-sm-3"><?= __("Fiyat"); ?></th>
        </tr>
        <tr class="interim_payments-info">
            <th colspan="3" class="danger" style="text-align: center;"><?= __("Ödemesi Yapılmayan Siparişler"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sale_has_products as $unpaid): ?>
            <?php $unpaid_quantity = $unpaid['quantity'] - ($unpaid['paid_cash'] + $unpaid['paid_credit'] + $unpaid['paid_voucher'] + $unpaid['paid_ticket'] + $unpaid['paid_sodexo']); ?>
            <?php if ($unpaid_quantity > 0): ?>
                <tr data-id="<?= $unpaid['id'] ?>">
                    <td class="quantity col-sm-3"><?= $unpaid_quantity ?></td>
                    <td class="col-sm-6"><?= $unpaid['title'] ?></td>
                    <td class="total col-sm-3"><?= numberFormat($unpaid['price'], 2) ?></td>
                    <input type="hidden" name="sale_has_products[<?= $unpaid['id'] ?>]" value="<?= $unpaid_quantity ?>">
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>
<!-- END - Unpaid products -->



<!-- Payable products(basket) -->
<form>
    <table class="table table-hover interim_payments-payable">
        <thead>
            <tr>
                <th colspan="3" class="warning" style="text-align: center;"><?= __("Ödemesi Yapılacak Siparişler"); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr class="interim_payments-info">
                <td colspan="3" style="text-align: center;"><h4><?= __("Ürün Seçiniz"); ?></h4></td>
            </tr>           
        </tbody>
    </table>
</form>
<!-- END - Payable products(basket) -->


<!-- END - Unpaid products -->
<table class="table table-hover interim_payments-paid">
    <thead>
        <tr>
            <th colspan="3" class="active" style="text-align: center;"><?= __("Ödemesi Yapılmış Siparişler"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sale_has_products as $paid): ?>
            <?php $paid_quantity = $paid['paid_cash'] + $paid['paid_credit'] + $paid['paid_voucher'] + $paid['paid_ticket'] + $paid['paid_sodexo']; ?>
            <?php if ($paid_quantity): ?>
                <tr>
                    <td class="active col-sm-3"><?= $paid_quantity ?></td>
                    <td class="active col-sm-6"><?= $paid['title'] ?></td>
                    <td class="active col-sm-3"><?= numberFormat($paid['price'], 2) ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php if (!isset($paid_quantity)): ?>
            <tr class="interim_payments-info">
                <td colspan="3" style="text-align: center;"><h4><?= __("Ödemesi Yapılmış Ürün Bulunamadı"); ?></h4></td>
            </tr>
        <?php endif; ?>                    
    </tbody>
</table>