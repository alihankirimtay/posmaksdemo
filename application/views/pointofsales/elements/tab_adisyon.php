<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group backButton">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Fatura"); ?>
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-primary invoice-print"><i class="fa fa-print"></i> <?= __("Yazdır"); ?></a>
            </div>
        </h3>
    </div>
    <div class="panel-body">
        <div class="container-fluid" style="width: 8cm !important; font-size: 14px !important;">
            <div class="invoice" style="padding:0 !important;">
                <div class="invoice-heading">
                    <div class="date-row" style="margin-bottom: 0px !important">
                        <div class="col-md-12 col-xs-12 text-center">
                            <img id="logo" class="img-responsive" src="<?= $location_invoice_template->logo ?>" alt="" style="width: 100px;">
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <p class="invoice-table-name" style="margin-bottom: 0px !important; font-weight: bold;">Masa Adı</p>
                            <p class="invoice-zone-name" style="margin-bottom: 0px !important; font-weight: bold;">Bölge Adı</p>
                        </div>
                        <div class="col-md-6 col-xs-6 invoice-id">
                            <p style="margin-bottom: 0px !important; font-weight: bold;">No: #<span class="invoice-no">000000</span></p>
                            <p class="invoice-datetime" style="margin-bottom: 0px !important; font-weight: bold;">01 01 2001</p>
                            <p class="adisyon-username" style="margin-bottom: 0px !important; font-weight: bold;">User Name</p>
                        </div>
                    </div>
                </div>
                <div class="invoice-body">

                    <!-- <div class="table-responsive" style="overflow: hidden;">           -->
                      <table class="table table-condensed" style="width: 100%;">
                        <thead>
                          <tr>
                            <th>Ürün</th>
                            <th class="text-right">Miktar</th>
                            <th class="text-right">Fiyat</th>
                            <th class="text-right">Toplam</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>

                        <tfoot>
                            <tr>
                                
                                <td colspan="2" class="text-right"><strong><?= __('Ara T') ?>:</strong></td>
                                <td colspan="2" class="invoice-subtotal text-right">0.00</td>
                            </tr>
                            <tr>
                                
                                <td colspan="2" class="text-right"><strong><?= __('Ara Ö') ?>:</strong></td>
                                <td colspan="2" class="invoice-interim-payment text-right">0.00</td>
                            </tr>
                            <tr>
                                
                                <td colspan="2" class="text-right"><strong><?= __('İndirim') ?>:</strong></td>
                                <td colspan="2" class=" invoice-discounttotal text-right">0.00</td>
                            </tr>
                            <tr>
                                
                                <td colspan="2" class="text-right"><strong><?= __('TOPLAM') ?>:</strong></td>
                                <td colspan="2" class=" invoice-amount text-right" style="font-weight: 900;">0.00</td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_name p-b-0" colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_phone p-y-0" colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_address p-t-0" colspan="5"></td>
                            </tr>
                        </tfoot>
                      </table>
                    <!-- </div> -->
                
                </div>
                <div class="row text-center" style="margin-top: -20px; font-weight: bold">
                    <?= $location_invoice_template->footer ?>
                </div>
            </div>
        </div>
    </div>
</div>