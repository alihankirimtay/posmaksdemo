<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group backButton">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Fatura"); ?>
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-primary invoice-print"><i class="fa fa-print"></i> <?= __("Yazdır"); ?></a>
            </div>
        </h3>
    </div>
        
    <div class="panel-body p-a-0">
        <div class="container">
            <div class="invoice">
                <div class="invoice-heading">
                    <div class="row date-row">
                        <div class="col-md-6 col-xs-6">
                            <img id="barcode" src="" alt="">
                        </div>
                        <div class="col-md-6 col-xs-6 invoice-id">
                            <h4>No: #<span class="invoice-no">000000</span></h4>
                            <h5 class="invoice-datetime">01 01 2001</h5>
                        </div>
                    </div>
                    <div class="row">
                        <?= $location_invoice_template->header ?>
                    </div>
                </div>
                <div class="invoice-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?= __("İade Ürün"); ?></th>
                                <th class="text-right"><?= __("Miktar"); ?></th>
                                <th class="text-right"><?= __("Vergi"); ?></th>
                                <th class="text-right"><?= __("Fiyat"); ?></th>
                                <th class="text-right"><?= __("Toplam"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?= __("Ürün Adı"); ?>
                                    <p class="invoice-additional_title"></p>
                                </td>
                                <td class="text-right"><?= __("Miktar"); ?></td>
                                <td class="text-right">
                                    <?= __("%Vergi"); ?>
                                    <p class="invoice-additional_tax"></p>
                                </td>
                                <td class="text-right">
                                    <?= __("Fiyat"); ?>
                                    <p class="invoice-additional_price"></p>
                                </td>
                                <td class="text-right">
                                    <?= __("Toplam"); ?>
                                    <p class="invoice-additional_total"></p>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-right"><strong><?= __("Ara Toplam:"); ?></strong></td>
                                <td class="text-right invoice-subtotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-right"><strong><?= __("Vergi:"); ?></strong></td>
                                <td class="text-right invoice-taxtotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-right"><strong><?= __("İndirim:"); ?></strong></td>
                                <td class="text-right invoice-discounttotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-right"><strong><?= __("İADE TOPLAM:"); ?></strong></td>
                                <td class="text-right invoice-amount">0.00</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <?= $location_invoice_template->footer ?>
                </div>
            </div>
        </div>
    </div>
</div>