<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title p-l-1">
        	<div class="btn-group">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Masa Taşı"); ?>
        </h3>
    </div>
        
    <div class="panel-body p-a-0 p-l-1">
        <div id="tab_buttons_container"></div>
        <div id="tab_contents_container"></div>
    </div>
</div>