<?php

    $i = $j = 0;

    $price = function ($product) {
        return numberFormat($product->price + $product->price * $product->tax / 100, 2);
    };


    $styles = function ($product) {
        
        if ($product->image && file_exists(FCPATH . $product->image)) {
            $product->image = base_url($product->image);
            return [
                'background' => "background-image: url({$product->image}); background-size: cover; background-position: 50% 50%;",
                'text_color' => "color:{$product->color};"
            ];
        }

        $color = get_text_color($product->color);
        return [
            'background' => "background:{$product->color};",
            'text_color' => "color:{$color};"
        ];
    };

?>

<?php if ($categories): ?>


    <ul class="nav nav-pills">
        <?php foreach ($categories as $category): ?>
            <li class="<?= $i ? '' : 'active' ?> p-b-1">
                <a class="btn btn-grey btn-sm" data-toggle="pill" href="#tab-products-<?= $category->id ?>"><?= $category->title ?></a>
            </li>
            <?php $i++; ?>
        <?php endforeach ?>
    </ul>


    <div class="tab-content p-a-0">
        <?php foreach ($categories as $category): ?>
            <div id="tab-products-<?= $category->id ?>" class="tab-pane fade <?= $j ? '' : 'in active' ?>" style="overflow-y: auto;">
                <div class="panel-body p-a-0 p-x-1">

                <?php foreach ($category->products as $product): ?>
                        <?php $style = $styles($product); ?>

                        <div class="col-sm-6 col-md-3">
                            <div class="product thumbnail btn btn-ripple m-b-0" data-id="<?= $product->id ?>" data-bar-code="<?= $product->bar_code ?>" style="height: 150px;<?= $style['background'] ?>">
                                <div class="caption caption-footer bg-middle-dark">
                                    <b class="small" style="<?= $style['text_color'] ?>"><?= $product->title ?></b>
                                    <p class="small" style="font-size: 1em; <?= $style['text_color'] ?>"><?= $price($product) ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
            <?php $j++; ?>
        <?php endforeach; ?>
    </div>

<?php endif; ?>