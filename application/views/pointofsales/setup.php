<?php 
    $device = $this->input->get('device');
?>
<div class="panel panel-default" id="pos-setup">

    <div class="panel-heading">
        <h3 class="panel-title">
            <?= __("Kasa Kurulumu"); ?>
        </h3>
    </div>
        
    <div class="panel-body m-t-3">
        <form class="col-sm-4 col-sm-offset-4 m-t-3">
            <div class="form-group m-t-3">
                <select name="location_id" class="form-control input-lg">
                    <option value=""><?= __("Restoran seç"); ?></option>
                    <?php foreach ($this->user->locations_array as $location): ?>
                        <option value="<?= $location['id'] ?>"><?= $location['title'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <select name="pos_id" class="form-control input-lg">
                    <option value=""><?= __("Kasa seç"); ?></option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="username" class="hidden">
                <input type="text" name="username" class="form-control input-lg" placeholder="<?= __("Kullanıcı Adı veya Eposta"); ?>" autocomplete="off">
            </div>
            <div class="form-group p-b-3">
                <input type="password" name="password" class="form-control input-lg" placeholder="<?= __("Şifre"); ?>" autocomplete="off">
            </div>

            <?php if (isset($productions)): ?>
                
                <div class="auto-print"> 
                    <table id="productions_printers" class="table table-hover table-responsive">
                        <thead>
                            <th><?= __('Üretim Yeri') ?></th>
                            <th><?= __('Printer') ?></th>
                            <th class="text-center"><?= __('Otomatik Yazdır') ?></th>
                        </thead>
                        <tbody>
                            <?php foreach ($productions as $production): ?>
                                <tr>
                                    <td>
                                        <?= $production->title ?>
                                        <input type="text" name="productions[<?= $production->id ?>]" class="hidden" value="<?= $production->id ?>">
                                    </td>
                                    <td>
                                        <select name="printers[<?= $production->id ?>]" class="form-control">
                                            <option value=""><?= __('Seç') ?></option>
                                        </select>
                                    </td>
                                    <td class="text-center"><input name="autoprint[<?= $production->id ?>]" type="checkbox"></td> 
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            
            <?php endif ?>

            <div class="form-group m-t-3">
                <button type="button" class="btn btn-primary btn-xxl btn-block" id="submit-setup"><?= __('Ayarla') ?></button>
                <input type="hidden" name="device" value="<?= $device ?>">
            </div>
        </form>
    </div>
</div>