<?php foreach ($floors as $key => $floor): ?>

    <div class="panel-group" id="floor-tab-<?= $key.$DIFF ?>">

        <div class="panel panel-info">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#floor-tab-<?= $key.$DIFF ?>" href="#floor-<?= $floor->id.$DIFF ?>">
                    <h4 class="panel-title"><?= $floor->title ?></h4>
                </a>
            </div>
            <div id="floor-<?= $floor->id.$DIFF ?>" class="panel-collapse collapse">

            	<!-- 1 PANEL BODY -->
                <div class="panel-body">


		                <?php foreach ($zones as $key1 => $zone): ?>

		                	<?php if ($zone->floor_id != $floor->id): ?>
		                		<?php continue; ?>
		                	<?php endif; ?>

		                	<!-- 2 PANEL -->
		                    <div class="panel-group" id="zone-tab-<?= $key1.$DIFF ?>">
		          
			                    <div class="panel panel-success">
			                        <div class="panel-heading">
			                            <a data-toggle="collapse" data-parent="#zone-tab-<?= $key1.$DIFF ?>" href="#zone-<?= $zone->id.$DIFF ?>">
			                                <h4 class="panel-title"><?= $zone->title ?></h4>
			                            </a>
			                        </div>
			                        <div id="zone-<?= $zone->id.$DIFF ?>" class="panel-collapse collapse">
			                        	<!-- 2 PANEL BODY -->
			                            <div class="panel-body">

			                            	<?php if ($tables): ?>

												<div class="table-responsive">
													<!-- TABLE -->
			                                        <table class="table table-hover">
			                                            <thead>
			                                                <tr>
			                                                    <th>Masa</th>
			                                                    <th>Açılış</th>
			                                                    <th>Fiyat</th>
			                                                    <th>Durum</th>
			                                                    <th></th>
			                                                </tr>
			                                            </thead>
			                                            <tbody>

			                                            	<?php foreach ($tables as $key => $table): ?>
			                                            		<?php if ($table->floor_id == $floor->id && $table->zone_id == $zone->id): ?>
			                                            			
			                                            		<?php $opened_at = '-'; ?>
			                                            		<?php $amount = '-'; ?>
			                                            		<?php $status = '<span class="label label-success">BOŞ</span>'; ?>
		                                    
		                                    					<?php if ($table->status): ?>
		                                    						<?php
		                                    							$sale = $this->db->select('amount')->where([
								                                            'active'    => 1,
								                                            'point_id'  => $table->id,
								                                            'return_id' => null,
								                                            'status !=' => 'cancelled'
							                                        	])->order_by('id', 'DESC')->get('sales', 1)->row();

							                                        	$status    = '<span class="label label-danger">DOLU</span>';
								                                        $amount    = number_format(@$sale->amount, 2);
								                                        $opened_at = dateConvert($table->opened_at, 'client', dateFormat());
		                                    						?>
		                                    					<?php endif; ?>

		                                    					<?php $button = "<a href=\"".base_url('pointofsales/index/' . $table->id)."\" class=\"btn btn-success btn-blue btn-ripple\">SEÇ</a>"; ?>
		                                    					<?php if ($this->input->get('empty_tables')): ?>
		                                    						<?php if ((int) $this->input->post('current_table') == $table->id): ?>
		                                    							<?php $button = "<a href=\"javascript:void(0);\" class=\"btn btn-success btn-blue disabled btn-ripple\">TAŞI</a>"; ?>
		                                    						<?php else: ?>
		                                    							<?php $button = "<a href=\"javascript:void(0);\" data-target=\"{$table->id}\" class=\"btn btn-success btn-blue btn-ripple move-table\">TAŞI</a>"; ?>
		                                    						<?php endif; ?>
		                                    					<?php endif; ?>


		                                    					<tr>
						                                            <td><?= $table->title ?></td>
						                                            <td><?= $opened_at ?></td>
						                                            <td><?= $amount ?></td>
						                                            <td><?= $status ?></td>
						                                            <td class="text-right"><?= $button ?></td>
						                                        </tr>

		                           								<?php endif; ?>
		                            						<?php endforeach; ?>

					                            		</tbody>
					                                </table> <!-- END TABLE -->
					                            </div>

		                            		<?php endif; ?>

			                            </div> <!-- END 2 PANEL BODY -->
			                        </div>
			                    </div>
		                    </div> <!-- END 2 PANEL -->

		                <?php endforeach; ?>


                </div> <!-- END 1 PANEL BODY -->
            </div>
        </div>
    </div>

<?php endforeach; ?>