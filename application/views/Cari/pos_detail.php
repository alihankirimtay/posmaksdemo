<?php 
    function selectedTypes($type_id, $types)
    {
        foreach ($types as $type) {
            if ($type['id'] == $type_id) {
                return $type['title'];
            }
        }
    }
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Kasa Ve Bankalar"); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h1 class="pull-right m-b-0"><?= $pos->title; ?></h1>
                        <div class="col-md-10"></div>
                        <div class="col-md-12">
                            <h3 class="pull-right m-b-0"><?= numberFormat($pos->amount,2); ?>₺</h3>
                        </div>
                        <!-- FILTER START -->
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                <?= __('Filtrele') ?> <span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="" method="get" class="form-horizontal" role="form">
                                    <div class="form-content">

                                        <div class="col-xs-12 col-sm-12 col-md-6">

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="d" class="form-control datetimepicker-basic" value="<?= dateConvert($date['date_from'], 'client', dateFormat()) ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?= dateConvert($date['date_to'], 'client', dateFormat()) ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                                    <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </ul>
                        </div>                        
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("Tarih"); ?></th>
                                    <th><?= __("İşlem Türü"); ?></th>
                                    <th><?= __("İlgili Hesap"); ?></th>
                                    <th><?= __("Açıklama"); ?></th>
                                    <th><?= __("Meblağ"); ?></th>
                                    <th><?= __("Bakiye"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($transactions as $transaction): ?>
                                <tr>
                                    <td><?= dateConvert($transaction->date, 'client', dateFormat()) ?></td>
                                    <td><?= selectedTypes($transaction->type_id, $types); ?></td>
                                    <td><?= ($transaction->related_transaction_id) ? $transaction->related_transaction->account_title : ""; ?></td>
                                    <td><?= $transaction->desc ?></td>
                                    <td><?= ($transaction->increment) ? "+" : "-"  ?><?= numberFormat($transaction->amount, 2) ?></td>
                                    <td><?= numberFormat($transaction->pos_amount, 2) ?></td>
                                </tr>
                            <?php endforeach ?>
                            <?php foreach ($sales as $sale): ?>
                                <tr>
                                    <td><?= dateConvert($sale->payment_date, 'client', dateFormat()) ?></td>
                                    <td><?= ($sale->customer_id) ? "Cari-Satış" : selectedTypes(4, $types); ?></td>
                                    <td><?= ($sale->account_title) ? $sale->account_title : $pos->title.'- Satış'; ?></td>
                                    <td><?= ($sale->return_id) ? $sale->desc : "Pos Satışı" ?></td>
                                    <td><?= ($sale->return_id) ? '-'.numberFormat($sale->amount, 2) : numberFormat($sale->amount, 2) ?></td>
                                    <td></td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
