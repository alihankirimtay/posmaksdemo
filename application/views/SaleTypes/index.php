<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Sale Types</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active">Transactions</a></li>
                    <li><a href="#" class="active">Sale Types</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Sale Types</h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('saletypes/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> New Sale Type</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-5">Title</th>
                                    <th class="col-md-4">Created</th>
                                    <th class="col-md-1">Status</th>
                                    <th class="col-md-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($saletypes as $saletype): ?>
                                    <tr>
                                        <td><?php ECHO $saletype->title; ?></td>
                                        <td><?php ECHO $saletype->created; ?></td>
                                        <td><?php ECHO get_status($saletype->active); ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('saletypes/edit/'.$saletype->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('saletypes/delete/'.$saletype->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
