<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1> <?= __("Abonelik Ayarları"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('memberships'); ?>"><?= __("Abonelik Bilgileri"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Abonelik Ayarları"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="row">

                                <?php $this->load->view('Memberships/elements/settings_form_content'); ?>

                                <div class="col-sm-12 small">
                                    <b><?= __("POSMAKS'ı artık kullanmak istemiyor musunuz?"); ?></b>
                                    <p class="margin-bottom-5"><?= __("POSMAKS aboneliğinizi iptal etmeyi düşünüyorsanız 0216 352 02 02 numaralı telefondan ya da destek@posmaks.com adresinden destek ekibimize ulaşarak iptal talebinizi tarafımıza iletmenizi rica ederiz. Destek ekibimiz iptal işlemleri konusunda size yardımcı olacaktır."); ?></p>
                                    <p class="margin-bottom-5"><?= __("Aboneliğinizi iptal ettiğinizde, ödemiş olduğunuz dönemin sonuna kadar POSMAKS hesabınıza erişiminiz devam eder, sonrasında ise kesilir. POSMAKS'a girmiş olduğunuz verileri dışa aktarmak isterseniz bunu belirtmeniz durumunda destek ekibimiz size yol gösterecektir."); ?></p>
                                    <p class="margin-bottom-5"><?= __("Abonelik iptal politikamız çerçevesinde ücret iadesi yapılmamaktadır."); ?></p>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-block" name="dosubmit" data-url="<?= base_url('memberships/settings');?>" ><?= __("Kaydet"); ?></button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
