    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1> <?= __("Envanter Oluştur"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('inventory'); ?>"><?= __("Envanter"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Envanter Oluştur"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Envanter Detayları"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("İsim"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Açıklama"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select name="location" data-placeholder="Restoran Seç" class="chosen-select">
                                        <option value="0"><?= __("Restoran Seç"); ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?php ECHO $location['id']; ?>"><?php ECHO $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Seri/Barkod"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="barcode" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Adet"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="quantity" class="form-control" min="0" step="1" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Fiyat"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price" class="form-control" min="0" step="0.01" value="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Garanti Bitiş"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="warranty_exp" class="form-control datetimepicker-basic">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Durum"); ?></label>
                                <div class="col-md-9">
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active1" checked="checked" value="1" >
                                        <label for="active1"><?= __("Aktif"); ?></label>
                                    </div>
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active0" value="0" >
                                        <label for="active0"><?= __("Pasif"); ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('inventory'); ?>" class="btn btn-grey"><?= __("İptal"); ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('inventory/add');?>" ><?= __("Kaydet"); ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('inventory/add');?>" data-return="<?= base_url('inventory'); ?>" ><?= __("Kaydet & Çık"); ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
