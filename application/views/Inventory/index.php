<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Envanter"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Envanter"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $current_location, 'inventory/index/', 'Restoranlar'); ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __("Tüm Envanter"); ?></h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('inventory/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i><?= __("Envanter Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("İsim"); ?></th>
                                    <th><?= __("Restoran"); ?></th>
                                    <th><?= __("Seri/Barkod"); ?></th>
                                    <th><?= __("Adet"); ?></th>
                                    <th><?= __("Fiyat"); ?></th>
                                    <th><?= __("Garanti Bitiş"); ?></th>
                                    <th><?= __("Oluşturuldu"); ?></th>
                                    <th><?= __("Durum"); ?></th>
                                    <th><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $item): ?>
                                    <tr>
                                        <td><?= $item->title ?></td>
                                        <td><?= $item->location ?></td>
                                        <td><?= $item->barcode ?></td>
                                        <td><?= $item->quantity ?></td>
                                        <td><?= $item->price ?></td>
                                        <td><?= $item->warranty_exp ? dateReFormat($item->warranty_exp, 'Y-m-d H:i:s', dateFormat()) : "" ?></td>
                                        <td><?= dateConvert($item->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($item->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('inventory/edit/'.$item->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('inventory/delete/'.$item->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                            <?php if ($item->history): ?>
                                                <a class="btn btn-warning btn-xs" href="<?php ECHO base_url('inventory/history/'.$item->id); ?>"> <i class="fa fa-history"></i> </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>