<div class="row">
    <div class="col-md-12">

        <div class="panel">
            <div class="panel-body">


                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">
                        
                        <div class="col-xs-12 col-sm-12">
                            <div class="col-xs-12 col-sm-5">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-xs-12"><?= __('Restoran:') ?> </label>
                                    <div class="col-md-9 col-xs-12">
                                        <select name="location" class="form-co1ntrol chosen-select">
                                            <option value="0"><?= __('Seç') ?></option>
                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                <option value="<?= $location['id']; ?>" <?php selected($location_id, $location['id']) ?>>
                                                    <?= $location['title'] ;?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="form-group">
                                    <label class="control-label col-md-5 col-xs-12"><?= __('Tarih:') ?> </label>
                                    <div class="col-md-7 col-xs-12">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= $date_client_today_format ?>" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <div class="form-group">
                                    <div class="col-md-9 col-xs-10">
                                        <button type="submit" class="btn btn-primary"><?= __('Seç') ?></button>
                                        <input type="hidden" name="tab" value="fullness">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
            <div class="panel-body">
                <div id="chart-fullness" style="width: 100%; height: 100%;"></div>
            </div>
        </div>

    </div>
</div>