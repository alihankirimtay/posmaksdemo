<div class="row">
    <div class="col-md-12">

        <div class="panel">
            <div class="panel-body">


                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">
                        
                        <div class="col-xs-12 col-sm-6">
                            <div class="col-xs-8 col-sm-8">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-xs-12"><?= __('Restoran:') ?> </label>
                                    <div class="col-md-9 col-xs-12">
                                        <select name="location" class="form-co1ntrol chosen-select">
                                            <option value="0"><?= __('Seç') ?></option>
                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                <option value="<?= $location['id']; ?>" <?php selected($current_location, $location['id']) ?>>
                                                    <?= $location['title'] ;?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <div class="col-md-9 col-xs-10">
                                        <button type="submit" class="btn btn-primary"><?= __('Seç') ?></button>
                                        <input type="hidden" name="tab" value="endofday">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-xs-12 col-sm-6">
                            <?php if (!$endofdays): ?>
                                <div class="col-xs-8 col-sm-8">
                                    <div class="form-group">
                                        <label class="control-label col-md-5 col-xs-12"><?= __('Başlangıç Tarihi:') ?> </label>
                                        <div class="col-md-7 col-xs-12">
                                            <div class="inputer">
                                                <div class="input-wrapper">
                                                    <input type="text" name="first_date" class="form-control datetimepicker-basic" value="<?= dateConvert2(date('Y-m-d H:i:s'), 'client', 'Y-m-d H:i:s', dateFormat()) ?>" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php endif;?>
                            <div class="col-xs-4 col-sm-4 pull-right">
                                <div class="form-group text-right">
                                    <a href="#" class="btn btn-success" id="create_end_of_day"><?= __('Z Raporu Oluştur') ?></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>


            </div>
        </div>


        <div class="col-xs-12 col-sm-12">
            <div class="list-group">
                <a href="#" class="list-group-item active"><?= __('Geçmiş Raporlar') ?></a>
                <div class="list-group-item">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?= __('Başlangıç Tarihi') ?></th>
                                <th><?= __('Bitiş Tarihi') ?></th>
                                <th><?= __('Toplam') ?></th>
                                <?php foreach ($payment_types as $payment_type): ?>
                                    <th><?= $payment_type->title ?></th>
                                <?php endforeach; ?>
                                <th><?= __('İşlem') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($endofdays as $key => $endofday): ?>
                                <tr class="<?= (!$key ? 'info' : '' ) ?>">
                                    <td><?= dateConvert2($endofday->start_date, 'client', 'Y-m-d H:i:s', dateFormat()) ?></td>
                                    <td><?= dateConvert2($endofday->end_date, 'client', 'Y-m-d H:i:s', dateFormat()) ?></td>
                                    <td><?= number_format($endofday->amount, 2) ?></td>
                                    <?php foreach ($payment_types as $payment_type): ?>
                                        <th><?= number_format($endofday->{$payment_type->alias}, 2) ?></th>
                                    <?php endforeach; ?>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('reports/deleteEndofday/'.$endofday->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        
    </div>
</div>