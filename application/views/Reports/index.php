<?php
    
    $activeTab = function ($tab = null) {
        if ($this->input->get('tab') == $tab) {
            return 'class="active"';
        }
    };

?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Raporlar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Raporlar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <ul class="nav nav-tabs nav-justified" role="tablist">
        <?php if (hasPermisson('reports.index')): ?>
            <li id="gunlukOzet" <?= $activeTab(null) ?>><a href="<?= base_url('reports?tab=index') ?>"><?= __('ÖZET') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.home')): ?>
            <li id="satis" <?= $activeTab('home') ?>><a href="<?= base_url('reports?tab=home') ?>"><?= __('SATIŞLAR') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.daily')): ?>
            <li id="gunlukKasa" <?= $activeTab('daily') ?>><a href="<?= base_url('reports?tab=daily') ?>"><?= __('KASİYERLER') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.monthly')): ?>
            <li id="aylıkKasa" <?= $activeTab('monthly') ?>><a href="<?= base_url('reports?tab=monthly') ?>"><?= __('AYLIK KASA') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.status')): ?>
            <li id="gunlukDurum" <?= $activeTab('status') ?>><a href="<?= base_url('reports?tab=status') ?>"><?= __('GELİR-GİDER') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.stock')): ?>
            <li id="stokRaporu" <?= $activeTab('stock') ?>><a href="<?= base_url('reports?tab=stock') ?>"><?= __('STOK') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.endofday')): ?>
            <li id="gunSonu" <?= $activeTab('endofday') ?>><a href="<?= base_url('reports?tab=endofday') ?>"><?= __('Z RAPORU') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.fullness')): ?>
            <li id="doluluk" <?= $activeTab('fullness') ?>><a href="<?= base_url('reports?tab=fullness') ?>"><?= __('YOĞUNLUK') ?></a></li>
        <?php endif; ?>

        <?php if (hasPermisson('reports.prim')): ?>
            <li id="primRapor" <?= $activeTab('prim') . $activeTab('primdetails') ?>><a href="<?= base_url('reports?tab=prim') ?>"><?= __('PRİM RAPORU') ?></a></li>
        <?php endif; ?>

    </ul>

    <?= $tab_content ?>
</div>
