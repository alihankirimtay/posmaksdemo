<?php 
    $selectedSupplier = function ($customers, $supplier_id) {
        foreach ($customers as $customer) {
            if ($customer['id'] == $supplier_id) {
                return 'selected="selected"';
            }
        }
    };
?>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Faturalı Gider Oluştur"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("İşlemler"); ?></a></li>
                    <li><a href="<?= base_url('expenses'); ?>"><?= __("Giderler"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Faturalı Gider Oluştur"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Faturalı Gider Detayları"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Tür"); ?></label>
                                <div class="col-md-9">
                                    <select name="expense_type_id" class="chosen-select">
                                        <option value=""><?= __("Tür seç"); ?></option>
                                        <?php foreach ($expense_types as $expense_type): ?>
                                            <option value="<?= $expense_type['id']; ?>"><?= $expense_type['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <option value=""><?= __("Restoran seç"); ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>"><?= $location['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Tedarikçi"); ?></label>
                                <div class="col-md-9">
                                    <select name="customer_id" class="chosen-select">
                                        <option value=""><?= __("Tedarikçi seç"); ?></option>
                                        <?php foreach ($customers as $customer): ?>
                                            <option value="<?=$customer['id']; ?>" <?= ($supplier_id == $customer['id']) ? 'selected="selected"' : ''  ?> ><?= $customer['name'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Fiş/Fatura Tarihi"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="receipt_date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i'), 'client', dateFormat()) ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Ödeme Durumu"); ?></label>
                                <div class="btn-group col-md-6 btn-background" data-toggle="buttons">
                                    <a href="#stepPayable" class="btn btn-danger active col-md-3 m-r-3" data-toggle="tab">
                                        <input type="radio" name="paid-type" value="payable" checked="checked" autocomplete="off"/><?= __("Ödenecek"); ?>
                                    </a>
                                    <a href="#stepPaid" class="btn btn-primary col-md-3" data-toggle="tab">
                                        <input type="radio" name="paid-type" value="paid" autocomplete="off"/><?= __("Ödendi"); ?>                            
                                    </a>
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="tab-content tab-content-0">
                                    <div class="tab-pane active" id="stepPayable">
                                        <label class="col-md-3 control-label"><?= __("Ödeme Tarihi"); ?></label>
                                        <div class="col-md-9">
                                            <input type="text" name="payable_date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i'), 'client', dateFormat()) ?>"/>                   
                                        </div>   
                                    </div>

                                    <div class="tab-pane" id="stepPaid">
                                        <label class="col-md-3 control-label"><?= __("Ödeme Tarihi"); ?></label>
                                        <div class="col-md-9">
                                            <input type="text" name="paid_date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i'), 'client', dateFormat()) ?>"/>  
                                        </div>
                                        <div class="col-md-offset-3 col-md-3 m-t-2">
                                            <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                                                <a href="#stepPaymentBank" class="btn btn-primary active" data-toggle="tab">
                                                    <input type="radio" name="account-type" value="bank" checked="checked" autocomplete="off"/><?= __("Bankalar"); ?>
                                                </a>
                                                <a href="#stepPaymentPos" class="btn btn-primary" data-toggle="tab">
                                                    <input type="radio" name="account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>
                                                </a>
                                            </div>
                                        </div>  

                                        <div class="tab-content tab-content-0 col-md-12 m-t-2">
                                            <div class="tab-pane active" id="stepPaymentBank">
                                                <label class="col-md-3 control-label"><?= __("Banka"); ?></label>
                                                <div class="col-md-3">
                                                    <select name="bank[id]" class="chosen-select" style="display: none;">
                                                        <option value=""><?= __("Banka Seçiniz"); ?></option>
                                                        <?php foreach ($banks as $bank): ?>
                                                            <option value="<?= $bank['id']; ?>"><?= $bank['title'] ;?></option>
                                                        <?php endforeach; ?>
                                                    </select>                    
                                                </div>   
                                            </div>

                                            <div class="tab-pane" id="stepPaymentPos">
                                                <label class="col-md-3 control-label"><?= __("Kasa"); ?></label>
                                                <div class="col-md-3">
                                                    <select name="pos[id]" class="chosen-select" style="display: none;">
                                                        <option value=""><?= __("Kasa Seçiniz"); ?></option>
                                                        <?php foreach ($pos as $single_pos): ?>
                                                            <option value="<?= $single_pos['id']; ?>"><?= $single_pos['title'] ;?></option>
                                                        <?php endforeach; ?>
                                                    </select>                    
                                                </div>    
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Gider açıklaması"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Not"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <table id="stocks" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th colspan="6"><?= __("Gider Kalemleri"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- STOCKS -->
                                        </tbody>
                                        <tfoot  class="text-right">
                                            <tr>
                                                <th><button type="button" id="add_stock_row" class="btn btn-default btn-ripple btn-grey"><i class="fa fa-plus"></i> <?= __("Yeni Satır Ekle"); ?></button></th>
                                                <td colspan="3"><?= __("Ara Toplam:"); ?></td>
                                                <td id="total_subtotal">0</td>
                                                <td rowspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><?= __("Toplam Vergi:"); ?></td>
                                                <td id="total_tax">0</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><?= __("Genel Toplam:"); ?></td>
                                                <td id="total_amount">0</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="type_id" value="3">

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('expenses'); ?>" class="btn btn-grey"><?= __("İptal"); ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('expenses/addwithstock');?>" ><?= __("Kaydet"); ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('expenses/addwithstock');?>" data-return="<?= base_url('expenses'); ?>" ><?= __("Kaydet & Çık"); ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
